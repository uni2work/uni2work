#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later


[[ -n "${FORCE_RELEASE}" ]] && exit 0

set -e

if [ -n "$(git status --porcelain)" ]; then
    echo "Working directory isn't clean" >&2
    exit 1
fi

if [ "$(git rev-parse --abbrev-ref HEAD)" != "master" ]; then
    echo "Not on master" >&2
    exit 1
fi

ourHash=$(git rev-parse HEAD)
theirHash=$(git ls-remote origin -h refs/heads/master | awk '{ print $1; }')

if [ "$theirHash" != "$ourHash" ]; then
    echo "Local HEAD is not up to date with remote master" >&2
    exit 1
fi
