-- SPDX-FileCopyrightText: 2022 Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

import Prelude     (IO)
import Application (appMain)

main :: IO ()
main = appMain
