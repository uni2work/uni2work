-- SPDX-FileCopyrightText: 2022 Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE PackageImports #-}
import "uniworx" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
