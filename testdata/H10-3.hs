-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-  Übung H10-3 zur Vorlesung "Programmierung und Modellierung"
    Lehrstuhl für theoretische Informatik, LMU München
    Steffen Jost, Leah Neukirchen

    Bitte nur die Zeilen mit
      error "TODO" -- TODO: Ihre Aufgabe !!!
    bearbeiten.
    (Sie dürfen an diesen Stellen auch beliebig
    viele neue Zeilen einfügen.)

    Entweder mit ghc kompilieren und ausführen oder
    einfach in ghci laden und main auswerten.
-}


import Control.Monad.Trans.State

type Wetter = String
data Welt = Welt { zeit :: Int, wetter :: Wetter }
  deriving Show

main =
  let startState = Welt { zeit=0, wetter="Regen" }
      (result,finalState) = runState actions startState
  in do
    putStrLn "Zustand Welt bei Start ist: "
    print startState
    putStrLn "Zustand Welt bei Ende ist: "
    print finalState
    putStrLn "Ergebnis der Aktion ist: "
    print result


actions :: State Welt [(String,Int)]
actions = do
  tick
  tick
  tick
  tick
  wetter1 <- swapWetter "Sonne"
  zeit1 <- gets zeit
  let r1 = (wetter1, zeit1)
  tick
  tick
  wetter2 <- swapWetter "Sturm"
  zeit2 <- zeit <$> get
  let r2 = (wetter2, zeit2)
  tick
  return [r1,r2]


--- !!! NUR AB HIER BEARBEITEN !!!



tick :: State Welt ()
tick =
  error "TODO: tick noch nicht implementiert!" -- TODO: Ihre Aufgabe !!!




swapWetter :: Wetter -> State Welt Wetter
swapWetter =
  error "TODO: swapWetter noch nicht implementiert!" -- TODO: Ihre Aufgabe !!!



















