-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{- Übung H10-2 zur Vorlesung "Programmierung und Modellierung"
   Lehrstuhl für theoretische Informatik, LMU München
   Steffen Jost, Leah Neukirchen
-}

import Control.Monad

chainAction1 :: Monad m => a -> [(a -> m a)] -> m a
chainAction1 = undefined -- !!! TODO !!!
    
chainAction2 :: Monad m => a -> [(a -> m a)] -> m a
chainAction2 = undefined -- !!! TODO !!!

chainAction3 :: Monad m => a-> [(a -> m a)] -> m a
chainAction3 = undefined -- !!! TODO !!!


tellOp :: (Show a, Show b) => (a -> b) -> a -> IO b
tellOp f x = let fx = f x in do
  putStrLn $ (show x) ++ " -> " ++ (show fx)
  return fx
  
test1 :: [Int -> IO Int]  
test1 = map tellOp [(*3),(+1),(`mod` 7),(+5),(*2)]
  
