# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [30.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v30.2.0...v30.2.1) (2023-04-04)

## [30.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v30.1.2...v30.2.0) (2023-04-04)


### Features

* **mass-input:** some support for nested mass inputs ([1b77c14](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1b77c142f1840b750a15d18c8df9b6aed6416967))
* **workflows:** admin ui for overviews ([6280731](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/62807312cea7a5be62019c60e7073bc1f111a8b3))


### Bug Fixes

* mass-input/util-registry interaction ([5449b26](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5449b2636e9af4feff8c8d5118e0a364c0bec147))
* **study-features:** catch study features parse errors; introduce IssueLog for catched data issues ([5578734](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5578734db8055566578854beda67ec3f410c26c1))
* **util-registry:** debug output ([4fd6b8c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4fd6b8cf3a9fc8fad5912119fd208d28844ff74a))
* **workflows-json-export:** efficiently det' if payload has files ([6543117](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/654311755e1bbb208f4b28dfee7575d3b997b3ff))
* **workflows:** restore ability to clear payloads ([ed75eb6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed75eb6ebf670f062108d05009d2558cb8d5e308))
* **workflows:** revert dropping payloads which are not in form ([f7f071d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f7f071dc6587d2c2c5d5e68689172a60aaeb4efc))

## [30.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v30.1.1...v30.1.2) (2023-03-20)


### Bug Fixes

* introduce memcached versioning ([dfc0693](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dfc0693a82587f47706df9b0621152a2f286024a))

## [30.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v30.1.0...v30.1.1) (2023-02-27)

## [30.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v30.0.0...v30.1.0) (2023-02-27)


### Features

* **workflows:** model overviews ([dc32274](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dc32274ad692bdf945854efe08dae305c22fc872))
* **workflows:** overview navigation ([6e00583](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6e005838323f22edcda0bfbb7ce8b357012e8a4f))
* **workflows:** overview routes and auth ([7b710ff](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7b710ff25d9366c09cd61c06f177026e5d366737))
* **workflows:** overview sorting & filter ([c3d6d67](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c3d6d6788a9255b22f05f9718c8c04c42fa19a9c))
* **workflows:** overviews ([5141a6f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5141a6fcb258c6ee5e0dae57276ee112555207f7))


### Bug Fixes

* **workflows:** overview breadcrumbs ([25e1ead](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/25e1eada1bb5a9bf8584d29adc7b2b97ee00d260))

## [30.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v29.0.0...v30.0.0) (2023-02-01)


### ⚠ BREAKING CHANGES

* **ldap:** hotfix for parsing new dfnEduPersonFeaturesOfStudy format; fix for old format for e.g. disabled accounts

### Bug Fixes

* **ldap:** hotfix for parsing new dfnEduPersonFeaturesOfStudy format; fix for old format for e.g. disabled accounts ([748f4f6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/748f4f633ef837507ddeab4f09e5640f2e3305db))

## [29.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.3.2...v29.0.0) (2023-02-01)


### ⚠ BREAKING CHANGES

* **ldap:** hotfix for parsing new dfnEduPersonFeaturesOfStudy format

### Bug Fixes

* **ldap:** hotfix for parsing new dfnEduPersonFeaturesOfStudy format ([acaf27a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/acaf27a0d5daac262914d92b9ec8cc715e2fc72a))

## [28.3.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.3.1...v28.3.2) (2023-01-27)


### Bug Fixes

* **dbtable:** disable broken table settings for now ([d110867](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d11086742b8edfcfc1c7adfc71d23233d61e9fd2))
* **dbtable:** disable fetching previous sorting for now ([046be44](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/046be444e8f3759b008ade9ff568a5760186eff7))
* **workflow:** assert workflow edge payload fields alignment correctly wrt. at-least-one semantics ([b2e6723](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b2e67230f83299fefe6562720542d11e195c15f9))

## [28.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.3.0...v28.3.1) (2022-12-17)


### Bug Fixes

* **eoexamsr:** allow system-exam-offices to view exam results ([3bafd67](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3bafd670c51e8ba7dc04951839b048fe6a9d57e0))
* **eousersr:** correctly deduplicate add user form entries ([f883662](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f883662dec772dea5353236a52baa403b1921c2f))
* **login:** do not display error message on successful u2w-internal login ([d0ee3ee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d0ee3ee247a443ce22d1c50dc8a96346850fe54c))
* **workflows:** correctly suppress display of hidden edges in history ([8313d8f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8313d8f43fecdfd8cf29716d7c92695f379ba2dd))
* **workflows:** display automatic and hidden edges ([8052543](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8052543d85557f17c5ae143b8547145bc5b9a425))

## [28.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.2.1...v28.3.0) (2022-11-04)


### Features

* **ldap:** resort to userPrincipalName if mail is not specified ([10e7226](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/10e7226171118e594b0d9f710a819c4f791800ad))


### Bug Fixes

* **workflows:** expose AuthResult when doing auth for workflow init ([321643e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/321643eacb89cb921681d767eba8b59eb7c92b08))

## [28.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.2.0...v28.2.1) (2022-10-18)

## [28.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.1.0...v28.2.0) (2022-10-18)


### Features

* **workflows:** support executing workflow edges via json POST ([96e4415](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96e441509411be1644869de71d0368d5f3139854))
* **workflows:** validate workflow edge form submissions ([0f70583](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0f7058382f3333212a99006e5efcfc4205d5549f))

## [28.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v28.0.0...v28.1.0) (2022-10-14)


### Features

* **site-layout:** add link to source code to footer ([4a76fce](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4a76fce880046485a813d0413d0aa970616f2c6a))

## [28.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.6.0...v28.0.0) (2022-10-07)


### ⚠ BREAKING CHANGES

* **cron:** correctly compute nextCronMatch

### Features

* **auth:** new auth predicate auth-tag-logged-in ([c675312](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c6753128f555cbe7967420a6e0eb7a647082ba9c))


### Bug Fixes

* **cron:** correctly compute nextCronMatch ([8913dc6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8913dc6aa2dfb447d41b40d996362f9ca84df779)), closes [#793](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/793)

## [27.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.5.0...v27.6.0) (2022-09-30)


### Features

* **workflow:** allow to check for edges in checkWorkflowRestriction ([53f8786](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/53f8786925e64252970b8b30612ca56f440ce467))
* **workflow:** allow to check for nodes in history in checkWorkflowRestriction ([4ef1d50](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4ef1d50a55a99b61ed50676daf695005d9b11c8e))
* **workflow:** display substages in workflow stage graph ([146c15c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/146c15c8ad418131ecc157017fa09529be23c499))
* **workflow:** display workflow stage graph with current stage highlighted ([96a287b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96a287bd30ae55aea8acd7a67612a37606b76507))
* **workflows:** display stage progression ([9b1d7bf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9b1d7bf83431ac06517782f849f75e54c1282b0a))

## [27.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.4.0...v27.5.0) (2022-09-21)


### Features

* **eo-exams:** csv-export ([9acad76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9acad7612e52f9d6500039e866678b29ee4bc987))

## [27.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.3.1...v27.4.0) (2022-09-20)


### Features

* **workflow:** display emails in workflows ([c0cd9d4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c0cd9d40043ef91d58612717177d7f8de375a893))

## [27.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.3.0...v27.3.1) (2022-09-20)

## [27.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.2.4...v27.3.0) (2022-09-20)


### Features

* **course-users:** use table sorting from database as sorting default if available ([01958b3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/01958b3285b3631e3342a6d2f2c9c102d27e0181))
* **dbtable:** add options below dbtable for restoring default sorting ([6c4a25b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6c4a25bfb6e92cce8c8a828cc46319430c08ae51))
* **dbtable:** allow opting out of persisting table sorting ([37e78b5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/37e78b5363ba588df42f4bb1603ec5ecff49f8e7))
* **dbtable:** move table-sorting update from separate route to dbtable ([fdfd6c2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fdfd6c27e4f598691603f42430eecd65479bfd95))
* **dbtable:** prefill table settings from current settings ([ebfdf02](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ebfdf02067936e63000fddc8c5e56adb5864578d))
* **dbtable:** redirect to site with corresponding sorting on settings change ([659391d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/659391d03dbb230bcefa34396017e27c7b617fda))
* **dbtable:** refine table settings template logic ([09e7fa0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/09e7fa05ec1c3737ba3b3fc74464c75ccb45e889))
* **exam-office-exams:** use table sorting from database as sorting default if available ([fd5b6f4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fd5b6f4519392e2363f77d177f25202ce197fdd6))
* **frontend:** no-chevron option for show-hide utility ([76818c2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/76818c269c443e08bce6a27d18d7a6acaf39ce38))
* **frontend:** no-save option for show-hide utility ([96fee2e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96fee2ea600acf5ecd7a2dc75d50bd18ca0b3e92))
* **frontend:** no-toggle-highlight option for show-hide utility ([a2f8546](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a2f8546625b595d714c634bbbd5ff3d9eb3ba1c2))
* **frontend:** post table-sorting request from async-table util ([93c03c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/93c03c514ef660ab9b86e703b6778a239f2debdc))
* **frontend:** post table-sorting request on async-table sort ([866c9c0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/866c9c0bd63b31af2f90a5f004c277eec6f25697))
* **table-sorting:** implement backend-handler for persisting table-sorting in database ([7f2286e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7f2286e14eff00edfcf058e1169ceca80d4bf366))
* **table-sorting:** implement handler-util for fetching currently stored table sorting ([e7f173f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e7f173f1cc982527f593f71a7f7eff512fc1b146))


### Bug Fixes

* **dbtable:** move settings widget down and out of table form ([f1322d2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f1322d26af5f1ffb8cdecce8e022e75dae83499e))
* **form:** add form attribute to allow for modals inside forms ([e75afb7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e75afb720ef0871ee4ce78060b788142b524f5ad))
* **frontend:** correctly compose urls in fetch ([918686d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/918686d5140fd4ff59a508b67d8e1a2b35206182))
* **frontend:** fix show-hide no-chevron behaviour ([9bb2531](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9bb25314172ad932c180bc9119eedce3d61fbd17))

## [27.2.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.2.3...v27.2.4) (2022-09-19)


### Bug Fixes

* **frontend:** fix typo in NavigateAwayPrompt utility ([b5ff71d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b5ff71d733f277edced7f83fe92574fac3651c12))

## [27.2.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.2.2...v27.2.3) (2022-09-08)


### Bug Fixes

* **workflows:** allow payload reference roles to work retroactively ([483c54c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/483c54c0da3253ac69a103e2bd6896fee0ba09d9))

## [27.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.2.1...v27.2.2) (2022-09-07)


### Bug Fixes

* **exam:** apply part weights in automatic grade computation ([27986bb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/27986bbb5b7d4423bf6f1abef4164177e7209bfe))
* **exam:** show correctly weighted part results ([2a69d8e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2a69d8e3593677c997acbc07f2e364e1c029b403))

## [27.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.2.0...v27.2.1) (2022-09-05)


### Bug Fixes

* disable empty check on workflow lists (again, for now) ([ab01176](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ab011768dd8170d6938fbdf79cbd780a18621d25))
* **workflows:** further improve generated sql ([01d9b98](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/01d9b980ed825eb795bc7f4c17157d0def8e22ac))
* **workflows:** reduce size of sql generated for auth ([a2c0e77](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a2c0e77273db94594debadf32e86998b60749fe8))

## [27.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.1.1...v27.2.0) (2022-09-01)


### Features

* **eo-fields:** additionally scope exam-office fields by study field type ([882b5b0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/882b5b038be2883db32ddfe310dac5328f20d116))
* **eo-fields:** exam office fields form for every study field type ([65cfc95](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/65cfc954a9b90452163c40d2adbe97c3d885f237))
* **eo-fields:** scope auth on field type ([901232c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/901232c812377473edf9e74a7d336ff939d4949c))


### Bug Fixes

* **eo-fields:** fix typos in messages ([d45234c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d45234cd2aa75657702a6e7a2a066d03d90a4f55))
* **guess-user:** better handle duplicates in returned list of users ([2ddbc67](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2ddbc67c3150fe791cf662de29831c25b51cb735))

## [27.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.1.0...v27.1.1) (2022-08-18)


### Bug Fixes

* **exam-correct:** fix util-breaking typo ([0deee1e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0deee1ecbc78e15bb44e596f3bb417c8e6eb3a11))

## [27.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.0.5...v27.1.0) (2022-08-13)


### Features

* do ldap lookup in user fields ([cdeebb3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cdeebb3bd4f0a2f71e80284050c172d3c922da60))
* guess users on workflow spec upload ([ae6a3bf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ae6a3bf874b567caf529cca38e0219c7ef70e9ed))
* **guessUser:** convert ldap results by closeness ([ae96f76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ae96f764c7e65c80bc8664970602a0192ab498fc))
* GuessUserIdent ([efdac0b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/efdac0bc453d96efa98b9d9a3cc159d45b331fd4))
* have guessUser do ldap lookups more often ([0fcd898](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0fcd8980ffe0cac1aca9752f67d46be0a27f5fd5))
* **volatile-cluster:** add option to disable validation in workflow day fields ([5807c83](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5807c8318c3f585a60f3df2a5e59470df21d233f))

## [27.0.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.0.4...v27.0.5) (2022-08-02)

## [27.0.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.0.3...v27.0.4) (2022-07-26)


### Bug Fixes

* **pruneLastExecs:** prune cronLastExec entries ([f3417c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f3417c5a8c5cbb643d0499d3a46f56c38c6840e7))

## [27.0.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.0.2...v27.0.3) (2022-07-26)

## [27.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.0.1...v27.0.2) (2022-07-24)


### Bug Fixes

* **cron:** dont execute jobs with notAfter < now ([763a14f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/763a14f51352721c9398363ee42768aadb792093))
* **jobs:** fix pruneLastExec ([716a9a8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/716a9a8eb8725c40fd3a17084c1e71fc5bf2e79a))
* **pruneLastExecs:** dont prune cronLastExec entry if still within debouncing range ([7a5b8f3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7a5b8f39d16e0d15ca65d0d1472d22c46f552f01))

## [27.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v27.0.0...v27.0.1) (2022-07-23)


### Bug Fixes

* **jobs:** delete cron_last_exec entries when they do not change outcome ([f343225](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f343225ac20b7b4403babc69fc09851c88f0fec4))

## [27.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.3.2...v27.0.0) (2022-07-23)


### ⚠ BREAKING CHANGES

* **jobs:** eliminate JSON-parse overhead in execCrontab

### Bug Fixes

* **assimilate-user:** avoid weird db behaviour on job update ([a4ef035](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a4ef0351780262c3560c9a8eafd29f6eedbfdfe0))
* **backmigration:** dont set job columns to not null too early ([b5e78ab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b5e78ab66bb0046f0927f20f6ae8b3dbd0a1c4e2))
* **jobs:** eliminate JSON-parse overhead in execCrontab ([c35ff7f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c35ff7f04d8e46a8d0cb4247c0ee1993c2802759))
* **migration:** fix treatment of null job values ([e84ab1f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e84ab1fd9cf4c263302e190680a019ee4edf6d05))
* **migration:** migrate job contents to tmp column instead of db column type casting ([f6d8fbe](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f6d8fbefa9173a0b547cc68addbbe7becbc65456))

## [26.3.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.3.1...v26.3.2) (2022-07-20)

## [26.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.3.0...v26.3.1) (2022-07-13)


### Bug Fixes

* **migration:** fix undo of workflow-state-tables ([0f3a6ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0f3a6ef1bccbb6ba74a1882e2b10766e8d502c9a))

## [26.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.2.0...v26.3.0) (2022-07-12)


### Features

* **workflows:** implement all top scopes ([cf39ecd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf39ecdb2398d013c084ccea75d1833020f80262))


### Bug Fixes

* re-implement !empty for workflow-workflow-lists ([20fa267](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/20fa2672f75a127fd7bf49ace74d0c3623d88251))
* **workflows:** don't leak information about node finality ([6b198d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6b198d7b93dabcb9504acb154ca3d6ef708db83a))
* **workflows:** handle archivation for final initial state ([d1b1cd4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d1b1cd4f9881a7585c8f02553994111edc12ddf9))
* **workflows:** hide empty workflow-workflow lists again ([f7b099c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f7b099cfa196eee781a2654c0f4cab07caea4aa4))
* **workflows:** index archived field in db for performance ([1743788](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1743788e2a14493b68f1cd49acf82c9b35a8aee4))
* **workflows:** restore workflow-workflow list functionality ([aa77519](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aa775194af574afab50d25f0cdd1bd67100d184c))

## [26.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.1.1...v26.2.0) (2022-06-22)


### Features

* **course-users:** better explanation and suggestions on import exceptions ([a025ada](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a025ada58db794aad46a87ea5903ed78e52b76f3))
* **course-users:** csv import for CUsersR ([f080632](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f080632cdb8f72c5d17ce384bfacf13a0d346a4d))
* **course-users:** prohibit (de)registration on import when it is now allowed ([db1d849](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/db1d849e125dd401ba68db7a8199c5f398a59762))
* **course-users:** reregister users via import ([9e99377](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e993779dfe3a3fb6e6d1d4a7922fe878387955f))
* **course-users:** resolve users by email ([bed1368](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bed1368eb038b684c6fb969c552c7ec9c661d7d3))


### Bug Fixes

* **course-users:** correctly import new users ([55ea019](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/55ea01946d73a1425ce431ab9d7c384d82dd8127))
* **course-users:** correctly set submission group via csv import ([b107275](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b1072754283bd21cf3f21cf3a342ca1f556157b7))
* **course-users:** do not deregister already deregistered participants on import ([b7f44ad](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7f44ad212a27e99f675311a3fad94b5f6f6c0b1))
* **course-users:** fix set submission group via csv import ([d922232](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d9222324d70df3e23037349ab0510ac8d5cbe2c3))
* **course-users:** set submission group via csv import on register ([f02c85c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f02c85c3429685a472c40bea4dc120a10f7fc7a2))
* **sub-groups:** better submission group action descriptor ([f34d43b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f34d43b07962175891883bfbe299242a403013ba))

## [26.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.1.0...v26.1.1) (2022-06-06)

## [26.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.0.1...v26.1.0) (2022-06-05)


### Features

* **apis:** further integrate servant ([bf2ff2d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bf2ff2dc9c4a986e9088feca41c00123bf2c785a))
* **apis:** integrate servant ([e3d504b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e3d504bd113f76854748929201c7e476c2911ee0))
* **apis:** support servant-generic ([e8bbaa0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e8bbaa0463afb11998d1a21a9e4ff068ba2b7ea3))
* **apis:** version negotiation ([76e0bcf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/76e0bcf693afeb15edae0b8170c3a813b10ae062))
* **app-settings:** add duration after which finalized WorkflowWorkflows will be archived ([465a92b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/465a92b9829db9a9969cf370c635bbf9407f07f5))
* **external-apis:** add ExternalApisList ([4216785](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4216785e90931ec54dc723166442a7acbe491851))
* **external-apis:** create new external api registrations ([559f9db](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/559f9db7d58e26758209e61ef5ac8adc48b25221))
* **external-apis:** idents, info, pong, delete, and expiry ([90679e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/90679e00952ee4d6df584b52ceba99c0216e222e))
* **help:** update help instructions, add support times ([5201a93](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5201a93de67e5589cca8511722f8513ab5f12fb8))
* link api docs ([b277bd8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b277bd8424bdb8aef6c0c2e22acd70854f6ba18b))
* **nix:** add postman for api debugging ([5a964f3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5a964f347c9306989f753a69cd56a92404aed699))
* **servant:** dry-run support ([47df8a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/47df8a312f72c7d019a536e7f2155ab7bd52febf))
* **workflows:** add archived timestamp ([088c2f5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/088c2f5c751e868dbe50b7fdc04fc9f3f0206966))
* **workflows:** implement archive and list page actions ([fac92f9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fac92f9b5053d9f11dc1d72c81d290acd888cd63))
* **workflows:** implement archive routes ([4416094](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/441609436a4e4a78f4ff826603a2a2be2dcabb41))
* **workflows:** implement breadcrumbs for archive routes ([4adaf1e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4adaf1e806cf3bd1181c78c3d75d57692ed62356))
* **workflows:** implement handlers for listing all workflows ([85c24f7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/85c24f713a974bcb932513da162d5224c2d76fa3))
* **workflows:** restrict all (except admin) workflow lists on non-archived ([97723ad](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/97723ad895c080819c29105717aeb9cfdd19c7a1))
* **workflows:** set archived timestamp on state change ([23b1065](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/23b106554567db8d75478389ef043f17b7b43458))
* **workflows:** show info and warnings about scheduled/performed archivation ([424692d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/424692d61110042462d7d88310290a8f682bd23a))


### Bug Fixes

* **async-tabel, async-form:** removed destroyAll call ([236009e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/236009e508245ba10aa5f45b9514c8186ab186cf))
* **migration:** dont force app settings ([4486a00](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4486a00d45ff9783115064b640b58f006b2e78d7))
* **test:** add missing workflow instance ([6c92440](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6c92440c6419012ef96a7410e769c8724a8d2206))
* **workflows:** add missing Hashable instance for WorkflowWorkflowListType ([6e46e4e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6e46e4e9ef34130582a3511ff01b42f4428dd1a1))
* **workflows:** correct interpolation of archived state in headings ([84d3327](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/84d3327db37aa1177cc2e9b40fa9fc3e03ec7867))

## [26.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v26.0.0...v26.0.1) (2022-04-22)


### Bug Fixes

* **frontend:** various fe incompatabilities with updated tooling ([46530c6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/46530c6c644bec65c2219fd9b5830bc5bb8e9baf))

## [26.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.29.3...v26.0.0) (2022-04-22)


### ⚠ BREAKING CHANGES

* **system-message:** fix on-volatile-cluster-settings model default

### Bug Fixes

* **system-message:** fix on-volatile-cluster-settings model default ([4027f31](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4027f3144b613dbe67ca44f7a3142c13ff6f4dff))
* **webpack:** switch to wp-5 assets ([ec4d710](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec4d710ce0a5429160365397ad17c23b71c0a4cf))

## [25.29.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.29.2...v25.29.3) (2022-04-21)


### Bug Fixes

* **system-message:** add volatile cluster setting model default ([6655582](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6655582ace098808bfcea90ca85fce2fe0024d2b))

## [25.29.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.29.1...v25.29.2) (2022-04-21)

## [25.29.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.29.0...v25.29.1) (2022-04-21)


### Bug Fixes

* update lodash.debounce and defer imports ([f03dae6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f03dae66e92a36ca7cea01fa8b508f7f8612200a))

## [25.29.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.28.0...v25.29.0) (2022-04-21)


### Features

* **workflows:** additional text field types ([4a34344](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4a34344c332291ceaed192ee1f0a73d97d855eeb))


### Bug Fixes

* **workflows:** always show navigation item ([82a4ecc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/82a4eccaa4fc5d6df3ff0bc71a025973f12c7dfd))
* **workflows:** properly distinguish anonymous/automatic ([21a1fb5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/21a1fb543b347a871b21db636d217e0816bd5388))

## [25.28.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.27.0...v25.28.0) (2022-04-20)


### Features

* **forms:** honeypots for unauthorized users ([8085c30](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8085c30420dcca9d8c493da8241a5e10d9ef8122))
* **frontend:** remove deprecated tail.DateTime ([6bfbff4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6bfbff41f6081dd65b0a4afa9155a125f31de973))
* **system-msg:** add volatile cluster settings to system message forms ([f8f9dc0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f8f9dc0621a18862bcf83364b874e176553ed7b1))
* **system-msg:** display system status messages on volatile cluster settings only ([da253f7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/da253f7fbdb919bb9cc52310148dcc28c382cfa4))
* **system-msg:** display volatile cluster settings in msg list ([32bed15](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/32bed159615b96ebab23766d748a844b1ba42f41))
* **translation mistakes:** courses and landing page ([9cff722](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9cff7220231a227859c771b720084e8a6952b5c1))
* **translation mistakes:** done ([229e379](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/229e3793c6266a2936ffb32854cb14e0bb0ba510))
* **translation mistakes:** megre request ([ff07768](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ff077680b6fde39f4f82ba162781e471a9f3071c))
* **translation mistakes:** until admin ([43e5f9f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/43e5f9f7aa340d564557ec499859049dcb2ac194))


### Bug Fixes

* **system-msg:** use correct required features for form elems ([b99cda0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b99cda06aa22b3b9a3c026f80873cb2b68d93cdb))

## [25.27.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.26.0...v25.27.0) (2022-02-12)


### Features

* **communication:** add recipient option for course participants in at least one tutorial ([8dabb63](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8dabb63603341c7e2d7dadb95deeb77f864c14a0))
* **course-users:** export eppn to csv and json ([3c79703](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3c797039cc0784a2831167c11ed1b7bb8ff78daa))
* **course-users:** export eppn to csv and json ([6feefeb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6feefeb3e16c8b2327cdb2746a8263394f9293f9))
* **courses:** add search bars for shorthands and titles ([8e1b9b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8e1b9b9abae0c794ce12f10e5f75f410f903d927))
* **exam-users:** allow resolving exam users by eppn on csv-import ([6a041dc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6a041dc4c9c6ae62c7c7f1641eeb2f5417f32b8d))
* **exam-users:** allow resolving exam users by eppn on csv-import ([d677d35](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d677d35319332aae28410d1b7cdf09d295920fac))
* **exam-users:** export eppn for exam users ([ff1fe20](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ff1fe20efed340529a3a12858d82d668e5fe2e85))
* **exam-users:** export eppn for exam users ([d4ba513](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d4ba513d7948cc6e7136bd05543edb9d9764f78c))
* **submissions:** add option 'Set corrections as done' ([880eb3b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/880eb3b1ad2d5e4b12f767eb68d29eabfbd79e17))
* **submissions:** Apply suggestions to reduce lines of code ([2f1ecd3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2f1ecd397936f4e4ac018eb4f9aaf49da37e1e81))


### Bug Fixes

* **exams:** exam results of non-registered users now show correctly ([b294b1c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b294b1cfc4bab4b5ec5247d37097873748759727))
* **submissions:** add check if users in `groupMembers` are already submittors on submission ([4854d9c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4854d9c8661cc47447a794a4f2963e7e40fe678d))
* **submissions:** notDE, notEN for unambiguous negation ([ae66fdf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ae66fdfb8aa51cd7f6e6ad4e263e52cd7043abcb))
* **submissions:** shorter solution: remove check for `CourseParticipantActive` ([a358cdd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a358cdd100065014a1b7dc1055d7fc2ea1265011))

## [25.26.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.25.0...v25.26.0) (2022-02-04)


### Features

* **csv:** add export-exam-label as csv option ([de917a8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/de917a8d8646c4df599d945bc4979e226dd5192f))
* **eo-exams:** select column for exam list in case of actions ([42f58da](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/42f58da44fe1b0cc016979b9cde0300832cc0ae1))
* **eoexamsr:** implement label sorting ([808c2fc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/808c2fc7708cbf69172cc348e8a2c47c641287b1))
* **eoexamsr:** introduce GET param to control synced display ([09261ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/09261ac7578d8abdcaa39bbdcf12fc6ddad9ce22))
* **eoexamsr:** use user get-synced setting if no param present ([e60d125](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e60d125e05bcf152b57fa132b4957405c40ae03f))
* **labels:** actions for setting and removing labels ([9e81f03](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e81f03742586ef40e8ff896e303ba11d95f120a))
* **labels:** hide csv export option for non-exam offices ([ec55a40](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec55a40bc11500105de0b9b9c5cbeb1ba035d53f))
* **labels:** label filter ([544b9ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/544b9ef76260eb9aecb1a5ee2ca3467f08cd99f7))
* **labels:** set export label on exam csv export ([4557fdd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4557fdda928fa5f24f4da48aff44fa8b8d955344))
* **labels:** set export label on external exam csv export ([2071f59](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2071f5912d4d183abb55f258450b374821f426e6))
* **profile:** upsert eo-labels on form submit ([cae652b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cae652b512137d3a85248d164fb16a6a7d8c097f))
* **user:** add get-labels to exam office user setting ([5a3c590](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5a3c590b72394fb1fd4544aace2a762300b4551d))
* **user:** add get-labels user setting ([6a10bd7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6a10bd78f55068eca71a0c190dcee5b066430b93))
* **user:** introduce exam office user settings ([6788f92](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6788f923ed10759a4d5235c119b94e4c2a35a8b4))


### Bug Fixes

* fix typo ([0dffa04](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0dffa04caa141e8e5c060ba342610ebd0049d548))
* **subs:** primarily order subs list by assigned time ([803d8b3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/803d8b30df9d8764dd515476f942c0e08e1da1f6))
* avoid column-off-by-one with URL-links to tr elems ([dbdd3dc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dbdd3dc5652ea274c91efc1c25f161c6fd3d850d))
* hlint ([2286086](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/22860863e59f0f927d6424552b913297fee24f14))
* **eo-exams:** display exams without label ([5fe01ce](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5fe01ce8dc87e4a318be1885a053b7403813cffa))
* **eo-exams:** fix eo-labels query ([eba56e4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eba56e4d621aa62fdee7535c8b83e0b61f0352d8))
* **labels:** correct forced bool value for no export label ([7b16351](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7b16351e4b84eba0be1db7d723b4953e991b42b5))
* **labels:** fix exam-label delete action ([b1991ee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b1991eead90a21a296fa0436485ea2532223c72d))
* **labels:** implement label deletion on ProfileR ([da39b05](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/da39b05627058a1472929b46ae1c2adb0b1fe2c9))
* **tests:** complete test user definition ([11b7786](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/11b77867acd83a1fe23341d6aa9b0e3fd66506c0))

## [25.25.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.24.1...v25.25.0) (2022-01-21)


### Features

* **communication:** support attachments in course/tutorial comm's ([5bd9ea8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5bd9ea85e8f0e4387cf47116bf42c4441bdbe8b3))
* **file-field:** cumulative size limit ([b749039](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b749039636c61157b5fc0bea9848ab9828ee671c))

## [25.24.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.24.0...v25.24.1) (2021-12-29)


### Bug Fixes

* **courses:** enhanced description of study modules ([89fadb2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/89fadb242037151ea792667cab85fc502b135f57))

## [25.24.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.23.0...v25.24.0) (2021-12-28)


### Features

* **course:** show study module on course overview page ([dbc5e99](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dbc5e99109285d4427832820a77a6b47a8098f62))
* **course:** study modules as new course property ([cb00de7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb00de7960c91d87f5f8fb7ecb29dd15cb61a5a3))

## [25.23.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.22.4...v25.23.0) (2021-12-14)


### Features

* **check-all:** added shift click functionality ([da1c8b5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/da1c8b54510ee1436fefe97ba32372a08299b83e))
* **checkrange:** added tooltip ([ce6f09d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ce6f09dd857f53dc8c350d7d29b2164c78645b59))
* **checkrange:** new util checkrange ([337bf73](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/337bf73067f2b98450d0388a1c064f0d2f9c456c))
* **checkrange:** unchecking a range is possible ([154f2e3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/154f2e35cc0e154ff80002b2e0aff3a76afa1ed6))
* **erweiterung such-filter usersr:** first try ([da3b339](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/da3b3391bd5aa9990dfb2818847cf8524ee68a9d))
* **messages:** added frontend translation class ([61c773f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/61c773f51cddb65dd0529f17799cbf7871023137))
* **tooltips:** added translatable tooltip ([e74b610](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e74b61065a5de811bd411c0e863fddf9b9baada0))


### Bug Fixes

* **check-all:** correct constructor argument ([02ce82e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/02ce82e9d2026730fd4716a2c0b070c38a6fc53f))
* **frontend-tooltips:** icon is shown ([86ee2fb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/86ee2fb14c05e3b6a78c6c51bf961b6c41d3e5c5))
* **modal:** modals are never destroyed ([7dbe1ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7dbe1ac08aacbda3b145a0da394706273dd6c639))
* **modal:** modals are never destroyed ([53dab90](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/53dab90810675f743ece284883da9c4c0e84270e))

## [25.22.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.22.3...v25.22.4) (2021-10-26)


### Bug Fixes

* **routes:** make access to workflows free ([29c54db](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/29c54db06f01659a3a6419009964a85cd11d5441))

## [25.22.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.22.2...v25.22.3) (2021-10-21)


### Bug Fixes

* **navigation:** always link workflows nav to instances ([adf9709](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/adf9709567d9a320f2c17d3c5dde940c2f9d8862))

## [25.22.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.22.1...v25.22.2) (2021-10-13)

## [25.22.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.22.0...v25.22.1) (2021-10-02)


### Bug Fixes

* **course-admins:** display course admins as admins instead of assistants ([f1fe444](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f1fe4447fbe7e96e55aaf284c7083338b5135ab6))

## [25.22.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.21.0...v25.22.0) (2021-08-30)


### Features

* **event-manager:** added method to register a list of listeners ([1a8fb23](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1a8fb230441f73b9b1fe593f4df1005a06d9628e))
* **event-manager:** mutation observers can be managed via the event manager ([34b4f48](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/34b4f48386c8fff4569b12eb3f7919e7c77d33c0))
* **http-client:** added possibility to remove specific interceptors ([0823df3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0823df33b5f157af290aca9a65f1df429048e53b))
* **tutoriumsdaten:** application restore ([d4a73e6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d4a73e699a399b02cadcea03e614c789671ee6d1))
* **tutoriumsdaten:** firts draft ([e972788](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e972788f540a9ce6c3fdf841313057b62a579d72))
* **tutoriumsdaten:** termin ([ebcb234](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ebcb23429ff09c56ba4a00a6cb8f082ee83e1fe8))
* **util_registry:** impelmented destroyAll(scope) method in the utilRegistry ([f1ef2e5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f1ef2e5ec776ad8a1fb7737eb0ed4f79218afd61))
* implemented an event manager ([c1c3536](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c1c35369d1ae017971e6d8cbafc06844f02fd00d))


### Bug Fixes

* **async-form:** destroy all after response is processed ([14a16c7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/14a16c7283483ff22ce22b070a430a61d24a7f35))
* **communication-recipients:** fixed undefined error with context and a few minor issues ([03ac803](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/03ac80342e0f3fcb1db2adf34f86a2c0d8fabf0f))
* **enter-is-tab.js:** implemented destroy method in enter-is-tab Util ([d1b9952](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d1b995269060c670d85f715671bfc9947b9f3e9a))
* **hide-columns:** removed clear storage from destroy method ([b3b0d65](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b3b0d6585068ecbc665e819b31c94f4d96a0fec5))
* **hide-colums:** small fix ([50a3ac1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/50a3ac1790f26c1e6f983d3687352d7811173110))
* **http-client:** strict equality check ([5078b56](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5078b56c16513f3b289bc4824ce0c7914b1a220d))
* **interactive-fieldset:** small fix ([4c2c683](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4c2c68327e75f5f51271853159c232fdd7bba21e))
* **navigate-away-promp:** removed unnecessary destroyAll ([7a07159](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7a0715906c8336ed64bbfabdf614746ade9f661f))
* **password:** added cleanUP ([204ce39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/204ce39f7c2f9c405971aa59da068a5389dda417))
* **show-hide:** storage manager is not cleared ([9453689](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/945368972da36f4ff0da99f38c71ea9a1c7157d7))
* **storage-manager:** clear is working without options as well ([f1c50e1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f1c50e137f4f11140c1171dd9d86bc5511b9e771))
* **tooltips:** correct regex match ([c8d36ea](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c8d36ea52dbef0a2e1aaec6aedaf7edd524975dc))
* **tooltips:** removed else case ([8d0241e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8d0241e727efb5051e5848f2a0c835d7a8d3ae6b))
* **util-registry:** filtering activeUtilInstances when a util is destroyed ([5b4ac75](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5b4ac7587438e7adb21e0ff3d9d629b6ff00263a))
* **util-registry:** handle negative indices correctly ([cbc03f5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbc03f57c56df9c96a84da15a68d26904b75a65f))
* fixed a few minor issues ([6320cd9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6320cd927a84445f056dc782fb440d276cb26009))
* prompt not shwowing up after submit/close ([abe8415](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/abe84156d508dca8fce549b24d5902d24afc0dbf))
* smaller fixes and typos ([1f978e6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1f978e65a82a91fb728a7ee2970a4fd9e6beb521))

## [25.21.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.20.2...v25.21.0) (2021-08-20)


### Features

* **corrections-r:** allow csv exporting one line per submittor ([7aadb66](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7aadb6662bc8db76436f8d41ded7156acb98418e))
* **corrections-r:** authorship statement state ([51522ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/51522efc7c9915115e0d8791320a03e35d2933c8))
* **corrections-r:** csv export ([2a6248e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2a6248e3d5d4f4de5f1c7d6c6bcf092dc9873a2e)), closes [#705](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/705)
* **corrections-r:** filter/sort by pseudonym ([153af8c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/153af8c6b4042430bb4bc120fa5c24a5d114e4c1))
* **corrections-r:** json export ([fe8e4bb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fe8e4bbd4f6a8b1b1c54808ebc96ee675a078648))
* **course admin:** application restore ([cb4ed8d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb4ed8d9887e521f47689c118baf439846cd4514))
* **course admin:** done ([15689c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/15689c597ef407583b01dabc9f7631e9dc90b009))
* **course admin:** no new-line ([0a6a174](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0a6a1749d351e626383e513293af280f78552009))
* **lecturer type:** aenderung ([89e1d67](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/89e1d675c3be0fec106e84920184a8c95dfa6346))
* **link password time:** application restore ([6d536c3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6d536c39bd9f3117f18d2e52c93f178aea4a002d))
* **link password time:** done ([4490e9a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4490e9ad20c55153e81a344c7dbf7813cb219108))
* **link password time:** done ([2321216](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2321216b0f4f194c7cd8b47eb020819d6aa1f2e5))
* **link password time:** new time format ([df2a9bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/df2a9bc20fe9f958cbee98315b644ec2fcba0630))
* **link password time:** restore application ([c5c5417](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c5c541709b5053c08d21bdd753bb99df574c6c5b))
* **link password time:** restore application ([85006ff](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/85006ff389188b56a8b61943621c190c9a9503b7))
* **sorting tutorial table:** application restore ([9dc12de](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9dc12de056e73736659c053b0eabef66ca524047))
* **sorting tutorial table:** done ([482241d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/482241d033c32c52c31ea20920a4fec07ba975dd))
* **tutor tabel sorting:** dbt sorting tutors added ([b1787cd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b1787cd77e8a643accc0ef54cc18c87df215680c))


### Bug Fixes

* **corrections-r:** allow filtering by matriculation ([1b6b781](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1b6b781e82c39bc29c8984c587ac836f0da77a02))
* **csv:** less quoting in semicolon separated lists ([42f1eab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/42f1eabb2c984a7d30ea8b90710c68aff8af9f97))

## [25.20.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.20.1...v25.20.2) (2021-08-16)


### Bug Fixes

* **submissions:** maintain anonymity ([0184a5f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0184a5fe3b1af635318fa0fa317e3497f24fbc90))

## [25.20.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.20.0...v25.20.1) (2021-08-13)


### Bug Fixes

* **interval jobs:** avoid accumulation, reduce job size ([24491b4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/24491b446b870698564adb9718e868e082873539))
* **jobs:** more general no queue same ([b1143cb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b1143cb12bea48d75a2453f92122edcfb4fe51f1))
* **volatile-cluster-config:** fix pathpiece instance ([dcd5ddd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dcd5dddec82da359a2100360cfeb6845ed320821))

## [25.20.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.19.3...v25.20.0) (2021-08-12)


### Features

* **submission-show:** display authorship statements ([cbd6d7d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbd6d7d2b098f8e2c921fd7a56a458d62331d784))
* **submissions:** display authorship statements ([7749238](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7749238e554b612a8bf69e6beb94efe3e5d02973))
* **submissions:** display submittors more explicitly ([d2e2456](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d2e2456f6204245d933fb6abc87c44388ce3e339))

## [25.19.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.19.2...v25.19.3) (2021-08-02)


### Bug Fixes

* **submissions:** more precise feedback ([d151b6f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d151b6fc14e5b32d9f07923149923d5ab7ea4880))

## [25.19.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.19.1...v25.19.2) (2021-07-30)


### Bug Fixes

* **jobs:** flush only partially for reliability ([59c7c17](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/59c7c1766588052383754b16e575347fa960ad6a))
* **submissions:** allow user to resolve themself for auth'stmt' ([5bbb86a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5bbb86aa7750dd907f49cb3ba5daf2cee8485bae))
* **submissions:** cascade delete to authorship statements ([fcce16d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fcce16d838e5cba3187a82a5762b831d7df54cd0))
* **submissions:** don't leak info from corrected versions of files ([66f5e96](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/66f5e96eca4cbcb6cb092092b1b1b069ce30f159))

## [25.19.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.19.0...v25.19.1) (2021-07-26)


### Bug Fixes

* build ([071df90](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/071df906da6c41afa226f944a90c2f294eeba243))
* **workflows:** disabled warning for top workflows/instances ([17ed2fa](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/17ed2fad2230944c629c6a0c8d8181f6fec8983f))

## [25.19.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.18.2...v25.19.0) (2021-07-26)


### Features

* **workflows:** replace pages with warning if turned off ([8634d20](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8634d20e2ad2d3746cf7b6111b91db9e57e4863b))

## [25.18.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.18.1...v25.18.2) (2021-07-21)


### Bug Fixes

* **arc:** actually invalidate ([ef4734e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef4734ebb671d9ef19c284a4c5cc9412d6e62874))

## [25.18.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.18.0...v25.18.1) (2021-07-21)


### Bug Fixes

* typo ([26c3a60](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/26c3a60592c02570ceeed42cc977ad223baa16ae))
* **authorship-statements:** resolve exam-part to exam properly ([3a2d031](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3a2d031bb5f5b4d6e5df06f8ec82957a1bc81a72))

## [25.18.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.17.1...v25.18.0) (2021-07-21)


### Features

* load shedding ([9df0686](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9df0686086ff7b64d401a2302edd2fe7636db111))

## [25.17.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.17.0...v25.17.1) (2021-07-21)


### Bug Fixes

* build ([9fd95d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9fd95d181c498d460eaf30436ff110f7c1f9413e))

## [25.17.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.16.0...v25.17.0) (2021-07-18)


### Features

* demand authorship statements ([34b3e6a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/34b3e6ae21b38a5b8389deade5deeb77b0981ead))
* i18n form ([2d95f35](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2d95f353c1209a4d3528c6aaf53c832bf5429a34))
* show authorship statement requirement for sheet ([5e96982](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5e969825ad0c84c240b5c17b011dacbb63f4bfdf))
* **exams:** basic required optional action for authorship statements ([5cc41ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5cc41aeef94993a24538b2f88af1fb75625036a8))
* **exams:** disable and set use-custom field according to school setting ([22dfd33](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/22dfd33aca9b8ad797c2617bbc656cf8276edf38))
* **exams:** display school default in form ([abd68ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/abd68ac0322a34afb62c416b60965e87ee6f10c2))
* **exams:** do form validation ([bf7b25c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bf7b25ca9e9d11df94b91f7483ee339cefd3e0c9))
* **exams:** first do-nothing stub for exam-wide authorship statements ([0392297](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0392297ddbfccbb9a08e678696a9cedd1098121a))
* **exams:** use template authorship statement settings if applicable ([57a259d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/57a259d8a2822ac1c593663e99f6e41163909c91))
* **schools:** add school settings regarding authorship statements ([cb8e338](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb8e3385889c0c4c13418bc69af091b9c8a3f22f))
* **schools:** more school-wide configuration authorship statements ([960bd76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/960bd76acafc9cd077b831b67a281eb7b20e703c))
* **schools:** store school authorship statements as html ([09927ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/09927ae14004f7a27f816ad874704969641dad83))
* **sheets:** add required flag and definition ([541dd76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/541dd7688ffa36be8a968f26f920507ed5aae646))
* **sheets:** display authship req on SShowR ([44473b4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/44473b45756c5df20e6a81927867de191cf70366))
* **sheets:** eliminate authship statement required Bool ([0735c05](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0735c05a7489957ed500bac1c006f4ecfdab74f3))
* **sheets:** fetch school statement as statement default ([a39a0d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a39a0d7c8763e158dae5750afac8a78bd953dcdf))
* **sheets:** introduce sheet-specific statements for exam-unrelated sheets and as exam-statement overrides ([3f87f20](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3f87f20eb14e5db8a63c61885c4570689169ebed))


### Bug Fixes

* **exams:** better behaviour for optional statements wrt school default ([fe78377](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fe78377fae8af7766f9720628aebef599656ed2f))
* **exams:** correctly treat school-mode optional as off by default ([ac86832](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ac86832b34a605e5d64d56ef08a871bf307347a8))
* **exams:** fix form validation wrt non-empty statements ([0082135](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0082135c56b7fc0e5db3af6910f8365e12920c46))
* **exams:** fixhance exam authship form section ([4109db6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4109db6f815fbb49c861177b3caecb98c2a963d8))
* **exams:** prefill with school authship statement in optional mode ([0cd8f4c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0cd8f4c02f383f43b5e3ea059cd3acd38595ab56))
* **exams:** remove deprecated/unnecessary form validation wrt. authship statements ([bf059a1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bf059a132094e53c3ef956582b5e13517e9c133d))
* **exams:** set use-custom correctly if forced ([8bb6140](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8bb61401a77f20fcb35aa05401bf16285aad1d93))
* **schools:** fix schools form wrt. discouraged modes ([53a8f1b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/53a8f1ba122466312947cdbdb49749a61acab37c))
* **schools:** insert correct authorship statement definition for exam-unrelated sheets ([2272647](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/227264743e0e8d0acf76839300a034b4bb1bf2a6))
* **schools:** perform authorship statement inserts ([579371c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/579371cffd87c247805bf4ead8bc2c278269a5ee))
* **schools:** rename messages ([0e62073](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0e6207376043af8fe0929019e3c39f80bcfea9a6))
* **schools:** switch authorship modes to required in form ([8fb49dd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8fb49dd602f4eb854b300b5b399206aa2fbca87b))
* **schools:** use StoredMarkup instead of Html for authorship statement ([67c3016](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67c30165ae90603e8a97ad2661d2bacb92e2e53f))
* **sheet-show:** move message ([1d8a2ce](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1d8a2cef60a688bd514d529f8e1230e462811f1e))
* **sheets:** fixhance sheet authship form section ([7192cb5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7192cb527c7f66c320308a80de9906a6edc6e9ec))

## [25.16.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.15.3...v25.16.0) (2021-07-13)


### Features

* **personalised-sheet-files:** seeds ([cf67945](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf679452928c14200e1eb3877987ee299fbf9f6f))

## [25.15.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.15.2...v25.15.3) (2021-07-08)


### Bug Fixes

* avoid subSelectForeign join issues ([576fccb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/576fccb5222a5dbd19db69f142a39b4155b7486d))

## [25.15.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.15.1...v25.15.2) (2021-07-06)


### Bug Fixes

* **explained-selection-field:** support linebreak in titles ([627a2df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/627a2df7adf41651e698d8cd9d632d066fc2f868))

## [25.15.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.15.0...v25.15.1) (2021-07-06)


### Bug Fixes

* **cache:** atomicity & workflow instance invalidations ([ef7fde9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef7fde937ebf1bc31e3706fba1da166bb82133c5))

## [25.15.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.14.2...v25.15.0) (2021-07-05)


### Features

* **course material:** auto vorschläge für materialtype ([decdda3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/decdda359d16cce429a7e7a07d4674840e5fe6af))
* **course material:** first two filters ([90e4a62](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/90e4a620f0c1671ff332db1910c176e58ccbac06))
* **course material:** materialDescription in progress ([89e9887](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/89e9887fe1112cbc21517e4b501ead33f5a969ba))
* **course material:** materialdescription search implemented ([3a9622d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3a9622dfb8474d9f3764f5870197e317a96d9de3))
* **course material:** merge-request suggestions ([dc5fc3f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dc5fc3f710363f0644c43866505e32095b41ce92))
* **course material:** runDB für cid nur einmal ([c09acbb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c09acbbf8a7b95176b3d52449b3b9d26e315ccd6))
* **course material:** small empty-bug fixed ([d8b1f97](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d8b1f9788c74ea5d7dc4f1f45432649d9601106a))
* **workflows:** update instances from definitions ([32efdae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/32efdae839b1a3e43ed4161d20e598964970f15e))


### Bug Fixes

* **workflows:** workflow-definition edit translations ([5c5cbad](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5c5cbaddf8b33f455ff18789806a3e0f9ac447ed))
* typo course-assistant ([c7ce167](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c7ce1679de799285ec7a9a0a62c0a202b9078eb3))

## [25.14.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.14.1...v25.14.2) (2021-06-28)


### Bug Fixes

* **health:** monitor flush by check interval not flush interval ([03226ec](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/03226eca6aba91e2f10f5828a38f2a15d747dd0c))

## [25.14.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.14.0...v25.14.1) (2021-06-28)


### Bug Fixes

* fix creating new terms ([9676615](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9676615c55cbecce02dca95b01ad69c1a2455f1d))

## [25.14.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.13.1...v25.14.0) (2021-06-27)


### Features

* **allocations:** admin-interface registrations ([5e38f03](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5e38f03a85992964d4c9cf5100c4c8a4a8762aaf)), closes [#677](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/677)
* **allocations:** create & edit, list & download matching logs ([5320a4f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5320a4fe98f26576e6b72a1411107f410333009a))
* **allocations:** delete allocation-users ([6a1a64a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6a1a64a6113fcae3a654e472fabdf5a3f622f549))
* **allocations:** display number of ratings and vetos to admins ([6da8ad3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6da8ad348182185f773ab08a10ff59a6a1e89b85))
* **allocations:** edit allocation-user and their applications ([4daf33a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4daf33a1a0c2c621e295aef50aae9e2dc5d5a7e8))
* **allocations:** highlight app's of users without alloc'-user ([300c378](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/300c3787867622a7fe3580b63cbbfba86a8b21f3))
* **course-list:** filter by allocation ([de39686](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/de39686d89f6ec410bc50eaca058082dc727547d)), closes [#715](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/715)
* **news:** active allocations for lecturers ([cde0122](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cde012252907c57506eef44868b3d25fae50a8f7))
* **terms:** time based term activity ([df073ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/df073ef7947eb80dc35fe955b92e635881eb50fa))
* **workflows:** enum fields ([426c40f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/426c40f0a4f596804eca723e09894f9c5606af6e))
* **workflows:** new field CaptureDateTime ([5944a17](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5944a174bc8a749c60718b58d656f44cd21e7ecf))
* **workflows:** restrict day field wrt. current time ([b742731](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7427315119843e6b2cebad4f6f420d57c2efaf0))


### Bug Fixes

* **news-allocations:** i18n ([5a23d87](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5a23d87380badfe1ee8e6b4c93730050cc42305f))
* added check in async table and removeddebug log output ([f807e2a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f807e2af78aa0d1d135990d764df9da89a0e61d0))
* possible workaround? ([757e148](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/757e1480329d11521c1ef7afb78702a251fd5b89))

## [25.13.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.13.0...v25.13.1) (2021-06-07)


### Bug Fixes

* added uw-enter-as-tab to CCommR subject field ([93a829b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/93a829b81b45639b0841bac72dc57416b50ef01c))
* **submissions:** fix distribution without consideration for deficit ([5035dff](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5035dff9021260cd45dabfc175bb535bdc19dc71)), closes [#713](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/713)
* changed DEBUG_MODE back ([cc63f63](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cc63f636e928c9071bbc3208b77aa20d88800a17))
* changed enter to tab behavior in CCommR ([7aeb8e6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7aeb8e61f46dcc5c918df20450df2fdf1d10f217))
* changed keypress to keydown. ([9288e5c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9288e5c203a88368ec853ba7001290df87069fc6))
* next input area is now selected via a css query ([1aaf254](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1aaf254c3c3c07f95b63bdf760791859a80aa4a4))

## [25.13.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.12.1...v25.13.0) (2021-06-03)


### Features

* **participants:** basic funktions added ([b96327b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b96327b18dafcd020c94bb84c6aafffb53544076))
* **participants:** corrections ([fd11121](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fd111215447aff817399db379a4ca8e90eb73cff))
* **participants:** corrections 2 ([d6ce0c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d6ce0c47d92fac76ccdc59805fcdbd3ad932d3e3))
* **participants:** first finished verson ([0a3fd23](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0a3fd23e22a81b3636fb3ac224dce52df3f752f2))
* **participants:** second version, Intersection added ([02354f0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/02354f0998e61c236bc982848b9d709c927690f5))
* **participants:** small Name-change ([6f3243d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6f3243d90bdc137e7f2ea9fe8e271f1cdc32dfbd))
* **participants:** small Name-change ([eced778](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eced7781ae346e285b7f3949917f23883b4dfaa8))
* **submission-list:** bulk download submission originals ([d7f2d11](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d7f2d113929f9dc11291d6db916c8944ae158c3b)), closes [#707](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/707)


### Bug Fixes

* better pathPieceJoined ([adcd5d5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/adcd5d5aee3d541fbf65a532b81d86f236575b7b))
* valid binary ci instance ([8cfdd28](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8cfdd286517e0a9ca99dd31b9d220560adc6c93d))
* **auth:** properly restrict various auth by school ([6f04a6b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6f04a6b693e99b573efcc94023dab0be4d6d83bb))
* **memcached:** don't 500 upon hitting item size limit ([d79a539](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d79a539f71e8250f677ac4e0b42c9ffd4de50af5))

## [25.12.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.12.0...v25.12.1) (2021-05-19)


### Bug Fixes

* **submissions:** hide correction-only files ([575fadc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/575fadcd8cbc4d899ed0ab5d58e3fa8aa64df111))

## [25.12.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.11.0...v25.12.0) (2021-05-19)


### Features

* allow examFinished before examEnd ([21bbb92](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/21bbb92d4c5bca8e175d2be97515e14f67ad696b))


### Bug Fixes

* properly apply auth to corrections in sheet table ([d59f686](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d59f6860215ebbffde61062a501b5eeeabdb58ae)), closes [#700](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/700)

## [25.11.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.10.5...v25.11.0) (2021-05-11)


### Features

* **submissions:** optionally disable consideration for deficit ([c6a6ec7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c6a6ec721c2a863d324ddfb5d2b2c1e42e659067))

## [25.10.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.10.4...v25.10.5) (2021-05-07)


### Bug Fixes

* update imprint & add instructions for help ([eec9a39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eec9a3974fc4cde5cc70ab650d018667ce044a92))

## [25.10.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.10.3...v25.10.4) (2021-05-06)


### Bug Fixes

* **workflow-workflow-list:** restore default sorting ([454a917](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/454a91702bdbbed7e473ef94a603bcea2e716406))

## [25.10.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.10.2...v25.10.3) (2021-05-05)


### Bug Fixes

* restore workflowWorkflowList columns ([e55c6d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e55c6d795fd724bdb732e22d13c96d6b67ea7da1))

## [25.10.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.10.1...v25.10.2) (2021-05-04)

## [25.10.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.10.0...v25.10.1) (2021-05-04)

## [25.10.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.9.3...v25.10.0) (2021-04-15)


### Features

* **workflows:** list involved users ([d8878a9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d8878a905e07f1b5fb5159ecdaf70f27e9c1dc37))

## [25.9.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.9.2...v25.9.3) (2021-04-14)

## [25.9.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.9.1...v25.9.2) (2021-04-14)

## [25.9.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.9.0...v25.9.1) (2021-04-14)

## [25.9.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.8.1...v25.9.0) (2021-04-13)


### Features

* partial support for lsf import ([37cdc77](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/37cdc775b5b2d3e4cd1cc22858b2c05e75de8a3c)), closes [#686](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/686)


### Bug Fixes

* build ([5c709f1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5c709f1bbb077d981fbd5d59e9c0f30cddbb468d))
* prevent deleting sheet-referenced exam parts ([9859c2e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9859c2e99c1e0c7531ee38864a24ff279a8e6a7c)), closes [#681](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/681)

## [25.8.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.8.0...v25.8.1) (2021-04-09)

## [25.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.7.0...v25.8.0) (2021-04-08)


### Features

* additional general purpose caching tier (memcachedLocal) ([939ab37](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/939ab37588bb71b14b8a9f3ab58d7440f598faf9))


### Bug Fixes

* typo ([f155a4b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f155a4bf08d169309c05e3efbb47a246f3010816))

## [25.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.6.1...v25.7.0) (2021-03-30)


### Features

* **course-users-table:** json export ([6f291b2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6f291b2e6893554193732b059758794fe2b7fa51))

## [25.6.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.6.0...v25.6.1) (2021-03-30)


### Bug Fixes

* **admin-tokens:** avoid option none ([af3ec98](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/af3ec98de512f72220d363b9dd0c06532ae1a960))
* add missing do ([55319c8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/55319c8c5060a0d8763abb56c27d30e852c51f52))
* buttons know about ALL actions from other buttons ([11664dc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/11664dcd82c13eef1c395e2e590c4fb0c587aa65))
* check space of occurrences after ignoring ([fabf56c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fabf56c1640c94f806d43aaca264100cbc39b840))
* correct rebase-sourced error ([02589e4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/02589e4d00de233d847d6be71e44f9fc451fbfe9))
* correctly apply suggestion ([67d6fd7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67d6fd7d438a31b50e6f4e6e921873ee11b32e9c))
* correctly handle original minimizeRooms-flag ([d5bd504](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d5bd5042ad920b26df847845cc437c3f0616575c))
* correctly report NoUsers for ExamRoomRandom ([16cbc78](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/16cbc78878615a8d123de5d8fda11136685a824c))
* oops ([f6cbf99](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f6cbf99245ffdd19a2d6c9acc7c0b9a7f8df45ca))
* sort occurrences in the right order ([732df50](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/732df5053033c3533f52850cc6220dd06a7e3500))
* use extraUsers instead of extraCapacity for unrestricted pseudo-capacity ([2be9d76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2be9d76af2b3e9fd52284c639a4c3f6dc1c51779))

## [25.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.5.3...v25.6.0) (2021-03-29)


### Features

* **frontend:** password visibilty toggle ([f0e4547](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f0e45477fa85a1d82750597cdaf122e41e9c7764))

## [25.5.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.5.2...v25.5.3) (2021-03-24)

## [25.5.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.5.1...v25.5.2) (2021-03-24)

## [25.5.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.5.0...v25.5.1) (2021-03-23)


### Bug Fixes

* remove cached-db-runner ([ff82700](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ff8270042f74d8019e121aebf8636472e1e4d79e))

## [25.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.4.0...v25.5.0) (2021-03-23)


### Features

* **course-participants:** csv export first name/surname separately ([1036926](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1036926470792bf3409ba3a224886d48b7e1d314))

## [25.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.3.0...v25.4.0) (2021-03-19)


### Features

* **submissions:** also warn correctors about multiple submissions ([8795edd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8795edd1fa452d012704146481c8318d206634a5))
* **submissions:** warn about multiple submissions for same user ([c19a00d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c19a00dcefb2dcae017026edb6e1c7cb6ce16841))


### Bug Fixes

* **auth:** wrong caching for external-exam-staff ([9d1f1c6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9d1f1c691085ec65ad0f19cc51602a59ee133fc4))
* **submissions:** improve submission process ([7219131](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/72191315b6daed78cd0f31b02627e1d27db620f3)), closes [#675](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/675)

## [25.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.2.0...v25.3.0) (2021-03-18)


### Features

* **exams:** exam finish button ([78d0f25](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/78d0f2522db759c2ee465e040939c92b2f9a1891))


### Bug Fixes

* **submissions:** take care when to display corrections ([a6390ec](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a6390eccbd164ee5e821d3ecb0fab794a417425a))

## [25.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.1.2...v25.2.0) (2021-03-18)


### Features

* **csv-export:** .xlsx ([5c51394](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5c513946c15ed215f6958be1c7a435f03314f115))
* **submissions:** improve behaviour of sheet-type-exam-part ([91a5166](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/91a51664c32bd17e4c2d1cd496bf05338146291d)), closes [#676](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/676)


### Bug Fixes

* **csv-export:** mime confusion ([8bdaae0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8bdaae0881fe98c4c5f69f1332ac2ffb0ca83081))

## [25.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.1.1...v25.1.2) (2021-03-17)

## [25.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.1.0...v25.1.1) (2021-03-16)


### Bug Fixes

* weight random token impersonation towards active users ([a314f64](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a314f64a70d9e7e427383c8d656d9bdceed5f9f3))

## [25.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.0.5...v25.1.0) (2021-03-16)


### Features

* admins can efficiently generate many tokens for random users ([600bbe5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/600bbe5d7e9051e4a4eac540b01ff358666ebc9c))


### Bug Fixes

* typo ([f931c67](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f931c67a9ecf37bd9a6c9814ee61de7cb054dcc5))
* **test:** isNullResultJustified reported false positives ([292f5cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/292f5cf91b56953189ee72e42b822d66761ff3bb))
* check if number of relevant user is >0 to prevent crash ([317b95b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/317b95be317ea038ad9fa398fc0c0c456b53495d))
* correctly calculate maximum user name length ([cd07a56](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cd07a56a9fd3ee99b74e5304581574671e3689a0))
* handle rare cases where a mappingDescription with start>end would be produced ([c99d96e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c99d96ecb8a43400eb10dfe192bf751cb00a9d25))
* make sure to report NoUsers, regardless of rule ([9c928b0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9c928b0375c1aab0c46768101849ce8daeae9b81))
* **test:** fixed compiler errors (oops) ([bc42f30](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bc42f3072fd37ee6f37c70a0b3999d9ac793b240))
* ensure termination for non-{'A'..'Z']-names ([873d5a0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/873d5a02adae8f33db349bd9de3c7bd49331d27f))
* examAutoOccurence no longer user >100% of a room ([eaf245b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eaf245beaaa1f739d6b857712f1e4ea5b53e7c82))
* increase size of test instances again (oops) ([4e76fe7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4e76fe7e504515845d468fc3251a38c90aaaaf66))
* make sure it compiles again + add 2-letter name ([d60f935](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d60f93561f5ee84d460645a945db35ac6b55e97d))
* make sure line-break algorithm respects available lines ([e487cef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e487ceff5858671eb0bcbd813e9de0d3b4c74f75))
* make sure unfortunate combination doesn't only produce 0-9 ranges for matrikelnummer ([8e4cb09](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8e4cb0917db1098f5b19be0dfad4c6fafb900c49))
* mappingDescription doesn't overlap for the first n rooms/with small names/matrikelnummer ([fc35fd2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fc35fd26c1eb699d6eb8aa1b9febb48641c26d05))
* shown ranges "include" special mappings ([7e1b75c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7e1b75c2e167c75ebc3a05f881ad7fb07c29af55))
* spelling plugin had a suggestion; actually Hello World commit :p ([7b0fd61](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7b0fd61f7f8bf1e995209bec7b44231b5ba011a6))
* user with a pre-assigned room count towards the capacity limit ([4fc0535](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4fc05351fa8048752f2ec3260dcaac64f962c9a3))

## [25.0.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.0.4...v25.0.5) (2021-03-13)


### Bug Fixes

* **authorisation:** inverted logic for empty ([65814c0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/65814c005e2637bb5f6347bf1f35133654538e7a))

## [25.0.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.0.3...v25.0.4) (2021-03-12)


### Bug Fixes

* tests ([4803026](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4803026a2c091128a7370c12f0c06de9bd7b9180))

## [25.0.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.0.2...v25.0.3) (2021-03-12)


### Bug Fixes

* invalidate nav caches ([e88b6d6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e88b6d6bab3ea4577af3cd9465e66aa7e48177a2))

## [25.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.0.1...v25.0.2) (2021-03-12)

## [25.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v25.0.0...v25.0.1) (2021-03-11)


### Bug Fixes

* **auth-caching:** submission-group ([896bd41](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/896bd41e3b415283cce16cb84a8219b8d4c1702c))

## [25.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.9.2...v25.0.0) (2021-03-08)


### ⚠ BREAKING CHANGES

* **auth:** additional authorisation caching

### Features

* **auth:** user independent authorisation caching ([63f0d3c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/63f0d3c37ad4a02a5cbdf76398d4a9c74a0a0b59))
* **messages:** implement custom parser for message files ([bb877eb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb877eb81396211a801496061ea603b39753829b))
* **messages:** mkMessageAddition ([ea33d84](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ea33d844cc4acb2503fc4780c7895299eb9d5ef5))

## [24.9.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.9.1...v24.9.2) (2021-03-01)

## [24.9.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.9.0...v24.9.1) (2021-03-01)


### Bug Fixes

* build ([cf33f0a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf33f0af84166b040de4b9a685a58d9884bc67f8))

## [24.9.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.8.0...v24.9.0) (2021-02-26)


### Features

* **db:** track source of database accesses ([23ff9d9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/23ff9d9222d7a75b2931827a6cc0335aafe753a1))
* **transaction-log:** more details about submission files ([b9cc5b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b9cc5b9970cc0b1f61c9df03c54901d9d6e822d0))


### Bug Fixes

* **jobs:** wake more often during waitUntil ([6115b83](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6115b83bb32d767ef54f2a2ae210ad1c7415e69c))

## [24.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.7.0...v24.8.0) (2021-02-23)


### Features

* **monitoring:** observe database connection opening/closing ([d801a2f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d801a2f84ae42862dfc357a58ee47dd6dc39eef8))

## [24.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.6.0...v24.7.0) (2021-02-23)


### Features

* **db:** provide our own implementation of connection pooling ([50fdcb4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/50fdcb4540e6bfbc8da9ed10ed06d6f6ce443cf9))

## [24.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.5.0...v24.6.0) (2021-02-21)


### Features

* **db:** optionally disable some db connection pooling ([35ac503](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/35ac503bf971ace21c49646aa15e8b94b7a3e823))

## [24.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.4.3...v24.5.0) (2021-02-21)


### Features

* **bot-mitigations:** only logged in table sorting ([fb6ae08](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb6ae089c63174edc1d84512ea35378ab8cd0e0e))

## [24.4.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.4.2...v24.4.3) (2021-02-20)


### Bug Fixes

* **jobs:** use more read only/deferrable transactions ([db48bbb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/db48bbb7765604aaab8f8d5c540793b1ceaff16a))

## [24.4.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.4.1...v24.4.2) (2021-02-19)


### Bug Fixes

* **missing-files:** properly account for workflows ([c272618](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c272618aa6dd68a1acb5b959c6d905978b26eb07))

## [24.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.4.0...v24.4.1) (2021-02-19)


### Bug Fixes

* **files:** count personalised sheet files as alive ([e54b985](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e54b985815fbbc637d8f4681ac55b3d46e2263a3))

## [24.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.3.0...v24.4.0) (2021-02-17)


### Features

* **caching:** introduce cache prewarming ([8d1f216](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8d1f216b5b6ee2a59c3fb80f5dd4a701d9dad5ef))

## [24.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.2.1...v24.3.0) (2021-02-15)


### Features

* **minio:** use separate bucket for temporary files ([1cd79d3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1cd79d35e2761d84bb904a77e74d5cacb0b2244c))
* **personalised-sheet-files:** restrict download by exam ([a8f2688](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a8f268852a256209f6ab187167c2a1c066618c4c))


### Bug Fixes

* **exam-bonus:** avoid divide by zero if all sheets are bonus ([0fd7e86](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0fd7e86695f47047c9e4e1bb8efe9477103707ab)), closes [#671](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/671)
* **exam-bonus:** fix rounding ([854fa6b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/854fa6b968ed01d60dea0d9ba6fc93d37e5ec361)), closes [#672](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/672)

## [24.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.2.0...v24.2.1) (2021-02-11)


### Bug Fixes

* **arc:** reduce lock contention ([1be391f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1be391f5f5bf2588939fea92809dd629c0a69d99))

## [24.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.1.5...v24.2.0) (2021-02-10)


### Features

* implement in-memory cache for file download ([36debd8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/36debd865f6e74856c74bd658dc4694140183fed))


### Bug Fixes

* unbreak arc ([8ecb460](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8ecb460f39f48557b5935b1cd18709ba197d3490))
* **jobs:** prevent offloading instances from deleting cron last exec ([e61b561](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e61b5611b1568180aa3ccfc3e3b981eb9a13cd53))

## [24.1.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.1.4...v24.1.5) (2021-02-09)

## [24.1.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.1.3...v24.1.4) (2021-02-09)

## [24.1.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.1.2...v24.1.3) (2021-02-09)

## [24.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.1.1...v24.1.2) (2021-02-09)

## [24.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.1.0...v24.1.1) (2021-02-09)

## [24.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v24.0.0...v24.1.0) (2021-02-08)


### Features

* ensure cached study feature relevance is up to date ([8798f54](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8798f547a60a7fa7c0849e20e1b0e9d012ac9312))


### Bug Fixes

* restore storting for exam-office exams ([5698e9c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5698e9ca0bb19585b9a9d2d3c10f8b5f99ae5db9))

## [24.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.7.0...v24.0.0) (2021-02-01)


### ⚠ BREAKING CHANGES

* **jobs:** Job offloading

### Features

* **jobs:** batch job offloading ([09fb26f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/09fb26f1a892feba32185166223f8f95611ea9ef))


### Bug Fixes

* **workflows:** don't cache instance-list empty for correctness ([cb1e715](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb1e715e9b2da2f5ac0bd03b636de0f961307efd))

## [23.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.6.0...v23.7.0) (2021-01-27)


### Features

* **dbtable:** extra representations ([2c0fc63](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2c0fc63be1de02e8acffbc6a9c5ee83b061c5825))
* **exams:** exam sheets ([500000b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/500000ba0f6f3b3c32cfd7593e5468796660d46b))


### Bug Fixes

* more verbose watchdog notification failures ([48028c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/48028c40532577f74430340ed924af7116b8bd96))
* **mass-input:** properly escape query selector ([9a3f401](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a3f401b38e86e2f9e7fa722698a437d853b422e))
* nonmoving-gc still segfaults ([c404ce9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c404ce9b3529cf402a0f9d649ca3299df09ba089))

## [23.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.5.0...v23.6.0) (2020-12-15)


### Features

* **massinput:** reduce size of ajax requests ([72838e2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/72838e2592f159ab79c9f245ac28a4f9bd807e19))

## [23.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.4.3...v23.5.0) (2020-12-12)


### Features

* **auth:** record student ldap role ([50455e6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/50455e68a1e89e16a5905b976a09d265afa08bba))
* **workflows:** explanation text ([aba6737](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aba673756e9e40057625c41da8b32d378e9b67c6))


### Bug Fixes

* **tokens:** introduce clock leniency and remove start for downloads ([8939a8b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8939a8b90a39a26614da18dd3985aee253cd191f))
* hopefully improve workflow auth performance ([1d3fd8c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1d3fd8c8a7824d6c6d043f4114067238af4bdc6e))

## [23.4.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.4.2...v23.4.3) (2020-12-10)

## [23.4.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.4.1...v23.4.2) (2020-12-10)


### Bug Fixes

* hopefully speed up aeson via ffi ([a00ba10](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a00ba10e9cf1ffa534908b9125730e88179052eb))

## [23.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.4.0...v23.4.1) (2020-12-10)

## [23.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.3.0...v23.4.0) (2020-12-09)


### Features

* use c++ library for json parsing from database ([f226751](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f22675189e19d1cce20362121ef8c8aebe3628f1))


### Bug Fixes

* **jobs:** weaken crontab guarantees for performance ([212e316](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/212e316c7e256f7883e0b883942e98bf795d870b))

## [23.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.2.2...v23.3.0) (2020-12-09)


### Features

* don't redirect monitoring routes & crontab tokens ([3a106d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3a106d1ee5998ddeea852b3b0398c2f330664a63))
* **admin-crontab:** export as json ([bbd4916](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bbd4916f3a556ce4c05eb3b2b5268c9c072fdfdd))
* **jobs:** queue by jobctl priority ([a27a553](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a27a553e0a9782eda6023ec0b8b1055757bb511f))

## [23.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.2.1...v23.2.2) (2020-12-09)


### Bug Fixes

* **jobs:** adjust job handling to hopefully reduce load ([ed38f93](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed38f93537b57b5b3e4563dc0259d805760071bc))

## [23.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.2.0...v23.2.1) (2020-12-08)


### Bug Fixes

* **downloads:** do download links via redirect ([3ba41d8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3ba41d8f24b4358ad7a045eba0f630e1e2b67663))
* **files:** better configuration for file batch jobs ([3a90c88](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3a90c88b359f3e0cb0ed03df6e81b1532509ea48))

## [23.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.1.2...v23.2.0) (2020-12-06)


### Features

* **workflows:** improve linter ([316097a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/316097a07ed89e40ecbf3dd8a7160eca95bd7a67))


### Bug Fixes

* **auth:** fix infinite auth loop for workflow files ([21cf6cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/21cf6cfa873b841c2f9f8ab9f69c08ea72fc2420))

## [23.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.1.1...v23.1.2) (2020-12-05)


### Bug Fixes

* submission download token generation broke viewing ([e1b6084](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e1b60844cb77b1fd41900d0a3c4829ba21b6b3fe))

## [23.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.1.0...v23.1.1) (2020-12-05)


### Bug Fixes

* remove manually inserted error for testing ([8c17f33](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8c17f3354a6e7768ecf427e4c0a899cbff9c7e0a))

## [23.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.0.3...v23.1.0) (2020-12-04)


### Features

* **admin-workflows:** allow uploading graph spec as file ([48208c9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/48208c9105ba49feb784ea3143b610ed5b11b517))
* **errors:** redirect errors back to ApprootDefault ([fbf21d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fbf21d7313d7c2795c171b85a621ad2235eb68c9))
* **inject-files:** additionally throttle by file count ([3cf0335](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3cf033560e90ddc104e4056c470459f92b6eb4ae))
* **workflows:** edge messages ([c22004e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c22004e1b2f3cd85297faaf41d76954c0625e308))
* **workflows:** make admin or token sufficient for all roles ([7a7cd4d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7a7cd4d07c907611cf72e5ebe3ae41c3a401ef64))
* **workflows:** proper workflow-workflow-tables ([ac08846](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ac08846c267f20fa053e3bd73bea72b224b636c6))
* allow separating user generated content into separate domain ([707b41d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/707b41d4ec9fa92238eaeb4e77f32d8bd8052c46))
* **workflows:** prepare for admin-workflow-instance-edit ([ee6fecb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ee6fecb79e4807beceadd15f19e41393f7707135))


### Bug Fixes

* **admin-workflows:** fix workflow definition descriptions forms ([f9d933b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f9d933bdacaf21618da9dc74e7bd6bea5e369aa7))
* **errors:** better handling of errors from separated approots ([833b674](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/833b674c31ef3d4bf3b9b1af13201f33c98ef82f))
* **tests:** generate sensible WorkflowPayloadLabels ([8a888d3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8a888d3945f0fd0d67ef83bae621744c943b99de))
* **workflows:** properly offer previous payload files ([aa0404a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aa0404a0075acbcd4c6f94984acdbb4d68f08d0a))

## [23.0.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.0.2...v23.0.3) (2020-11-29)


### Bug Fixes

* build ([68b8b45](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/68b8b458b1e02ec992df811cca2cda08a2f77d9a))
* build ([6322fd4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6322fd449bd83edf2e1909b95c6aa4c795d49a54))
* build ([b1641ad](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b1641ad57e036c33d12cc9002f2355231676f9d1))
* build ([43bb0ab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/43bb0abe7218f6d43b52d9a64e62f0dc29b9972e))
* **rooms:** honor roomHidden ([ed5d871](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed5d871182954e2f0a9a5063f61277d925628c40))

## [23.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.0.1...v23.0.2) (2020-11-28)


### Bug Fixes

* **tests:** remove invalid claim of commutativity ([d2f0361](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d2f0361e49114e6dc6c55e64b677b8c842e93bee))
* build ([23a21b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/23a21b905c902bea5fe88abd84da600e757a194e))

## [23.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v23.0.0...v23.0.1) (2020-11-27)


### Bug Fixes

* csp-sandbox downloads ([50cbba1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/50cbba114a850469ed6893e697d0c329c8e894e0))
* non-dev build ([dfea399](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dfea39907cd114e6d8aea29ae3835dd323ca4df8))
* **auth:** authorize exam offices by school ([946a42b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/946a42b7f01016652d03dd214fdf2bc7202ab8ab))
* **csv:** ignore empty lines ([211ff5e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/211ff5eacc83bb47e564dd88e11bc18ae7e0a6af))

## [23.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v22.1.1...v23.0.0) (2020-11-25)


### ⚠ BREAKING CHANGES

* **migration:** ManualMigration
* **workflows:** digests now json encode via base64

Also improve efficiency of marking workflow files as referenced

### Features

* **exams:** optionally close on finish ([4b525ea](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4b525ea8246706d191fce109d4a9d1f5cc4c22d1)), closes [#652](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/652)
* **external-exams:** open defaults wrt. external exam schools ([ef1411e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef1411efdb644e300656403c071cfcdef9caf077)), closes [#651](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/651)
* **migration:** switch from versions to enum ([f2fb7d8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f2fb7d8c267fed96f0dbdb237f4984c8996fbce8))
* **rooms:** different room types & hidden rooms ([319c75a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/319c75a85aa5e5f7e2f2af328d69960e1df3cb80))
* **theses:** additional state explanation ([1e38734](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1e3873485e3da81b25dd0a0eb5aed7b9e0fe42b2))
* **workflows:** add missing instances; correct Int64 workaround ([8b32ede](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8b32edee64509ca5a3d5fc206192d4fa43cc1971))
* **workflows:** additional work on WorkflowWorkflowWorkflow ([5108e14](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5108e1494aa8b2bc8b383a349d1d2a4e0249501f))
* **workflows:** create new workflow definitions ([4d63d30](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4d63d306347ed452822b6bea101cdf4391363ed1))
* **workflows:** definition route stubs & i18n ([e3b5b93](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e3b5b93c71e49203e428382cfabb3d536f290cc4))
* **workflows:** delete definitions ([bda4f81](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bda4f81702d94d81427a4980b217be8cae2b9152))
* **workflows:** further work on WorkflowWorkflowWorkflow ([5b897c7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5b897c7a42067d6a7918dc7bc9640b5c3d8a1367))
* **workflows:** initiate ([fd7c91f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fd7c91f5b8aa2645e0e072115d6a7da58323971a))
* **workflows:** list & edit definitions ([ff370c6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ff370c68c735c492e8e588a8bb8e4055aa8cc0f4))
* **workflows:** node messages ([6a7a892](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6a7a892c74ad7da906a841fbfd031cca59174a8c))
* **workflows:** placeholder handlers ([baea302](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/baea302e48dd6c603eebba7040923f0c23266f40))
* **workflows:** wire up ws-school ([82b3a63](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/82b3a6364c77c64a653e927cd0242d64ffcf9d2a))


### Bug Fixes

* **i18n:** missing workflow translations ([ed4ee13](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed4ee1320bd2eaeb462a1e6b72b0f4ff24e447f3))
* **workflow:** add missing optional ([8608e83](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8608e83ef80497b7ddcc451759a98a07b435fc08))
* **workflow:** fix false instance with atrocious instances ([8812f24](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8812f24d9060314d60c3ae495ea7daccdabff30d))
* **workflow:** fix node and graph FromJSON instances ([263fee1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/263fee19f25a4782bf426347e385eedd1742c8da))
* **workflow:** fix types ([ce1acec](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ce1acec444762d254ccf07b37c280ffb02934669))
* **workflow:** fix types ([4334253](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4334253122f4208fb4dc61f5cbf4436f122b14e2))
* **workflow-types:** fix Int64 workaround; update test defs ([ce9648e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ce9648e47a870e3a051591631c2eb3a26c6b4b3c))
* **workflow-types:** minor import fix ([b19c1b3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b19c1b31b7b079f3abe6aff93e3b5603050c6131))
* **workflows:** add missing import/reexport ([5e92a6e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5e92a6e04aeb9a54ab361f83c7168e57ae5e9c33))
* **workflows:** cleanup ([0a3eaa2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0a3eaa29946ba00ba0c9597d124f4ca5cc25620d))
* **workflows:** integrate in new master ([99f3fca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/99f3fca6d08f098b996931c8c4736eefbc9db77c))
* **workflows:** navigation order ([c5eea64](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c5eea64b270369ef69e1aa368ec87f0a8846e1fd))
* **workflows:** prefer payload label from target state ([2619b08](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2619b08ad1be2921d2cdd568f9419852c374df10))
* tests ([3c322af](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3c322af49e2021b2963fef9fbe303fa70ec77e18))
* **workflows:** refer by id in model ([94f78a0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/94f78a07d9376670122a2adce01cf7180a64d33d))
* **workflows:** ui improvements ([c7f4fa0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c7f4fa0e412d2b920a3819ffed5b79b8aeea2842))

## [22.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v22.1.0...v22.1.1) (2020-11-14)

## [22.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v22.0.0...v22.1.0) (2020-11-10)


### Features

* partial/conditional downloads & video streaming ([5b28303](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5b28303539e28024b43addb413aedc4e5ee0e470))


### Bug Fixes

* translation ([80960f4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/80960f42c578c201f78e226653431e9dd965cfce))
* **personalised-sheet-files:** don't delete files when "keep" ([6008cb0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6008cb040dea268e0a096f6c2fafa87f321d115f))

## [22.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v21.1.1...v22.0.0) (2020-11-06)


### ⚠ BREAKING CHANGES

* **html-field:** StoredMarkup

### Bug Fixes

* **html-field:** introduce stored-markup ([e25e8a2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e25e8a2f4ca65afc29acc8a3884df9acf77d4398))

## [21.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v21.1.0...v21.1.1) (2020-11-06)


### Bug Fixes

* **course:** better explanation for material access ([78c5bc5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/78c5bc5258c9305deafac18b010dc6a41e5ea864))

## [21.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v21.0.3...v21.1.0) (2020-11-05)


### Features

* **sheets:** upload-empty-ok ([ab1940c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ab1940cb09e824fbba03264b5451fa8b17c5c804))

## [21.0.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v21.0.2...v21.0.3) (2020-11-05)


### Bug Fixes

* **mails:** prevent emails being resent to due archiving errors ([8cf39dc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8cf39dcbe68cefcc50691ae8a7194315d18420d6))

## [21.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v21.0.1...v21.0.2) (2020-11-04)


### Bug Fixes

* build ([fa61b46](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fa61b46d308753354623df17241b5312f324321e))

## [21.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v21.0.0...v21.0.1) (2020-11-04)


### Bug Fixes

* **mail:** better separation of sender/from/envelope-from ([0dbf4f8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0dbf4f8bde99431cafeec954dc164a73227154ad))

## [21.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.14.0...v21.0.0) (2020-11-04)


### ⚠ BREAKING CHANGES

* **course:** AccessPredicates now take continuation

### Features

* **course:** warning if re-registration is not possible ([4451cee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4451ceedf7bde0da7f3bb4c0818b79d7c5df1cbd)), closes [#646](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/646)
* **mail:** archive all sent mail & better verp ([1666081](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1666081fea0eec0bf5440a100db0e8cc69be8295))


### Bug Fixes

* **course:** don't delete applications when deregistering ([b666408](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b6664089f75dcb3b2c89dbd2941c064e8aa86404)), closes [#648](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/648)
* **courses:** better defaults for application/registration ([1c2c8fe](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1c2c8fe3d99176e079d0473dd45039b44128c491))

## [20.14.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.13.0...v20.14.0) (2020-11-02)


### Features

* **users:** assimilation ([ef51c6e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef51c6e7c34effa691125e4313876d95feda96af))


### Bug Fixes

* **exam-users:** prevent exam results without registration via csv ([1c6ac4c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1c6ac4cb4a52ac7e69e615e0e3ff96432b173962))
* work around conduit-bug releasing fh to early ([3ff2cf1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3ff2cf1fec1bf582fe1d5e1f6ee08dcc85d6bc00))
* **exams:** error messages for foreign key constraint violations ([ca29a66](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ca29a66330a977a1f28bbdbe9a733aef10371427))

## [20.13.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.12.1...v20.13.0) (2020-10-20)


### Features

* **allocations:** display participant counts to admins ([b79bac7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b79bac777c6d349a626ea4efa6c43141b7f669d0))


### Bug Fixes

* **allocations:** fix allocation-course-accept-substitutes ([b4df980](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b4df98069982752e36e69571f5557a6179b44cff))

## [20.12.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.12.0...v20.12.1) (2020-10-14)


### Bug Fixes

* **auth:** prettier active directory errors in help messages ([b631ed7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b631ed7d0620748fd833c4cda4b421dc147d0906))
* **migration:** don't consider changelog in requiresMigration ([ea95d74](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ea95d74cb5572688531ba0fdeed3983fb70ab236))

## [20.12.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.11.1...v20.12.0) (2020-10-14)


### Features

* **ldap:** expose active directory errors ([51ed7e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/51ed7e0a26a94d2178a4ca10ad7ea36b99076b54))

## [20.11.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.11.0...v20.11.1) (2020-10-14)


### Bug Fixes

* **allocations:** work around yesod weirdness wrt "none" ([4a731ec](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4a731eca4e69b5ee080f229a602e76f5ae165c64))

## [20.11.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.10.0...v20.11.0) (2020-10-13)


### Features

* **allocations:** allocation-course-accept-substitutes ([8abcd65](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8abcd65edf2a1bf5b6de62103af7427fa7ed7db3))
* **authorisation:** cookie-active-auth-tags ([0d372c6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0d372c636a735b4003448ab2518f6354b08ca042))


### Bug Fixes

* **changelog:** try not to crash on unknown changelog items ([850c8d4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/850c8d4dae47489e0dbf0eb46276eaf0002bf123))

## [20.10.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.9.0...v20.10.0) (2020-10-12)


### Features

* **allocations:** ui for adding applicants ([7b7f11e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7b7f11e72853e11717c671d434397c707eff3b7f))

## [20.9.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.8.1...v20.9.0) (2020-10-12)


### Features

* **exams:** auth ExamResults by ExamExamOfficeSchools ([29a3e24](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/29a3e24bcf01cd9c893857eda00dcd249e6cbbe2))
* **exams:** exam staff & additional schools ([94436ee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/94436ee0e1ce2cbf13a66f9ad81883d7286acb9b))

## [20.8.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.8.0...v20.8.1) (2020-10-12)


### Bug Fixes

* **authorization:** have AllocationTime consider ParticipantState ([b69481e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b69481e88fb20890b4ece7a0023dcfdad21604d6))

## [20.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.7.0...v20.8.0) (2020-10-10)


### Features

* **allocations:** csv-export new-assigned ([a4114a7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a4114a79f1bfd968bb9d300f0c39400a8904ee7c))

## [20.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.6.0...v20.7.0) (2020-10-10)


### Features

* **allocations:** include study features in users table ([7f7d2c7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7f7d2c795767fd6fac1fa4a10a304e3e3d2280c3))

## [20.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.5.1...v20.6.0) (2020-10-06)


### Features

* **study-features:** cache study features term relevance ([8f6d54d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8f6d54d0125e01f5c8a90843b54129d6412b79f1))


### Bug Fixes

* **study-features:** also apply caching to table columns ([564c0b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/564c0b975ae65881cb3a168855b36e4b1614a6cb))

## [20.5.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.5.0...v20.5.1) (2020-09-29)


### Bug Fixes

* **exams:** default exam mode to Nothing ([4b459ea](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4b459ea1430a4947364562f1a9881596325696ad))

## [20.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.4.1...v20.5.0) (2020-09-28)


### Features

* **allocations:** notify about new courses ([18921e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/18921e06d1deeb41d705eabacc2d348bac76197f))
* **allocations:** show staff descriptions ([b359468](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b35946859309fbb526043194c8620c5fc0844809))
* **changelog:** implement changelog like faq ([d9d353f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d9d353fcb7652c46a15016b5d2f400162c8271ef))
* **exams:** check exam_discouraged_modes ([f9c50c8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f9c50c80f22770f5376396923b8921eaac3e7216))
* **exams:** exam design & school exam rules ([f7bab3b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f7bab3befc4c42cde430699681f8caf8a959ab39))


### Bug Fixes

* tests ([65e0688](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/65e06882d2491da5e30b1401db6ecc81efcac58b))
* **allocations:** notify for new course upon registration ([9e0b43a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e0b43a60d26a05f6e1b9d4dae2b2f75dd52fff1))
* tests ([ca81f3b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ca81f3b0f2913431cbaf399c33ed30a21979ce69))

## [20.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.4.0...v20.4.1) (2020-09-23)


### Bug Fixes

* **metrics:** larger range for worker_state_duration ([34a5265](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/34a52653d71140bcc664cbe864cad069441b5c6e))

## [20.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.3.2...v20.4.0) (2020-09-23)


### Features

* **files:** monitor missing files ([fb0ae65](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb0ae65ac5928443abc01de9b57c69849d6a6b21))


### Bug Fixes

* **jobs:** better flushing, correct metrics, better etas ([e4416e7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e4416e7f0e2ea2cf9db0e61cf2d20c27260ccaf8))

## [20.3.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.3.1...v20.3.2) (2020-09-22)


### Bug Fixes

* **files:** don't inject serializable ([2ca024b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2ca024b9351df800b57d3235c4a00776cd669952))

## [20.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.3.0...v20.3.1) (2020-09-22)


### Bug Fixes

* **jobs:** improve job worker healthchecks & logging ([2a84edc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2a84edccb4cdfddc2bdc03ebdd2b934fd7f53884))

## [20.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.2.0...v20.3.0) (2020-09-21)


### Features

* **jobs:** move held-up jobs to different workers ([284aae1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/284aae12135ad97b1cf85b45f1176da6930876ee))

## [20.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.1.1...v20.2.0) (2020-09-21)


### Features

* **logging:** additional logging for inject-files ([cbf41b2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbf41b2ea061aa276f455dde1e31464d106cd3d7))
* improve logging/metrics wrt. batch jobs ([d21faf4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d21faf4de0d40a3683ff2a7a3020bc85717f827c))
* **metrics:** measure file i/o ([4801d22](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4801d22cb360dcd936c57494ff2ff02655431409))


### Bug Fixes

* **exam-form:** sort occurrences and parts ([6d47549](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6d475497c0caee49ad34c5c3c6e7b1bf91ca0ba2))

## [20.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.1.0...v20.1.1) (2020-09-18)


### Bug Fixes

* **file-jobs:** improve log messages ([e099e13](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e099e13816d2ca79cbcc6a84fe970052980c0feb))
* **jobs:** delimit resource allocation to within handler ([7038099](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7038099389fcca684a9e1a3f28f76629e0c194bd))
* **metrics:** sort metrics ([e5ae152](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e5ae1521a0577df35abe13b6bcc602f3a38a6f9c))
* migration ([dd23559](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dd235590b47a90d70753458ffc7ab61c771f3d9b))

## [20.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v20.0.0...v20.1.0) (2020-09-17)


### Features

* **sheet:** warn about no submission without not graded ([9373266](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/937326639a02c576f278b79b8ebb441a2652bece)), closes [#342](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/342)


### Bug Fixes

* **eexamlistr:** allow access for users with exam results ([885de44](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/885de4403c0172b3e9c3b59c277628106a7e925b))
* **files:** fix download of non-injected files ([ce54adc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ce54adce6b67f3de95d65d74ff62b36cccdba47e))

## [20.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.3.1...v20.0.0) (2020-09-11)


### ⚠ BREAKING CHANGES

* **files:** files now chunked

### Features

* **files:** avoid initial unnecessary rechunking ([e80f7d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e80f7d7a89e205ce53a70178e0b44d9b0ddf5b97))
* **files:** chunk prune-unreferenced-files finer ([58c2420](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/58c242045887673f69c368668803574d829cc823))
* **files:** chunking ([8f608c1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8f608c19552ef7bd6ce61af92496b3d5f5bf61b1))
* **files:** content dependent chunking ([d624a95](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d624a951c54bda86e04d440eba9901d2a65153b9))


### Bug Fixes

* zip handling & tests ([350ee79](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/350ee79af3c8fcc480970166a559596873beab2a))

## [19.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.3.0...v19.3.1) (2020-09-10)


### Bug Fixes

* **dbtable:** calculate height of header correctly ([5659f2d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5659f2df1e6ea473794075d85f2a43fc1037fce9)), closes [#634](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/634)

## [19.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.2.2...v19.3.0) (2020-08-28)


### Features

* add user-system-function ([abc37ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/abc37aca9c2aa5eafe7eea9333886b43189d5591))
* automatically sync system functions from ldap ([297ff4f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/297ff4f02591339dda7f3270cc9cd332e18febb7))
* course applications study features ([44eeffc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/44eeffcc70a8b4c119e1a88a9ef01c687fe2e10a))
* generated columns tooltip ([2c4080d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2c4080d0e0d7f59829238830a5200116a9d884ec))
* implement system-exam-office ([42aee66](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/42aee66d1f9c189a6a6b13b1970c61e0299630ae))
* log ldap error messages on invalid-credentials ([0b4fade](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0b4fadedd2d7ffbb58598d9844e1c7d97cabc447))
* reduce number of study features for courses ([51a98f0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/51a98f067086bcef3daff601b53d5eb45f4a27f0))
* restore study features in all tables ([363f7ab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/363f7abc192872ebd2a609b8bd89b58032bc9131))
* study feature filtering ([96d0ba8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96d0ba8f7a1c8d8d4e895541b66e36d35392fb25))
* support for ldap primary keys ([bbfd182](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bbfd182ed93d1e602229a2fd1ac1e0fa4c4439ef))
* **study-features:** add study-features-first-observed ([dcb83d9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dcb83d96fc0e52c0c322e50d9467d9a2bed90359))
* **study-features:** further restriction by course ([f7a9bc8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f7a9bc831a3b0ef58fcbf7918be9f5e3b262641e))


### Bug Fixes

* don't set user-last-authentication during ldap sync ([fdaad16](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fdaad16e713e69a7b47f80a690a97d2ff5eb9986))
* missing translations ([dcfdb51](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dcfdb5130d19e737147bfe9065a6ccb5edf49a77))
* order of on in exam office auth ([f44f150](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f44f1507471a9310a9c88738ca5b3d8268afc136))
* tests ([018d26f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/018d26f4a1a1cf411324aeac56ce4d4203670942))
* tests ([5541619](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5541619372f4a4e46ccc403004e869afdfaed7b0))

## [19.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.2.1...v19.2.2) (2020-08-26)


### Bug Fixes

* have exam deregistration always delete stored grades ([24f428b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/24f428b13bb181bec99417b4e69fc538e35acbcf))

## [19.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.2.0...v19.2.1) (2020-08-26)


### Bug Fixes

* improve hidecolumns behaviour ([9a4f30b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a4f30b811fdf8c58ec5c50c185628eb3158931a))

## [19.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.1.5...v19.2.0) (2020-08-24)


### Bug Fixes

* migrate so as not to resend allocation notifications ([132a510](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/132a510))
* **notification-form:** define rules for all notification-triggers ([0261b39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0261b39)), closes [#561](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/561)


### Features

* **allocations:** merge notifications ([9e9e53e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e9e53e))



## [19.1.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.1.4...v19.1.5) (2020-08-19)



## [19.1.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.1.3...v19.1.4) (2020-08-18)



## [19.1.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.1.2...v19.1.3) (2020-08-17)



## [19.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.1.1...v19.1.2) (2020-08-17)



## [19.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.1.0...v19.1.1) (2020-08-17)



## [19.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v19.0.0...v19.1.0) (2020-08-17)


### Bug Fixes

* hlint ([7e14fef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7e14fef))
* hlint ([58c933c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/58c933c))
* hlint ([662943b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/662943b))
* typo ([a1b03e8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a1b03e8))
* **eecorrectr:** encrypt eeid ([5d9ca45](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5d9ca45))
* **eecorrectr:** use default time ([3369155](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3369155))
* **guess-user:** fix ldap-lookup condition and refactor ([ad4ae71](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ad4ae71))
* **hide-columns:** account for undefined element in isTableHider ([ee5a005](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ee5a005))


### Features

* **eecorrectr:** add handlers and navigation ([be2eb3c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/be2eb3c))
* **eecorrectr:** basic handler structure (WIP) ([de02895](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/de02895))
* **eecorrectr:** more appropriate error messages ([3b4c7fe](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3b4c7fe))
* **exam-correct:** add hasMore to no-op reponse ([e941083](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e941083))
* **exam-correct:** display more info ([ef52f02](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef52f02))
* **exam-correct:** limit number of matching users (BE) ([d4d27f8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d4d27f8))
* **guess-user:** add option to limit query ([4154a39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4154a39))
* **guess-user:** replace guessUser and usages ([ca96518](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ca96518))
* **guess-user:** variant of guessUser ([58ae9dd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/58ae9dd))



## [19.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.6.0...v19.0.0) (2020-08-15)


### refactor

* split foundation & llvm ([c68a01d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c68a01d))


### BREAKING CHANGES

* split foundation



## [18.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.5.0...v18.6.0) (2020-08-11)


### Bug Fixes

* **personalised-sheet-files:** more thorough check wrt sub-warnings ([0b0eaff](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0b0eaff))
* hlint ([5ea7816](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5ea7816))
* **course-visibility:** (more) correct visibility check for favourites ([796a806](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/796a806))
* **course-visibility:** account for active auth tags everywhere ([c99433c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c99433c))
* **course-visibility:** allow access for admin-like roles ([7569195](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7569195))
* **course-visibility:** allow deregistration from invisible courses ([29da6e2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/29da6e2))
* **course-visibility:** allow for caching Nothing results of getBy ([f129ce6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f129ce6))
* **course-visibility:** check for mayEdit on course list ([b1d0893](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b1d0893))
* **course-visibility:** correctly count courses on AllocationListR ([7530287](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7530287))
* **course-visibility:** fix favourites ([1ac3c08](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1ac3c08))
* **course-visibility:** rework routes ([7ce60a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7ce60a3))
* **course-visibility:** show icon to lecturers only ([cbb8e72](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbb8e72))
* **course-visibility:** visibility for admin-like users ([43f625b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/43f625b))


### Features

* **course-visibility:** account for visibility in routes ([cb0bf15](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb0bf15))
* **course-visibility:** account for visibility on AllocationListR ([4185742](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4185742))
* **course-visibility:** account for visibility on AShowR ([df7a784](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/df7a784))
* **course-visibility:** account for visibility on TShowR ([0ff07a5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0ff07a5))
* **course-visibility:** add invisible icon to CShowR title ([6c0adde](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6c0adde))
* **course-visibility:** add visibleFrom,visibleTo ([222d566](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/222d566))
* **course-visibility:** allow access for exam correctors ([dfa70ee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dfa70ee))
* **course-visibility:** display icon in course list for lecturers ([17dbccf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/17dbccf))
* **course-visibility:** error on visibleFrom > visibleTo ([9494019](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9494019))
* **course-visibility:** hide invisible courses from favourites + icon ([d86fed7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d86fed7))
* **course-visibility:** more precise description on CShowR ([6fbb2ea](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6fbb2ea))
* **course-visibility:** no invisible courses in course list ([24f1289](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/24f1289))
* **course-visibility:** now as default visibleFrom for new courses ([7bdf8ca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7bdf8ca))
* **course-visibility:** redirect to NewsR after deregister (WIP!) ([183aa8d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/183aa8d))
* **course-visibility:** reorder course form ([7af82bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7af82bc))
* **course-visibility:** rework visibility check for ZA courses ([a16eb1a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a16eb1a))
* **course-visibility:** warn on deregister from invisible course ([16ad72d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/16ad72d))
* **course-visibility:** warn on invisibility during registration ([23aca1c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/23aca1c))
* **personalised-sheet-files:** collated ignore ([1fe63a2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1fe63a2))
* **personalised-sheet-files:** download from CUsersR ([93d0ace](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/93d0ace))
* **personalised-sheet-files:** finish upload functionality ([ed5fb6e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed5fb6e))
* **personalised-sheet-files:** i18n ([f452b2b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f452b2b))
* **personalised-sheet-files:** introduce routes & work on crypto ([9ee44aa](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9ee44aa))
* **personalised-sheet-files:** participant interaction ([db205f6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/db205f6))



## [18.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.4.0...v18.5.0) (2020-08-03)


### Bug Fixes

* **jobs:** queue certain jobs at most once ([1be9716](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1be9716))


### Features

* admin-crontab-r ([460c133](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/460c133))



## [18.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.3.0...v18.4.0) (2020-08-02)


### Bug Fixes

* **migration:** make index migration truly idempotent ([7a17535](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7a17535))
* weird sql casting ([eb9c676](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eb9c676))
* **set-serializable:** logging limit ([60be62b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/60be62b))
* better concurrency behaviour ([a0392dd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a0392dd))
* suppress exceptions relating to expired sessions ([d47d6aa](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d47d6aa))


### Features

* migrate indexes ([dfe68d5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dfe68d5))
* **files:** safer file deletion ([88a9239](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/88a9239))



## [18.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.2.2...v18.3.0) (2020-07-28)


### Bug Fixes

* **campus-auth:** properly handle login failures ([ec42d83](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec42d83))
* correct (switch) sheetHint and sheetSolution mail templates ([d6f0d28](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d6f0d28))


### Features

* **failover:** treat alternatives cyclically ([9213b75](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9213b75))



## [18.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.2.1...v18.2.2) (2020-07-23)


### Bug Fixes

* **file-upload:** size limitation was inverted ([de53c80](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/de53c80))
* **submission:** race condition allowed creating multiple subs ([02fc0d4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/02fc0d4))



## [18.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.2.0...v18.2.1) (2020-07-22)



## [18.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.1.0...v18.2.0) (2020-07-21)


### Bug Fixes

* shutdown behaviour & tests ([19b8b06](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/19b8b06))


### Features

* **metrics:** observe login attempts ([0c7e56f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0c7e56f))



## [18.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v18.0.0...v18.1.0) (2020-07-20)


### Bug Fixes

* tests ([b4b4a96](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b4b4a96))
* tests ([4854d83](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4854d83))
* tests ([96b3ba4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96b3ba4))
* **deletion:** fix usage of deleteR from POST handler ([c87c9c1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c87c9c1))
* **files:** allow clobbering files during form submission ([a60ad1a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a60ad1a))
* **migration:** omit index for old versions of postgres ([cf412a4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf412a4))


### Features

* **csv:** don't limit number of exported rows ([e62d7a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e62d7a3))
* **sheets:** require exam registration ([d770afd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d770afd))



## [18.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.8.0...v18.0.0) (2020-07-17)


### Bug Fixes

* **ldap-failover:** improve concurrency & error handling ([da1bf86](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/da1bf86))


### Features

* **db:** automatic retry of database transactions upon system error ([e7a5162](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e7a5162))
* **files:** buffer uploads to minio ([d9e9179](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d9e9179))
* **files:** further balance file jobs ([1926917](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1926917))
* **files:** move uploads from buffer to database ([9a2cba5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a2cba5))
* **invitations:** anonymous invitations ([1380d9d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1380d9d))


### BREAKING CHANGES

* **db:** transactions need to be retryable, now



## [17.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.7.0...v17.8.0) (2020-07-17)


### Bug Fixes

* tests ([daa1f83](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/daa1f83))
* **specific file submission:** swap labels ([7fadcf5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7fadcf5))


### Features

* **course-comm:** recipient categories for sheets and exams ([2fd060d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2fd060d))



## [17.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.6.5...v17.7.0) (2020-07-13)


### Features

* **corrections:** better highlight corrected files ([46ce477](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/46ce477)), closes [#602](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/602)
* **file-uploads:** maximum file sizes ([9dee134](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9dee134))



## [17.6.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.6.4...v17.6.5) (2020-06-26)


### Bug Fixes

* **check-all:** fix column collection ([9935efe](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9935efe))



## [17.6.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.6.3...v17.6.4) (2020-06-24)



## [17.6.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.6.2...v17.6.3) (2020-06-24)



## [17.6.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.6.1...v17.6.2) (2020-06-24)



## [17.6.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.6.0...v17.6.1) (2020-06-24)


### Bug Fixes

* **frontend:** improve performance of table-related utils ([eff273b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eff273b)), closes [#603](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/603)
* **generic-file-field:** better explain extension restrictions ([342c64a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/342c64a)), closes [#509](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/509)
* **rating-files:** support integral points values ([62dd7b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/62dd7b9)), closes [#604](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/604)
* **ratings:** improve decoding error reporting ([c873150](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c873150))



## [17.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.5.0...v17.6.0) (2020-06-18)


### Bug Fixes

* hlint & build ([036c74e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/036c74e))


### Features

* **corrections:** override rating_done & documentation ([bbbfa94](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bbbfa94)), closes [#525](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/525) [#274](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/274)
* **rating:** pretty-print to new yaml based format ([2bf4846](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2bf4846))
* **ratings:** i18n rating file names ([1195231](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1195231))
* **ratings:** parsing for new format ([af79473](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/af79473))



## [17.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.4.1...v17.5.0) (2020-06-16)


### Bug Fixes

* **i18n:** missing translations ([14b1706](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/14b1706))


### Features

* **system-messages:** manual priority ([cf06f79](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf06f79))
* **terms:** better prediction of term dates ([e5732df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e5732df))
* **terms:** improve term display/editing ([8b7e8e4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8b7e8e4)), closes [#485](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/485)



## [17.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.4.0...v17.4.1) (2020-06-15)



## [17.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.3.0...v17.4.0) (2020-06-14)


### Bug Fixes

* **corrections-overview:** behavioural fixes ([e10cfe9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e10cfe9))
* **xss-sanitize:** use forked version ([fb50d5b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb50d5b))


### Features

* **course-participants:** csv export exercise sheets ([06f47c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/06f47c5))
* **course-participants:** show exercise sheets (first cornice) ([26cc8e4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/26cc8e4))
* **dbtable:** add support for Cornice ([fdeb251](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fdeb251))



## [17.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.2.1...v17.3.0) (2020-05-29)


### Bug Fixes

* **exam-users:** don't crash when participant doesn't have bonus ([0fa910a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0fa910a))


### Features

* **submission:** allow restriction of submittors via token ([0fa8d37](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0fa8d37))



## [17.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.2.0...v17.2.1) (2020-05-28)



## [17.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.1.1...v17.2.0) (2020-05-26)


### Bug Fixes

* **cron:** work around extraneous sheet notifications ([cbe211b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbe211b))
* **submission:** allow non-group-subs when user isn't in sub-group ([9a35c85](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a35c85))


### Features

* **correction:** allow lecturers to set corrector ([f74581c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f74581c)), closes [#414](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/414)



## [17.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.1.0...v17.1.1) (2020-05-26)


### Bug Fixes

* **cron:** time out sheet notifications ([d5a897c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d5a897c))



## [17.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v17.0.0...v17.1.0) (2020-05-25)


### Bug Fixes

* build ([7147bb4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7147bb4))
* **sheet-inactive-notification:** improve wording ([8af6bde](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8af6bde)), closes [#514](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/514)


### Features

* **applicants:** disclose applicant emails & allow communication ([6711173](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6711173))
* **course-communication:** one recipient group per tutorial ([99f23f2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/99f23f2)), closes [#428](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/428)
* **notifications:** sheet-hint & sheet-solution ([f11b215](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f11b215))



## [17.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.5.0...v17.0.0) (2020-05-23)


### Bug Fixes

* **correction-upload:** better error messages wrt rating files ([8bb3bc5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8bb3bc5))
* **i18n:** missing translations & changelog ([76663b0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/76663b0))


### Features

* **dry-run:** implement dry-run ([002775e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/002775e))
* **load:** allow creation of submissions without login (w/ token) ([2e826d3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2e826d3))
* **sheets:** better explain rating-done ([3944ce0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3944ce0))
* **sheets:** pass-always ([b2ebce4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b2ebce4))


### BREAKING CHANGES

* **dry-run:** runDBRead



## [16.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.4.2...v16.5.0) (2020-05-19)


### Bug Fixes

* **i18n:** missing translations ([d0ce45b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d0ce45b))
* broken dom ([02e8825](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/02e8825))
* **serversession-backend-memcached:** don't throw on deleteSession ([bcd3e46](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bcd3e46))
* **submissions:** off-by-one when isLecturer ([01e61f9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/01e61f9))


### Features

* **exams:** show number of registrations to course admins ([ec020c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec020c5))
* **faq:** exam-points ([aebc05d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aebc05d)), closes [#595](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/595)
* **forms:** improve field labeling & error reporting ([3820b45](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3820b45)), closes [#588](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/588)
* **util-registry:** ensure specific start ordering ([baf8b18](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/baf8b18)), closes [#587](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/587)



## [16.4.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.4.1...v16.4.2) (2020-05-13)



## [16.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.4.0...v16.4.1) (2020-05-13)


### Bug Fixes

* **failover:** don't always record as failed ([16643b6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/16643b6))



## [16.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.3.1...v16.4.0) (2020-05-12)


### Bug Fixes

* **html-field:** remove warning about html-input ([d0358b4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d0358b4))
* **i18n:** missing translations ([b6a2412](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b6a2412))
* **sql:** fix transaction behaviour of setSerializable ([e5acdad](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e5acdad)), closes [#535](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/535)
* **sql:** quiet warnings in setSerializable ([859ae5e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/859ae5e))


### Features

* **communication:** send test emails ([d90da85](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d90da85))
* **multi-user-field:** multi-user-invitation-field ([c072b85](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c072b85))
* **submission:** add correction to sub-show-r ([e060080](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e060080))



## [16.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.3.0...v16.3.1) (2020-05-10)


### Bug Fixes

* **generic-file-field:** allow .zip when doUnpack ([46e9908](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/46e9908))



## [16.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.2.2...v16.3.0) (2020-05-10)


### Features

* **admin-test:** download test ([daaeb09](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/daaeb09))



## [16.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.2.1...v16.2.2) (2020-05-08)


### Bug Fixes

* **submission-groups:** wrong sql query for finding buddies ([0679626](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0679626))



## [16.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.2.0...v16.2.1) (2020-05-08)


### Bug Fixes

* **submission-multi-archive:** fix cleanup & improve ([27731ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/27731ac))



## [16.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.1.0...v16.2.0) (2020-05-07)


### Bug Fixes

* **course-register:** swapped warning message ([32c0605](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/32c0605))


### Features

* **multi-user-field:** improve placeholder ([2936eef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2936eef))



## [16.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.0.5...v16.1.0) (2020-05-06)


### Features

* **eeusers:** fix form & finish implementation ([7d3e9a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7d3e9a3))
* **eeusersr:** audit external exam result delete ([baa3fd8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/baa3fd8))
* **eeusersr:** audit external exam result result and occurrence edits ([ed3f761](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed3f761))
* **eeusersr:** audit external exam result result edit ([0d54757](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0d54757))
* **eeusersr:** more on actions, TODO audit ([d4b784a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d4b784a))
* **eeusersr:** stubs for new actions ([4d48730](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4d48730))
* **external-exams:** add actions to EEUsers ([2cf4895](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2cf4895))



## [16.0.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.0.4...v16.0.5) (2020-05-06)


### Bug Fixes

* **migration:** handle deleted courses & users ([35621df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/35621df))



## [16.0.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.0.3...v16.0.4) (2020-05-06)


### Bug Fixes

* **migration:** typos ([e508277](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e508277))



## [16.0.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.0.2...v16.0.3) (2020-05-05)


### Bug Fixes

* **i18n:** s/Typ/Art/ ([0e43851](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0e43851)), closes [#493](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/493)
* **migration:** typo ([fb7c7ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb7c7ef))



## [16.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.0.1...v16.0.2) (2020-05-05)


### Bug Fixes

* **corrections-grade-r:** add get following post ([14f9ab6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/14f9ab6)), closes [#532](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/532)
* **jobs:** reduce likelihood for multiple queueing of notifications ([970ca78](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/970ca78))



## [16.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v16.0.0...v16.0.1) (2020-05-05)


### Bug Fixes

* **exams:** don't show manual bonus as inconsistent ([fb54c84](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb54c84))
* **interactive-fieldset:** fix behaviour for nested fieldsets ([65b429a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/65b429a))



## [16.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.6.1...v16.0.0) (2020-05-05)


### Features

* **async-table:** history api ([c348b7c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c348b7c)), closes [#426](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/426)
* **course-participants:** course-deregister-no-show ([bf64eaf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bf64eaf)), closes [#499](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/499)
* **course-participants:** introduce CourseParticipantState ([d5b65a1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d5b65a1)), closes [#499](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/499) [#371](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/371)
* **generic-file-field:** prevent multiple session files of same name ([98e1141](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/98e1141))
* **http-client:** baseUrl and defaultUrl ([693189f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/693189f))
* **i18n:** missing translations ([153bb1f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/153bb1f))


### BREAKING CHANGES

* **course-participants:** CourseParticipantState



## [15.6.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.6.0...v15.6.1) (2020-04-30)


### Bug Fixes

* **submission-groups:** prevent deleting group before insert ([f87cf7a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f87cf7a))



## [15.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.5.0...v15.6.0) (2020-04-28)


### Bug Fixes

* **health:** ldap check only admins ([f889ec6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f889ec6))
* **i18n:** submissionDownloadAnonymous ([e6af788](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e6af788))
* typo ([52670bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/52670bc))


### Features

* **corrections:** non-anonymous download w/ registered groups ([9032f80](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9032f80))
* **sheets:** submission groups & rework sheet form ([57f1ce9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/57f1ce9))
* **submission-groups:** invite w/ submission-group & audit ([7f10d44](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7f10d44))



## [15.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.4.1...v15.5.0) (2020-04-27)


### Bug Fixes

* **auth:** tutors may see sheet list ([e0c05f3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e0c05f3))
* **campus:** fix corner case with study features ([76098cc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/76098cc))


### Features

* **allocations:** switch to csprng ([3ea7371](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3ea7371))
* **ldap:** failover ([0e68b6c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0e68b6c))
* **news:** timeout sheets after a month ([31aa25a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/31aa25a))



## [15.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.4.0...v15.4.1) (2020-04-26)


### Bug Fixes

* **allocation:** don't restart cloneCount when allocating successors ([e1c6fd4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e1c6fd4))



## [15.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.3.0...v15.4.0) (2020-04-24)


### Bug Fixes

* typo ([c06a472](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c06a472))
* **faqs:** mention mail to set password ([32097d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/32097d1))
* **faqs:** wording ([02d284f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/02d284f))
* **navbar:** restore border to language buttons ([a2e9a9c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a2e9a9c))


### Features

* **faqs:** i18n ([a1a0fa3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a1a0fa3))
* **faqs:** initial ([7b53377](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7b53377))
* **faqs:** more faqs ([18766ed](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/18766ed))
* **faqs:** more links to faq ([10d44d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/10d44d1))
* **help:** attach last error message ([fdd6b1a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fdd6b1a))



## [15.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.2.0...v15.3.0) (2020-04-23)


### Bug Fixes

* **memcached:** navAccess & quick actions cache invalidations ([d05306a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d05306a))
* **system-message:** lastChanged & unhide logic error ([36abb3e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/36abb3e))


### Features

* **robots.txt:** disallow ahrefs ([9afee89](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9afee89))



## [15.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.1.2...v15.2.0) (2020-04-22)


### Bug Fixes

* **health:** more generous healthchecks ([466203d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/466203d))


### Features

* **caching:** aggressively cache nav items ([b9b0909](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b9b0909))
* **memcached:** introduce general purpose memcached ([e8c2dc5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e8c2dc5))



## [15.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.1.1...v15.1.2) (2020-04-19)


### Bug Fixes

* **mass-input:** defaultValue is safe ([03f36ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/03f36ae))



## [15.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.1.0...v15.1.1) (2020-04-17)


### Bug Fixes

* **course-users:** deregistration w/ allocation & w/o reason ([4f237e1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4f237e1))



## [15.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v15.0.0...v15.1.0) (2020-04-17)


### Bug Fixes

* **style:** padding of language buttons ([e704b23](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e704b23))
* **tests:** fix build ([b0f2304](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b0f2304))


### Features

* **course-user:** authorisation checks ([d15792c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d15792c))
* **course-user:** i18n ([da629a8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/da629a8))
* **course-user:** major improvements ([ced6ef2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ced6ef2)), closes [#126](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/126)
* **mass-input:** automatic add before submit ([7540a4f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7540a4f))
* **submissions:** ignore additional filename components ([38f69c3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/38f69c3))
* **submissions:** non-anonymized correction ([fd2c288](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fd2c288)), closes [#524](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/524) [#292](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/292)



## [15.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.6.0...v15.0.0) (2020-04-15)


### Bug Fixes

* **allocations:** better handle participants without applications ([05d37fb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/05d37fb))
* bump changelog & translate ([a75f3eb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a75f3eb))


### Features

* **system-messages:** hiding ([c81bc23](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c81bc23))
* **system-messages:** refactor cookies & improve system messages ([ead6015](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ead6015))


### BREAKING CHANGES

* **system-messages:** names of cookies & configuration changed



## [14.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.5.0...v14.6.0) (2020-04-09)


### Bug Fixes

* fix course duplicate message & name -> title for courses ([d87e8b7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d87e8b7))
* hlint ([908e6de](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/908e6de))


### Features

* admin interface to issue tokens ([738ab7b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/738ab7b))



## [14.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.4.0...v14.5.0) (2020-04-09)


### Features

* **news:** show system messages ([0d39924](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0d39924))
* **tokens:** multiple authorities ([bc47dcf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bc47dcf))



## [14.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.1.1...v14.4.0) (2020-04-07)


### Bug Fixes

* **dbtable:** improve sorting for haskell+sql ([fd8255d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fd8255d))
* **exam-form:** allow finished without start ([fbc3680](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fbc3680))
* **exams:** provide bonus information in return of examBonusGrade ([731231d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/731231d))
* configure sessions to be strictly same-site ([a7e64bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a7e64bc))
* **i18n:** add missing translations ([773c6c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/773c6c5))
* fix .dual-heated.degenerate ([6058692](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6058692))


### Features

* persist bearer tokens in session ([d8040e7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d8040e7))
* **allocations:** compute & accept allocations ([20ef95c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/20ef95c))
* **allocations:** display new allocations in user table ([bb20062](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb20062))
* **allocations:** improve accept ui and logging ([3422fd7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3422fd7))
* **allocations:** improve acceptance display ([cf03277](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf03277))
* **allocations:** improve display ([26f8f39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/26f8f39))
* **applications-list:** add warning regarding features of study ([cdbe12c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cdbe12c))
* **course-events:** add HideColumns for course events ([1138f9e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1138f9e))
* **course-events:** add optional note to course events ([6ad8f2e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6ad8f2e))
* **course-events:** course event note text -> html ([c8904d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c8904d1))
* **course-events:** hide note column if there are no notes to display ([1ac7f4e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1ac7f4e))
* **course-events:** show notes in course events table ([b2c4125](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b2c4125))
* **exams:** convenience for automatic grade calculation ([ec6a8ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec6a8ae))
* **serversessions:** move session storage to dedicated memcached ([9960059](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9960059)), closes [#390](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/390)
* more date & time formats ([936c366](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/936c366))



## [14.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.2.0...v14.3.0) (2020-03-31)


### Bug Fixes

* **exam-form:** allow finished without start ([fbc3680](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fbc3680))


### Features

* **course-events:** add HideColumns for course events ([1138f9e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1138f9e))
* **course-events:** add optional note to course events ([6ad8f2e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6ad8f2e))
* **course-events:** course event note text -> html ([c8904d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c8904d1))
* **course-events:** hide note column if there are no notes to display ([1ac7f4e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1ac7f4e))
* **course-events:** show notes in course events table ([b2c4125](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b2c4125))



## [14.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.1.1...v14.2.0) (2020-03-22)


### Bug Fixes

* **dbtable:** improve sorting for haskell+sql ([fd8255d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fd8255d))
* **exams:** provide bonus information in return of examBonusGrade ([731231d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/731231d))
* configure sessions to be strictly same-site ([a7e64bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a7e64bc))
* **i18n:** add missing translations ([773c6c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/773c6c5))
* fix .dual-heated.degenerate ([6058692](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6058692))


### Features

* **allocations:** compute & accept allocations ([20ef95c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/20ef95c))
* **allocations:** display new allocations in user table ([bb20062](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb20062))
* **allocations:** improve accept ui and logging ([3422fd7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3422fd7))
* **allocations:** improve acceptance display ([cf03277](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf03277))
* **allocations:** improve display ([26f8f39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/26f8f39))
* **applications-list:** add warning regarding features of study ([cdbe12c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cdbe12c))
* **exams:** convenience for automatic grade calculation ([ec6a8ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec6a8ae))
* **serversessions:** move session storage to dedicated memcached ([9960059](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9960059)), closes [#390](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/390)
* more date & time formats ([936c366](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/936c366))



## [14.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.1.0...v14.1.1) (2020-03-06)


### Bug Fixes

* **csv-import:** major usability improvements ([2dc6641](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2dc6641))



## [14.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v14.0.0...v14.1.0) (2020-03-06)


### Bug Fixes

* fix build & minor refactor ([bb9b4f0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb9b4f0))
* **course-users:** add missing dbt sorting ([1bc14c9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1bc14c9))
* **course-users:** insertUnique and only count and audit true inserts ([1325ff2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1325ff2))


### Features

* **corrections:** submission filter ([38dbfe7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/38dbfe7))
* **course-users:** allow for exam registration on CUsersR ([b8acc9b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b8acc9b))
* **course-users:** exams in dbtable and csv ([c23becc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c23becc))
* **course-users:** filter by exam registrations ([1d7d0ab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1d7d0ab))
* **course-users:** match filter titles with column titles ([ecd7bec](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ecd7bec))
* **course-users:** register exam action with optional occurrence ([34ad1df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/34ad1df))
* **csv:** export example data & improve zoned-time parsing ([49d9ab9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/49d9ab9))



## [14.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v13.0.1...v14.0.0) (2020-03-03)


### Bug Fixes

* **allocations:** show assignment green ([9d62b3a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9d62b3a))


### Features

* **allocations:** explanations & introduce grade-ordinal-proportion ([ee2e504](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ee2e504))
* **allocations:** show & export priority ([7462e03](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7462e03))
* **allocations:** table of allocation users ([2735d46](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2735d46))
* **allocations:** tooltips listing courses in users table ([6bca64c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6bca64c))
* **allocations:** upload of priorities ([a590f45](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a590f45))


### BREAKING CHANGES

* **allocations:** influence of grades on allocation priority now
relative when priorities are ordinal



## [13.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v13.0.0...v13.0.1) (2020-02-24)


### Bug Fixes

* fix rendering of weekdays ([94b87a2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/94b87a2))



## [13.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v12.1.0...v13.0.0) (2020-02-23)


### chore

* bump to lts-15.0 ([cfaea9c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cfaea9c))


### Features

* markdown help requests ([06f3ac6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/06f3ac6))
* pandoc-markdown based htmlField ([c5848b2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c5848b2))
* participants intersection ([697c3e1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/697c3e1))
* use pandoc to convert html emails to markdown (plaintext) ([4879bb8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4879bb8))
* **metrics:** monitor job durations ([0da6c49](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0da6c49))
* **metrics:** monitor job executor state ([b74bb53](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b74bb53))


### BREAKING CHANGES

* major version bumps
* markdown based HTML input



## [12.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v12.0.0...v12.1.0) (2020-02-19)


### Bug Fixes

* apply margin-left to both ol und ul ([c0d319e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c0d319e))


### Features

* **metrics:** report on health checks ([bec4023](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bec4023))
* targets on InfoLecturerR ([5ffee38](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5ffee38))



## [12.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v11.1.1...v12.0.0) (2020-02-19)


### Bug Fixes

* i18n ([3dd6e21](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3dd6e21))


### Features

* **exams:** allow mixed ExamGradingMode ([acffe04](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/acffe04))
* **exams:** improve handling of exam results everywhere ([0e49bc1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0e49bc1))


### BREAKING CHANGES

* **exams:** ExamResult now contains ExamResultPassedGrade



## [11.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v11.1.0...v11.1.1) (2020-02-14)


### Bug Fixes

* fix [#571](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/571) ([aefb7e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aefb7e0))
* **course-deregistration:** fix check on exam registration ([0b8c30f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0b8c30f))



## [11.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v11.0.0...v11.1.0) (2020-02-08)


### Bug Fixes

* **exam-correct:** add additional exam result td; table layout ([af32789](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/af32789))
* **exam-correct:** different values for examResult options ([aa794c0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aa794c0))
* **exam-correct:** fix attended values and submit on only exam-result ([df0aaca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/df0aaca))
* **exam-correct:** fix request bodies ([0b186a5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0b186a5))
* **exam-correct:** fix result info and response handling ([cd479e2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cd479e2))
* **exam-correct:** fix usage for non-lecturer ([dd7fe84](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dd7fe84))
* **exam-correct:** reintroduce examResults ([f7136bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f7136bc))
* **exam-correct:** send correct results ([2ca56fb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2ca56fb))


### Features

* **exam-correct:** explanation & length restriction ([1bf19a7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1bf19a7))
* **exam-correct:** hide result grade select ([edacc20](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/edacc20))



## [11.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.6.0...v11.0.0) (2020-02-07)


### Bug Fixes

* merge ([a9636af](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a9636af))
* **exam-correct:** add XSRF token to post header ([2fd996b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2fd996b))
* **exam-correct:** add XSRF token to post header ([2b30461](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2b30461))
* **exam-correct:** also persist local time on non-success ([41a9539](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/41a9539))
* **exam-correct:** also persist local time on non-success ([dcb79d4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dcb79d4))
* **exam-correct:** cut off at maxPoints for now (TODO) ([af8d77c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/af8d77c))
* **exam-correct:** fix addRow rowInfo ([88768eb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/88768eb))
* **storage-manager:** correctly use encryption key in decrypt call ([2667aac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2667aac))
* **storage-manager:** post salt and timestamp only when fetching key ([6340509](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6340509))
* fix webpack config ([50e4212](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/50e4212))
* **exam-correct:** correctly htmlify user on failure ([ef34755](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef34755))
* **exam-correct:** correctly htmlify user on failure ([595f46d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/595f46d))
* **exam-correct:** fix addRow rowInfo ([792da22](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/792da22))
* **exam-correct:** fix attributes in template ([62bf73a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/62bf73a))
* **exam-correct:** fix attributes in template ([000f97c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/000f97c))
* **exam-correct:** fix hlint ([630194c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/630194c))
* **exam-correct:** fix hlint ([c520918](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c520918))
* **exam-correct:** fix returning null if old and new results are equal ([968c6de](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/968c6de))
* **exam-correct:** fix returning null if old and new results are equal ([2e7bca6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2e7bca6))
* **exam-correct:** temporarily disable exam results (WIP) ([533e748](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/533e748))
* **storage-key:** fix types ([a0d067f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a0d067f))
* **storage-key:** fix types ([a23a473](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a23a473))
* **storage-manager:** remove and clear SessionStorage ([e42452e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e42452e))
* **storage-manager:** save salt and timestamp ([0282918](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0282918))
* better translation for "exam office" ([edbdceb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/edbdceb))
* design tweaks ([18ae758](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/18ae758))
* design tweaks ([68eb448](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/68eb448))
* fix hlint ([e60aef4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e60aef4))
* **exam-correct:** id on td instead of select ([1d0be2d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1d0be2d))
* **storage-manager:** remove and clear SessionStorage ([38b0a8e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/38b0a8e))
* **storage-manager:** save salt and timestamp ([8bee033](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8bee033))
* **style:** breadcrumb bar width ([7340fc1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7340fc1))
* do not apply target link height fix on targets in tables ([e7ff384](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e7ff384))
* fix hlint ([9ecffc8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9ecffc8))
* fix merge ([d19cca6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d19cca6))
* fix webpack config ([5393a55](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5393a55))
* typo ([4c58699](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4c58699))
* **storage-manager:** correctly use encryption key in decrypt call ([9e9726e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e9726e))
* **storage-manager:** post salt and timestamp only when fetching key ([301c88f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/301c88f))


### Features

* **correction-interface:** wire up ECorrectR ([d8801a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d8801a3))
* **exam-correct:** add basic interface stub ([623becf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/623becf))
* **exam-correct:** add sortable style and date column ([87bda16](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/87bda16))
* **exam-correct:** display backend error messages ([6fc0262](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6fc0262))
* **exam-correct:** general improvement ([23044b2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/23044b2))
* **exam-correct:** more on frontend name resolving ([905d445](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/905d445))
* **exam-correct:** more stub ([cbe6495](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbe6495))
* **exam-correct:** overwrite request cells from response ([c8edbb3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c8edbb3))
* **exam-correct:** postECorrectR stub ([5f9a176](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5f9a176))
* **exam-correct:** return user lookup result even for failure ([8e41820](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8e41820))
* **exam-correct:** submit on enter ([10de1a7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/10de1a7))
* **exam-correct:** work on delete ([014036e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/014036e))
* **hide-columns:** don't break on dom changes ([c519792](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c519792))
* pageactions for exam correct interface ([0d4dcf8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0d4dcf8))
* refine presentation of exam-correct ([95c1755](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/95c1755))
* **exam-correct:** persist results and more ([a7cc24b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a7cc24b))
* **exam-correct:** request refactor and handling of sent uuids ([f06ca00](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f06ca00))
* **exam-correct:** resend option on ambiguous entries (TODO refactor) ([512f4d9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/512f4d9))
* **exam-correct:** server date handling in frontend and refactor ([77e39be](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/77e39be))
* **exam-correct:** setup basic session storage manager, add util stub ([9cb64f2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9cb64f2))
* **exam-correct:** single runDB in POST handler; more response handling ([4cb62f8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4cb62f8))
* **exam-correct:** status icons (wip) ([3cc6814](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3cc6814))
* **exam-correct:** stub ([90359c8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/90359c8))
* **exam-correct:** upsert exam part results (TODO) ([c0f91bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c0f91bc))
* **exam-correct:** use examId instead as uw-exam-correct value ([2d9a877](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2d9a877))
* **exam-correct:** validate user input stub ([7f04862](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7f04862))
* **pageactions:** finish restoration ([e1cac76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e1cac76))
* **pageactions:** restore pageactions ([4bc48a5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4bc48a5))
* **pageactions:** restore pageactions ([926bd44](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/926bd44))
* **sort-table:** add basic SortTable util stub ([53131e2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/53131e2))
* **storage-key:** add breadcrumb and import ([8cf5d63](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8cf5d63))
* **storage-key:** add StorageKeyR to routes; minor Handler refactor ([2d1d58f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2d1d58f))
* **storage-key:** postStorageKeyR ([059efe5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/059efe5))
* **storage-manager:** add en-/decryption stub (WIP) and restructure ([54d852f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/54d852f))
* improve navigation ([95ffda2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/95ffda2))
* navbar header containers ([1348c91](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1348c91))
* restore & improve navbar contents ([51fc6dc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/51fc6dc))
* **exam-correct:** examResult interface, no styling or functionality ([970076e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/970076e))
* rename "Start" to "Beginn" in error messages ([66bd10e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/66bd10e))
* renamed "Bewertung abgeschlossen ab" to "Ergebnisse sichtbar ab" ([6b610e1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6b610e1))
* **correction-interface:** wire up ECorrectR ([df66c9b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/df66c9b))
* **exam-correct:** accept grades besides exam part results ([be187ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/be187ae))
* **exam-correct:** add basic interface stub ([cb7c9ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb7c9ac))
* **exam-correct:** add sortable style and date column ([9fa4245](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9fa4245))
* **exam-correct:** more on frontend name resolving ([daf9eee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/daf9eee))
* **exam-correct:** more stub ([6727dff](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6727dff))
* **exam-correct:** persist results and more ([53ff629](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/53ff629))
* **exam-correct:** postECorrectR stub ([a525cab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a525cab))
* **exam-correct:** request refactor and handling of sent uuids ([4a36a01](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4a36a01))
* **exam-correct:** resend option on ambiguous entries (TODO refactor) ([e252be2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e252be2))
* **exam-correct:** server date handling in frontend and refactor ([d8a080d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d8a080d))
* **exam-correct:** setup basic session storage manager, add util stub ([9a79156](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a79156))
* **exam-correct:** single runDB in POST handler; more response handling ([6837c44](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6837c44))
* **exam-correct:** status icons (wip) ([eefff9f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eefff9f))
* **exam-correct:** stub ([0467194](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0467194))
* **exam-correct:** upsert exam part results (TODO) ([650598f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/650598f))
* **exam-correct:** use examId instead as uw-exam-correct value ([5d7427a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5d7427a))
* **exam-correct:** validate user input stub ([431d004](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/431d004))
* **sort-table:** add basic SortTable util stub ([11c0bd0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/11c0bd0))
* **storage-key:** add breadcrumb and import ([1580d3f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1580d3f))
* **storage-key:** add StorageKeyR to routes; minor Handler refactor ([4d4dc8f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4d4dc8f))
* **storage-key:** postStorageKeyR ([b51c466](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b51c466))
* **storage-manager:** add en-/decryption stub (WIP) and restructure ([0016145](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0016145))
* **storage-manager:** store encryption info per location ([25a7c34](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/25a7c34))
* **storage-manager:** store encryption info per location ([8122ab1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8122ab1))


### BREAKING CHANGES

* major navigation refactor



## [10.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.5.0...v10.6.0) (2020-01-30)


### Bug Fixes

* date formatting ([0af3b87](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0af3b87))
* **exams:** exam-auto-occurrence introduced spurious MappingSpecial ([a1d5479](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a1d5479))
* exam auto-occurrence by matriculation ([3ef10d9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3ef10d9))
* non-exhaustive patterns ([5bff34e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5bff34e))


### Features

* exam auto-occurrence nudging ([a91fd7f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a91fd7f))
* warnings about multiple terms/schools ([91e1bf9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/91e1bf9))



## [10.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.4.1...v10.5.0) (2020-01-29)


### Bug Fixes

* submission user notification recipients for pseudonym subs ([a7b7bdb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a7b7bdb))
* typo ([ad5494e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ad5494e))


### Features

* **exams:** add warning about multiple automatic distributions ([7fc9fef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7fc9fef))
* **exams:** improve occurrence display ([2b56f26](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2b56f26))
* additional exam functions on show page ([214e895](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/214e895))
* bump changelog ([3bd7520](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3bd7520))
* **exam:** start work on automatic exam-occurrence assignment ([282df86](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/282df86))
* **exam:** working prototype of automatic occurrence assignment ([f89545f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f89545f))
* **exams:** automatic exam occurrence assignment ([e994faf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e994faf))


### Tests

* fix fakeUser ([62e8c89](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/62e8c89))
* fix imports ([1626d6b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1626d6b))



## [10.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.4.0...v10.4.1) (2020-01-17)


### Bug Fixes

* hlint ([4348efc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4348efc))



## [10.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.3.0...v10.4.0) (2020-01-17)


### Bug Fixes

* add missing translations ([d798dc4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d798dc4))
* improve csv import explanation ([729a8e8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/729a8e8))
* restrict guessUser to consistent queries ([bcd5326](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bcd5326))
* tests & hlint ([4e9b618](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4e9b618))
* ui improvements for (external-)exams ([b3ce3dd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b3ce3dd))
* **hide-columns:** bump storage manager minor version ([9053b87](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9053b87))
* **hide-columns:** no hide-columns in tail.datetime ([03bcf56](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/03bcf56))


### Features

* course-participant-lists ([88dd5a9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/88dd5a9))
* external exam csv export ([553c117](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/553c117))
* external exam csv import & ldap lookup during csv import ([1d14b6a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1d14b6a))
* external exams in exam office exams table ([3b739f7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3b739f7))
* notification about externalExamResults to exam-office ([a304840](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a304840))
* **external-exams:** auditing ([2b153c1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2b153c1))
* **external-exams:** create new exams ([94bb391](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/94bb391))
* **external-exams:** display staff & add' schools ([c14d90f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c14d90f))
* **external-exams:** edit existing exams ([1252a5f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1252a5f))
* **external-exams:** list ([fa3521d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fa3521d))
* **external-exams:** plan for student grade access ([b7506a0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7506a0))
* **external-exams:** requisite routes ([f25b21a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f25b21a))
* **hide-columns:** add hider label th attr ([6c05a8f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6c05a8f))
* **hide-columns:** add hider label th attr ([71e90a1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/71e90a1))
* **hide-columns:** add hider labels for material list ([ccafd95](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ccafd95))
* **hide-columns:** add hider labels for tutorial list on course page ([3553df2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3553df2))
* **hide-columns:** add hider labels for tutorial list on course page ([03e4ac1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/03e4ac1))
* **hide-columns:** add more hider labels ([555c4ae](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/555c4ae))
* **hide-columns:** add more hider labels ([eba58d8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/eba58d8))
* **hide-columns:** opt-out on select columns ([b03c10f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b03c10f))



## [10.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.2.0...v10.3.0) (2020-01-12)


### Bug Fixes

* fix app frontend test ([49bafe1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/49bafe1))
* improve exam occurrence ui ([83fa9c9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/83fa9c9))
* improve labeling of button to switch exam occurrence ([727b89b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/727b89b))
* tweak debouncing & canceling ([6b51cc5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6b51cc5))
* **async-table:** bind callback in updateTableFrom call ([cd3e72c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cd3e72c))
* **util-registry:** fix initAll and tests ([2620fb2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2620fb2))
* **util-registry:** start setup instances and not all active instances ([ddf94bf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ddf94bf))


### Features

* support exam registration including room (ExamRoomFifo) ([14bb020](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/14bb020))
* well known files ([068632b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/068632b))
* **async-table:** no submit on locked inputs ([22b3780](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/22b3780))
* **frontend:** split up util registry ([67e472f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67e472f))
* **util-registry:** more debug info for setup util instances ([00584f9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/00584f9))



## [10.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.1.0...v10.2.0) (2020-01-07)


### Bug Fixes

* divide by zero ([674b949](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/674b949))


### Features

* generate & include new favicon ([b78c484](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b78c484))
* **config:** improve configurability of VerpMode ([a7c3fe7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a7c3fe7))



## [10.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.0.1...v10.1.0) (2019-12-23)


### Bug Fixes

* **legal:** move anchor targets to headings ([a5c98e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a5c98e0))


### Features

* **copyright:** add english translation ([dbb0a57](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dbb0a57))
* **data-prot:** extend info on data saved ([2599e86](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2599e86))
* **data-protection:** data protection statement contd ([c3c533f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c3c533f))
* **legal:** fix translations and links ([cdc4053](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cdc4053))
* **legal:** move legal info to one single page ([565c6a4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/565c6a4))
* **tou:** add english translation ([ce8b1a6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ce8b1a6))
* **tou:** first stub of german tou ([74caeca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/74caeca))
* **tou:** implement Terms of Use (tou) route ([932cd5c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/932cd5c))
* **tou:** small fix in english translation ([aced70f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aced70f))
* **tou:** small fixes in german version ([246af70](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/246af70))



## [10.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v10.0.0...v10.0.1) (2019-12-19)


### Bug Fixes

* fix grid blowout on definition lists ([3cb3dcd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3cb3dcd))
* remove link icon on table sorting links ([e7e7d2b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e7e7d2b))



## [10.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v9.0.3...v10.0.0) (2019-12-18)


### Bug Fixes

* **allocation-list:** fix default sorting ([9eff3cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9eff3cf))
* **allocation-list:** fix sorting ([33d9bac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/33d9bac))
* **datepicker:** fixes [#456](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/456) ([613426b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/613426b))
* **hide-columns:** check for content div in isEmptyColumn ([615555e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/615555e))
* **hide-columns:** correctly hide hiders of previously hidden columns ([364991c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/364991c))
* **hide-columns:** fix crash if no row is present ([827cecd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/827cecd))
* **hide-columns:** fix repositioning of table hiders onclick ([9d8ca38](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9d8ca38))
* **hide-columns:** fix vertical positioning of hider and minor refactor ([3fbb4db](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3fbb4db))
* **hide-columns:** improve positioning ([e371412](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e371412))
* **hide-columns:** remove debug text from template ([9e449dd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e449dd))
* **pageaction:** fixes [#463](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/463) ([849c6c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/849c6c4))
* fix hlint ([37f0936](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/37f0936))
* **submission:** allow not modifying submissionUsers ([030fd7a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/030fd7a))


### Features

* **default-layout:** save handler ident to main content ([ba846be](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ba846be))
* **foundation:** move stuff out of Foundation ([e27beba](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e27beba))
* **frontend:** use webpack more extensively ([5d8c2af](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5d8c2af))
* **hide-columns:** better positioning of hiders ([761c6d3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/761c6d3))
* **hide-columns:** correct storage keys ([610d13a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/610d13a))
* **hide-columns:** fadein transformation ([506f94e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/506f94e))
* **hide-columns:** first stub of hide-column util with manual styling ([111821d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/111821d))
* **hide-columns:** get table wrapper ident for storage ident ([d55d3ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d55d3ef))
* **hide-columns:** hide empty columns per default ([d1232ce](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d1232ce))
* **hide-columns:** more (broken) styling; move hider elements in DOM ([e655bc6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e655bc6))
* **hide-columns:** more styling ([4908702](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4908702))
* **hide-columns:** refactor and auto-hide empty columns ([047c0a5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/047c0a5))
* **hide-columns:** set attributes for hide-columns and extra-stuff div ([169a479](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/169a479))
* **hide-columns:** styling stub with repositioning ([a9c17d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a9c17d7))
* **hide-columns:** support colspan & don't persist autohide ([0798d68](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0798d68))
* **storage-manager:** add storage manager library ([1023240](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1023240))
* **storage-manager:** location hierarchy ([80ff4ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/80ff4ac))
* **submission:** edit notifications ([98c0d69](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/98c0d69))
* **submission:** warn about deleting co-submissions ([e87f607](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e87f607))


### BREAKING CHANGES

* **hide-columns:** StorageManager version numbers
* **frontend:** Major frontend refactor



## [9.0.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v9.0.2...v9.0.3) (2019-12-03)


### Bug Fixes

* **submissions:** fix ambiguity with multiple past co-submissions ([6e4f469](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6e4f469))



## [9.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v9.0.1...v9.0.2) (2019-12-02)


### Bug Fixes

* **new-submissions:** always check for existing sub ([c7d23e6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c7d23e6))



## [9.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v9.0.0...v9.0.1) (2019-11-28)


### Bug Fixes

* **study-features:** account for existing StudyFeatures ([b6cada4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b6cada4))



## [9.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v8.0.1...v9.0.0) (2019-11-28)


### refactor

* **sub-study-fields:** reformulate as superStudyField ([b7d6f3c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7d6f3c)), closes [#531](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/531)


### BREAKING CHANGES

* **sub-study-fields:** superStudyField



## [8.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v8.0.0...v8.0.1) (2019-11-27)


### Bug Fixes

* work around regression in esqueleto ([25cf946](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/25cf946))



## [8.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.25.1...v8.0.0) (2019-11-27)


### Bug Fixes

* revert wrong hlint suggestion ([ba2ed97](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ba2ed97))
* **submission-create:** sanity check submittors in form ([3bf37a4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3bf37a4))
* uniworxdb ([e5608d2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e5608d2))
* **submission-create:** ensure number of buddies is acceptable ([ec24a04](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ec24a04))


### Features

* **features-of-study:** record parent & standalone candidates ([2621d36](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2621d36))
* **messages:** rename subs grade ([534c32d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/534c32d))
* **study-features:** complete StudyFeatures admin-interface ([c4c82f5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c4c82f5))


### refactor

* bump esqueleto & redo StudySubTerms ([0e027b1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0e027b1))


### BREAKING CHANGES

* Bumped esqueleto



## [7.25.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.25.0...v7.25.1) (2019-11-22)



## [7.25.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.24.0...v7.25.0) (2019-11-21)


### Features

* usergroups & metrics usergroup ([9204565](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9204565)), closes [#538](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/538)



## [7.24.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.23.2...v7.24.0) (2019-11-21)


### Bug Fixes

* **submissions:** fix users being deleted for other submissions ([2462c68](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2462c68))
* **watchdog:** improve status&watchdog notification ([2d4ccd6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2d4ccd6))
* typos ([97f62b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/97f62b9))
* **cron-exec:** consider lastExec before executing job ([43833db](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/43833db))


### Features

* **metrics:** basic collection & export of metrics ([b8f41ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b8f41ef))



## [7.23.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.23.1...v7.23.2) (2019-11-19)


### Bug Fixes

* **cron:** disallow jobs executing twice within scheduling precision ([bc74c9e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bc74c9e))
* **sheet list:** only show corrections after they are finished ([d4907cd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d4907cd)), closes [#533](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/533)



## [7.23.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.23.0...v7.23.1) (2019-11-19)


### Bug Fixes

* **cron:** consider scheduling precision in all time comparisons ([4ded04b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4ded04b))



## [7.23.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.22.1...v7.23.0) (2019-11-18)


### Bug Fixes

* **datepicker:** close datepickers on focus loss ([3f9ca5e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3f9ca5e))
* **datepicker:** close datepickers on focusout or click outside ([7fa0124](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7fa0124))
* **datepicker:** close on focusout of elements in document only ([ee0edc7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ee0edc7))
* **datepicker:** partial focusout and click fix ([434c0da](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/434c0da))
* **info-lecturer:** translate german headline ([069d15a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/069d15a))


### Features

* log sent notifications for analysis ([c5ef6bb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c5ef6bb)), closes [#535](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/535)



## [7.22.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.22.0...v7.22.1) (2019-11-14)



## [7.22.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.21.5...v7.22.0) (2019-11-14)


### Bug Fixes

* **corrections-grade:** fix inFix ([2c2dd8d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2c2dd8d))


### Features

* **corrections:** added missing titles; small message fixes ([018082e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/018082e))
* **corrections-grade:** additional column for sheetType ([4cb2d4f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4cb2d4f))
* **corrections-grade:** basic filter UI with pseudonyms ([d03fd4b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d03fd4b))
* **corrections-grade:** sorting by sheetType ([702fb1d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/702fb1d))
* **corrections-grade:** working additional filters ([c4eb2c0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c4eb2c0))



## [7.21.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.21.4...v7.21.5) (2019-11-13)



## [7.21.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.21.3...v7.21.4) (2019-11-13)



## [7.21.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.21.2...v7.21.3) (2019-11-13)



## [7.21.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.21.1...v7.21.2) (2019-11-12)



## [7.21.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.21.0...v7.21.1) (2019-11-11)



## [7.21.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.20.0...v7.21.0) (2019-11-08)


### Bug Fixes

* build ([5684213](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5684213))
* **sheets:** integrate corrector interface into SheetEdit ([acfd312](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/acfd312))
* improve explanation of multiUserField invitations ([954bb78](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/954bb78))
* **rating files:** better descriptions & tests ([5f04593](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5f04593))
* **submission:** ignore extension case within zips ([f8442cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f8442cf))


### Features

* better explain behaviour of submittorForm ([b973495](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b973495))
* pruning of unreferenced files ([ff161b2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ff161b2))



## [7.20.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.19.1...v7.20.0) (2019-10-31)


### Bug Fixes

* **datepicker:** fix for empty or browser-filled inputs ([3c24e5f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3c24e5f))
* **fe-i18n-spec:** fix tests ([339fa39](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/339fa39))
* **i18n:** custom language inference ([205d768](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/205d768))
* **i18n:** fix typos ([8af256e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8af256e))
* **i18n:** get started on i18n-breadcrumbs ([268d9e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/268d9e0))
* **i18n:** i18n for all widgets ([3fe278e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3fe278e))
* **i18n:** i18n in various places ([155ed1d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/155ed1d))
* **tests:** explicit post parameter name for dummy login ([2ccd50f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2ccd50f))
* **tests:** i18n changes ([9ba0e27](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9ba0e27))
* fix i18n widget files ([e517a8e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e517a8e))
* typos ([b9c284c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b9c284c))
* **i18n:** prepare translation file for en-eu ([281c98f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/281c98f))
* **i18n:** rename i18nWidgetFiles to proper language code ([33ddbfb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/33ddbfb))
* **mail:** use only RFC822-timezones ([59b8bb9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/59b8bb9))


### Features

* **changelog:** bump ([3d1636f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3d1636f))
* **glossary:** english glossary ([237c586](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/237c586))
* **glossary:** more de-de-formal ([7daa42d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7daa42d))
* **glossary:** most glossary entries in de-de-formal ([ba7c60e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ba7c60e))
* **i18n:** 12h-clock for english locales ([331ba1f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/331ba1f))
* **i18n:** additional en-eu ([83a458d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/83a458d))
* **i18n:** basic language switching ([352bdba](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/352bdba))
* **i18n:** close language select on click anywhere ([97a29ec](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/97a29ec))
* **i18n:** english imprint ([7b3ed79](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7b3ed79))
* **i18n:** english versions of imprint and data-protection ([4ee3ad0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4ee3ad0))
* **i18n:** get started on en-eu ([75677dc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/75677dc))
* **i18n:** missing message translations; small fixes ([aec4b21](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aec4b21))
* **i18n:** more en-eu ([67e40fd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67e40fd))
* **i18n:** more en-eu ([3058737](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3058737))
* **i18n:** more en-eu ([7c8dbc9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7c8dbc9))
* **i18n:** populate frontend datetime locale from backend settings ([498d616](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/498d616))
* more en-eu translations; minor fixes in de-de-formal ([870f1df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/870f1df))
* **i18n:** store language in user account ([f0f9411](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f0f9411))
* **info:** start glossary ([73b0546](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/73b0546))
* **info-lecturer:** add english translation; minor fixes in german ([a4fc555](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a4fc555))



## [7.19.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.19.1...v7.19.2) (2019-10-28)


### Bug Fixes

* **datepicker:** fix for empty or browser-filled inputs ([3c24e5f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3c24e5f))



## [7.19.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.19.0...v7.19.1) (2019-10-25)


### Bug Fixes

* **datepicker:** workaround for new Date(..) inconsistency ([d24ebf8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d24ebf8))



## [7.19.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.18.3...v7.19.0) (2019-10-24)


### Bug Fixes

* **datepicker:** handle output format when reformatting ([09622bd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/09622bd))


### Features

* **csv:** encoding ([81415e1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/81415e1))



## [7.18.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.18.2...v7.18.3) (2019-10-23)


### Bug Fixes

* **submission-form:** fix display of all courseParticipants ([b67819d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b67819d))



## [7.18.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.18.1...v7.18.2) (2019-10-20)



## [7.18.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.18.0...v7.18.1) (2019-10-20)


### Bug Fixes

* **assign-submissions:** avoid division by zero ([640326c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/640326c))



## [7.18.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.14...v7.18.0) (2019-10-17)


### Bug Fixes

* **file-upload-form:** don't check case of file extensions ([6c49c50](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6c49c50))
* **user-deregister:** remove tutorial participation ([cfcb28d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cfcb28d))


### Features

* **course-show:** show "not registered" ([96e1a30](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96e1a30))



## [7.17.14](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.13...v7.17.14) (2019-10-17)


### Bug Fixes

* **course-show:** show display-email for correctors & tutors ([a2e3699](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a2e3699))



## [7.17.13](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.12...v7.17.13) (2019-10-17)


### Bug Fixes

* **favourites:** clear old favourites when changing max number ([92fb6f2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/92fb6f2))



## [7.17.12](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.11...v7.17.12) (2019-10-17)



## [7.17.11](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.10...v7.17.11) (2019-10-16)


### Bug Fixes

* **test:** build ([443b871](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/443b871))



## [7.17.10](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.9...v7.17.10) (2019-10-16)


### Bug Fixes

* **tutorials:** improve creation interface ([bc248d0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bc248d0))



## [7.17.9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.8...v7.17.9) (2019-10-16)


### Bug Fixes

* **rights:** split applicant off participant ([9d709ca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9d709ca))



## [7.17.8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.7...v7.17.8) (2019-10-16)


### Bug Fixes

* **users:** fix broken email fallback ([f4e9f2c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f4e9f2c))



## [7.17.7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.6...v7.17.7) (2019-10-15)


### Bug Fixes

* **users:** fallback email to name ([7bf018c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7bf018c))



## [7.17.6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.5...v7.17.6) (2019-10-15)


### Bug Fixes

* fallback for determining user email ([6a1a256](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6a1a256))



## [7.17.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.4...v7.17.5) (2019-10-15)


### Bug Fixes

* occurence exception end times not shown correctly ([725468b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/725468b))



## [7.17.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.3...v7.17.4) (2019-10-15)


### Bug Fixes

* **allocations:** fix result notifications ([bb6703d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb6703d))



## [7.17.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.2...v7.17.3) (2019-10-14)



## [7.17.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.1...v7.17.2) (2019-10-14)


### Bug Fixes

* **users:** synchronise sex ([25912e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/25912e0))



## [7.17.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.17.0...v7.17.1) (2019-10-14)


### Bug Fixes

* build ([f92e555](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f92e555))
* **implementation:** spaces ([53471d1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/53471d1))



## [7.17.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.16.0...v7.17.0) (2019-10-14)


### Features

* **static pages:** touch ups ([d2c0043](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d2c0043))
* **users:** sex ([c2a8381](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c2a8381))



## [7.16.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.15.0...v7.16.0) (2019-10-14)


### Features

* **tutorials:** delegate control to tutors ([261f3ed](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/261f3ed))



## [7.15.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.14.1...v7.15.0) (2019-10-13)


### Features

* **allocations:** allow additional notifications ([cc20559](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cc20559))



## [7.14.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.14.0...v7.14.1) (2019-10-13)


### Bug Fixes

* typo ([23f4eb3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/23f4eb3))



## [7.14.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.13.0...v7.14.0) (2019-10-10)


### Features

* **course-users:** allow registering tutorial users manually ([d507d9b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d507d9b))
* **course-users:** include tutorial in csv-export ([1d5ddd1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1d5ddd1))



## [7.13.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.12.0...v7.13.0) (2019-10-09)


### Features

* bump changelog ([bc674af](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bc674af))
* **course:** allow csv-export of all features-of-study ([e60f1b2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e60f1b2))



## [7.12.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.11.0...v7.12.0) (2019-10-09)


### Features

* document CourseEvents ([db224cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/db224cf))



## [7.11.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.10.0...v7.11.0) (2019-10-09)


### Bug Fixes

* hlint ([b0b92b4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b0b92b4))
* **async-table:** fix condition for uw-async-table class ([9a87730](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a87730))
* **async-table:** uw-async-table instead of .uw-async-table ([a5d9bfc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a5d9bfc))
* do not add async-table class to empty tables ([b8e2911](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b8e2911))
* **datepicker:** insert datepicker after the form ([b590995](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b590995))
* **datepicker:** manually add scroll offset based on scroll target ([3ecf834](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3ecf834))
* **datepicker:** no manual positioning; update tail.datetime ([3cd71d6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3cd71d6))


### Features

* **changelog:** bump ([a684b90](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a684b90))
* **courses:** course events ([fa7f771](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fa7f771))



## [7.10.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.9.1...v7.10.0) (2019-10-09)


### Bug Fixes

* hlint ([c19f427](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c19f427))
* **tooltips:** add dark variants of theme independent colors ([e5c7aa0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e5c7aa0))


### Features

* **course:** csv export of course participants ([9a28dc8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a28dc8))
* **courses:** add NotificationCourseRegistered ([3750da8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3750da8))
* **info-lecturer:** add expiry time for newFeat ([fa9e6b5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fa9e6b5))
* **info-lecturer:** add inline newU2W icons ([5a49feb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5a49feb))
* **info-lecturer:** add newU2W icons on info page ([9f02ef0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9f02ef0))
* **info-lecturer:** minor adjustments ([64b391a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/64b391a))
* **info-lecturer:** more bullhorns ([4a5e7d9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4a5e7d9))
* **info-lecturer:** remove "news" section ([cb1e3a6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb1e3a6))
* **lecturer-info:** add planned features icon; update info ([a4068b4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a4068b4))
* **lecturer-info:** fix typos, add info (adding tutorial participants) ([5139825](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5139825))
* **lecturer-info:** replaced icons with icon-tooltips; edited text ([2ca7085](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2ca7085))
* **tooltip:** added test warning to admin test page ([885efd3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/885efd3))
* **tooltips:** add auto unzip and multiFileField tooltips ([276dcb6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/276dcb6))
* **tooltips:** add option for inline tooltips ([0b2e931](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0b2e931))
* **tooltips:** replace tooltips ([3b0e1d5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3b0e1d5))
* **tooltips:** tooltips from messages ([f85ab69](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f85ab69))



## [7.9.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.9.0...v7.9.1) (2019-10-07)


### Bug Fixes

* allow deregistering from full courses ([d7e1e67](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d7e1e67))



## [7.9.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.8.5...v7.9.0) (2019-10-05)


### Features

* **allocations:** show more information ([b7c54df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7c54df))



## [7.8.5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.8.4...v7.8.5) (2019-10-05)


### Bug Fixes

* fix form-notification styling ([0226593](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0226593))



## [7.8.4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.8.3...v7.8.4) (2019-10-05)


### Bug Fixes

* **course-user:** handle allocations when deregistering single users ([ef5bb70](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef5bb70))



## [7.8.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.8.2...v7.8.3) (2019-10-05)


### Bug Fixes

* typo ([a6e40f1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a6e40f1))



## [7.8.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.8.1...v7.8.2) (2019-10-04)


### Bug Fixes

* **favourites:** always move current course up ([56d89d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/56d89d7))



## [7.8.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.8.0...v7.8.1) (2019-10-04)


### Bug Fixes

* **allocation:** fix allocation-results notifications ([ed700a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ed700a3))



## [7.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.7.0...v7.8.0) (2019-10-04)


### Bug Fixes

* ordinalPriorities ([d4ab6f6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d4ab6f6))


### Features

* **course:** show direct registration dates ([8f284ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8f284ac))



## [7.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.6.0...v7.7.0) (2019-10-04)


### Features

* **allocations:** fingerprints & ordinal ratings ([60603cb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/60603cb))



## [7.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.5.0...v7.6.0) (2019-10-04)


### Features

* **allocations:** notification about finished allocation ([9323220](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9323220))
* **allocations:** properly save allocation-relevant course-deregs ([7a759b1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7a759b1))
* **favourites:** usability improvements ([fccc2ea](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fccc2ea))



## [7.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.4.2...v7.5.0) (2019-10-03)


### Features

* **allocations:** auxilliaries for allocation-algo ([47bfd8d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/47bfd8d))
* **allocations:** prototype assignment-algorithm ([0fcf48c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0fcf48c))



## [7.4.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.4.1...v7.4.2) (2019-10-01)


### Bug Fixes

* **course-news:** prevent display of edit-functions unless auth'ed ([89cc9ad](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/89cc9ad))



## [7.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.4.0...v7.4.1) (2019-10-01)


### Bug Fixes

* **course-news:** fix permissions ([9e5fde9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9e5fde9))



## [7.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.3.2...v7.4.0) (2019-10-01)


### Features

* **course:** introduce CourseNews ([aa93b75](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aa93b75))



## [7.3.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.3.1...v7.3.2) (2019-10-01)


### Bug Fixes

* **exam-users:** make csv import much more lenient ([2ddb566](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2ddb566))
* **mail:** honor userCsvOptions and userDisplayEmail ([89adf7f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/89adf7f))



## [7.3.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.3.0...v7.3.1) (2019-09-30)


### Bug Fixes

* **course-edit:** edit courses without being school-wide lecturer ([d7d1f27](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d7d1f27)), closes [#464](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/464)



## [7.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.2.2...v7.3.0) (2019-09-30)


### Bug Fixes

* **course-application:** better display of priorities ([64f7715](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/64f7715))


### Features

* **csv:** allow customisation of csv-export-options ([95ceedd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/95ceedd))



## [7.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.2.1...v7.2.2) (2019-09-30)


### Bug Fixes

* **authorisation:** keep showing allocations (ro) to lecturers ([c8e1d51](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c8e1d51))



## [7.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.2.0...v7.2.1) (2019-09-28)


### Bug Fixes

* fix build ([69f4a80](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/69f4a80))
* fix tutorial registration group applying globally ([d2ba173](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d2ba173))



## [7.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.1.2...v7.2.0) (2019-09-27)


### Bug Fixes

* bump changelog ([60a7bb2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/60a7bb2))
* don't treat ExamBonusManual as override ([16abcd2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/16abcd2))


### Features

* **course-applications:** automatic acceptance of direct applicants ([620950d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/620950d))



## [7.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.1.1...v7.1.2) (2019-09-26)


### Bug Fixes

* **exams:** include bonus points in sum for exam participants ([2bc6894](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2bc6894))



## [7.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.1.0...v7.1.1) (2019-09-26)


### Bug Fixes

* fix build ([d13ace4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d13ace4))



## [7.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v7.0.0...v7.1.0) (2019-09-26)


### Bug Fixes

* **datepicker:** select time from preselected date on edit ([d3375bb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d3375bb))
* **jobs:** cleaner shutdown of job-pool-manager ([adc8d46](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/adc8d46))


### Features

* **exams:** re-introduce ExamBonusManual ([54e94a6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/54e94a6))



## [7.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.11.1...v7.0.0) (2019-09-25)


### Bug Fixes

* fix startup on unix-socket ([39f1295](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/39f1295))
* improve async behaviour ([cc7a528](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cc7a528))
* make migration idempotent again ([9778404](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9778404))
* restore behaviour of waiting asynchronously for job-management ([5ebcd89](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5ebcd89))
* **communication:** make communication form more intuitive ([7a2b972](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7a2b972)), closes [#387](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/387)
* fix migration ([d2478a3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d2478a3))
* fix migration & tests ([e05ea8e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e05ea8e))
* migration ([4383eb1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4383eb1))
* syntax ([7afd569](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7afd569))
* **migration:** drop more tables in w.a. for inconsistent 21→22 ([d79dca6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d79dca6))
* typo ([fb1e42d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb1e42d))


### chore

* bump versions ([67e3b38](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67e3b38))


### Features

* **course:** additional crosslinking ([5eaba78](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5eaba78))
* **exam-users:** document part-* family of columns ([fe07a22](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fe07a22))
* **exams:** accept/reset computed results ([72342f1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/72342f1))
* **exams:** automatically compute examResults ([ea5a398](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ea5a398))
* **exams:** better display exam-result-information ([0ebda4d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0ebda4d))
* **exams:** csv-import of ExamPartResults ([29f4e28](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/29f4e28))
* **exams:** implement rounding of exambonus ([e97cd56](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e97cd56))
* **exams:** refine exam form ([014a17a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/014a17a))


### BREAKING CHANGES

* yesod >=1.6
* **exams:** examPartName no longer required
* **exams:** Introduces ExamPartNumbers



## [6.11.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.11.0...v6.11.1) (2019-09-17)


### Bug Fixes

* **changelog:** update changelog ([fa5358a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fa5358a))



## [6.11.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.10.0...v6.11.0) (2019-09-16)


### Bug Fixes

* **course:** add links between users & applications ([edaca1b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/edaca1b))
* **exam-office:** better logic for isSynced ([cb9ff32](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cb9ff32))
* **exams:** make examClosed a button ([530a8c6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/530a8c6))


### Features

* **exam-office:** course/user opt-outs ([484fa1c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/484fa1c))
* **exam-office:** exam-office permissions by courseSchool ([5841a7b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5841a7b))
* **exam-office:** exams list ([651f0bc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/651f0bc))
* **exam-office:** grade export ([72a7f6e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/72a7f6e))
* **exam-office:** notifications ([52e1844](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/52e1844))
* **exam-office:** show exam(Occurrence) end-time ([b638783](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b638783))
* **exam-office:** subscription management for users & fields ([f75cc64](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f75cc64))
* **exam-office:** user invitations ([123970a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/123970a))



## [6.10.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.9.0...v6.10.0) (2019-09-13)


### Features

* **exams:** notifications wrt. registration ([ae27ff0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ae27ff0))



## [6.9.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.8.0...v6.9.0) (2019-09-12)


### Features

* **users:** allow customisation of displayed email address ([2f38278](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2f38278)), closes [#459](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/459)
* **users:** allow customisation of userDisplayName ([a85f317](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a85f317)), closes [#346](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/346)



## [6.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.7.0...v6.8.0) (2019-09-12)


### Bug Fixes

* **allocations:** better explain capped allocation bounds ([a890e34](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a890e34))


### Features

* **allocations:** allow changing course capacity during allocation ([83e1c94](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/83e1c94))
* **allocations:** show bounds on assignments due to allocation ([91b249e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/91b249e))



## [6.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.6.0...v6.7.0) (2019-09-12)


### Bug Fixes

* **datepicker:** increase datepicker z-index in modals ([593a6a7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/593a6a7))
* **datepicker:** quickfix to fix datepicker position in modals ([3f9454a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3f9454a))
* **submission-users:** properly delete old invitations ([91c926b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/91c926b))


### Features

* **admin-users:** allow adding users ([67f1201](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67f1201))
* **health:** timeout all health checks ([33338cd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/33338cd))
* **invitations:** additional explanation for new users ([bb9c34f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb9c34f))



## [6.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.5.0...v6.6.0) (2019-09-09)


### Bug Fixes

* **audit:** add missing submission edit ([537e66e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/537e66e))
* **campus-login:** add i18n for ident placeholder ([692e533](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/692e533)), closes [#417](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/417)
* **course-edit:** improve instructions ([9d53730](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9d53730))
* fix tests ([a671937](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a671937))
* inherit authorization of CAddUserR in more places ([3391904](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3391904))
* typo ([fc5ffb7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fc5ffb7))
* **file-upload:** fix inverted logic for when upload is required ([3868e8f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3868e8f))


### Features

* **course-edit:** warn about long shorthands ([80cb16a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/80cb16a))
* **forms:** allow customisation of user-facing datalist values ([412ce98](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/412ce98))
* **forms:** show studyFeaturesField in studyFeaturesFieldFor ([b7496f9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7496f9)), closes [#451](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/451)



## [6.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.4.0...v6.5.0) (2019-09-05)


### Bug Fixes

* **course-edit:** expand rights of allocation admins ([7f2dd78](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7f2dd78))
* **jobs:** implement job priorities ([e29f042](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e29f042))


### Features

* **allocation-list:** show numbers of avail. and applied-to courses ([a3f236c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a3f236c))



## [6.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.3.0...v6.4.0) (2019-09-05)


### Bug Fixes

* **allocations:** don't show all allocation information to lecturers ([ad6c503](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ad6c503))


### Features

* **changelog:** prettify date formatting ([2b3aef7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2b3aef7))



## [6.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.2.1...v6.3.0) (2019-09-05)


### Bug Fixes

* fix build ([1a66716](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1a66716))


### Features

* **allocations:** notifications ([6d52ed5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6d52ed5))



## [6.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.2.0...v6.2.1) (2019-09-04)


### Bug Fixes

* **course-edit:** show old allocation ([fc53497](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fc53497)), closes [#450](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/450)



## [6.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.1.0...v6.2.0) (2019-09-02)


### Bug Fixes

* **datepicker:** removes idle cancel and submit buttons ([805676f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/805676f))


### Features

* **users:** ldap-synchronise arbitrary subsets of users ([0789536](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0789536))



## [6.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v6.0.0...v6.1.0) (2019-08-30)


### Bug Fixes

* **async-table:** update legacy call to datepicker ([d56e12d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d56e12d))


### Features

* **ldap:** manually trigger ldap sync ([83afb6f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/83afb6f))



## [6.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.5.0...v6.0.0) (2019-08-30)


### Bug Fixes

* **datepicker:** fix selecting date from manual input in internal format ([8bdcc92](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8bdcc92))
* **datepicker:** format time on copy paste as well ([99d9efa](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/99d9efa))


### Features

* **allocations:** additional info and explanation for participants ([38949cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/38949cf))
* **crontab:** cronjob for pruning expired invitations ([a9c5276](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a9c5276))
* **datepicker:** add option to change the position of the datepicker ([85f46ef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/85f46ef))
* **datepicker:** also parse manual input in internal format ([8a3ac72](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8a3ac72))
* **datepicker:** close datepicker on click outside ([88a6b85](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/88a6b85))
* **datepicker:** close datepicker on escape keydown ([0e5707a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0e5707a))
* **datepicker:** currently broken version using tail.datetime instead ([4282554](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4282554))
* **datepicker:** define instance collection singleton ([f5636b8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f5636b8))
* **datepicker:** display datepicker on the right ([cbb7e95](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cbb7e95))
* **datepicker:** do not replace value if input is no valid date ([ecab0ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ecab0ac))
* **datepicker:** format according to input type; position datepicker ([db345ee](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/db345ee))
* **datepicker:** format any dates before submission ([1eccb0e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1eccb0e))
* **datepicker:** format time on submit ([9f8749c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9f8749c))
* **datepicker:** formatting dates for mass-inputs ([b9fd4d7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b9fd4d7))
* **datepicker:** helper functions and updated tail.datetime fork ([2512d69](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2512d69))
* **datepicker:** more sane datetime config ([5a44263](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5a44263))
* **datepicker:** new approach stub for formatting dates in formdata ([9ea7b2e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9ea7b2e))
* **datepicker:** only update datepicker date if date is valid ([d857af3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d857af3))
* **datepicker:** switch to tail.datetime fork to fix time selection ([863971f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/863971f))
* **datepicker:** update dependencies ([427ffbf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/427ffbf))
* **invitations:** save expiresAt to DB ([1c2f2b7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1c2f2b7))
* **ldap:** automatically synchronise user data from ldap ([b39ba8b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b39ba8b))
* **navigate-away-prompt:** prompt on actual value change only ([293ab6d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/293ab6d))
* **schools:** implement cru ([18ae28a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/18ae28a))
* **user-schools:** allow users to override automatic school assoc' ([7d927fd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7d927fd))
* **user-schools:** automatically assign users to schools ([12067de](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/12067de))
* **users:** generalise UserLecturer and UserAdmin to UserFunction ([76f8da5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/76f8da5)), closes [#320](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/320)


### BREAKING CHANGES

* **users:** Remove UserLecturer and UserAdmin



## [5.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.4.0...v5.5.0) (2019-08-27)


### Bug Fixes

* **changelog:** add date ([52a88f8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/52a88f8))
* **course-applications-csv:** record rating time ([c2c6974](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c2c6974))


### Features

* optional ribbon ([c2e13cf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c2e13cf))



## [5.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.3.0...v5.4.0) (2019-08-27)


### Bug Fixes

* **course-edit:** only show allocation error message when relevant ([00a6ca8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/00a6ca8))


### Features

* **allocations:** serve archive of all application files by course ([5e393c5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5e393c5))
* allow editing of course applications outside of allocation ([e816a30](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e816a30))
* **course-applications:** csv transport ([cf0ec1a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf0ec1a))



## [5.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.2.3...v5.3.0) (2019-08-22)


### Bug Fixes

* **allocations:** fix behaviour of "active" dbTable-filter ([b694a09](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b694a09))
* **course list:** show complete registration span ([754d6ca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/754d6ca)), closes [#446](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/446)
* **home:** fix hlint and other minor bugs ([839251e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/839251e))


### Features

* **allocations:** add info page for allocations ([689b85a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/689b85a))
* **allocations:** show table of all allocations ([d621e61](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d621e61))
* **allocations:** show table of course applications ([f5da3be](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f5da3be))
* **home:** allow users to define exam warning time ([d23e222](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d23e222)), closes [#445](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/445)
* **home:** clean up homepage ([a6e2f64](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a6e2f64))



## [5.2.3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.2.2...v5.2.3) (2019-08-22)


### Bug Fixes

* **csv exam import:** ignore unchanged noshow and voided ([a346524](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a346524))



## [5.2.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.2.1...v5.2.2) (2019-08-22)



## [5.2.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.2.0...v5.2.1) (2019-08-21)


### Bug Fixes

* **csv upload exams:** allow ambiguous harmless study fields ([7d2937c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7d2937c))



## [5.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.1.0...v5.2.0) (2019-08-21)


### Bug Fixes

* **csv import:** csv import preview help text adjusted ([b7321df](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b7321df))
* **csv import:** fix spelling and expand help text ([2c57a77](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2c57a77))
* **exam import:** inactive registered features may be selected ([3c4172c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3c4172c))
* **routes:** change ex to sheet ([9d9ead9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9d9ead9))
* **sheet list:** do not show icons for inaccessible items ([0bb9a0f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0bb9a0f)), closes [#421](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/421)


### Features

* **csv import:** add explanation text ([6d0a4c1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6d0a4c1))



## [5.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.0.2...v5.1.0) (2019-08-19)


### Features

* **allocations:** add application form(s) ([ef625cd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ef625cd))
* **allocations:** add registration form ([c5b18fc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c5b18fc))
* **allocations:** implement application interface ([4dcc82a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4dcc82a))
* **allocations:** link allocations from home ([c759364](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c759364))
* **allocations:** set up routes ([c2df01c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c2df01c))



## [5.0.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.0.1...v5.0.2) (2019-08-13)


### Bug Fixes

* **course-deregister:** only delete relevant users exam results ([3997857](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3997857))



## [5.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v5.0.0...v5.0.1) (2019-08-12)



## [5.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.14.0...v5.0.0) (2019-08-12)


### Bug Fixes

* removed duplicated code from merge ([9fb9540](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9fb9540))
* **course-teaser:** don't collapse unless chevron is clicked ([fca99be](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fca99be))
* **course-teaser-css:** class name fixes ([8a92985](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8a92985))


### Features

* **course-registration:** allow independent course application ([a00698e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a00698e))
* **course-teaser:** checkbox field for open registration filter ([e4f150d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e4f150d))
* **course-teaser:** display sorting "pills" for course teasers ([d964e1f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d964e1f))
* **course-teaser:** filter by open registration ([c2c12b9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c2c12b9))
* **course-teaser:** final version of course-teaser for course list ([66b97d6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/66b97d6))
* **course-teaser:** hide lecturer entry if empty ([f7fb3c1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f7fb3c1))
* **course-teaser:** incomplete course teaser for course list ([9a97925](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9a97925))
* **course-teaser:** moved course teaser functionality to util ([c99a3c7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c99a3c7))
* **course-teaser:** no display of chevron without description ([5c88c13](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5c88c13))
* **course-teaser:** no page reload on sorting ([68b8d24](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/68b8d24))
* **course-teaser:** only true lecturers without assistants ([7926f29](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7926f29))
* **course-teaser:** redirecting to course/ ([aa20389](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/aa20389))
* **course-teaser:** reintroduced courseId and course-teaser.julius ([3b6e700](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3b6e700))
* **course-teaser:** show openCourses also to logged in users ([8cca548](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8cca548))
* **course-teaser:** unpolished version of course-teaser for course list ([ea5d54b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ea5d54b))
* **course-teaser:** working link to course pages ([8a49979](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8a49979))
* **course-teaser-css:** removed description label ([a25efb3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a25efb3))
* **course-teaser-filter:** filter for lecturers ([e96e17f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e96e17f))
* **course-teaser-filter:** working filters for semester and institute ([3b419b3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3b419b3))
* **courses:** rework couse registration ([79d4ae2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/79d4ae2))


### BREAKING CHANGES

* **courses:** auditing for course registrations and deregistrations, more
tightly couple exam results, exam registration, and course registration (delete
them together now)



## [4.14.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.13.1...v4.14.0) (2019-08-07)


### Bug Fixes

* **exam:** fix warning message ([60869fd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/60869fd))
* **info:** minor whitespace correction ([0ce4dd1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0ce4dd1))


### Features

* **info:** info seiten überarbeitet ([7459fc3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7459fc3))



## [4.13.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.13.0...v4.13.1) (2019-08-07)


### Bug Fixes

* fix collision with keyword "none" ([203dbd3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/203dbd3))



## [4.13.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.12.1...v4.13.0) (2019-08-06)


### Features

* **course-show:** show allocation name ([3c80235](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3c80235))
* **homepage:** add convenience links to term and school ([83445c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/83445c4))
* **homepage:** add prime action new course to homepage ([2208368](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2208368))



## [4.12.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.12.0...v4.12.1) (2019-08-06)


### Bug Fixes

* **exams:** allow occurrences after exam end ([3d63b35](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3d63b35))



## [4.12.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.11.0...v4.12.0) (2019-08-06)


### Features

* **exams:** improve immediate exam table on home page ([93e718f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/93e718f))



## [4.11.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.10.0...v4.11.0) (2019-08-06)


### Bug Fixes

* **course-edit:** additional permission checks wrt allocations ([fca5caa](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fca5caa))


### Features

* **audit:** automatic transaction log truncation ([248482b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/248482b))
* **audit:** introduce id-based format ([f602b79](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f602b79))
* **audit:** take IP from header ([fb027de](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fb027de))
* **exams:** show occurrenceRule in exam overview ([06673e0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/06673e0))



## [4.10.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.9.0...v4.10.0) (2019-08-05)


### Bug Fixes

* **jobs:** only write CronLastExec after job has executed ([67eda82](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67eda82))


### Features

* **notifications:** add NotificationExamResult ([a7e2921](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a7e2921))



## [4.9.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.8.0...v4.9.0) (2019-08-05)


### Features

* **allocations:** add courses to allocations ([14a9a45](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/14a9a45))
* **allocations:** create model for allocations ([82e3bf9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/82e3bf9))
* **allocations:** prevent course (de)registrations ([94a1208](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/94a1208))
* **allocations:** refine model for allocations ([069eb1e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/069eb1e))
* **csv-import:** automagically determine csv delimiters ([3555322](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3555322))



## [4.8.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.7.0...v4.8.0) (2019-07-31)


### Bug Fixes

* **exam add users:** correctly differentiate and fix messages ([a473599](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a473599))


### Features

* **exams:** better explain "enlist directly" ([f07eb3d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f07eb3d))



## [4.7.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.6.0...v4.7.0) (2019-07-30)


### Features

* **exam users:** course notes ([1e756be](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1e756be))
* **notification triggers:** redesign interface ([84c12b5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/84c12b5)), closes [#410](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/410)
* **users:** lecturer invitations ([e6c3be4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e6c3be4))
* **users:** switching between AuthModes & password changing ([0d610cc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0d610cc))



## [4.6.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.5.0...v4.6.0) (2019-07-26)


### Features

* **exam-users:** allow missing columns in csv import ([e242013](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e242013))



## [4.5.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.4.0...v4.5.0) (2019-07-26)


### Bug Fixes

* fix merge ([38afa90](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/38afa90))
* **csv-import:** fix incorrect map merge ([0d283fd](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0d283fd))
* **dbtable-ui:** fix position of submit button for pagesize ([cf35118](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf35118))
* **merge:** fix build ([0bd0260](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/0bd0260))


### Features

* **alert-icons:** add custom icons for alerts ([bc67500](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bc67500))
* **alerticons:** allow alerts to have custom icons ([d70a958](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d70a958))
* **alerts js:** support custom icons in Alerts HTTP-Header ([8833cb5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8833cb5))
* **corrections assignment:** add convenience to table header ([56c2fcc](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/56c2fcc))
* **course enrolement:** show proper icons in alerts ([b2b3895](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b2b3895))
* **exam-users:** provide better table defaults ([a689d19](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a689d19))
* **exams:** csv-based grade upload ([932145c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/932145c))
* **exams:** show exam results ([b8b308d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b8b308d))
* **users:** store first names and titles ([ceed070](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ceed070))



## [4.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.3.0...v4.4.0) (2019-07-24)


### Bug Fixes

* **exam-csv:** audit registrations/deregistrations ([a278cc5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a278cc5))
* **js:** fix i18n not loading ([a3ee6f6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a3ee6f6))


### Features

* **exams:** implement exam registration invitations ([dd90fd0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dd90fd0))



## [4.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.2.0...v4.3.0) (2019-07-24)


### Features

* **health:** check for active job workers ([d1abe53](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d1abe53))



## [4.2.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.1.2...v4.2.0) (2019-07-23)


### Bug Fixes

* **exam registration:** icons added to exam register message ([ce61528](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ce61528))
* **exams:** change heading to rooms if no occurrence times are shown ([5cb9404](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5cb9404))
* fix build ([caf4092](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/caf4092))


### Features

* **csv:** finish implementing csv import ([e35fed6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e35fed6))
* **csv:** implement csv import ([996bc2a](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/996bc2a))
* **exams:** allow assigning exam participants to occurrences ([e1996ac](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e1996ac))



## [4.1.2](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.1.1...v4.1.2) (2019-07-17)


### Bug Fixes

* **corrections:** properly link corrector emails ([9385595](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9385595))



## [4.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.1.0...v4.1.1) (2019-07-17)


### Bug Fixes

* **aform:** show info about required fields in all aforms ([63f6d01](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/63f6d01)), closes [#418](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/418)
* **submissions:** only notify submittors if rating changes doneness ([4f1162c](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/4f1162c))
* **submissions:** only notify submittors if rating is done ([8e0c379](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/8e0c379))
* **submissions:** submitting produces an success alert now ([bf20d6f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bf20d6f)), closes [#286](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/286)



## [4.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.0.1...v4.1.0) (2019-07-17)


### Features

* **exams:** allow forced deregistration ([1b532c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1b532c4))



## [4.0.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v4.0.0...v4.0.1) (2019-07-16)


### Bug Fixes

* **exams:** fix caculation of maximum exercise points ([a9e74ca](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a9e74ca))



## [4.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v3.0.0...v4.0.0) (2019-07-16)


### Features

* **csv:** add column explanations ([c8dca94](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c8dca94))


### BREAKING CHANGES

* **csv:** CsvColumnsExplained now required



## [3.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v2.1.1...v3.0.0) (2019-07-16)


### Bug Fixes

* **course and exam registration:** distinguish registrations buttons ([ad825b6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ad825b6)), closes [#416](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/416)
* **exam participant download:** fix icon not being shown ([a075b16](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/a075b16))
* **exams:** cleanup exam interface ([05e7b52](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/05e7b52))
* **sheet type info:** give better tooltips and name to sheet types ([9dbef1f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9dbef1f)), closes [#402](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/402)


### Features

* **exams:** csv-export exercise data ([2218103](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2218103))
* **exams:** filter on occurrence ([cf040ce](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cf040ce))
* **exams:** introduce examOccurrenceName ([379a7ed](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/379a7ed))
* **exams:** show exam bonus in webinterface ([2b23600](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2b23600))
* **sheetlist:** sort sheet file types in db by haskell Ord ([643cc41](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/643cc41))


### BREAKING CHANGES

* **exams:** examOccurrenceName
* **exams:** examStart and examPublishOccurrenceAssignments now optional



## [2.1.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v2.1.0...v2.1.1) (2019-07-10)


### Bug Fixes

* **assign correctors:** also show names of unenlisted correctors ([de49a77](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/de49a77))
* **build:** fix build ([49dc413](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/49dc413))



## [2.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v2.0.0...v2.1.0) (2019-07-10)


### Bug Fixes

* **corrector handling:** show correctors by a consistent order ([9c5ed5f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/9c5ed5f))
* **translation:** fix typos in translations; add bug to known bugs ([ac3f7bb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/ac3f7bb))


### Features

* **csv:** introduce csv export ([631bbef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/631bbef))



## [2.0.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v1.4.1...v2.0.0) (2019-07-10)


### Bug Fixes

* **correction:** comment column made wide in online correction form ([d83b1f6](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d83b1f6)), closes [#373](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/373)
* **number-input-fields:** number inputs made HTML5 compatible ([6098215](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6098215)), closes [#412](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/412)
* **ratings:** disallow ratings for graded sheets without point value ([c0b90c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c0b90c4))
* **tooltips:** fixes font-color when used in tableheaders ([f4bb70e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f4bb70e))


### Features

* **exams:** show study features of registered users ([04bea76](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/04bea76))


### BREAKING CHANGES

* **exams:** E.isInfixOf and E.hasInfix



## [1.4.1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v1.4.0...v1.4.1) (2019-07-04)



## [1.4.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v1.3.0...v1.4.0) (2019-07-03)


### Bug Fixes

* **home:** fix build ([551c4cb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/551c4cb))
* **massinput:** properly render massInputList ([7c28448](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7c28448))


### Features

* **exam:** audit exam registrations ([31931e7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/31931e7))
* **exam:** save registration timestamp ([78e4369](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/78e4369))



## [1.3.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v1.1.0...v1.3.0) (2019-07-03)


### Features

* **home:** show immediate exams on home page ([242cff3](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/242cff3))



## [1.1.0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/v1.0.0...v1.1.0) (2019-07-03)


### Bug Fixes

* **displayable:** fixed faulty display of db keys (SchoolId, TermId) ([c7312e8](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c7312e8))


### Features

* **exams:** add extremely rudimentary registration table ([31e6b72](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/31e6b72))



## 1.0.0 (2019-07-03)


### Bug Fixes

* **sheet corrector assigment:** minor bugfix ([749cd2f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/749cd2f))
* async table js util now knows current random css prefix ([cc90faf](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/cc90faf))
* **correction assignment:** correcting lecturer's names are shown now ([16c556b](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/16c556b))
* **corrector assignment:** sheet tabel mixed up columns sorted ([d07f53e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/d07f53e))
* **datepicker:** hide number input spinners in datepicker ([2073130](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2073130))
* **exam grading keys:** Fix spacing ([24aacef](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/24aacef))
* **exams:** Fix registration ([1684da0](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/1684da0))
* **fe:** style notifications acceptably for now ([fc80f08](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/fc80f08))
* **fe-async-table:** Emulate no-js behaviour when handling pagesize ([28dcc8d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/28dcc8d))
* **fe-check-all:** use arrow fn to keep scope in event listeners ([09e681e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/09e681e))
* **fe-deflist:** avoid horizontal scroll on pages with deflist ([16d422d](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/16d422d))
* **Help Widget, Corrector Assignment:** Modal Form closes in place; assign alerts ([89d5364](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/89d5364)), closes [#195](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/195)
* **info-lecturer:** Touch ups ([e1e26ab](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/e1e26ab))
* **many occurrences throughout the project:** Fix typo: occurence -> occurrence everywhere ([96387cb](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/96387cb))
* filter submission by not having corrector ([3bded50](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/3bded50))
* minor heat correction for correction overview ([5546849](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5546849))
* **ratings:** disallow ratings for graded sheets without point value ([463b2b7](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/463b2b7))
* **standard-version:** properly reset staging area before release ([5aa906e](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/5aa906e))


### Features

* **corrector-assignment:** show load/submission percentages ([228cd50](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/228cd50))
* make pagesize changes load async ([6486120](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6486120))
* **development:** add commitlint to ensure proper commit msgs ([dd528c1](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/dd528c1))
* **development:** add standard-version for automatic changelog generation ([c495ef5](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/c495ef5))
* **exams:** CRU (no D) for exams ([67a50c9](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/67a50c9))
* **exams:** exam registration ([99184ff](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/99184ff))
* **exams:** Form validation ([6fb1399](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/6fb1399))
* **fe-heatmap:** add css class heated for heatmap elements ([b09b876](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/b09b876)), closes [#405](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/issues/405)
* **forms:** Introduce more convenient form validation ([f8d0b02](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/f8d0b02))
* **standard-version:** allow adding additional changes to release ([7ed6fe4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/7ed6fe4))
* **standard-version:** complete release workflow ([605e62f](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/605e62f))


### Tests

* Does ist build with everything except for `makeClassy ''Entity`? Probably the functional dependency is to blame?! ([bb552c4](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/bb552c4))
* removing makeCLassyFor maybe build works then? ([2550f74](https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/2550f74))


### BREAKING CHANGES

* **standard-version:** Start of new versioning schema
