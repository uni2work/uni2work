# "Quick Start" Guide

The following description applies to Ubuntu and similar debian based Linux distributions.

## Prerequisites
These are the things you need to do/install before you can get started working on Uni2work.

### Install german locale
You will need to install the german locale at compile time.

Install:

- Edit `/etc/locale.gen` as root and uncomment/add the line `de_DE.UTF-8 UTF-8`
- Save the file and run `sudo locale-gen`

### Clone repository
Clone this repository and navigate into it
```sh
$ git clone https://gitlab.cip.ifi.lmu.de/jost/UniWorX.git && cd UniWorX
```

### `LDAP`
LDAP is needed to handle logins.

Install:
```sh
sudo apt-get install slapd ldap-utils
```

### `PostgreSQL`
PostgreSQL will serve as database for Uni2work.

Install:
```sh
$ sudo apt-get install postgresql
```

Switch to user *postgres* (got created during installation):
```sh
$ sudo -i -u postgres
```

Add new database user *uniworx*:
```sh
$ createuser --interactive
```

You'll get a prompt:

```sh
Enter name of role to add: uniworx
Shall the new role be a superuser? (y/n) y  [user must be superuser to create extensions]
Password: uniworx
...
```

Create database *uniworx*:
```sh
$ psql -c 'create database uniworx owner uniworx'
$ psql -c 'create database uniworx_test owner uniworx'
```

After you added the database switch back to your own user with `Ctrl + D`.

To properly access the database you now need to add a new linux user called *uniworx*. Enter "uniworx" as the password.
```sh
$ sudo adduser uniworx
```

### `Stack`
Stack is a toolbox for "Haskellers" to aid in developing Haskell projects.

Install:
```sh
$ curl -sSL https://get.haskellstack.org/ | sh
```

Setup stack and install dependencies. This needs to be run from inside the directory you cloned this repository to:
```sh
$ stack setup
```

During this step or the next you might get an error that says something about missing C libraries for `ldap` and `lber`. You can install these using
```sh
$ sudo apt-get install libsasl2-dev libldap2-dev
```

If you get an error that says *You need to install postgresql-server-dev-X.Y for building a server-side extension or libpq-dev for building a client-side application.*
Go ahead and install `libpq-dev` with
```sh
$ sudo apt-get install libpq-dev
```

Other packages you might need to install during this process:
```sh
$ sudo apt-get install pkg-config
$ sudo apt-get install libsodium-dev
```

### `Node` & `npm`
Node and Npm are needed to compile the frontend.

Install:
```sh
$ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
$ sudo apt-get install -y nodejs
```

Build the app:
```sh
$ npm run build
```

This might take a few minutes... if not hours... be prepared.

install yesod:
```sh
$ stack install yesod-bin --install-ghc
```

### Add dummy data to the database
After building the app you can prepare the database and add some dummy data:
```sh
$ ./db.sh -f
```

## Run Uni2work
```sh
$ npm run start
```

This will compile both frontend and backend and will start Uni2work in development mode (might take a few minutes the first time). It will keep running and will watch any file changes to automatically re-compile the application if necessary.

If you followed the steps above you should now be able to visit http://localhost:3000 and login as one of the accounts from the Development-Logins dropdown.

## Troubleshooting

Please see the [wiki](https://gitlab.cip.ifi.lmu.de/jost/UniWorX/wikis/home) for more infos.
