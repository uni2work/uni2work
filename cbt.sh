#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later


cbt_tunnels --username $CBT_USERNAME --authkey $CBT_AUTHKEY
