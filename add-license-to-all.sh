#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

for ext in $2; do
  for x in `find $1 -type f -name "*.$ext"`; do
    ./new-file.sh tmp.$ext $x $3
    echo '' >> tmp.$ext
    cat tmp.$ext | cat - $x > temp && mv temp $x
    rm tmp.$ext
  done
done
