// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/* eslint-disable */
module.exports = function(config) {
  config.set({
    //root path location to resolve paths defined in files and exclude
    basePath: '',
    //files/patterns to load in the browser
    files: ['frontend/src/**/*.spec.js'],

    //executes the tests whenever one of watched files changes
    autoWatch: true,
    //if true, Karma will run tests and then exit browser
    singleRun: true,
    //if true, Karma fails on running empty test-suites
    failOnEmptyTestSuite:false,
    //reduce the kind of information passed to the bash
    logLevel: config.LOG_WARN, //config.LOG_DISABLE, config.LOG_ERROR, config.LOG_INFO, config.LOG_DEBUG

    //list of frameworks you want to use, only jasmine is installed automatically
    frameworks: ['jasmine'],
    //list of browsers to launch and capture
    browsers: ['ChromeHeadless'],
    //list of reporters to use
    reporters: ['mocha','kjhtml'],

    client: {
      jasmine:{
        //tells jasmine to run specs in semi random order, false is default
        random: false
      }
    },

    /* karma-webpack config
       pass your webpack configuration for karma
       add `babel-loader` to the webpack configuration to make the ES6+ code readable to the browser */
    webpack: {
      module: {
        rules: [
          {
            test: /\.js$/i,
            exclude:/(node_modules)/,
            loader:'babel-loader',
            options:{
              presets:['@babel/preset-env']
            }
          },
          {
            test: /\.(css|scss|sass)$/i,
            loader:'null-loader',
          }
        ]
      }
    },
    preprocessors: {
      //add webpack as preprocessor to support require() in test-suits .js files
      './frontend/src/**/*.js': ['webpack']
    },
    webpackMiddleware: {
      //turn off webpack bash output when run the tests
      noInfo: true,
      stats: 'errors-only'
    },
    customLaunchers: {
      ChromeHeadless: {
        base: 'Chrome',
        flags: [
          '--headless',
          '--disable-gpu',
          '--no-sandbox',
          // Without a remote debugging port, Google Chrome exits immediately.
          '--remote-debugging-port=9222',
        ],
      }
    },

    /*karma-mocha-reporter config*/
    mochaReporter: {
      output: 'noFailures'  //full, autowatch, minimal
    }
  });
};
