#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later


[[ -n "${FORCE_RELEASE}" ]] && exit 0

set -e

[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -en .stack-work.lock "$0" "$@" || :

move-back() {
    mv -vT .stack-work .stack-work-test
    [[ -d .stack-work-build ]] && mv -vT .stack-work-build .stack-work
}

if [[ -d .stack-work-test ]]; then
    [[ -d .stack-work ]] && mv -vT .stack-work .stack-work-build
    mv -vT .stack-work-test .stack-work
    trap move-back EXIT
fi

extraArgs=()

[[ -n "${TEST_INCREMENTAL}" ]] && extraArgs+=("--ta" "--failure-report=.test-failures" "--ta" "--rerun" "--ta" "--rerun-all-on-success")

stack build --test --coverage --fast --flag uniworx:dev --flag uniworx:library-only "${extraArgs[@]}" ${@}
