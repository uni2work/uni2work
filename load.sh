#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# Options: see /test/Load.hs (Main)

set -e

[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -en .stack-work.lock "$0" "$@" || :

stack build --fast --flag uniworx:-library-only --flag uniworx:dev

stack exec uniworxload -- $@
