#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later


set -e

[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -en .stack-work.lock "$0" "$@" || :

if [[ -n "${1}" ]]; then
    target=".stack-work-${1}"
else
    target=".stack-work"
fi
shift

if [[ ! -d "${target}" ]]; then
    printf "%s does not exist or is no directory\n" "${target}" >&2
    exit 1
fi

if [[ "${target}" != ".stack-work" ]]; then
  if [[ -e .stack-work-clean ]]; then
      printf ".stack-work-clean exists\n" >&2
      exit 1
  fi

  move-back() {
      if [[ -d .stack-work ]]; then
          mv -vT .stack-work "${target}"
      else
          mkdir -v "${target}"
      fi
      [[ -d .stack-work-clean ]] && mv -vT .stack-work-clean .stack-work
  }

  mv -vT .stack-work .stack-work-clean
  mv -vT "${target}" .stack-work
  trap move-back EXIT
fi

(
    set -ex
    stack clean $@
)
