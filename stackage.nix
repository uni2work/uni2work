# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ nixpkgs ? import ./nixpkgs.nix
, snapshot ? "lts-13.21"
}:

let
  stackage = import (fetchTarball {
    url = "https://stackage.serokell.io/zb36jsy3r5h4ydz0pnp00g9vk94dvv03-stackage/default.nix.tar.gz";
    sha256 = "0h6f80gds0ds77y51hhiadh2h2k8njqq8n0gayp729ana9m9agma";
  });

  overlays =
    [ stackage."${snapshot}"
      (self: super: {
          haskell = super.haskell // {
            packages = super.haskell.packages // {
              "${snapshot}" = super.haskell.packages."${snapshot}".override {
                overrides = hself: hsuper: {
                  zip-archive = self.haskell.lib.overrideCabal hsuper.zip-archive (old: {
                    testToolDepends = old.testToolDepends ++ (with self; [ unzip which ]);
                  });
                  # stack = self.haskell.lib.doJailbreak hsuper.stack;
                  happy = self.haskell.lib.dontCheck hsuper.happy;
                  alex = self.haskell.lib.dontCheck hsuper.alex;
                };
              };
            };
          };
        }
      )
    ];

  inherit (nixpkgs { inherit overlays; }) pkgs;
in pkgs.haskell.packages."${snapshot}"
