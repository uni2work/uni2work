# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ ghc, nixpkgs ? import ./nixpkgs.nix }:

let
  # haskellPackages = import ./stackage.nix { inherit nixpkgs; };
  haskellPackages = pkgs.haskellPackages;
  inherit (nixpkgs {}) pkgs;
in pkgs.haskell.lib.buildStackProject {
  inherit ghc;
  inherit (haskellPackages) stack;
  name = "stackenv";
  buildInputs = (with pkgs;
    [ postgresql zlib libsodium gmp llvm_9
    ]) ++ (with haskellPackages;
    [ yesod-bin happy alex
    ]);
}
