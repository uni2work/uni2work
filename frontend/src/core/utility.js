// SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export function isValidUtility(utility) {
  if (!utility) {
    return false;
  }

  if (!utility.selector) {
    return false;
  }

  return true;
};

export function Utility(metadata) {
  if (!metadata.selector) {
    throw new Error('Utility needs to have a selector!');
  }

  return function (target) {
    target.selector = metadata.selector;
    target.isUtility = true;
  };
};
