// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import moment from 'moment';
import 'moment/locale/en-ie.js';
import 'moment/locale/de.js';

  /**
   * I18n
   *
   * This module stores and serves translated strings, according to the users language settings.
   *
   * Translations are stored in /messages/frontend/*.msg.
   *
   * To make additions to any of these files accessible to JavaScrip Utilities
   * you need to add them to the respective *.msg file and to the list of FrontendMessages
   * in /src/Utils/Frontend/I18n.hs.
   *
   */

export class I18n {

  _translations = {};
  _datetimeLocale = undefined;

  add(id, translation) {
    if (!this._translations[id]) {
      this._translations[id] = translation;
    } else {
      throw new Error('I18N Error: Attempting to set translation multiple times for »' + id + '«!');
    }
  }

  addMany(manyTranslations) {
    Object.keys(manyTranslations).forEach((key) => this.add(key, manyTranslations[key]));
  }

  get(id) {
    if (!this._translations[id]) {
      throw new Error('I18N Error: Translation missing for »' + id + '«!');
    }
    return this._translations[id];
  }


  setDatetimeLocale(locale) {
    if (!this._datetimeLocale) {
      moment.locale(locale);
      this._datetimeLocale = locale;
    } else {
      throw new Error('I18N Error: Attempting to set datetime locale multiple times!');
    }
  }

  getDatetimeLocale() {
    if (!this._datetimeLocale) {
      throw new Error('I18N Error: Attempting to access datetime locale when it has not been set!');
    }
    
    return this._datetimeLocale;
  }
}
