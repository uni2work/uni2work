// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { I18n } from './i18n';

describe('I18n', () => {
  let i18n;

  beforeEach(() => {
    i18n = new I18n();
  });

  // helper function
  function expectTranslation(id, value) {
    expect(i18n.get(id)).toMatch(value);
  }

  it('should create', () => {
    expect(i18n).toBeTruthy();
  });

  describe('add()', () => {
    it('should add the translation', () => {
      i18n.add('id1', 'translated-id1');
      expectTranslation('id1', 'translated-id1');
    });
  });

  describe('addMany()', () => {
    it('should add many translations', () => {
      i18n.addMany({
        id1: 'translated-id1',
        id2: 'translated-id2',
        id3: 'translated-id3',
      });
      expectTranslation('id1', 'translated-id1');
      expectTranslation('id2', 'translated-id2');
      expectTranslation('id3', 'translated-id3');
    });
  });

  describe('get()', () => {
    it('should return stored translations', () => {
      i18n.add('id1', 'something');
      expect(i18n.get('id1')).toMatch('something');
    });

    it('should throw error if translation is missing', () => {
      expect(() => {
        i18n.get('id1');
      }).toThrow();
    });
  });
});
