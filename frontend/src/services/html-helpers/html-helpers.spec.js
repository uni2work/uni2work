// SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { HtmlHelpers } from './html-helpers';

describe('HtmlHelpers', () => {
  let htmlHelpers;

  beforeEach(() => {
    htmlHelpers = new HtmlHelpers();
  });

  it('should create', () => {
    expect(htmlHelpers).toBeTruthy();
  });

  describe('parseResponse()', () => {
    let fakeHttpResponse;

    beforeEach(() => {
      fakeHttpResponse = {
        text: () => Promise.resolve('<div id="test-div">Test</div>'),
      };
    });

    it('should return a promise with idPrefix and element', (done) => {
      htmlHelpers.parseResponse(fakeHttpResponse).then(result => {
        expect(result.idPrefix).toBeDefined();
        expect(result.element).toBeDefined();
        expect(result.element.textContent).toMatch('Test');
        done();
      });
    });

    it('should nudge IDs', (done) => {
      htmlHelpers.parseResponse(fakeHttpResponse).then(result => {
        expect(result.idPrefix).toBeDefined();
        expect(result.element).toBeDefined();
        const elementWithOrigId = result.element.querySelector('#test-div');
        expect(elementWithOrigId).toBeFalsy();
        const elementWithNudgedId = result.element.querySelector('#' + result.idPrefix + 'test-div');
        expect(elementWithNudgedId).toBeTruthy();
        done();
      });
    });

    it('should not nudge IDs with option "keepIds"', (done) => {
      const options = { keepIds: true };

      htmlHelpers.parseResponse(fakeHttpResponse, options).then(result => {
        expect(result.idPrefix).toBe('');
        expect(result.element).toBeDefined();
        const elementWithOrigId = result.element.querySelector('#test-div');
        expect(elementWithOrigId).toBeTruthy();
        done();
      });
    });
  });
});
