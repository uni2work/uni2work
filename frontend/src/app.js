// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { HttpClient } from './services/http-client/http-client';
import { HtmlHelpers } from './services/html-helpers/html-helpers';
import { I18n } from './services/i18n/i18n';
import { UtilRegistry } from './services/util-registry/util-registry';
import { isValidUtility } from './core/utility';

import 'css.escape';

import './app.sass';

export class App {
  httpClient = new HttpClient();
  htmlHelpers = new HtmlHelpers();
  i18n = new I18n();
  utilRegistry = new UtilRegistry();

  constructor() {
    this.utilRegistry.setApp(this);

    document.addEventListener('DOMContentLoaded', () => this.utilRegistry.initAll());
  }

  registerUtilities(utils) {
    if (!Array.isArray(utils)) {
      throw new Error('Utils are expected to be passed as array!');
    }

    utils.filter(isValidUtility).forEach((util) => {
      this.utilRegistry.register(util);
    });
  }
}
