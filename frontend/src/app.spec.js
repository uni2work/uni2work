// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/* eslint-env node */

import { App } from './app';
import { Utility } from './core/utility';

@Utility({ selector: 'util1' })
class TestUtil1 { }

@Utility({ selector: 'util2' })
class TestUtil2 { }

const TEST_UTILS = [
  TestUtil1,
  TestUtil2,
];

describe('App', () => {
  beforeEach(() => {
    global.App = new App();
  });

  it('should create', () => {
    expect(global.App).toBeTruthy();
  });

  it('should init all utlites when page is done loading', () => {
    spyOn(global.App.utilRegistry, 'initAll');
    document.dispatchEvent(new Event('DOMContentLoaded'));
    expect(global.App.utilRegistry.initAll).toHaveBeenCalled();
  });

  describe('provides services', () => {
    it('HttpClient as httpClient', () => {
      expect(global.App.httpClient).toBeTruthy();
    });

    it('HtmlHelpers as htmlHelpers', () => {
      expect(global.App.htmlHelpers).toBeTruthy();
    });

    it('I18n as i18n', () => {
      expect(global.App.i18n).toBeTruthy();
    });

    it('UtilRegistry as utilRegistry', () => {
      expect(global.App.utilRegistry).toBeTruthy();
    });
  });

  describe('registerUtilities()', () => {
    it('should register the given utilities', () => {
      spyOn(global.App.utilRegistry, 'register');
      global.App.registerUtilities(TEST_UTILS);
      expect(global.App.utilRegistry.register.calls.count()).toBe(TEST_UTILS.length);
      expect(global.App.utilRegistry.register.calls.argsFor(0)).toEqual([TEST_UTILS[0]]);
      expect(global.App.utilRegistry.register.calls.argsFor(1)).toEqual([TEST_UTILS[1]]);
    });

    it('should throw an error if not passed an array of utilities', () => {
      expect(() => {
        global.App.registerUtilities({});
      }).toThrow();
    });
  });
});
