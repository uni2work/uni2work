// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import 'whatwg-fetch';

import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
window.ResizeObserver = window.ResizeObserver || Polyfill;
