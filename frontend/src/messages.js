// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export class Translations {
    static translations = {
        'checkrangeTooltip' : {
            'de' : 'Shift-Klick, um mehrere Zellen zu markieren.',
            'en' : 'Shift-click to mark multiple cells.',
        },
    };

    static getTranslation(key, language) {
        let json = Translations.translations[key];
        if(language === 'en') {
            return json.en;
        } else {
            return json.de;
        }
    }
};