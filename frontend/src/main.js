// SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { App } from './app';
import { Utils } from './utils/utils';

export const app = new App();
app.registerUtilities(Utils);

// attach the app to window to be able to get a hold of the
// app instance from the shakespearean templates
window.App = app;

// dont know where to put this currently...
// interceptor to throw an error if an http response does not match the expected content-type
// function contentTypeInterceptor(response, options) {
//   if (!options || !options.headers.get('Accept')) {
//     return;
//   }

//   const contentType = response.headers.get('content-type');
//   if (!contentType.match(options.accept)) {
//     throw new Error('Server returned with '' + contentType + '' when '' + options.accept + '' was expected');
//   }
// }

// window.HttpClient.addResponseInterceptor(contentTypeInterceptor);
