// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export class FrontendTooltips {

    static addToolTip(element, text) {
        let tooltipWrap = document.createElement('span');
        tooltipWrap.className = 'tooltip';
  
        let tooltipContent = document.createElement('span');
        tooltipContent.className = 'tooltip__content';
        tooltipContent.appendChild(document.createTextNode(text));
        tooltipWrap.append(tooltipContent);
  
        let tooltipHandle = document.createElement('span');
        tooltipHandle.className = 'tooltip__handle';
        let icon = document.createElement('i');
        icon.classList.add('fas');
        icon.classList.add('fa-question-circle');
        tooltipHandle.append(icon);
        tooltipWrap.append(tooltipHandle);

        element.append(tooltipWrap);
      }
}