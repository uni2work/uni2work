// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later


export const EVENT_TYPE = {
    CLICK : 'click',
    KEYDOWN : 'keydown',
    INVALID : 'invalid',
    CHANGE : 'change',
    MOUSE_OVER : 'mouseover',
    MOUSE_OUT : 'mouseout',
    SUBMIT : 'submit',
    INPUT : 'input',
    FOCUS_OUT : 'focusout',
    BEFOREUNLOAD : 'beforeunload',
    HASH_CHANGE : 'hashchange',
};



export class EventManager {
    _registeredListeners;
    _mutationObservers;


    constructor() {
        this._registeredListeners = [];
        this._mutationObservers = [];
    }

    registerNewListener(eventWrapper) {
        this._debugLog('registerNewListener', eventWrapper);
        let element = eventWrapper.element;
        element.addEventListener(eventWrapper.eventType, eventWrapper.eventHandler, eventWrapper.options);
        this._registeredListeners.push(eventWrapper);
    }

    registerListeners(eventWrappers) {
        eventWrappers.forEach((eventWrapper) => this.registerNewListener(eventWrapper));
    }

    registerNewMutationObserver(callback, domNode, config) {
        let observer = new MutationObserver(callback);
        observer.observe(domNode, config);
        this._mutationObservers.push(observer);
    }

    removeAllEventListenersFromUtil() {
        this._debugLog('removeAllEventListenersFromUtil');
        for (let eventWrapper of this._registeredListeners) {
            let element = eventWrapper.element;
            element.removeEventListener(eventWrapper.eventType, eventWrapper.eventHandler);
        }
        this._registeredListeners = [];        
    }

    removeAllObserversFromUtil() {
        this._mutationObservers.forEach((observer) => observer.disconnect());
        this.mutationObservers = [];
    }

    cleanUp() {
        this.removeAllObserversFromUtil();
        this.removeAllEventListenersFromUtil();
    }


    
    _debugLog() {}
    //_debugLog(fName, ...args) {
    //  console.log(`[DEBUGLOG] EventManager.${fName}`, { args: args, instance: this });
    //}
}

export class EventWrapper {
    _eventType;
    _eventHandler;
    _element;
    _options;

    constructor(_eventType, _eventHandler, _element, _options) {
        if(!_eventType || !_eventHandler || !_element) {
            throw new Error('Not enough arguments!');
        }
        this._eventType = _eventType;
        this._eventHandler = _eventHandler;
        this._element = _element;
        this._options = _options;
    }

    get eventType() {
        return this._eventType;
    }

    get eventHandler() {
        return this._eventHandler;
    }

    get element() {
        return this._element;
    }

    get options() {
        return this._options;
    }
}