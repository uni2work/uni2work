# Async Table Utility
Makes table filters, sorting and pagination behave asynchronously via AJAX calls.

## Attribute: `uw-async-table`

## Example usage:
(any regular table)
