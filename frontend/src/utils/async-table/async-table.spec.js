// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { AsyncTable, ASYNC_TABLE_INITIALIZED_CLASS } from './async-table';

const AppTestMock = {
  httpClient: {
    get: () => {},
  },
  htmlHelpers: {
    parseResponse: () => {},
  },
  utilRegistry: {
    initAll: () => {},
  },
};

describe('AsyncTable', () => {

  let asyncTable;

  beforeEach(() => {
    const element = document.createElement('div');
    const scrollTable = document.createElement('div');
    const table = document.createElement('table');
    table.id = 'ident';
    scrollTable.classList.add('scrolltable');
    scrollTable.appendChild(table);
    element.appendChild(scrollTable);
    asyncTable = new AsyncTable(element, AppTestMock);
  });

  it('should create', () => {
    expect(asyncTable).toBeTruthy();
  });

  it('should throw if element does not contain a .scrolltable', () => {
    const element = document.createElement('div');
    expect(() => {
      new AsyncTable(element, AppTestMock);
    }).toThrow();
  });

  it('should throw if element does not contain a table', () => {
    const element = document.createElement('div');
    expect(() => {
      new AsyncTable(element, AppTestMock);
    }).toThrow();
  });

  it('should throw if called without an element', () => {
    expect(() => {
      new AsyncTable();
    }).toThrow();
  });

  it('should destroy Async Table', () => {
    asyncTable.start();
    asyncTable.destroy();
    expect(asyncTable._eventManager._registeredListeners.length).toBe(0);
    expect(asyncTable._element.classList).not.toContain(ASYNC_TABLE_INITIALIZED_CLASS);
  });
});
