// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { AsyncForm, ASYNC_FORM_INITIALIZED_CLASS } from './async-form';

describe('AsyncForm', () => {

  let asyncForm;

  beforeEach(() => {
    const element = document.createElement('div');
    asyncForm = new AsyncForm(element);
  });

  it('should create', () => {
    expect(asyncForm).toBeTruthy();
  });

  it('should destroy asyncForm', () => {
    asyncForm.destroy();
    expect(asyncForm._eventManager._registeredListeners.length).toBe(0);
    expect(asyncForm._element.classList).not.toContain(ASYNC_FORM_INITIALIZED_CLASS);
  });

  it('should throw if called without an element', () => {
    expect(() => {
      new AsyncForm();
    }).toThrow();
  });
});
