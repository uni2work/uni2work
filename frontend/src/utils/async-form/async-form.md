# Async Form Utility

Prevents form submissions from reloading the page but instead firing an AJAX request.

## Attribute: `uw-async-form`
(works only on `<form>` elements)

## Example usage:
```html
<form uw-async-form method='POST' action='...'>
  ...
```

## Internationalization:
This utility expects the following translations to be available:
- `asyncFormFailure`\
text that gets shown if an async form request fails (e.g. 'Oops. Something went wrong.').
