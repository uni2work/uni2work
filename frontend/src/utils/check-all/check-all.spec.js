// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { CheckAll, CHECK_ALL_INITIALIZED_CLASS } from './check-all';

const MOCK_APP = {
  utilRegistry: {
    initAll: () => {},
  },
};

describe('CheckAll', () => {

  let checkAll;

  beforeEach(() => {
    const element = document.createElement('table');
    checkAll = new CheckAll(element, MOCK_APP);
  });

  it('should create', () => {
    expect(checkAll).toBeTruthy();
  });

  it('should throw if called without an element', () => {
    expect(() => {
      new CheckAll();
    }).toThrow();
  });

  it('should destroy CheckAll', () => {
    checkAll.destroy();
    expect(checkAll._eventManager._registeredListeners.length).toBe(0);
    expect(checkAll._element.classList).not.toEqual(jasmine.arrayContaining([CHECK_ALL_INITIALIZED_CLASS]));
  });

});
