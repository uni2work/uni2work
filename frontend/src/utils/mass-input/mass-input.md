# Mass Input Utility
Allows form shapes to be manipulated asynchronously.

Will asynchronously submit the containing form and replace the contents of the mass input element with the one from the BE response.

The utility will only trigger an AJAX request if the mass input element has an active/focused element whilst the form is being submitted.

## Attribute: `uw-mass-input`

## Example usage:
```html
<form method='POST' action='...'>
  <input type='text'>
  <div uw-mass-input>
    <input type='text'>
    <button type='submit'>
```
