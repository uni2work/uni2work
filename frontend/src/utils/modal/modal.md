# Modal Utility

## Attribute: `uw-modal`

## Params:
- `data-modal-trigger: string`\
Selector for the element that toggles the modal.
If trigger element has 'href' attribute the modal will be dynamically loaded from the referenced page
- `data-modal-closeable: boolean`\
If the param is present the modal will have a close-icon and can also be closed by clicking anywhere on the overlay

## Example usage:
```html
<div uw-modal data-modal-trigger='#trigger' data-modal-closeable>This is the modal content
<div id='trigger'>Click me to open the modal
```
