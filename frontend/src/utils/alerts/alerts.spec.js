// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Alerts, ALERTS_INITIALIZED_CLASS } from './alerts';

const MOCK_APP = {
  httpClient: {
    addResponseInterceptor: () => {},
    removeResponseInterceptor: () => {},
  },
};

describe('Alerts', () => {

  let alerts;

  beforeEach(() => {
    const element = document.createElement('div');
    alerts = new Alerts(element, MOCK_APP);
  });

  it('should create', () => {
    expect(alerts).toBeTruthy();
  });

  it('should destory alerts', () => {
    alerts.destroy();
    expect(alerts._eventManager._registeredListeners.length).toBe(0);
    expect(alerts._element.classList).not.toContain(ALERTS_INITIALIZED_CLASS);
  });

  it('should throw if called without an element', () => {
    expect(() => {
      new Alerts();
    }).toThrow();
  });
});
