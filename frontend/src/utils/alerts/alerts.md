# Alerts

Makes alerts interactive.

## Attribute: `uw-alerts`

## Types of alerts:
- `default`\
  Regular Info Alert
  Disappears automatically after 30 seconds
  Disappears after x seconds if explicitly specified via data-decay='x'
  Can be told not to disappear with data-decay='0'

- `success`\
  Currently no special visual appearance
  Disappears automatically after 30 seconds

- `warning`\
  Will be coloured warning-orange regardless of user's selected theme
  Does not disappear

- `error`\
  Will be coloured error-red regardless of user's selected theme
  Does not disappear

## Example usage:
```html
<div .alerts uw-alerts>
  <div .alerts__toggler>
  <div .alert.alert-info>
    <div .alert__closer>
    <div .alert__icon>
    <div .alert__content>
      This is some information
```
