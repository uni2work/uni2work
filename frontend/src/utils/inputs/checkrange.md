# Checkrange Utility
Is set on the table header of a specific row. Remembers the last checked checkbox. When the users shift-clicks another checkbox in the same row, all checkboxes in between are also checked.

# Attribute: table:not([uw-no-check-all]
(will be setup on all tables which use the util check-all)