# FileInput Utility
Wraps native file input

## Attribute: `uw-file-input`
(element must be an input of type='file')

## Example usage:
```html
<input type='file' uw-file-input>
```

## Internationalization:
This utility expects the following translations to be available:
- `filesSelected`:\
  label of multi-input button after selection\
  *example*: 'Dateien ausgewählt' (will be prepended by number of selected files)
- `selectFile`:\
  label of single-input button before selection\
  *example*: 'Datei auswählen'
- `selectFiles`:\
  label of multi-input button before selection\
  *example*: 'Datei(en) auswählen'

