# Checkbox Utility
Wraps native checkbox

## Attribute: (none)
(element must be an input of type='checkbox')

## Example usage:
```html
<input type='checkbox'>
```
