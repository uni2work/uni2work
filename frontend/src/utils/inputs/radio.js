// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Johannes Eder <ederj@cip.ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Utility } from '../../core/utility';
import './radio.sass';

const RADIO_CLASS = 'radiobox';
const RADIO_INITIALIZED_CLASS = 'radio--initialized';

@Utility({
  selector: 'input[type="radio"]:not([uw-no-radio])',
})
export class Radio {

  _element;
  _wrapperEl;
  _labelEl;

  constructor(element) {
    if (!element) {
      throw new Error('Radio utility cannot be setup without an element!');
    }

    this._element = element;

    if (this._element.closest('.radio-group')) {
      return false;
    }

    if (this._element.classList.contains(RADIO_INITIALIZED_CLASS)) {
      // throw new Error('Radio utility already initialized!');
      return false;
    }

    if (this._element.parentElement.classList.contains(RADIO_CLASS)) {
      // throw new Error('Radio element\'s wrapper already has class '' + RADIO_CLASS + ''!');
      return false;
    }

    const siblingEl = this._element.nextSibling;
    const parentEl = this._element.parentElement;

    this._wrapperEl = document.createElement('div');
    this._wrapperEl.classList.add(RADIO_CLASS);

    this._labelEl = document.createElement('label');
    this._labelEl.setAttribute('for', this._element.id);

    this._wrapperEl.appendChild(this._element);
    this._wrapperEl.appendChild(this._labelEl);

    parentEl.insertBefore(this._wrapperEl, siblingEl);

    this._element.classList.add(RADIO_INITIALIZED_CLASS);
  }

  destroy() {
    this._labelEl.remove();
    this._wrapperEl.remove();
    this._element.classList.remove(RADIO_INITIALIZED_CLASS);
  }
}
