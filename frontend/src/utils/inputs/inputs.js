// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Checkbox } from './checkbox';
import { FileInput } from './file-input';
import { FileMaxSize } from './file-max-size';
import { Password } from './password';
import { CheckRange } from './checkrange';

import './inputs.sass';
import './radio-group.sass';

export const InputUtils = [
  Checkbox,
  FileInput,
  FileMaxSize,
  Password,
  CheckRange,
];
