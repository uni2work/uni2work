// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Alerts } from './alerts/alerts';
import { Asidenav } from './asidenav/asidenav';
import { AsyncForm } from './async-form/async-form';
import { ShowHide } from './show-hide/show-hide';
import { AsyncTable } from './async-table/async-table';
import { CheckAll } from './check-all/check-all';
import { FormUtils } from './form/form';
import { InputUtils } from './inputs/inputs';
import { MassInput } from './mass-input/mass-input';
import { Modal } from './modal/modal';
import { Tooltip } from './tooltips/tooltips';
import { CourseTeaser } from './course-teaser/course-teaser';
import { NavbarUtils } from './navbar/navbar';
import { PageActionsUtils } from './pageactions/pageactions';
import { HideColumns } from './hide-columns/hide-columns';
import { ExamCorrect } from './exam-correct/exam-correct';
import { SortTable } from './sort-table/sort-table';

export const Utils = [
  Alerts,
  Asidenav,
  AsyncForm,
  AsyncTable,
  CheckAll,
  ShowHide,
  ...FormUtils,
  ...InputUtils,
  MassInput,
  Modal,
  ShowHide,
  Tooltip,
  CourseTeaser,
  ...NavbarUtils,
  ...PageActionsUtils,
  HideColumns,
  ExamCorrect,
  SortTable,
];
