// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Asidenav, ASIDENAV_INITIALIZED_CLASS } from './asidenav';

describe('Asidenav', () => {

  let asidenav;

  beforeEach(() => {
    const element = document.createElement('div');
    asidenav = new Asidenav(element);
  });

  it('should create', () => {
    expect(asidenav).toBeTruthy();
  });

  it('should destory asidenav', () => {
    asidenav.destroy();
    expect(asidenav._eventManager._registeredListeners.length).toBe(0);
    expect(asidenav._element.classList).not.toContain(ASIDENAV_INITIALIZED_CLASS);
  });

  it('should throw if called without an element', () => {
    expect(() => {
      new Asidenav();
    }).toThrow();
  });
});
