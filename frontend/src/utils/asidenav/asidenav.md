# Asidenav

Correctly positions hovered asidenav submenus and handles the favorites button on mobile

## Attribute: `uw-asidenav`

## Example usage:
```html
<div uw-asidenav>
  <div .asidenav>
    <div .asidenav__box>
      <ul .asidenav__list.list--iconless>
        <li .asidenav__list-item>
          <a .asidenav__link-wrapper href='#'>
            <div .asidenav__link-shorthand>EIP
            <div .asidenav__link-label>Einführung in die Programmierung
          <div .asidenav__nested-list-wrapper>
            <ul .asidenav__nested-list.list--iconless>
              Übungsblätter
              ...
```
