# ShowHide

Allows to toggle the visibilty of an element by clicking another element.

## Attribute: `uw-show-hide`

## Params:
- `data-show-hide-id: string` (optional)\
If this param is given the state of the utility will be persisted in the clients local storage.
- `data-show-hide-collapsed: boolean` (optional)\
If this param is present the ShowHide utility will be collapsed. This value will be overruled by any value stored in the LocalStorage.
- `data-show-hide-align: 'right'` (optional)\
Where to put the arrow that marks the element as a ShowHide toggle. Left of toggle by default.

## Example usage:
```html
<div>
  <div uw-show-hide>Click me
  <div>This will be toggled
  <div>This will be toggled as well
```
