# CourseTeaser

Handles expanding and collapsing single course teaser widgets on click.

## Example usage
```html
<div uw-course-teaser>
```