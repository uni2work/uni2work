// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Johannes Eder <ederj@cip.ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Utility } from '../../core/utility';
import { EventManager, EventWrapper, EVENT_TYPE } from '../../lib/event-manager/event-manager';
import './pageactions.sass';
import throttle from 'lodash.throttle';

export const PAGEACTION_SECONDARY_UTIL_SELECTOR = '.pagenav__list-item';
const PAGEACTION_SECONDARY_INITIALIZED_CLASS = '.pagenav-list-item--initialized';
const PAGEACTION_SECONDARY_CLASS = 'pagenav-secondary';

@Utility({
  selector: PAGEACTION_SECONDARY_UTIL_SELECTOR,
})
export class PageActionSecondaryUtil {
  _element;
  navIdent;
  idPrefix;
  radioButton;
  closeButton;
  container;
  wasOpen;
  _closer;

  _throttleUpdateWasOpen;

  _eventManager;

  constructor(element) {
    if (!element) {
      throw new Error('Pageaction Secondary utility needs to be passed an element!');
    }

    if (element.classList.contains(PAGEACTION_SECONDARY_INITIALIZED_CLASS)) {
      return false;
    }

    this._element = element;

    this._eventManager = new EventManager();

    const childContainer = this._element.querySelector('.pagenav-item__children');

    if (!childContainer) {
      return false;
    }

    if (this._element.classList.contains(PAGEACTION_SECONDARY_CLASS)) {
      this.navIdent = 'secondary';
    } else {
      const links = Array.from(this._element.querySelectorAll('.pagenav-item__link')).filter(l => !childContainer.contains(l));

      if (!links || Array.from(links).length !== 1) {
        throw new Error('Pageaction Secondary utility could not find associated link!');
      }
      this.navIdent = links[0].id;
    }
    
    const idMatch = this.navIdent.match(/^(r[0-9]+__)(.+)$/);
    if (idMatch) {
      this.idPrefix = idMatch[1];
      this.navIdent = idMatch[2];
    } else {
      this.idPrefix = '';
    }
    
    this.radioButton = document.getElementById(`${this.idPrefix}pageaction-item__expand-${this.navIdent}`);
    if (!this.radioButton) {
      throw new Error('Pageaction Secondary utility could not find associated radio button!');
    }

    this.closeButton = document.getElementById(`${this.idPrefix}pageaction-item__expand-none`);
    if (!this.closeButton) {
      throw new Error('Pageaction Secondary utility could not find radio button for closing!');
    }

    this.container = document.querySelector('.pagenav-item__children-wrapper');
    if (!this.container) {
      throw new Error('Pageaction Secondary utility could not find associated container!');
    }
   
    this._closer = this._element.querySelector('.pagenav-item__close-label');
    if (this._closer) {
      this._closer.classList.add('pagenav-item__close-label--hidden');
    }

    this.updateWasOpen();
    this.throttleUpdateWasOpen = throttle(this.updateWasOpen.bind(this), 100, { leading: false, trailing: true });
    
    this._element.classList.add(PAGEACTION_SECONDARY_INITIALIZED_CLASS);
  }

  start() {
    if (!this.container)
      return;
    const windowClickEv = new EventWrapper(EVENT_TYPE.CLICK, ((event) => this.clickHandler(event)).bind(this), window);
    const radioButtonChangeEv = new EventWrapper(EVENT_TYPE.CHANGE,  this.throttleUpdateWasOpen.bind(this), this.radioButton);
    this._eventManager.registerListeners([windowClickEv, radioButtonChangeEv]);
  }

  clickHandler(event) {
    if (!this.container.contains(event.target) && window.document.contains(event.target) && this.wasOpen) {
      this.close();
    }
  }

  close() {
    this.radioButton.checked = false;
    this.throttleUpdateWasOpen();
  }

  isOpen() {
    return this.radioButton.checked;
  }

  updateWasOpen() {
    this.wasOpen = this.isOpen();
  }

  destroy() {
    this._eventManager.cleanUp();
    if(this._closer && this._closer.classList.contains('pagenav-item__close-label--hidden'))
      this._closer.classList.remove('pagenav-item__close-label--hidden');
    this._element.classList.remove(PAGEACTION_SECONDARY_INITIALIZED_CLASS);
   }
}



export const PageActionsUtils = [
  PageActionSecondaryUtil,
];
