# Reactive Submit Button Utility
Disables a forms LAST sumit button as long as the required inputs are invalid
 (only checks if the value of the inputs are not empty)

## Attribute: (none)
(automatically setup on all form tags)

## Params:
- `data-formnorequired: string`\
  If present the submit button will never get disabled

## Example usage:
```html
<form uw-reactive-submit-button>
  <input type='text' required>
  <button type='submit'>
```
