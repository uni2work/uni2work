# Auto Submit Button Utility
Hides submit buttons in forms that are submitted programmatically.
We hide the button using JavaScript so no-js users will still be able to submit the form.

## Attribute: `uw-auto-submit-button`

## Example usage:
```html
<button type='submit' uw-auto-submit-button>Submit
```

