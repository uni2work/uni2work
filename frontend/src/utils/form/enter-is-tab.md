# Enter is Tab Utility
When the user presses enter on a form that uses this utility, the enter is converted to a tab in order to not send the form.

## Attribute:
`uw-enter-as-tab`

## Example usage:
<input type="text" value="" uw-enter-as-tab="" class=" enter-as-tab--initialized">