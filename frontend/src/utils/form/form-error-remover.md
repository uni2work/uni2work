# Form Error Remover Utility
Removes errors from inputs when they are focused

## Attribute: (none)
(automatically setup on all form tags)

## Example usage:
(any regular form that can show input errors)
