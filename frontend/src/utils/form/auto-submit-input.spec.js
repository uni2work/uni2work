// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { AutoSubmitInput, AUTO_SUBMIT_INPUT_INITIALIZED_CLASS } from './auto-submit-input.js';

describe('Auto-submit-input', () => {

    let autoSubmitInput;
  
    beforeEach(() => {
        const form = document.createElement('form');
        const element = document.createElement('input');
        element.setAttribute('type', 'text');
        form.append(element);
        autoSubmitInput = new AutoSubmitInput(element);
    });
  
    it('should create', () => {
      expect(autoSubmitInput).toBeTruthy();
    });
  
    it('should destory auto-submit-button', () => {
      autoSubmitInput.destroy();
      expect(autoSubmitInput._eventManager._registeredListeners.length).toBe(0);
      expect(autoSubmitInput._element.classList).not.toEqual(jasmine.arrayContaining([AUTO_SUBMIT_INPUT_INITIALIZED_CLASS]));
    });
  
    it('should throw if called without an element', () => {
      expect(() => {
        new AutoSubmitInput();
      }).toThrow();
    });
  });