# Auto Submit Input Utility
Programmatically submits forms when a certain input changes value

## Attribute: `uw-auto-submit-input`

## Example usage:
```html
<input type='text' uw-auto-submit-input />
```
