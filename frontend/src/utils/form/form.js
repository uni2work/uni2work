// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import './form.sass';
import { AutoSubmitButton } from './auto-submit-button';
import { AutoSubmitInput } from './auto-submit-input';
import { Datepicker } from './datepicker';
import { FormErrorRemover } from './form-error-remover';
import { FormErrorReporter } from './form-error-reporter';
import { InteractiveFieldset } from './interactive-fieldset';
import { NavigateAwayPrompt } from './navigate-away-prompt';
import { CommunicationRecipients } from './communication-recipients';
import { EnterIsTab } from './enter-is-tab';

export const FormUtils = [
  AutoSubmitButton,
  AutoSubmitInput,
  Datepicker,
  FormErrorRemover,
  FormErrorReporter,
  InteractiveFieldset,
  NavigateAwayPrompt,
  CommunicationRecipients,
  EnterIsTab,
  // ReactiveSubmitButton // not used currently
];
