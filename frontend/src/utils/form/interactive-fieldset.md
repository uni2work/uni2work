# Interactive Fieldset Utility
Shows/hides inputs based on value of particular input

## Attribute: `uw-interactive-fieldset`

## Params:
- `data-conditional-input: string`\
  Selector for the input that this fieldset watches for changes
- `data-conditional-value: string`\
  The value the conditional input needs to be set to for this fieldset to be shown. Can be omitted if conditionalInput is a checkbox
- `data-conditional-negated`\
  If present, negates the match on `data-conditional-value`

## Example usage:
### example with text input
```html
<input id='input-0' type='text'>
<fieldset uw-interactive-fieldset data-conditional-input='#input-0' data-conditional-value='yes'>...</fieldset>
<fieldset uw-interactive-fieldset data-conditional-input='#input-0' data-conditional-value='no'>...</fieldset>
```

### example with `<select>`
```html
<select id='select-0'>
  <option value='0'>Zero
  <option value='1'>One
<fieldset uw-interactive-fieldset data-conditional-input='#select-0' data-conditional-value='0'>...</fieldset>
<fieldset uw-interactive-fieldset data-conditional-input='#select-0' data-conditional-value='1'>...</fieldset>
```

### example with checkbox
```html
<input id='checkbox-0' type='checkbox'>
<input id='checkbox-1' type='checkbox'>
<fieldset uw-interactive-fieldset data-conditional-input='#checkbox-0'>...</fieldset>
<fieldset uw-interactive-fieldset data-conditional-input='#checkbox-1'>...</fieldset>
```
