# Datepicker Utility
Provides UI for entering dates and times

## Attribute: (none)
(automatically setup on all relevant input tags)

## Example usage:
(any form that uses inputs of type date, time, or datetime-local)

## Methods

### static unformatAll(form, formData)

Call this function on a form and its formData to get back a new FormData object with "unformatted" date values (i.e. all dates formatted from fancy format to backend format).