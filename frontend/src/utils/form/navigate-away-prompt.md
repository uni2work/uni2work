# Navigate Away Prompt Utility
This utility asks the user if (s)he really wants to navigate away from a page containing a form if (s)he already touched an input.

- Form-Submits will not trigger the prompt.
- Utility will ignore forms that contain auto submit elements (buttons, inputs).

## Attribute: (none)
(automatically setup on all form tags that dont automatically submit, see AutoSubmitButtonUtil)
(Does not setup on forms that have uw-no-navigate-away-prompt)

## Example usage:
(any page with a form)
