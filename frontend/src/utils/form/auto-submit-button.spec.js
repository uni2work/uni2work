// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { AutoSubmitButton, AUTO_SUBMIT_BUTTON_INITIALIZED_CLASS, AUTO_SUBMIT_BUTTON_HIDDEN_CLASS } from './auto-submit-button.js';

describe('Auto-submit-button', () => {

    let autoSubmitButton;
  
    beforeEach(() => {
      const element = document.createElement('div');
      autoSubmitButton = new AutoSubmitButton(element);
    });
  
    it('should create', () => {
      expect(autoSubmitButton).toBeTruthy();
    });
  
    it('should destory auto-submit-button', () => {
      autoSubmitButton.destroy();
      expect(autoSubmitButton._element.classList).not.toContain(AUTO_SUBMIT_BUTTON_INITIALIZED_CLASS);
      expect(autoSubmitButton._element.classList).not.toContain(AUTO_SUBMIT_BUTTON_HIDDEN_CLASS);
    });
  
    it('should throw if called without an element', () => {
      expect(() => {
        new AutoSubmitButton();
      }).toThrow();
    });
  });