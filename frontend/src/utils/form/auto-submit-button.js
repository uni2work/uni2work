// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Utility } from '../../core/utility';

export const AUTO_SUBMIT_BUTTON_UTIL_SELECTOR = '[uw-auto-submit-button]';

const AUTO_SUBMIT_BUTTON_INITIALIZED_CLASS = 'auto-submit-button--initialized';
const AUTO_SUBMIT_BUTTON_HIDDEN_CLASS = 'hidden';

@Utility({
  selector: AUTO_SUBMIT_BUTTON_UTIL_SELECTOR,
})
export class AutoSubmitButton {
  _element;

  constructor(element) {
    this._element = element;
    if (!element) {
      throw new Error('Auto Submit Button utility needs to be passed an element!');
    }

    if (element.classList.contains(AUTO_SUBMIT_BUTTON_INITIALIZED_CLASS)) {
      return false;
    }

    // hide and mark initialized
    element.classList.add(AUTO_SUBMIT_BUTTON_HIDDEN_CLASS, AUTO_SUBMIT_BUTTON_INITIALIZED_CLASS);
  }

  destroy() {
    this._element.classList.remove(AUTO_SUBMIT_BUTTON_INITIALIZED_CLASS);
    this._element.classList.remove(AUTO_SUBMIT_BUTTON_HIDDEN_CLASS);
  }
}
