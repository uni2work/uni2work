// SPDX-FileCopyrightText: 2022 Johannes Eder <ederj@cip.ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Utility } from '../../core/utility';
import { StorageManager, LOCATION } from '../../lib/storage-manager/storage-manager';

const SORT_TABLE_IDENT = 'uw-sort-table';

@Utility({
  selector: `table[${SORT_TABLE_IDENT}]`,
})
export class SortTable {

  _storageManager = new StorageManager('SORT_TABLE', '0.0.0', { location: LOCATION.LOCAL });

  _element;

  constructor(element) {
    if (!element) {
      throw new Error('Sort Table utility cannot be setup without an element!');
    }

    this._element = element;
  }

  destroy() {
    this._storageManager.clear();
  }

}
