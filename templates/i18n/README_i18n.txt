# SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

This directories contains all language dependent widgets.

Each widget requires its own directories, the name of which is needed in the source code, e.g. for directory "imprint"
  $(i18nWidgetFile "imprint")
inside this directory must be one file per language "de.hamlet", etc. 

