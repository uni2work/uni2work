$newline never

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later

<p>
  Um eine Prüfungsleistung einzutragen können Sie in der #
  Teilnehmer-Spalte einen beliebigen eindeutigen Identifikator des #
  Teilnehmers/der Teilnehmerin angeben.<br />

  Es können nur Ergebnisse für Studierende eingetragen werden, die #
  bereits Prüfungsteilnehmer:in sind. #
  Über diese Oberfläche können keine neuen Benutzer zur Klausur #
  angemeldet werden.<br />

  Vermutlich eindeutig ist die Matrikelnummer des Teilnehmers/der Teilnehmerin, aber #
  auch der Name oder ein Teil der Matrikelnummer können unter #
  Umständen bereits eindeutig sein.<br />

  Wenn Felder für Ergebnisse frei gelassen werden, wird an dieser #
  Stelle nichts in die Datenbank eingetragen.<br />

  Beim Senden von Ergebnissen wird der bisherige Stand in der #
  Datenbank überschrieben. #
  Es werden auch Ergebnisse überschrieben, die andere Benutzer:innen #
  eingetragen haben.<br />

  Bereits eingetragene Ergebnisse können auch gelöscht werden; es ist #
  danach für den jeweiligen Teil der Prüfung kein Ergebnis mehr in der #
  Datenbank hinterlegt.<br />

  Falls eine automatische Notenberechnung konfiguriert ist, müssen die #
  berechneten Ergebnisse noch auf der Seite der Klausurteilnehmerliste #
  akzeptiert werden.
