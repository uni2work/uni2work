$newline never

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later

If the exercise sheet is associated with an exam, the settings of the exam are applied.

<br>

An exercise sheet is associated with an exam if one of the following is true:

<ul>
  <li>
    An exam was manually configured under “_{MsgSheetAuthorshipStatementExam}”
  <li>
    The exercise sheet is valued “_{MsgSheetTypeExamPartPoints}”
  <li>
    Registration for an exam is required to submit for the exercise sheet (“_{MsgSheetRequireExam}”)
