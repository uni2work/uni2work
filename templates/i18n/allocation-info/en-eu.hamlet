$newline text

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later

<p>
  Every central allocations progresses through the following stages
  (usually in order):
<dl .deflist>
  <dt .deflist__dt>
    _{MsgAllocationStaffRegister}
  <dd .deflist__dd>
    <p>
      Course administrators may register their courses for participation in
      the central allocation and deregister them again at will.
    <p>
      Whether students are expected to provide text or files with their
      applications is configured on a course-by-course basis.
      Course administrators may specify instructions for application e.g. what
      content the application files should have.
    <p>
      Courses registered for participation in the central allocation can not
      gain participants throughout the entire course of the allocation.
      Not even through manual enrollment by course administrators.

  <dt .deflist__dt>
    _{MsgAllocationRegister}
  <dd .deflist__dd>
    <p>
      Only during this phase may students apply to the various courses which
      participate in the central allocation.
    <p>
      Applicants may assign priorities (“this course would be my first choice”
      down to “I will only participate in this course if nothing else is
      available to me”) to courses they apply to.
      Multiple courses may have the same priority.
    <p>
      Applications and priorities may be freely edited and retracted during
      the application period.
    <p>
      Students may request more than one placement from a central allocation.
      Additional placements are made if sufficient capacity is available
      and/or placements are sufficiently urgent.
    <p>
      Application texts and/or files need to be specified for each course
      individually, as requested by the course administrators.
    <p>
      During the application phase courses that participate in the
      central allocation are forced to be visible.
      “_{MsgCourseVisibleFrom}” and “_{MsgCourseVisibleTo}” are thus
      ignored during this phase.

  <dt .deflist__dt>
    _{MsgAllocationStaffAllocation}
  <dd .deflist__dd>
    <p>
      Only during this phase may course administrators inspect and grade
      applications.
    $# <p>
    $#   Nur in manchen Zentralanmeldungen dürfen Veranstalter
    $#   Bewerber jetzt direkt ablehnen und/oder übernehmen.
    $# <p>
    $#   Veranstalter haben noch eine letzte Möglichkeit,
    $#   die Kurskapazität anzupassen.
    <p>
      Course administrators have one last chance to edit their courses'
      capacities.

  <dt .deflist__dt>
    _{MsgAllocationProcess}
  <dd .deflist__dd>
    <p>
      Placements in courses are made with regard to the current study
      progress, urgency, and the grading of the application as determined by a
      course administrator.
      Further details wrt. the allocation process can be found #
      <a href=#{faqItemUrlAllocationNoPlaces}>
        in the FAQ
      .
    <p>
      Applicants are directly enrolled in the selected courses.
      Applicants which leave courses they were assigned in the course of a
      central allocation may be affected negatively by this in future central
      allocations.
    $# <p>
    $#   Only after placements have been made, may course administrators enroll
    $#   and deregister participants of their courses.
    $#   Course administrators can request to be assigned successors to fill
    $#   their courses' remaining capacity.

  <dt .deflist__dt>
    _{MsgAllocationSubstitutesProcess}
  <dd .deflist__dd>
    <p>
      System administrators occasionally trigger the distribution of
      allocation places to substitute participants.
    <p>
      Course administrators may specify a deadline until which they
      are prepared to accept substitutes for their courses.
    <p>
      <em>
        Caution: #
      If the administrators for a certain course do not specify a
      deadline no substitutes will ever be assigned to that course. #
      <em>
        In general without consulting the course adminstrators.

  <dt .deflist__dt>
    _{MsgAllocationRegisterByStaff}
  <dd .deflist__dd>
    <p>
      Direct registration to courses is prevented from the time at
      which they are registered to a central allocation.
    <p>
      Starting with “_{MsgAllocationRegisterByStaffFrom}” course
      administrators are once again permitted to add/remove
      participants to and from their courses.

  <dt .deflist__dt>
    _{MsgAllocationRegisterByCourse}
  <dd .deflist__dd>
    <p>
      Students can, starting with
      “_{MsgAllocationRegisterByCourseFrom}”, once again register
      themselves for the courses directly.
      The rules specified by course administrators for direct
      registration to their courses apply.
    <p>
      It is therefore advised that course administrators take care to
      define sensible deadlines for direct registration to their
      courses, even if they participate in a central allocation.
    <p>
      The central allocation usually fills all available places.
      It is therefore not recommended that students do not participate
      in the central allocation in hopes of being able to register
      themselves afterwards.

<p>
  Central allocation procedure may vary.
  <br>
  <em>
    In particular: #
  If, on the central allocation's page, no times are specified for one of the
  phases mentioned above, the time in question has not been determined yet!
<p>
  Multiple central allocations are handled entirely independently of one
  another.

