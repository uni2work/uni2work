$newline never

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later

<section>
  ^{priosForm}
<section>
  <dl .deflist>
    <dt .deflist__dt>
      Effect size of ratings for numeric priorities
      <p .deflist__explanation>
        Course administrators may rate their applications. #

        The allocation process is impacted by such ratings to a degree #
        corresponding to a difference in numeric priorities of plus or #
        minus the given value.
    <dd .deflist__dd>
      #{rationalToFixed2 gradeScale}
    <dt .deflist__dt>
      Effect size of ratings for priorities based on a sorted list
      <p .deflist__explanation>
        Course administrators may rate their applications. #

        The allocation process is impacted by such ratings to a degree #
        corresponding to a difference in placement within the sorted #
        list of plus or minus the given proportion.
    <dd .deflist__dd>
      #{textPercent gradeOrdinalProportion 1}
