$newline never

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later

<section .profile>
  <dl .deflist.profile-dl>
      <dt .deflist__dt>
        _{MsgIdent}
      <dd .deflist__dd .email>
        #{userIdent}
      <dt .deflist__dt>
        _{MsgAuthModeSet}
      <dd .deflist__dd>
        _{userAuthentication}
      <dt .deflist__dt>
        _{MsgNameSet}
      <dd .deflist__dd>
        ^{nameWidget userDisplayName userSurname}
      $maybe matnr <- userMatrikelnummer
        <dt .deflist__dt>
          _{MsgTableMatrikelNr}
        <dd .deflist__dd>
          #{matnr}
      <dt .deflist__dt>
        _{MsgTableEmail}
      <dd .deflist__dd>
        #{mailtoHtml userEmail}
      $if userEmail /= userDisplayEmail
        <dt .deflist__dt>
          _{MsgUserDisplayEmail}
        <dd .deflist__dd .email>
          #{userDisplayEmail}
      $if showAdminInfo
        <dt .deflist__dt>
          _{MsgUserCreated}
        <dd .deflist__dd>
          ^{formatTimeW SelFormatDateTime userCreated}
        <dt .deflist__dt>
          _{MsgLastLogin}
        <dd .deflist__dd>
          $maybe llogin <- userLastAuthentication
            ^{formatTimeW SelFormatDateTime llogin}
          $nothing
            _{MsgNeverSet}
        <dt .deflist__dt>
          _{MsgProfileLastLdapSynchronisation}
        <dd .deflist__dd>
          $maybe lsync <- userLastLdapSynchronisation
            ^{formatTimeW SelFormatDateTime lsync}
          $nothing
            _{MsgNeverSet}
        $maybe pKey <- userLdapPrimaryKey
          <dt .deflist__dt>
            _{MsgProfileLdapPrimaryKey}
          <dd .deflist__dd .ldap-primary-key>
            #{pKey}
        <dt .deflist__dt>
          _{MsgTokensLastReset}
        <dd .deflist__dd>
          $maybe lastInvalidated <- userTokensIssuedAfter
            ^{formatTimeW SelFormatDateTime lastInvalidated}
          $nothing
            _{MsgNeverSet}
    $forall (function, schools) <- Map.toList functions
      <dt .deflist__dt>_{function}
      <dd .deflist__dd>
        <ul .list--inline .list--iconless .list--comma-separated>
          $forall ssh <- schools
            <li>
              #{ssh}
    $if not $ null lecture_corrector
      <dt .deflist__dt>_{MsgProfileCorrector}
      <dd .deflist__dd>
        <ul .list-ul>
          $forall (E.Value tid, E.Value ssh, E.Value csh) <- lecture_corrector
            <li .list-ul__item>
              <a href=@{CourseR tid ssh csh CShowR}>#{tid}-#{ssh}-#{csh}
    $if not $ null studies
      <dt .deflist__dt>_{MsgAdminFeaturesHeading}
      <dd .deflist__dd>
        <div .scrolltable>
          <table .table.table--striped.table--hover.table--condensed>
            <tr .table__row>
              <th .table__th>_{MsgTableStudyTerm}
              <th .table__th>_{MsgTableStudyFeatureDegree}
              <th .table__th>_{MsgTableStudyFeatureType}
              <th .table__th>_{MsgTableStudyFeatureAge}
              <th .table__th>_{MsgTableStudyFeatureValid}
              <th .table__th>_{MsgTableStudyFeatureUpdate}

            $forall ((Entity _ StudyFeatures{studyFeaturesType, studyFeaturesSemester, studyFeaturesValid, studyFeaturesFirstObserved, studyFeaturesLastObserved}), (Entity _ degree), (Entity _ field)) <- studies
              <tr.table__row>
                <td .table__td>_{field}
                <td .table__td>_{degree}
                <td .table__td>_{studyFeaturesType}
                <td .table__td>#{studyFeaturesSemester}
                <td .table__td>#{hasTickmark studyFeaturesValid}
                <td .table__td>
                  $maybe fObs <- studyFeaturesFirstObserved
                    ^{formatTimeRangeW SelFormatDateTime fObs $ Just studyFeaturesLastObserved}
                  $nothing
                    ^{formatTimeW SelFormatDateTime studyFeaturesLastObserved}
<section>
  <div .container>
    $if hasRows
      <div .container>
        <h2>_{MsgProfileCourses}
        <div .container>
          ^{ownedCoursesTable}

  <div .container>
    <h2>_{MsgProfileCourseParticipations}
    <div .container>
      ^{enrolledCoursesTable}

  <div .container>
    <h2>_{MsgProfileCourseExamResults}
    <div .container>
      ^{examTable}

  <div .container>
    <h2>_{MsgProfileTutorials}
    <div .container>
      ^{ownTutorialTable}

  <div .container>
    <h2>_{MsgProfileTutorialParticipations}
    <div .container>
      ^{tutorialTable}

  <div .container>
    <h2>_{MsgProfileSubmissionGroups}
    <div .container>
      ^{submissionGroupTable}

  <div .container>
    <h2>_{MsgProfileSubmissions}
    <div .container>
      ^{submissionTable}
      <em>_{MsgProfileRemark}
      _{MsgProfileGroupSubmissionDates}

  <div .container>
    <h2> _{MsgTableCorrector}
    <div .container>
      ^{correctionsTable}

    <em>_{MsgProfileRemark}
    _{MsgProfileCorrectorRemark}
    <a href=@{CorrectionsR}>_{MsgProfileCorrections}

^{profileRemarks}
