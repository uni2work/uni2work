$newline never

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later

<li .workflow-history--item :is (_WorkflowActionUser . _WHIASelf) whiUser:.workflow-history-item__self>
  <div .workflow-history--item-user>
    $case whiUser
      $of WorkflowActionAutomatic
        <span .workflow-history--item-user-special>
          _{MsgWorkflowWorkflowWorkflowHistoryUserAutomatic}
      $of WorkflowActionUnauthenticated
        <span .workflow-history--item-user-special>
          _{MsgWorkflowWorkflowWorkflowHistoryUserNotLoggedIn}
      $of WorkflowActionUser WHIASelf
        <span .workflow-history--item-user-special>
          _{MsgWorkflowWorkflowWorkflowHistoryUserSelf}
      $of WorkflowActionUser (WHIAOther (Entity _ User{userDisplayName, userSurname, userDisplayEmail}))
        ^{nameEmailWidget userDisplayEmail userDisplayName userSurname}
      $of WorkflowActionUser WHIAGone
        <span .workflow-history--item-user-special>
          _{MsgWorkflowWorkflowWorkflowHistoryUserGone}
      $of WorkflowActionUser WHIAHidden
        <span .workflow-history--item-user-special>
          _{MsgWorkflowWorkflowWorkflowHistoryUserHidden}
  <div .workflow-history--item-time>
    ^{formatTimeW SelFormatDateTime whiTime}
  <div .workflow-history--item-action-states>
    <dl .deflist>
      <dt .deflist__dt>
        _{MsgWorkflowWorkflowWorkflowHistoryActionLabel}
      <dd .deflist__dd>
        $maybe mActionLbl <- whiVia
          $maybe actionLbl <- mActionLbl
            #{actionLbl}
          $nothing
            <span .workflow-history--item-action-special>
              _{MsgWorkflowWorkflowWorkflowHistoryActionHidden}
        $nothing
          <span .workflow-history--item-action-special>
            _{MsgWorkflowWorkflowWorkflowHistoryActionAutomatic}
      $maybe mFromLbl <- whiFrom
        <dt .deflist__dt>
          _{MsgWorkflowWorkflowWorkflowHistoryFromLabel}
        <dd .deflist__dd>
          $maybe (fromLbl, fromFin) <- mFromLbl
            #{fromLbl}
            $maybe icn <- fromFin
              &nbsp;#{icon icn}
          $nothing
            <span .workflow-history--item-state-special>
              _{MsgWorkflowWorkflowWorkflowHistoryStateHidden}
      <dt .deflist__dt>
        _{MsgWorkflowWorkflowWorkflowHistoryToLabel}
      <dd .deflist__dd>
        $maybe (toLbl, toFin) <- whiTo
          #{toLbl}
          $maybe icn <- toFin
            &nbsp;#{icon icn}
        $nothing
          <span .workflow-history--item-state-special>
            _{MsgWorkflowWorkflowWorkflowHistoryStateHidden}
  $if not (onull whiPayloadChanges)
    <div .workflow-history--item-payload-changes>
      <div .workflow-history--item-payload-changes-label>
        _{MsgWorkflowWorkflowWorkflowHistoryPayloadLabel}
      <dl .deflist>
        $forall (payloadLbl, (newPayload, mFileRoute)) <- whiPayloadChanges
          <dt .deflist__dt>
            #{payloadLbl}
          <dd .deflist__dd>
            $if is _Nothing mFileRoute && null newPayload
              —
            $else
              <ul .list--iconless>
                $maybe fileRoute <- mFileRoute
                  <li>
                    <a href=#{fileRoute}>
                      _{MsgWorkflowPayloadFiles}
                $forall pItem <- newPayload
                  <li>
                    ^{payloadToWidget pItem}
