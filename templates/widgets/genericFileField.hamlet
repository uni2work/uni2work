$newline never

$# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
$#
$# SPDX-License-Identifier: AGPL-3.0-or-later


$maybe ident <- identSecret
  <input type=hidden name=#{fieldName} id=#{fieldId}--ident value=#{ident}>
  
$if not (null fileInfos)
  <div .file-input__list-wrapper>
    <table .file-input__list .table .table--striped .table--hover .table--condensed>
      <colgroup>
        <col .checkbox-only>
        <col .tooltip-only>
        <col>
      <thead>
        <tr>
          <th .table__th>
          <th .table__th colspan=2>
            _{MsgUtilPreviouslyUploadedInfo}
      <tbody>
        $forall FileUploadInfo{..} <- fileInfos
          <tr>
            <td .table__td>
              <input type=checkbox id=#{fuiHtmlId} name=#{fieldName} :fuiChecked:checked value=#{review _FileTitle fuiTitle} :fuiForced:readonly>
            <td .table__td .tooltip-only>
              $if fuiSession
                ^{messageTooltip uploadOnlySessionMessage}
            <td .table__td>
              <label for=#{fuiHtmlId} .file-input__list-item>
                #{fuiTitle}

  <div .file-input__info>
    _{MsgUtilPreviouslyUploadedDeletionInfo}

  <div .file-uploads-label>_{MsgUtilAddMoreFiles}

$# new files
<input type="file" uw-file-input name=#{fieldName} id=#{fieldId} :fieldMultiple:multiple :acceptRestricted:accept=#{accept} :req && null fileInfos:required :is _Just fieldMaxFileSize || is _Just fieldMaxCumulativeSize:data-max-size=#{maybe "-1" tshow (ignoreNothing min fieldMaxFileSize fieldMaxCumulativeSize)}>

$if fieldMultiple
  <div .file-input__info>
    _{MsgMultiFileUploadInfo}

$maybe exts <- fmap toNullable fieldRestrictExtensions
  <div .file-input__info>
    _{MsgUploadModeExtensionRestriction}: #
    <ul .list--inline .list--comma-separated .list--iconless>
      $forall ext <- bool id (mappend zipExtensions) mayUnpack exts
        <li .file-path>
          #{ext}
    $if fieldMultiple
      <br>
      _{MsgUploadModeExtensionRestrictionMultipleTip}

$maybe maxSize <- fieldMaxFileSize
  <div .file-input__info>
    $if fieldMultiple
      _{MsgFileUploadMaxSizeMultiple (textBytes maxSize)}
    $else
      _{MsgFileUploadMaxSize (textBytes maxSize)}

$maybe maxSize <- fieldMaxCumulativeSize
  <div .file-input__info>
    _{MsgFileUploadCumulativeMaxSize (textBytes maxSize)}

$if not (fieldOptionForce fieldUnpackZips)
  <div .file-input__unpack>
    ^{iconTooltip (i18n MsgAutoUnzipInfo) Nothing False}
    <label for=#{fieldId}_zip>_{MsgAutoUnzip}
    <input type=checkbox id=#{fieldId}--zip name=#{fieldName} value=#{unpackZips} :fieldOptionDefault fieldUnpackZips:checked>
