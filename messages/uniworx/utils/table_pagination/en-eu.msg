# SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

PSLimitNonPositive: “pagesize” must be greater than zero
NoTableContent: No entries
DBTablePagesize: Entries per page
DBTablePagesizeAll: All
CsvFile: CSV file
CsvImportExplanationLabel: Informating regarding CSV import
CsvColumnsExplanationsLabel: Column & cell format
CsvImport: CSV import
CsvImportNotConfigured: CSV import not configured
CsvImportConfirmationHeading: CSV import preview (no changes have been made yet)
CsvImportConfirmationTip: No changes have been made yet! Importing this CSV file corresponds to performing the edits listed below. Please choose the edits that should be performed before finalising the import.
CsvImportUnnecessary: Importing the given CSV file does not correspond to performing any edits
CsvImportSuccessful n: Successfully imported CSV file. #{n} #{pluralEN n "edit" "edits"} have been performed.
CsvImportAborted: CSV import aborted
DBCsvImportActionToggleAll: All/None
DBCsvDuplicateKey: Two rows in the CSV file reference the same database entry and are thus invalid
DBCsvDuplicateKeyTip: Please remove one of the lines listed below and try again.
DBCsvKeyException: For a row in the CSV file it could not be determined whether it references any database entry.
DBCsvException: An error occurred while computing the set of edits this CSV import corresponds to.
DBCsvParseError: An uploaded file could not be interpreted as CSV of the expected format.
DBCsvParseErrorTip: The Uni2work-component that handles CSV decoding has reported the following error:
CsvColumnsExplanationsTip: Meaning and format of the columns contained in imported and exported CSV files
CsvExportExample: Export example CSV
CsvExampleData: Example data
CourseSortingOnlyLoggedIn: The user interface for sorting this table is only active for logged in users
LecturersForN n: #{pluralEN n "Lecturer" "Lecturers"}
Registered: Enrolled
Registration: Enrolment
TableCourseDescription: Description
RowCount count: #{count} matching #{pluralEN count "entry" "entries"}
JSONFieldDecodeFailure aesonFailure: Could not parse JSON: #{aesonFailure}
DBTIRowsMissing n: #{pluralDE n "A line" "A number of lines"} vanished from the database since the form you submitted was generated for you
Page num: #{num}
