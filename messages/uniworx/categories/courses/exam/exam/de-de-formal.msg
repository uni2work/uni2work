# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

ExamRegistrationInviteDeadline: Einladung nur gültig bis
ExamRegistrationEnlistDirectly: Bekannte Nutzer:innen sofort als Teilnehmer:innen eintragen
ExamRegistrationEnlistDirectlyTip: Sollen, wenn manche der E-Mail-Adressen bereits in Uni2work mit Nutzer:innen assoziiert sind, jene Nutzer:innen direkt zur Prüfung hinzugefügt werden? Ansonsten werden Einladung an alle E-Mail-Adressen (nicht nur unbekannte) versandt, die die Nutzer:innen zunächst akzeptieren müssen um Prüfungsteilnehmer:in zu werden.
ExamRegistrationRegisterCourse: Nutzer:in auch zum Kurs anmelden
ExamRegistrationRegisterCourseTip: Nutzer:innen, die keine Kursteilnehmer:innen sind, werden sonst nicht zur Prüfung angemeldet.
ExamRegistrationInviteField: Einzuladende E-Mail-Adressen
ExamParticipantsRegisterHeading: Prüfungsteilnehmer:in hinzufügen
ExamParticipantsInvited n@Int: #{n} #{pluralDE n "Einladung" "Einladungen"} per E-Mail verschickt
ExamRegistrationAndCourseParticipantsRegistered n@Int: #{n} #{pluralDE n "Teilnehmer:in" "Teilnehmer:innen"} #{pluralDE n "wurde" "wurden"} sowohl zum Kurs, als auch zur Prüfung angemeldet
ExamRegistrationNotRegisteredWithoutCourse n@Int: #{n} #{pluralDE n "Teilnehmer:in" "Teilnehmer:innen"} #{pluralDE n "wurde" "wurden"} nicht zur Prüfung angemeldet, da #{pluralDE n "er/sie" "sie"} nicht zum Kurs angemeldet #{pluralDE n "ist" "sind"}
ExamAutoOccurrenceMinimizeRooms: Verwendete Räume/Termine minimieren
ExamAutoOccurrenceMinimizeRoomsTip: Soll, für die Aufteilung, die Liste an Räumen/Terminen zunächst reduziert werden, sodass nur so wenige Räume verwendet werden, wie nötig (größte zuerst)?
ExamAutoOccurrenceOccurrencesChangedInFlight: Raumliste wurde verändert
ExamAutoOccurrenceParticipantsAssigned num@Int64: Verteilungstabelle erfolgreich gespeichert und #{num} #{pluralDE num "Teilnehmer:in" "Teilnehmer:innen"} zugewiesen
Proportion c@Text of'@Text prop@Rational !ident-ok: #{c}/#{of'} (#{rationalToFixed2 (100 * prop)}%)
ProportionNoRatio c@Text of'@Text !ident-ok: #{c}/#{of'}
ExamCorrectHeading examname@Text: Prüfungsergebnisse für #{examname} eintragen
ExamCorrectErrorNeedleTooShort: Dieser Identifikator ist zu kurz.
ExamCorrectErrorMultipleMatchingParticipants: Dem Identifikator konnten mehrere Prüfungsteilnehmer:innen zugeordnet werden.
ExamCorrectErrorNoMatchingParticipants: Dem Identifikator konnte kein Prüfungsteilnehmer/keine Prüfungsteilnehmerin zugeordnet werden.
ExamCorrectErrorPartResultOutOfBounds examPartNumber@ExamPartNumber: Prüfungsergebnis für Teil #{examPartNumber} ist nicht größer Null.
ExamCorrectErrorPartResultOutOfBoundsMax examPartNumber@ExamPartNumber maxPoints@Points: Prüfungsergebnis für Teil #{examPartNumber} liegt nicht zwischen 0 und #{maxPoints}.
UnauthorizedExamCorrectorGrade: Sie haben nicht die Berechtigung für diese Prüfung Gesamtergebnisse einzutragen.
MailSubjectExamCorrectorInvitation tid@TermId ssh@SchoolId csh@CourseShorthand examn@ExamName: [#{tid}-#{ssh}-#{csh}] Einladung zum Korrektor/zur Korrektorin für #{examn}
ExamCorrectorInviteHeading examn@ExamName: Einladung zum Korrektor/zur Korrektorin für #{examn}
ExamCorrectorInviteExplanation: Sie wurden eingeladen, Prüfungs-Korrektor:in zu sein.
ExamCorrectorInvitationAccepted examn@ExamName: Sie wurden als Korrektor:in für #{examn} eingetragen
ExamNameTaken exam@ExamName: Es existiert bereits eine Prüfung mit Namen #{exam}
ExamEdited exam@ExamName: #{exam} erfolgreich bearbeitet
ExamEditHeading examn@ExamName: #{examn} bearbeiten
ExamNameTip: Muss innerhalb der Veranstaltung eindeutig sein
ExamDescription: Beschreibung
ExamFormTimes: Zeiten
ExamFormOccurrences: Prüfungstermine/Räume
ExamFormAutomaticFunctions: Automatische Funktionen
ExamFormCorrection: Korrektur
ExamFormParts: Teile
ExamFormMode: Ausgestaltung der Prüfung
ExamFormGrades: Prüfungsleistungen
ExamStart: Beginn
ExamEnd: Ende
ExamTimeTip: Nur zur Information der Studierenden, die tatsächliche Zeitangabe erfolgt pro Prüfungstermin/Raum
ExamVisibleFrom: Sichtbar ab
ExamVisibleFromTip: Ohne Datum nie sichtbar und keine Anmeldung möglich
ExamRegisterFrom: Anmeldung ab
ExamRegisterTo: Anmeldung bis
ExamRegisterFromTip: Zeitpunkt ab dem sich Kursteilnehmer:innen selbständig zur Prüfung anmelden können; ohne Datum ist keine Anmeldung möglich
ExamDeregisterUntil: Abmeldung bis
ExamPublishOccurrenceAssignments: Termin- bzw. Raumzuteilung den Teilnehmer:innen mitteilen um
ExamPublishOccurrenceAssignmentsTip: Ab diesem Zeitpunkt können Teilnehmer:innen einsehen zu welcher Teilprüfung bzw. welchen Raum sie angemeldet sind
ExamPublishOccurrenceAssignmentsParticipant: Termin- bzw. Raumzuteilung einsehbar ab
ExamPartsFrom: Teile anzeigen ab
ExamPartsFromTip: Ab dem gegebenen Zeitpunkt wird die Liste von Prüfungsteilen/Aufgaben veröffentlicht, nicht jedoch die jeweilige Maximalpunktzahl. Ohne Zeitpunkt wird die Liste ab "Ergebnisse sichtbar ab" angezeigt.
ExamFinishedTip: Zeitpunkt zu dem Prüfungergebnisse den Teilnehmer:innen gemeldet werden; ohne Datum werden die Prüfungsergebnisse zunächst nie gemeldet
ExamFinishedTipCloseOnFinished: Zeitpunkt zu dem Prüfungergebnisse den Teilnehmer:innen und den Prüfungsverwaltungen gemeldet werden; ohne Datum werden die Prüfungsergebnisse zunächst nie gemeldet
ExamPublicStatistics: Statistik veröffentlichen
ExamPublicStatisticsTip: Soll die automatisch berechnete statistische Auswertung auch den Teilnehmer:innen angezeigt werden, sobald diese ihre Noten einsehen können?
ExamAutomaticGrading: Automatische Notenberechnung
ExamAutomaticGradingTip: Sollen die Gesamtleistungen der Teilnehmer:innen automatisch aus den in den einzelnen Teilprüfungen erreichten Leistungen berechnet werden? Etwaige Bonuspunkte werden dabei berücksichtigt. Manuelles Überschreiben der Gesamtleistung ist dennoch möglich.
ExamBonus: Bonuspunkte-System
ExamGradingMode: Bewertungsmodus
ExamGradingModeTip: In welcher Form werden Prüfungsleistungen für diese Prüfung eingetragen?
ExamStaff: Prüfer:innen/Verantwortliche Hochschullehrer:innen
ExamStaffTip: Geben Sie bitte in jedem Fall einen Namen an, der Prüfer:in/Veranstalter:in/Hochschullehrer:in eindeutig identifiziert! Sollte der Name des Prüfers/der Prüferin allein womöglich nicht eindeutig sein, so geben Sie bitte eindeutig identifizierende Zusatzinfos, wie beispielsweise den Lehrstuhl bzw. die LFE o.Ä., an.
ExamExamOfficeSchools: Zusätzliche Institute
ExamExamOfficeSchoolsTip: Prüfungsbeauftragte von Instituten, die Sie hier angeben, erhalten im System (zusätzlich zum primären Institut des zugehörigen Kurses) volle Einsicht in sämtliche für diese Prüfung hinterlegten Leistungen, unabhängig von den Studiendaten der Teilnehmer:innen.
ExamCorrectorEmail: E-Mail
ExamCorrectors: Korrektor:innen
ExamCorrectorsTip: Hier eingetragene Korrektor:innen können zwischen Beginn der Prüfung und "Bewertung abgeschlossen ab" Ergebnisse für alle Teilprüfungen und alle Teilnehmer:innen im System hinterlegen.
ExamCorrectorAlreadyAdded: Ein Korrektor/eine Korrektorin mit dieser E-Mail ist bereits für diese Prüfung eingetragen
ExamRoom: Raum
ExamRoomManual': Keine automatische bzw. selbstständige Zuteilung
ExamRoomSurname': Nach Nachname
ExamRoomMatriculation': Nach Matrikelnummer
ExamRoomRandom': Zufällig pro Teilnehmer:in
ExamRoomFifo': Auswahl durch Teilnehmer:in bei Anmeldung
ExamRooms: Räume
ExamRoomRoom: Raum
ExamRoomRoomHidden: Raum nur für Angemeldete
ExamRoomRoomHiddenTip: Soll der Raum nur zu diesem Termin/Raum angemeldeten Prüfungsteilnehmer:innen angezeigt werden?
ExamRoomAlreadyExists: Prüfung ist bereits eingetragen
ExamRoomName: Interne Bezeichnung
ExamRoomCapacity: Kapazität
ExamRoomCapacityNegative: Kapazität darf nicht negativ sein
ExamRoomTime: Termin
ExamRoomStart: Beginn
ExamRoomEnd: Ende
ExamRoomDescription: Beschreibung
ExamRoomAssigned: Zugeteilt
ExamRoomRegistered: Anmeldung
ExamRoomMappingSurname: Nachnamen beginnend mit
ExamRoomMappingMatriculation: Matrikelnummern endend in
ExamRoomLoad: Auslastung
ExamPart: Teilprüfung/Aufgabe
ExamParts: Teilprüfungen/Aufgaben
ExamPartWeightNegative: Gewicht aller Teilprüfungen muss größer oder gleich Null sein
ExamPartAlreadyExists: Teilprüfung mit diesem Namen existiert bereits
ExamPartNumber: Nummer
ExamPartNumbered examPartNumber@ExamPartNumber: Teil #{view _ExamPartNumber examPartNumber}
ExamPartNumberTip: Wird als interne Bezeichnung z.B. bei CSV-Export verwendet
ExamPartName: Titel
ExamPartNameTip: Wird den Studierenden angezeigt
ExamPartMaxPoints: Maximalpunktzahl
ExamPartWeight: Gewichtung
ExamPartWeightTip: Wird vor Anzeige oder automatischen Notenberechnung mit der erreichten Punktzahl und der Maximalpunktzahl multipliziert; Änderungen hier passen also auch bestehende Korrekturergebnisse an (derart geänderte Noten müssen erneut manuell übernommen werden)
ExamPartResultPoints: Erreichte Punkte
ExamPartSheets: Übungsblätter
ExamPartsFromMustBeBeforeFinished: "Teile anzeigen ab" muss vor "Ergebnisse sichtbar ab" liegen
ExamPartCannotBeDeletedDueToResults exampartnum@ExamPartNumber: Teil #{exampartnum} kann nicht gelöscht werden, da bereits Prüfungsergebnisse für diesen Teil eingetragen wurden.
ExamPartCannotBeDeletedDueToSheetReference exampartnum@ExamPartNumber sheetName@SheetName: Teil #{exampartnum} kann nicht gelöscht werden, da Übungsblatt #{sheetName} den Bewertungsmodus „als Prüfungsaufgabe“ trägt.
ExamRegisterToMustBeAfterRegisterFrom: "Anmeldung ab" muss vor "Anmeldung bis" liegen
ExamDeregisterUntilMustBeAfterRegisterFrom: "Abmeldung bis" muss nach "Anmeldung bis" liegen
ExamStartMustBeAfterPublishOccurrenceAssignments: Beginn muss nach Veröffentlichung der Termin- bzw. Raumzuordnung liegen
ExamEndMustBeAfterStart: Beginn der Prüfung muss vor ihrem Ende liegen
ExamFinishedMustBeAfterEnd: "Ergebnisse sichtbar ab" muss nach Ende liegen
ExamFinishedMustBeAfterStart: "Ergebnisse sichtbar ab" muss nach Beginn liegen
ExamOccurrenceEndMustBeAfterStart eoName@ExamOccurrenceName: Beginn des Termins #{eoName} muss vor seinem Ende liegen
ExamOccurrenceStartMustBeAfterExamStart eoName@ExamOccurrenceName: Beginn des Termins #{eoName} muss nach Beginn der Prüfung liegen
ExamOccurrenceEndMustBeBeforeExamEnd eoName@ExamOccurrenceName: Ende des Termins #{eoName} liegt nach dem Ende der Prüfung
ExamOccurrenceDuplicate eoRoom@Text eoRange@Text: Raum #{eoRoom}, Termin #{eoRange} kommt mehrfach mit der selben Beschreibung vor
ExamOccurrenceDuplicateName eoName@ExamOccurrenceName: Interne Terminbezeichnung #{eoName} kommt mehrfach vor
ExamOccurrenceRoomIsUnset !ident-ok: —
ExamOccurrenceRoomIsHidden: Raum wird nur Teilnehmer:innen angezeigt
ExamOccurrenceCannotBeDeletedDueToRegistrations eoName@ExamOccurrenceName: Termin #{eoName} kann nicht gelöscht werden, da noch Teilnehmer:innen diesem Termin zugewiesen sind. Über die Liste von Prüfungsteilnehmern können Sie zunächst die entsprechenden Terminzuweisungen entfernen.
ExamRegistrationMustFollowSchoolSeparationFromStart dayCount@Int: Nach Regeln des Instituts #{pluralDE dayCount "muss" "müssen"} zwischen "Anmeldung ab" und "Beginn" mindestens #{dayCount} #{pluralDE dayCount "Tag" "Tage"} liegen.
ExamRegistrationMustFollowSchoolDuration dayCount@Int: Nach Regeln des Instituts #{pluralDE dayCount "muss" "müssen"} zwischen "Anmeldung ab" und "Anmeldung bis" mindestens #{dayCount} #{pluralDE dayCount "Tag" "Tage"} liegen.
ExamModeRequiredForRegistration: Nach Regeln des Institus muss die "Ausgestaltung der Prüfung" vollständig angegeben sein, bevor "Anmeldung ab" festgelegt werden kann.
ExamModeSchoolDiscouraged: Nach Regeln des Instituts wird von der angegebenen "Ausgestaltung der Prüfung" abgeraten
ExamStaffRequired: „Prüfer:innen/Verantwortilche Hochschullehrer:innen” muss angegeben werden
ExamNotRegistered: Nicht zur Prüfung angemeldet
ExamRegistered: Zur Prüfung angemeldet
ExamsHeading: Prüfungen
ExamCreated exam@ExamName: #{exam} erfolgreich angelegt
ExamNew: Neue Prüfung
ExamDeregisteredSuccess exam@ExamName: Erfolgreich von der Prüfung #{exam} abgemeldet
ExamRegisteredSuccess exam@ExamName: Erfolgreich zur Prüfung #{exam} angemeldet
MailSubjectExamRegistrationInvitation tid@TermId ssh@SchoolId csh@CourseShorthand examn@ExamName: [#{tid}-#{ssh}-#{csh}] Einladung zum Teilnehmer/zur Teilnehmerin für #{examn}
ExamRegistrationInviteHeading examn@ExamName: Einladung zum Teilnehmer/zur Teilnehmerin für #{examn}
ExamRegistrationInviteExplanation: Sie wurden eingeladen, Prüfungsteilnehmer:in zu sein.
ExamUnauthorizedParticipant: Angegebener Benutzer/angegebene Benutzerin ist nicht als Teilnehmer:in dieser Veranstaltung registriert.
ExamRegistrationInvitationAccepted examn@ExamName: Sie wurden als Teilnehmer:in für #{examn} eingetragen
ExamFinishedParticipant: Bewertung voraussichtlich abgeschlossen
ExamLoginToRegister: Um sich zum Kurs anzumelden müssen Sie zunächst in Uni2work anmelden
ExamRegisterForOccurrence: Anmeldung zur Prüfung erfolgt durch Anmeldung zu einem Termin/Raum
ExamShowIdentificationRequired: Prüfungsteilnehmer:innen müssen sich ausweisen können. Halten Sie dafür einen amtlichen Lichtbildausweis (Personalausweis, Reisepass, Aufenthaltstitel) und Ihren Studierendenausweis bereit.
CsvColumnExamUserSurname: Nachname(n) des Teilnehmers/der Teilnehmerin
CsvColumnExamUserFirstName: Vorname(n) des Teilnehmers/der Teilnehmerin
CsvColumnExamUserName: Voller Name des Teilnehmers/der Teilnehmerin (gewöhnlicherweise inkl. Vor- und Nachname(n))
CsvColumnExamUserMatriculation: Matrikelnummer des Teilnehmers/der Teilnehmerin
CsvColumnExamUserOccurrence: Prüfungstermin/-Raum, zu dem der Teilnehmer/die Teilnehmerin angemeldet ist
CsvColumnExamUserExercisePoints: Anzahl von Punkten, die der Teilnehmer/die Teilnehmerin im Übungsbetrieb erreicht hat
CsvColumnExamUserExercisePointsMax: Maximale Anzahl von Punkten, die der Teilnehmer/die Teilnehmerin im Übungsbetrieb bis zu seinem Prüfungstermin erreichen hätte können
CsvColumnExamUserExercisePasses: Anzahl von Übungsblättern, die der Teilnehmer bestanden hat
CsvColumnExamUserExercisePassesMax: Maximale Anzahl von Übungsblättern, die der Teilnehmer/die Teilnehmerin bis zu seinem Prüfungstermin bestehen hätte können
CsvColumnExamUserBonus: Anzurechnende Bonuspunkte
CsvColumnExamUserParts: Erreichte Punktezahlen in den Teilprüfungen, sofern vorhanden; eine Spalte pro Teilprüfung
CsvColumnExamUserResult: Erreichte Prüfungsleistung; "passed", "failed", "no-show", "voided", oder eine Note ("1.0", "1.3", "1.7", ..., "4.0", "5.0")
CsvColumnExamUserCourseNote: Notizen zum Teilnehmer/zur Teilnehmerin
CsvColumnExamOfficeExamUserOccurrenceStart: Prüfungstermin (ISO 8601)
CsvColumnUserStudyFeatures: Alle relevanten Studiendaten des Teilnehmers/der Teilnehmerin als Semikolon (;) separierte Liste
ExamUserCsvName tid@TermId ssh@SchoolId csh@CourseShorthand examn@ExamName: #{foldCase (termToText (unTermKey tid))}-#{foldedCase (unSchoolKey ssh)}-#{foldedCase csh}-#{foldedCase examn}-teilnehmer:in
AchievedPasses: Bestandene Blätter
AchievedPoints: Erreichte Punkte
ExamBonusAchieved: Bonuspunkte
ExamCourseUserNote: Notiz
ExamNoFilter: Keine Einschränkung
ExamUserResetBonus: Auch Bonuspunkte zurücksetzen
ExamUserResetParts: Auch Teilergebnisse zurücksetzen
ExamPoints: Punkte
ExamResultNone: Kein Prüfungsergebnis
ExamAction: Aktion
ExamUsersExamDataRequiresRegistration: Wenn Prüfungsbezogene Daten (Teil-/Ergebnis, Termin/Raum, Bonus) gesetzt bzw. angepasst werden sollen, muss der jeweilige Teilnehmer zur Prüfung angemeldet sein bzw. werden.
ExamNoOccurrence: Kein Termin/Raum
ExamBonusNone: Keine Bonuspunkte
ExamUserCsvCourseNoteDeleted: Notiz wird gelöscht
ExamUsersDeregistered count@Int64: #{show count} Teilnehmer:in von der Prüfung abgemeldet
ExamUsersOccurrenceUpdated count@Int64: Termin/Raum für #{show count} #{pluralDE count "Teilnehmer:in" "Teilnehmer:innen"} gesetzt
ExamUsersResultsAccepted count@Int64: Prüfungsergebnis für #{show count} #{pluralDE count "Teilnehmer:in" "Teilnehmer:innen"} übernommen
ExamUsersResultsReset count@Int64: Prüfungsergebnis für #{show count} #{pluralDE count "Teilnehmer:in" "Teilnehmer:innen"} zurückgesetzt
ExamUsersPartResultsSet count@Int64: Teilprüfungsergebnis für #{show count} #{pluralDE count "Teilnehmer:in" "Teilnehmer:innen"} angepasst
ExamUsersBonusSet count@Int64: Bonuspunkte für #{show count} #{pluralDE count "Teilnehmer:in" "Teilnehmer:innen"} angepasst
ExamUsersResultSet count@Int64: Prüfungsergebnis für #{show count} #{pluralDE count "Teilnehmer:in" "Teilnehmer:innen"} angepasst
ExamUsersHeading: Prüfungsteilnehmer:innen
ExamCorrectHeadDate: Zeit
ExamCorrectHeadParticipant: Teilnehmer:in
ExamCorrectHeadPart exampartnum@ExamPartNumber !ident-ok: #{exampartnum}
ExamCorrectHeadStatus !ident-ok: Status
ExamCorrectUserCandidatesMore: und weitere
ExamCorrectExamResultDelete: Prüfungsergebnis löschen
ExamCorrectExamResultNone: Keine Änderung
ExamCorrectButtonSend: Senden
ExamResultGrade: Note
ExamResultPass: Bestanden/Nicht Bestanden
ExamResultNoShow: Nicht erschienen
ExamResultVoided: Entwertet

#templates /exam-users
ExamCloseHeading: Prüfung abschließen
ExamAutoOccurrenceHeading: Automatische Raum-/Terminverteilung

#templates /exam-show
ExamShowOnline !ident-ok: Online/Offline
ExamShowSynchronicity: Synchron/Asynchron
ExamNever: Nie
ExamShowAids: Erlaubte Hilfsmittel
ExamShowRequiredEquipment: Erforderliche Hilfsmittel
ExamShowRequiredEquipmentNoneSet: Keine Angabe durch die Kursverwalter:innen
ExamClosed: Noten gemeldet
ExamGradingRule: Notenberechnung
ExamBonusRule: Prüfungsbonus aus Übungsbetrieb
ExamOccurrenceRule: Verfahren
ExamOccurrenceRuleParticipant: Termin- bzw. Raumzuteilungsverfahren
ExamRegisteredCount: Anmeldungen
ExamRegisteredCountOf num@Int64 count@Int64 !ident-ok: #{num}/#{count}
ExamOccurrences: Termine
GradingFrom: Ab
ExamNoShow: Nicht erschienen
ExamVoided: Entwertet

#templates eidgets/bonus-rule
ExamBonusManualParticipants: Von den Kursverwalter:innen manuell berechnet
ExamBonusPoints possible@Points: Maximal #{showFixed True possible} Prüfungspunkte
ExamBonusPointsPassed possible@Points: Maximal #{showFixed True possible} Prüfungspunkte, falls die Prüfung auch ohne Bonus bereits bestanden ist

ExamUserResetToComputedResult: Prüfungsergebnis zurücksetzen

UtilExamBonusRule: Prüfungsbonus aus Übungsbetrieb
ExamBonusPoints': Umrechnung von Übungspunkten
ExamBonusManual': Manuelle Berechnung
ExamBonusMaxPoints: Maximal erreichbare Prüfungs-Bonuspunkte
ExamBonusMaxPointsTip: Bonuspunkte werden, anhand der erreichten Übungspunkte bzw. der Anzahl von bestandenen Übungsblättern, linear zwischen null und der angegebenen Schranke interpoliert.
ExamBonusMaxPointsNonPositive: Maximaler Prüfungsbonus muss positiv und größer null sein
ExamBonusOnlyPassed: Bonus nur nach Bestehen anrechnen
ExamBonusRound: Bonus runden auf
ExamBonusRoundNonPositive: Vielfaches, auf das gerundet werden soll, muss positiv und größer null sein
ExamBonusRoundTip: Bonuspunkte werden kaufmännisch auf ein Vielfaches der angegeben Zahl gerundet.
ExamAutomaticOccurrenceAssignment: Termin- bzw. Raumzuteilung
ExamAutomaticOccurrenceAssignmentTip: Sollen Prüfungsteilnehmer automatisch auf die zur Verfügung stehenden Räume bzw. Termine verteilt werden, sich selbstständig einen Raum bzw. Termin aussuchen dürfen oder manuell durch Kursverwalter zugeteilt werden? Die automatische Verteilung muss von einem Kursverwalter ausgelöst werden und geschieht nicht mit Ablauf einer Frist o.Ä.. Manuelle Umverteilung bzw. vorheriges Festlegen von Zuteilungen einzelner Teilnehmer ist somit immer möglich.
UtilExamGradingRule: Notenberechnung
ExamGradingKey': Nach Schlüssel
ExamGradingKey: Notenschlüssel
ExamGradingKeyTip: Die Grenzen beziehen sich auf die effektive Maximalpunktzahl, nachdem etwaige Bonuspunkte aus dem Übungsbetrieb angerechnet und die Ergebnise der Teilprüfungen mit ihrem Gewicht multipliziert wurden
Points: Punkte
PointsMustBeNonNegative: Punktegrenzen dürfen nicht negativ sein
PointsMustBeMonotonic: Punktegrenzen müssen aufsteigend sein

ExamModeFormNone: Keine Angabe
ExamModeFormCustom: Benutzerdefiniert
ExamModeFormAids: Erlaubte Hilfsmittel
ExamModeFormOnline !ident-ok: Online/Offline
ExamModeFormSynchronicity: Synchron/Asynchron
ExamModeFormRequiredEquipment: Erforderliche Hilfsmittel
ExamModeFormRequiredEquipmentIdentificationTip: Es wird stets ein Hinweis angezeigt, dass Teilnehmer sich ausweisen können müssen.

ExamRoomMappingRandomHere: Zufällig
ExamAutoOccurrenceExceptionRuleNoOp: Kein Verfahren zur automatischen Verteilung gewählt
ExamAutoOccurrenceExceptionNotEnoughSpace: Mehr Teilnehmende als verfügbare Plätze
ExamAutoOccurrenceExceptionNoUsers: Nach dem gewähltem Verfahren können keine Teilnehmenden verteilt werden
ExamAutoOccurrenceExceptionRoomTooSmall: Automatische Verteilung gescheitert. Ein anderes Verteil-Verfahren kann erfolgreich sein. Alternativ kann es helfen Räume zu minimieren oder kleine Räume zu entfernen.
ExamBonusInfoPoints: Zur Berechnung von Bonuspunkten werden nur jene Blätter herangezogen, deren Aktivitätszeitraum vor Start des jeweiligen Termin/Prüfung begonnen hat
ExamUserCsvSheetName tid@TermId ssh@SchoolId csh@CourseShorthand examn@ExamName: #{foldCase (termToText (unTermKey tid))}-#{foldedCase (unSchoolKey ssh)}-#{foldedCase csh}-#{foldedCase examn} Teilnehmer

ExamRoomCapacityTip: Maximale Anzahl an Prüfungsteilnehmern für diesen Termin/Raum; leer lassen  für unbeschränkte Teilnehmeranzahl
ExamRoomMappingRandom: Verteilung
ExamFinishHeading: Prüfungsergebnisse sichtbar schalten
ExamEditWouldBreakSheetTypeReference: Durch Ihre Änderungen würde ein Prüfungsteil gelöscht, auf den durch ein Übungsblatt noch eine Referenz besteht.
ExamEditExamNameTaken exam@ExamName: Es existiert bereits eine Prüfung mit Namen #{exam}

Date: Datum
ExamRegistrationRegisteredWithoutField n@Int: #{n} #{pluralDE n "Teilnehmer:in wurde" "wurden"} sowohl zur Prüfung, als auch #{pluralDE n "Teilnehmer:innen ohne assoziiertes Studienfach" "ohne assoziierte Studienfächer"} zum Kurs angemeldet, da #{pluralDE n "kein eindeutiges Hauptfach bestimmt werden konnte" "keine eindeutigen Hauptfächer bestimmt werden konnten"}
ExamRegistrationParticipantsRegistered n@Int: #{n} #{pluralDE n "Teilnehmer:in wurde" "Teilnehmer:innen wurden"} zur Prüfung angemeldet
ExamOpenBook: Open Book
ExamClosedBook: Closed Book
ExamOnline !ident-ok: Online
ExamOffline !ident-ok: Offline
ExamSynchronous: Synchron
ExamAsynchronous: Asynchron
ExamRequiredEquipmentNone: Nichts
ExamRequiredEquipmentPen: Stift
ExamRequiredEquipmentPaperPen: Stift & Papier
ExamRequiredEquipmentCalculatorPen: Stift & Taschenrechner
ExamRequiredEquipmentCalculatorPaperPen: Stift, Papier & Taschenrechner
ExamRequiredEquipmentWebcamMicrophoneInternet: Webcam & Mikrophon
ExamRequiredEquipmentMicrophoneInternet: Mikrophon
ExamRegistrationTime: Angemeldet seit

ExamUserDeregister: Teilnehmer:in von Prüfung abmelden
ExamUserAssignOccurrence: Termin/Raum zuweisen
ExamUserAcceptComputedResult: Berechnetes Prüfungsergebnis übernehmen
ExamUserSetPartResult: Teilergebnis setzen
ExamUserSetBonus: Bonuspunkte setzen
ExamUserSetResult: Prüfungsergebnis setzen
ExamUserMarkSynchronised: Prüfungsleistung als synchronisiert markieren
ExamUserCsvCourseRegister: Benutzer:in zum Kurs und zur Prüfung anmelden
ExamUserCsvRegister: Kursteilnehmer:in zur Prüfung anmelden
ExamUserCsvAssignOccurrence: Teilnehmer:innen einen anderen Termin/Raum zuweisen
ExamUserCsvDeregister: Teilnehmer:in von der Prüfung abmelden
ExamUserCsvOverrideBonus: Bonuspunkte entgegen Bonusregelung überschreiben
ExamUserCsvOverrideResult: Ergebnis entgegen automatischer Notenberechnung überschreiben
ExamUserCsvSetBonus: Bonuspunkte eintragen
ExamUserCsvSetResult: Ergebnis eintragen
ExamUserCsvSetPartResult: Ergebnis einer Teilprüfung eintragen
ExamUserCsvSetCourseNote: Teilnehmer-Notizen anpassen
ExamUserCsvExceptionNoMatchingUser: Benutzer:in konnte nicht eindeutig identifiziert werden. Alle Identifikatoren des Benutzers/der Benutzerin (Vorname(n), Nachname, Voller Name, Matrikelnummer, ...) müssen exakt übereinstimmen. Sie können versuchen für diese Zeile manche der Identifikatoren zu entfernen (also z.B. nur eine Matrikelnummer angeben) um dem System zu erlauben nur Anhand der verbleibenden Identifikatoren zu suchen. Sie sollten dann natürlich besonders kontrollieren, dass das System den fraglichen Benutzer/die fragliche Benutzerin korrekt identifiziert hat.
ExamUserCsvExceptionMultipleMatchingUsers: Benutzer:in konnte nicht eindeutig identifiziert werden. Es wurden mehrere Benutzer:innen gefunden, welche mit den gegebenen Identifikatoren übereinstimmen. Sie können versuchen, für diese Zeile weitere Identifikatoren anzugeben damit nur noch der gewünschte Benutzer/die gewünschte Benutzerin mit diesen identifiziert werden kann.
ExamUserCsvExceptionNoMatchingStudyFeatures: Das angegebene Studienfach konnte keinem Studienfach des Benutzers/der Benutzerin zugeordnet werden. Sie können versuchen für diese Zeile die Studiengangsdaten zu entfernen um das System automatisch ein Studienfach wählen zu lassen.
ExamUserCsvExceptionNoMatchingOccurrence: Raum/Termin konnte nicht eindeutig identifiziert werden. Überprüfen Sie, dass diese Zeile nur interne Raumbezeichnungen enthält, wie sie auch für die Prüfung konfiguriert wurden.
ExamUserCsvExceptionMismatchedGradingMode expectedGradingMode@ExamGradingMode actualGradingMode@ExamGradingMode: Es wurde versucht eine Prüfungsleistung einzutragen, die zwar vom System interpretiert werden konnte, aber nicht dem für diese Prüfung erwarteten Modus entspricht. Der erwartete Bewertungsmodus kann unter "Prüfung bearbeiten" angepasst werden ("Bestanden/Nicht Bestanden", "Numerische Noten" oder "Gemischt").
ExamUserCsvExceptionNoOccurrenceTime: Es wurde versucht eine Prüfungsleistung ohne einen zugehörigen Zeitpunkt einzutragen. Sie können entweder einen Zeitpunkt pro Student in der entsprechenden Spalte hinterlegen, oder einen voreingestellten Zeitpunkt unter "Bearbeiten" angeben.
TitleExamAutoOccurrence tid@TermId ssh@SchoolId csh@CourseShorthand examn@ExamName: #{tid} - #{ssh} - #{csh} #{examn}: Automatische Raum-/Terminverteilung
ExamGradingPass: Bestanden/Nicht Bestanden
ExamGradingGrades: Numerische Noten
ExamGradingMixed: Gemischt
ExamFinished: Ergebnisse sichtbar ab

ExamAuthorshipStatementSection: Eigenständigkeitserklärung
ExamAuthorshipStatementRequired: Eigenständigkeitserklärung für prüfungszugehörige Übungsblattabgaben einfordern?
ExamAuthorshipStatementRequiredTip: Sollen für alle zu dieser Prüfung zugehörige Übungsblätter die Abgebenden (bei Abgabegruppen jedes Gruppenmitglied) aufgefordert werden, eine Eigenständigkeitserklärung zu akzeptieren?
ExamAuthorshipStatementRequiredForcedTip: Für dieses Institut ist vorgeschrieben, dass für alle zu diese Prüfung zugehörigen Übungsblätter die Abgebenden (bei Abgabegruppen jedes Gruppenmitglied) aufgefordert werden, eine Eigenständigkeitserklärung zu akzeptieren.
ExamAuthorshipStatementContent: Eigenständigkeitserklärung
ExamAuthorshipStatementContentForcedTip: Für dieses Institut ist die institutsweit vorgegebene Eigenständigkeitserklärung für prüfungsrelevante Übungsblätter zu verwenden. Benutzerdefinierte Erklärungen sind nicht gestattet. Für alle zu diese Prüfung zugehörigen Übungsblätter werden die Abgebenden (bei Abgabegruppen jedes Gruppenmitglied) aufgefordert, diese Eigenständigkeitserklärung zu akzeptieren.