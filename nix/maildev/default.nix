# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

final: prev:
{
  nodePackages = (import ./node2nix.nix { pkgs = final; inherit (final.config) system; }) // prev.nodePackages;
}
