#!/usr/bin/env zsh

set -e

# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

typeset -a requiredLangs
requiredLangs=(de en)


fix=1
while getopts ':f' arg; do
    case $arg in
	f) fix=0 ;;
	\*) print nothing: $OPTARG; exit 2;;
	\?) print invalid option: $OPTARG; exit 2;;
    esac
done
shift $OPTIND-1


function translations() {
    msgFile=$1

    sed -r 's/^([^ :]+).*$/\1/' ${msgFile} \
	| sed -r '/^\s*#/d' \
	| sort
}

typeset -a msgFiles
msgFiles=(messages/**/*.msg(.N))

typeset -a msgDirectories
msgDirectories=()
for msgFile (${msgFiles}); do
    if ! [[ ${msgDirectories[(ie)${msgFile:h}]} -le ${#msgDirectories} ]]; then
	msgDirectories+=(${msgFile:h})
    fi
done

msgDifference=0

for msgDirectory (${msgDirectories}); do
    typeset -a dirMsgFiles
    dirMsgFiles=()
    for msgFile (${msgFiles}); do
	if [[ ${msgFile:h} == ${msgDirectory} ]]; then
	    dirMsgFiles+=(${msgFile})
	fi
    done

    for lang (${requiredLangs}); do
	foundLang=0
	for msgFile (${dirMsgFiles}); do
	    [[ ${msgFile:t} =~ "^${lang}[-.]" ]] || continue
	    foundLang=1
	    break
	done

	if [[ $foundLang -ne 1 ]]; then
	    msgDifference=1
	    printf "%s missing required language %s\n" $msgDirectory $lang
	fi
    done

    (
	diffDir=""
	function cleanup() {
	    cd
	    [[ -n ${diffDir} ]] && rm -rf ${diffDir}
	}
	trap cleanup EXIT
	diffDir=$(mktemp -d)

	typeset -a diffArgs
	diffArgs=()

	for msgFile (${dirMsgFiles}); do
	    translations ${msgFile} > ${diffDir}/${msgFile:t}
	    diffArgs+=(-L ${msgFile} ${diffDir}/${msgFile:t})
	done

	# printf ">>> %s\n" ${msgDirectory}
	if [[ $fix != 0 ]]; then
            if [[ ${#dirMsgFiles} -gt 1 ]]; then
	        diff -u0 --suppress-common-lines -wB ${diffArgs} | grep -vE '^@@.*@@'
	        diffStatus=$pipestatus[1]
            else
                diffStatus=1
            fi
	else
            if [[ ${#dirMsgFiles} -gt 1 ]]; then
	        diff -u0 --suppress-common-lines -wB ${diffArgs} >/dev/null
	        diffStatus=$?
            else
                diffStatus=1
            fi

	    if [[ ${diffStatus} == 1 ]]; then
		./translate.hs msgs ${dirMsgFiles} && diffStatus=0
	    fi
	fi

	return ${diffStatus}
    ) || msgDifference=1

    if [[ $fix == 0 && $msgDifference != 0 ]]; then
	exit 1
    fi
done


typeset -a templateFiles
templateFiles=(templates/i18n/**/*(.))

templateDifference=0

for templateDirectory (templates/i18n/**/*(FN)); do
    # printf ">>> %s\n" ${templateDirectory}
    
    typeset -a templateExtensions
    templateExtensions=()
    for templateFile (${templateFiles}); do
	[[ ${templateFile:h} == ${templateDirectory} ]] || continue
	if ! [[ ${templateExtensions[(ie)${templateFile:e}]} -le ${#templateExtensions} ]]; then
	    templateExtensions+=(${templateFile:e})
	fi
    done

    typeset -a templatePrefixes
    templatePrefixes=()
    for templateFile (${templateFiles}); do
	[[ ${templateFile:h} == ${templateDirectory} ]] || continue

        templatePrefix=$(sed -r 's/^(.*\.)?[^.]+\.[^.]+$/\1/' <<<"${templateFile:t}")

	if ! ((${templatePrefixes[(Ie)${templatePrefix}]})); then
	    templatePrefixes+=("${templatePrefix}")
	fi
    done

    # printf "%d %s\n" ${#templatePrefixes} "${templatePrefixes}"

    for ext (${templateExtensions}); do
	for lang (${requiredLangs}); do
            for prefixQ (${(q)templatePrefixes}); do
                prefix=${(Q)prefixQ}
                # printf ">> %s %s %s\n" ${prefix} ${lang} ${ext}
                
	        foundLang=1
	        for templateFile (${templateDirectory}/*.${ext}); do
                    # printf "%s\n" ${templateFile}
		    [[ ${templateFile:t} =~ "^${prefix}${lang}[-.]" ]] || continue
                    # printf "match\n"
		    foundLang=0
		    break
	        done
                
                # printf ">> %s\n" ${foundLang}
                
	        if [[ $foundLang -ne 0 ]]; then
		    templateDifference=1
		    [[ $fix != 0 ]] && printf "%s: %s*.%s (%s)\n" "$templateDirectory" "$prefix" "$ext" "$lang"
                    
		    if [[ $fix == 0 ]]; then
		        ./translate.hs dir $templateDirectory && templateDifference=0
		    fi
	        fi
            done
	done
    done
done

if [[ $msgDifference -ne 0 || $templateDifference -ne 0 ]]; then
    exit 1
fi
