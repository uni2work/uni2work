# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "master";
    };
    flake-utils = {
      type = "github";
      owner = "numtide";
      repo = "flake-utils";
      ref = "master";
    };
  };

  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem
    (system:
      let pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
          overlay = import ./nix/maildev;

          inherit (pkgs.lib) recursiveUpdate;
      in {
        devShell = import ./shell.nix { pkgs = self.legacyPackages.${system}; nixpkgsPath = nixpkgs; };

        legacyPackages = recursiveUpdate (overlay self.legacyPackages.${system} pkgs) pkgs;
      }
    );
}
