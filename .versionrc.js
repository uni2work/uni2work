// SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const standardVersionUpdaterYaml = require.resolve('standard-version-updater-yaml');
 
module.exports = {
  scripts: {
    // postbump: './sync-versions.hs && git add -- package.yaml', // moved to bumpFiles
    postchangelog: 'sed \'s/^### \\[/## [/g\' -i CHANGELOG.md'
  },
  packageFiles: ['package.json', 'package.yaml'],
  bumpFiles: [
    {
      filename: 'package.json',
      type: 'json'
    },
    {
      filename: 'package-lock.json',
      type: 'json'
    },
    {
      filename: 'package.yaml',
      updater: standardVersionUpdaterYaml
    }
  ],
  commitUrlFormat: 'https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/commit/{{hash}}',
  compareUrlFormat: 'https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/compare/{{previousTag}}...{{currentTag}}',
  issueUrlFormat: 'https://gitlab2.rz.ifi.lmu.de/uni2work/uni2work/issues/{{id}}',
  userUrlFormat: 'https://gitlab2.rz.ifi.lmu.de/{{user}}'
};
