#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later


set -e

[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -en .stack-work.lock "$0" "$@" || :

move-back() {
    mv -vT .stack-work .stack-work-test
    [[ -d .stack-work-build ]] && mv -vT .stack-work-build .stack-work
}

if [[ -d .stack-work-test ]]; then
    [[ -d .stack-work ]] && mv -vT .stack-work .stack-work-build
    mv -vT .stack-work-test .stack-work
    trap move-back EXIT
fi

exec -- stack build --test --fast --flag uniworx:dev --flag uniworx:library-only uniworx:test:hlint
