-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module WFLint
  ( main
  ) where

import Import
import Utils.Workflow.Lint
import Handler.Utils.Workflow.Form (FormWorkflowGraph)

import qualified Data.ByteString as ByteString
import qualified Data.Yaml as Yaml

import System.IO (hPutStrLn)
import System.Exit


exitParseError, exitLintIssues :: Int
exitParseError = 2
exitLintIssues = 3
  
die' :: (MonadIO m, Exception (Element mono), MonoFoldable mono) => Handle -> Int -> mono -> m a
die' h err excs = liftIO $ do
  forM_ excs $ hPutStrLn h . displayException
  exitWith $ ExitFailure err

main :: IO ()
main = do
  mwf <- Yaml.decodeEither' <$> ByteString.getContents
  (wf :: FormWorkflowGraph) <- either (die' stderr exitParseError . Identity) return mwf
  for_ (lintWorkflowGraph wf) $ die' stdout exitLintIssues
