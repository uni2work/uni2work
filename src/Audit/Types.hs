-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@ifi.lmu.de>, Sarah Vaupel <sarah.vaupel@uniworx.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Audit.Types
  ( Transaction(..)
  , Issue(..)
  ) where

import ClassyPrelude.Yesod hiding (derivePersistFieldJSON)

import Model
import Model.Types.TH.JSON

import Utils.PathPiece

import Data.Aeson.TH


data Transaction
  = TransactionTermEdit
    { transactionTerm   :: TermId
    }

  | TransactionExamRegister
    { transactionExam   :: ExamId
    , transactionUser   :: UserId
    }
  | TransactionExamDeregister
    { transactionExam   :: ExamId
    , transactionUser   :: UserId
    }

  | TransactionExamPartResultEdit
    { transactionExamPart :: ExamPartId
    , transactionUser     :: UserId
    }
  | TransactionExamPartResultDeleted
    { transactionExamPart :: ExamPartId
    , transactionUser     :: UserId
    }

  | TransactionExamBonusEdit
    { transactionExam   :: ExamId
    , transactionUser   :: UserId
    }
  | TransactionExamBonusDeleted
    { transactionExam   :: ExamId
    , transactionUser   :: UserId
    }

  | TransactionExamResultEdit
    { transactionExam   :: ExamId
    , transactionUser   :: UserId
    }
  | TransactionExamResultDeleted
    { transactionExam   :: ExamId
    , transactionUser   :: UserId
    }

  | TransactionCourseParticipantEdit
    { transactionCourse :: CourseId
    , transactionUser   :: UserId
    }
  | TransactionCourseParticipantDeleted
    { transactionCourse :: CourseId
    , transactionUser   :: UserId
    }

  | TransactionCourseApplicationEdit
    { transactionCourse :: CourseId
    , transactionUser   :: UserId
    , transactionCourseApplication :: CourseApplicationId
    }
  | TransactionCourseApplicationDeleted
    { transactionCourse :: CourseId
    , transactionUser   :: UserId
    , transactionCourseApplication :: CourseApplicationId
    }

  | TransactionSubmissionEdit
    { transactionSubmission :: SubmissionId
    , transactionSheet      :: SheetId
    }
  | TransactionSubmissionDelete
    { transactionSubmission :: SubmissionId
    , transactionSheet      :: SheetId
    }

  | TransactionSubmissionUserEdit
    { transactionSubmission :: SubmissionId
    , transactionUser       :: UserId
    }
  | TransactionSubmissionUserDelete
    { transactionSubmission :: SubmissionId
    , transactionUser       :: UserId
    }

  | TransactionSubmissionFileEdit
    { transactionSubmissionFile :: Entity SubmissionFile
    }
  | TransactionSubmissionFileDelete
    { transactionSubmissionFile :: Entity SubmissionFile
    }

  | TransactionExamOfficeUserAdd
    { transactionOffice :: UserId
    , transactionUser :: UserId
    }
  | TransactionExamOfficeUserDelete
    { transactionOffice :: UserId
    , transactionUser :: UserId
    }
  | TransactionExamOfficeFieldEdit
    { transactionOffice :: UserId
    , transactionField :: StudyTermsId
    , transactionFieldType :: StudyFieldType
    }
  | TransactionExamOfficeFieldDelete
    { transactionOffice :: UserId
    , transactionField :: StudyTermsId
    , transactionFieldType :: StudyFieldType
    }
  | TransactionTutorialEdit
    { transactionTutorial :: TutorialId
    }
  | TransactionTutorialDelete
    { transactionTutorial :: TutorialId
    }

  | TransactionExternalExamEdit
    { transactionExternalExam :: ExternalExamId
    }

  | TransactionExternalExamOfficeSchoolEdit
    { transactionExternalExam :: ExternalExamId
    , transactionSchool       :: SchoolId
    }
  | TransactionExternalExamOfficeSchoolDelete
    { transactionExternalExam :: ExternalExamId
    , transactionSchool       :: SchoolId
    }

  | TransactionExternalExamStaffEdit
    { transactionExternalExam :: ExternalExamId
    , transactionUser         :: UserId
    }
  | TransactionExternalExamStaffDelete
    { transactionExternalExam :: ExternalExamId
    , transactionUser         :: UserId
    }
  | TransactionExternalExamStaffInviteEdit
    { transactionExternalExam :: ExternalExamId
    , transactionEmail        :: UserEmail
    }
  | TransactionExternalExamStaffInviteDelete
    { transactionExternalExam :: ExternalExamId
    , transactionEmail        :: UserEmail
    }

  | TransactionExternalExamResultEdit
    { transactionExternalExam :: ExternalExamId
    , transactionUser         :: UserId
    }
  | TransactionExternalExamResultDelete
    { transactionExternalExam :: ExternalExamId
    , transactionUser         :: UserId
    }

  | TransactionSubmissionGroupSet
    { transactionCourse       :: CourseId
    , transactionUser         :: UserId
    , transactionSubmissionGroup :: SubmissionGroupName
    }
  | TransactionSubmissionGroupUnset
    { transactionCourse       :: CourseId
    , transactionUser         :: UserId
    }

  | TransactionUserAssimilated
    { transactionUser
    , transactionAssimilatedUser :: UserId
    }
  | TransactionUserIdentChanged
    { transactionOldUserIdent
    , transactionNewUserIdent    :: UserIdent
    }

  | TransactionAllocationUserEdited
    { transactionUser       :: UserId
    , transactionAllocation :: AllocationId
    }
  | TransactionAllocationUserDeleted
    { transactionUser       :: UserId
    , transactionAllocation :: AllocationId
    }
  
  deriving (Eq, Ord, Read, Show, Generic, Typeable)

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  , fieldLabelModifier     = camelToPathPiece' 1
  , tagSingleConstructors  = True
  , sumEncoding            = TaggedObject "transaction" "data"
  } ''Transaction

derivePersistFieldJSON ''Transaction


data Issue
  = IssueLdapParseError
    { issueLdapUserIdent :: UserIdent
    , issueLdapInput     :: Text
    , issueLdapError     :: Text
    }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  , fieldLabelModifier     = camelToPathPiece' 1
  , tagSingleConstructors  = True
  , sumEncoding            = TaggedObject "issue" "data"
  } ''Issue

derivePersistFieldJSON ''Issue
