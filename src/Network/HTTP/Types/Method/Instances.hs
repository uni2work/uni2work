-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Network.HTTP.Types.Method.Instances
  (
  ) where

import ClassyPrelude
import Data.Binary (Binary)

import Network.HTTP.Types.Method

import Utils.PathPiece (pathPieceJSON, pathPieceJSONKey)

import Web.PathPieces


deriving stock instance Generic StdMethod
deriving anyclass instance Binary StdMethod
deriving anyclass instance Hashable StdMethod

instance PathPiece Method where
  toPathPiece = decodeUtf8
  fromPathPiece = Just . encodeUtf8

pathPieceJSON ''Method
pathPieceJSONKey ''Method
