-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Network.Minio.Instances
  (
  ) where

import ClassyPrelude

import Network.Minio

import qualified UnliftIO.Exception as UnliftIO
import Control.Monad.Catch


instance MonadThrow Minio where
  throwM = UnliftIO.throwIO

instance MonadCatch Minio where
  catch = UnliftIO.catch

instance MonadMask Minio where
  mask = UnliftIO.mask
  uninterruptibleMask = UnliftIO.uninterruptibleMask
  generalBracket acq rel inner = withUnliftIO $ \UnliftIO{..} -> generalBracket (unliftIO acq) ((unliftIO .) . rel) $ unliftIO . inner
