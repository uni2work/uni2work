-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.SafeYAML
  ( decodeSafeYAMLFileThrow
  ) where

import ClassyPrelude

import Data.SafeJSON (SafeJSON, safeFromJSON)
import qualified Data.Yaml as Yaml
import Data.Aeson.Types (parseEither)

decodeSafeYAMLFileThrow :: (MonadIO m, SafeJSON a) => FilePath -> m a
decodeSafeYAMLFileThrow f = Yaml.decodeFileThrow f >>= either (throwIO . Yaml.AesonException) return . parseEither safeFromJSON
