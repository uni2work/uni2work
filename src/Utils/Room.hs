-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Room
  ( roomReferenceText
  ) where

import Import.NoModel
import Model.Types.Room

roomReferenceText :: RoomReference -> Text
roomReferenceText = \case
  RoomReferenceSimple{roomRefText} -> roomRefText
  RoomReferenceLink{roomRefLink} -> pack $ uriToString id roomRefLink mempty
