-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Workflow
  ( RouteWorkflowScope, DBWorkflowScope, IdWorkflowScope, CryptoIDWorkflowScope
  , _DBWorkflowScope
  , fromRouteWorkflowScope, toRouteWorkflowScope
  , DBWorkflowGraph, IdWorkflowGraph
  , _DBWorkflowGraph
  , DBWorkflowGraphAuth, IdWorkflowGraphAuth
  , _DBWorkflowGraphAuth
  , DBWorkflowActionUser, IdWorkflowActionUser
  , _DBWorkflowActionUser
  , DBWorkflowFieldPayloadW, IdWorkflowFieldPayloadW
  , _DBWorkflowFieldPayloadW
  , DBWorkflowOverviewSpec, IdWorkflowOverviewSpec
  , _DBWorkflowOverviewSpec
  , decryptWorkflowStateIndex, encryptWorkflowStateIndex
  , isTopWorkflowScope, isTopWorkflowScopeSql
  , selectWorkflowInstanceDescription
  , SharedWorkflowGraphException(..), getSharedDBWorkflowGraph, getSharedIdWorkflowGraph
  , insertSharedWorkflowGraph
  , SharedWorkflowOverviewSpecException(..), getSharedDBWorkflowOverviewSpec, getSharedIdWorkflowOverviewSpec
  , insertSharedWorkflowOverviewSpec
  , matchesWorkflowWorkflowListType
  ) where

import Import.NoFoundation
-- import Foundation.Type

import qualified Data.CryptoID.Class.ImplicitNamespace as I
import qualified Crypto.MAC.KMAC as Crypto
import qualified Data.ByteArray as BA
import qualified Data.Binary as Binary
import Crypto.Hash.Algorithms (SHAKE256)
import qualified Crypto.Hash as Crypto
import Language.Haskell.TH (nameBase)
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Safe as SafeJSON

-- import Handler.Utils.Memcached

import qualified Database.Esqueleto.Legacy as E
import qualified Database.Esqueleto.Utils as E
-- import qualified Database.Esqueleto.Internal.Internal as E

{-# ANN module ("HLint: ignore Use newtype instead of data" :: String) #-}


type RouteWorkflowScope = WorkflowScope TermId SchoolId (TermId, SchoolId, CourseShorthand)
type DBWorkflowScope = WorkflowScope TermIdentifier SchoolShorthand SqlBackendKey
type IdWorkflowScope = WorkflowScope TermId SchoolId CourseId
type CryptoIDWorkflowScope = WorkflowScope TermId SchoolId CryptoUUIDCourse


_DBWorkflowScope :: Iso' IdWorkflowScope DBWorkflowScope
_DBWorkflowScope = iso toScope' toScope
  where
    toScope' scope = scope
      & over (typesCustom @WorkflowChildren @(WorkflowScope TermId SchoolId CourseId) @(WorkflowScope TermIdentifier SchoolId CourseId)) unTermKey
      & over (typesCustom @WorkflowChildren @(WorkflowScope TermIdentifier SchoolId CourseId) @(WorkflowScope TermIdentifier SchoolShorthand CourseId)) unSchoolKey
      & over (typesCustom @WorkflowChildren @(WorkflowScope TermIdentifier SchoolShorthand CourseId) @(WorkflowScope TermIdentifier SchoolShorthand SqlBackendKey) @CourseId @SqlBackendKey) (view _SqlKey)
    toScope scope' = scope'
      & over (typesCustom @WorkflowChildren @(WorkflowScope TermIdentifier SchoolShorthand SqlBackendKey) @(WorkflowScope TermId SchoolShorthand SqlBackendKey)) TermKey
      & over (typesCustom @WorkflowChildren @(WorkflowScope TermId SchoolShorthand SqlBackendKey) @(WorkflowScope TermId SchoolId SqlBackendKey)) SchoolKey
      & over (typesCustom @WorkflowChildren @(WorkflowScope TermId SchoolId SqlBackendKey) @(WorkflowScope TermId SchoolId CourseId) @SqlBackendKey @CourseId) (review _SqlKey)

fromRouteWorkflowScope :: ( MonadHandler m
                          , BackendCompatible SqlReadBackend backend
                          )
                       => RouteWorkflowScope
                       -> MaybeT (ReaderT backend m) IdWorkflowScope
fromRouteWorkflowScope rScope = $cachedHereBinary rScope . hoist (withReaderT $ projectBackend @SqlReadBackend) . forOf (typesCustom @WorkflowChildren @(WorkflowScope TermId SchoolId (TermId, SchoolId, CourseShorthand)) @(WorkflowScope TermId SchoolId CourseId) @(TermId, SchoolId, CourseShorthand) @CourseId) rScope $ \(tid, ssh, csh) -> MaybeT . getKeyBy $ TermSchoolCourseShort tid ssh csh

toRouteWorkflowScope :: ( MonadHandler m
                        , BackendCompatible SqlReadBackend backend
                        )
                     => IdWorkflowScope
                     -> MaybeT (ReaderT backend m) RouteWorkflowScope
toRouteWorkflowScope scope = $cachedHereBinary scope . hoist (withReaderT $ projectBackend @SqlReadBackend) . forOf (typesCustom @WorkflowChildren @(WorkflowScope TermId SchoolId CourseId) @(WorkflowScope TermId SchoolId (TermId, SchoolId, CourseShorthand)) @CourseId @(TermId, SchoolId, CourseShorthand)) scope $ \cId -> MaybeT (get cId) <&> \Course{..} -> (courseTerm, courseSchool, courseShorthand)


type IdWorkflowGraph = WorkflowGraph FileReference UserId
type DBWorkflowGraph = WorkflowGraph FileReference SqlBackendKey


_DBWorkflowGraph :: Iso' IdWorkflowGraph DBWorkflowGraph
_DBWorkflowGraph = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowGraph FileReference UserId) @(WorkflowGraph FileReference SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowGraph FileReference SqlBackendKey) @(WorkflowGraph FileReference UserId) @SqlBackendKey @UserId) (review _SqlKey)


type IdWorkflowGraphAuth = WorkflowGraphAuth UserId
type DBWorkflowGraphAuth = WorkflowGraphAuth SqlBackendKey

_DBWorkflowGraphAuth :: Iso' IdWorkflowGraphAuth DBWorkflowGraphAuth
_DBWorkflowGraphAuth = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowGraphAuth UserId) @(WorkflowGraphAuth SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowGraphAuth SqlBackendKey) @(WorkflowGraphAuth UserId) @SqlBackendKey @UserId) (review _SqlKey)


type IdWorkflowActionUser = WorkflowActionUser UserId
type DBWorkflowActionUser = WorkflowActionUser SqlBackendKey


_DBWorkflowActionUser :: Iso' IdWorkflowActionUser DBWorkflowActionUser
_DBWorkflowActionUser = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowActionUser UserId) @(WorkflowActionUser SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowActionUser SqlBackendKey) @(WorkflowActionUser UserId) @SqlBackendKey @UserId) (review _SqlKey)


type IdWorkflowFieldPayloadW = WorkflowFieldPayloadW FileReference UserId
type DBWorkflowFieldPayloadW = WorkflowFieldPayloadW FileReference SqlBackendKey


_DBWorkflowFieldPayloadW :: Iso' IdWorkflowFieldPayloadW DBWorkflowFieldPayloadW
_DBWorkflowFieldPayloadW = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowFieldPayloadW FileReference UserId) @(WorkflowFieldPayloadW FileReference SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowFieldPayloadW FileReference SqlBackendKey) @(WorkflowFieldPayloadW FileReference UserId) @SqlBackendKey @UserId) (review _SqlKey)


type IdWorkflowOverviewSpec = WorkflowOverviewSpec UserId
type DBWorkflowOverviewSpec = WorkflowOverviewSpec SqlBackendKey


_DBWorkflowOverviewSpec :: Iso' IdWorkflowOverviewSpec DBWorkflowOverviewSpec
_DBWorkflowOverviewSpec = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowOverviewSpec UserId) @(WorkflowOverviewSpec SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowOverviewSpec SqlBackendKey) @(WorkflowOverviewSpec UserId) @SqlBackendKey @UserId) (review _SqlKey)


data WorkflowStateIndexKeyException
  = WorkflowStateIndexCryptoIDKeyCouldNotDecodeRandom
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

workflowStateIndexCryptoIDKey :: (MonadCrypto m, MonadCryptoKey m ~ CryptoIDKey) => WorkflowWorkflowId -> m CryptoIDKey
workflowStateIndexCryptoIDKey wwId = cryptoIDKey $ \cIDKey -> either (const $ throwM WorkflowStateIndexCryptoIDKeyCouldNotDecodeRandom) (views _3 return) . Binary.decodeOrFail . fromStrict . BA.convert $
  Crypto.kmac @(SHAKE256 CryptoIDCipherKeySize) (encodeUtf8 . (pack :: String -> Text) $ nameBase 'workflowStateIndexCryptoIDKey) (toStrict $ Binary.encode wwId) cIDKey

encryptWorkflowStateIndex :: ( MonadCrypto m
                             , MonadCryptoKey m ~ CryptoIDKey
                             , MonadHandler m
                             )
                          => WorkflowWorkflowId -> WorkflowStateIndex -> m CryptoUUIDWorkflowStateIndex
encryptWorkflowStateIndex wwId stIx = do
  cIDKey <- workflowStateIndexCryptoIDKey wwId
  $cachedHereBinary (wwId, stIx) . flip runReaderT cIDKey $ I.encrypt stIx

decryptWorkflowStateIndex :: ( MonadCrypto m
                             , MonadCryptoKey m ~ CryptoIDKey
                             , MonadHandler m
                             )
                          => WorkflowWorkflowId -> CryptoUUIDWorkflowStateIndex -> m WorkflowStateIndex
decryptWorkflowStateIndex wwId cID = do
  cIDKey <- workflowStateIndexCryptoIDKey wwId
  $cachedHereBinary (wwId, cID) . flip runReaderT cIDKey $ I.decrypt cID


isTopWorkflowScope :: WorkflowScope termid schoolid courseid -> Bool
isTopWorkflowScope = (`elem` [WSGlobal', WSTerm', WSSchool', WSTermSchool']) . classifyWorkflowScope

isTopWorkflowScopeSql :: E.SqlExpr (E.Value DBWorkflowScope) -> E.SqlExpr (E.Value Bool)
isTopWorkflowScopeSql = (`E.in_` E.valList [WSGlobal', WSTerm', WSSchool', WSTermSchool']) . classifyWorkflowScopeSql
  where classifyWorkflowScopeSql = (E.->. "tag")


selectWorkflowInstanceDescription :: ( MonadHandler m
                                     , BackendCompatible SqlReadBackend backend
                                     )
                                  => WorkflowInstanceId
                                  -> ReaderT backend m (Maybe (Entity WorkflowInstanceDescription))
selectWorkflowInstanceDescription wiId = withReaderT (projectBackend @SqlReadBackend) $ do
  descLangs <- E.select . E.from $ \workflowInstanceDescription -> do
    E.where_ $ workflowInstanceDescription E.^. WorkflowInstanceDescriptionInstance E.==. E.val wiId
    return $ workflowInstanceDescription E.^. WorkflowInstanceDescriptionLanguage
  descLang <- traverse selectLanguage . nonEmpty $ E.unValue <$> descLangs
  fmap join . for descLang $ \descLang' -> getBy $ UniqueWorkflowInstanceDescription wiId descLang'


data SharedWorkflowGraphException
  = SharedWorkflowGraphNotFound SharedWorkflowGraphId
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

getSharedDBWorkflowGraph :: ( MonadHandler m
                            , BackendCompatible SqlReadBackend backend
                            )
                         => SharedWorkflowGraphId
                         -> ReaderT backend m DBWorkflowGraph
getSharedDBWorkflowGraph swgId = $cachedHereBinary swgId . withReaderT (projectBackend @SqlReadBackend) $ do
  maybe (liftHandler . throwM $ SharedWorkflowGraphNotFound swgId) (return . sharedWorkflowGraphGraph) =<< get swgId

getSharedIdWorkflowGraph :: ( MonadHandler m
                            , BackendCompatible SqlReadBackend backend
                            )
                         => SharedWorkflowGraphId
                         -> ReaderT backend m IdWorkflowGraph
getSharedIdWorkflowGraph = fmap (review _DBWorkflowGraph) . getSharedDBWorkflowGraph

insertSharedWorkflowGraph :: ( MonadIO m
                             , BackendCompatible SqlBackend backend
                             )
                          => DBWorkflowGraph
                          -> ReaderT backend m SharedWorkflowGraphId
insertSharedWorkflowGraph graph = withReaderT (projectBackend @SqlBackend) $
    swgId' <$ repsert swgId' (SharedWorkflowGraph swgId graph)
  where
    swgId = WorkflowGraphReference . Crypto.hashlazy $ Aeson.encode graph
    swgId' = SharedWorkflowGraphKey swgId


data SharedWorkflowOverviewSpecException
  = SharedWorkflowOverviewSpecNotFound SharedWorkflowOverviewSpecId
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

getSharedDBWorkflowOverviewSpec :: ( MonadHandler m
                                   , BackendCompatible SqlReadBackend backend
                                   )
                                => SharedWorkflowOverviewSpecId
                                -> ReaderT backend m DBWorkflowOverviewSpec
getSharedDBWorkflowOverviewSpec swosId = $cachedHereBinary swosId . withReaderT (projectBackend @SqlReadBackend) $ do
  maybe (liftHandler . throwM $ SharedWorkflowOverviewSpecNotFound swosId) (return . sharedWorkflowOverviewSpecSpec) =<< get swosId

getSharedIdWorkflowOverviewSpec :: ( MonadHandler m
                                   , BackendCompatible SqlReadBackend backend
                                   )
                                => SharedWorkflowOverviewSpecId
                                -> ReaderT backend m IdWorkflowOverviewSpec
getSharedIdWorkflowOverviewSpec = fmap (review _DBWorkflowOverviewSpec) . getSharedDBWorkflowOverviewSpec

insertSharedWorkflowOverviewSpec :: ( MonadIO m
                                    , BackendCompatible SqlBackend backend
                                    )
                                 => DBWorkflowOverviewSpec
                                 -> ReaderT backend m SharedWorkflowOverviewSpecId
insertSharedWorkflowOverviewSpec oSpec = withReaderT (projectBackend @SqlBackend) $
    woId' <$ repsert woId' (SharedWorkflowOverviewSpec woId oSpec)
  where
    woId = WorkflowOverviewReference . Crypto.hashlazy $ SafeJSON.encode oSpec
    woId' = SharedWorkflowOverviewSpecKey woId


matchesWorkflowWorkflowListType :: WorkflowWorkflowListType
                                -> E.SqlExpr (E.Value UTCTime) -- ^ @now@
                                -> E.SqlExpr (Entity WorkflowWorkflow)
                                -> E.SqlExpr (E.Value Bool)
matchesWorkflowWorkflowListType wwListType now workflowWorkflow
  | is _WorkflowWorkflowListAll wwListType = E.true
  | otherwise = E.maybe
      (E.val $ isn't _WorkflowWorkflowListArchive wwListType)
      (\archivedOn -> (bool (E.>.) (E.<=.) $ is _WorkflowWorkflowListArchive wwListType) archivedOn now)
      (workflowWorkflow E.^. WorkflowWorkflowArchived)
