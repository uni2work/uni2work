-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Widgets
  ( i18n
  ) where

import ClassyPrelude.Yesod
import Yesod.Core.Types.Instances ()


i18n :: forall m msg.
        ( MonadWidget m
        , RenderMessage (HandlerSite m) msg
        ) => msg -> m ()
i18n = toWidget . (SomeMessage :: msg -> SomeMessage (HandlerSite m))
