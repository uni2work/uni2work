-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Frontend.I18n
  ( FrontendMessage(..)
  , frontendDatetimeLocales
  ) where

import ClassyPrelude
import Data.Universe

import Control.Lens
import Utils.PathPiece

import Web.PathPieces
import Data.Aeson
import Data.Aeson.Types (toJSONKeyText)
import Data.Aeson.TH
import qualified Data.Char as Char

import Data.List.NonEmpty (NonEmpty(..))
import Text.Shakespeare.I18N (Lang)


-- | I18n-Messages used in JavaScript-Frontend
--
-- Only nullary constructors are supported
--
-- @MsgCamelCaseIdentifier@ gets translated to @camelCaseIdentifier@ in Frontend (see `nullaryPathPiece` and `deriveJSON` below)
data FrontendMessage = MsgFilesSelected
                     | MsgSelectFile
                     | MsgSelectFiles
                     | MsgAsyncFormFailure
                     | MsgFileTooLarge
                     | MsgFileTooLargeMultiple
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Typeable)
instance Universe FrontendMessage
instance Finite FrontendMessage
instance Hashable FrontendMessage

nullaryPathPiece ''FrontendMessage $ over _head Char.toLower . mconcat . drop 1 . splitCamel

deriveJSON defaultOptions
  { constructorTagModifier = over _head Char.toLower . mconcat . drop 1 . splitCamel
  } ''FrontendMessage

instance ToJSONKey FrontendMessage where
  toJSONKey = toJSONKeyText toPathPiece
instance FromJSONKey FrontendMessage where
  fromJSONKey = FromJSONKeyTextParser $ parseJSON . String


frontendDatetimeLocales :: NonEmpty Lang
frontendDatetimeLocales = "de" :| ["en"]
