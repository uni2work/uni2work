-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Frontend.Notification
  ( NotificationType(..)
  , notification
  , notificationWidget
  ) where

import ClassyPrelude.Yesod
import Settings

import Utils.Message
import Utils.Icon

import Control.Lens
import Control.Lens.Extras (is)


data NotificationType
  = NotificationNarrow
  | NotificationBroad
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)

makePrisms ''NotificationType


notification :: NotificationType
             -> Message
             -> WidgetFor site ()
notification nType Message{ messageIcon = messageIcon', .. }
    = $(widgetFile "widgets/notification")
  where
    messageIcon = fromMaybe defaultIcon messageIcon'
    defaultIcon = case messageStatus of
      Success   -> IconNotificationSuccess
      Info      -> IconNotificationInfo
      Warning   -> IconNotificationWarning
      Error     -> IconNotificationError
      Nonactive -> IconNotificationNonactive

notificationWidget :: Yesod site
                   => NotificationType
                   -> MessageStatus
                   -> WidgetFor site ()
                   -> WidgetFor site ()
notificationWidget nType ms = notification nType <=< messageWidget ms
