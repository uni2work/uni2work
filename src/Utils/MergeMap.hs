-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.MergeMap
  ( MergeHashMap(..), _MergeHashMap
  , MergeMap(..), _MergeMap
  ) where

import ClassyPrelude

import qualified Data.Map as Map
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Vector as V

import Control.Monad (zipWithM)
import Control.Monad.Fail

import Data.Data (Data)

import Data.Aeson (ToJSON, FromJSON, FromJSONKey)
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Types as Aeson

import Data.Coerce

import Control.Lens


newtype MergeHashMap k v = MergeHashMap { unMergeHashMap :: HashMap k v }
  deriving (Show, Generic, Typeable, Data)
  deriving newtype ( Eq, Ord, Hashable
                   , Functor, Foldable, NFData
                   , ToJSON
                   )

makePrisms ''MergeHashMap
makeWrapped ''MergeHashMap

type instance Element (MergeHashMap k v) = v
  
instance MonoFoldable (MergeHashMap k v)
instance MonoFunctor (MergeHashMap k v)
instance MonoTraversable (MergeHashMap k v)
instance GrowingAppend (MergeHashMap k v)

instance Traversable (MergeHashMap k) where
  traverse = _MergeHashMap . traverse

instance FunctorWithIndex k (MergeHashMap k)
instance TraversableWithIndex k (MergeHashMap k) where
  itraverse = _MergeHashMap .> itraverse
instance FoldableWithIndex k (MergeHashMap k)

instance (Eq k, Hashable k, Semigroup v) => Semigroup (MergeHashMap k v) where
  (MergeHashMap a) <> (MergeHashMap b) = MergeHashMap $ HashMap.unionWith (<>) a b
instance (Eq k, Hashable k, Semigroup v) => Monoid (MergeHashMap k v) where
  mempty = MergeHashMap HashMap.empty
instance (Eq k, Hashable k, FromJSON v, FromJSONKey k, Semigroup v) => FromJSON (MergeHashMap k v) where
  parseJSON = case Aeson.fromJSONKey of
      Aeson.FromJSONKeyCoerce -> Aeson.withObject "HashMap ~Text" $
          coerce @(Aeson.Parser (HashMap k v)) @(Aeson.Parser (MergeHashMap k v)) . fmap HashMap.fromList . traverse (\(k, v) -> (coerce @Text @k k, ) <$> Aeson.parseJSON v Aeson.<?> Aeson.Key k) . HashMap.toList
      Aeson.FromJSONKeyText f -> Aeson.withObject "HashMap" $
          fmap MergeHashMap . HashMap.foldrWithKey (\k v m -> HashMap.insertWith (<>) (f k) <$> Aeson.parseJSON v Aeson.<?> Aeson.Key k <*> m) (pure mempty)
      Aeson.FromJSONKeyTextParser f -> Aeson.withObject "HashMap" $
          fmap MergeHashMap . HashMap.foldrWithKey (\k v m -> HashMap.insertWith (<>) <$> f k Aeson.<?> Aeson.Key k <*> Aeson.parseJSON v Aeson.<?> Aeson.Key k <*> m) (pure mempty)
      Aeson.FromJSONKeyValue f -> Aeson.withArray "HashMap" $ \arr ->
          fmap (MergeHashMap . HashMap.fromListWith (<>)) . zipWithM (parseIndexedJSONPair f Aeson.parseJSON) [0..] $ otoList arr
    where
      parseIndexedJSONPair :: (Aeson.Value -> Aeson.Parser a) -> (Aeson.Value -> Aeson.Parser b) -> Int -> Aeson.Value -> Aeson.Parser (a, b)
      parseIndexedJSONPair keyParser valParser idx value = p value Aeson.<?> Aeson.Index idx
        where
          p = Aeson.withArray "(k, v)" $ \ab ->
              let n = V.length ab
              in if n == 2
                   then (,) <$> parseJSONElemAtIndex keyParser 0 ab
                            <*> parseJSONElemAtIndex valParser 1 ab
                   else fail $ "cannot unpack array of length " ++
                               show n ++ " into a pair"

      parseJSONElemAtIndex :: (Aeson.Value -> Aeson.Parser a) -> Int -> Vector Aeson.Value -> Aeson.Parser a
      parseJSONElemAtIndex p idx ary = p (V.unsafeIndex ary idx) Aeson.<?> Aeson.Index idx


newtype MergeMap k v = MergeMap { unMergeMap :: Map k v }
  deriving (Show, Generic, Typeable, Data)
  deriving newtype ( Eq, Ord
                   , Functor, Foldable, NFData
                   , ToJSON
                   )

makePrisms ''MergeMap
makeWrapped ''MergeMap

type instance Element (MergeMap k v) = v
  
instance MonoFoldable (MergeMap k v)
instance MonoFunctor (MergeMap k v)
instance MonoTraversable (MergeMap k v)
instance GrowingAppend (MergeMap k v)

instance Traversable (MergeMap k) where
  traverse = _MergeMap . traverse

instance FunctorWithIndex k (MergeMap k)
instance TraversableWithIndex k (MergeMap k) where
  itraverse = _MergeMap .> itraverse
instance FoldableWithIndex k (MergeMap k)

instance (Ord k, Semigroup v) => Semigroup (MergeMap k v) where
  (MergeMap a) <> (MergeMap b) = MergeMap $ Map.unionWith (<>) a b
instance (Ord k, Semigroup v) => Monoid (MergeMap k v) where
  mempty = MergeMap Map.empty
instance (Ord k, FromJSON v, FromJSONKey k, Semigroup v) => FromJSON (MergeMap k v) where
  parseJSON = case Aeson.fromJSONKey of
      Aeson.FromJSONKeyCoerce -> Aeson.withObject "Map ~Text" $
          coerce @(Aeson.Parser (Map k v)) @(Aeson.Parser (MergeMap k v)) . fmap Map.fromList . traverse (\(k, v) -> (coerce @Text @k k, ) <$> Aeson.parseJSON v Aeson.<?> Aeson.Key k) . HashMap.toList
      Aeson.FromJSONKeyText f -> Aeson.withObject "Map" $
          fmap MergeMap . Map.foldrWithKey (\k v m -> Map.insertWith (<>) (f k) <$> Aeson.parseJSON v Aeson.<?> Aeson.Key k <*> m) (pure mempty) . Map.fromList . HashMap.toList
      Aeson.FromJSONKeyTextParser f -> Aeson.withObject "Map" $
          fmap MergeMap . Map.foldrWithKey (\k v m -> Map.insertWith (<>) <$> f k Aeson.<?> Aeson.Key k <*> Aeson.parseJSON v Aeson.<?> Aeson.Key k <*> m) (pure mempty) . Map.fromList . HashMap.toList
      Aeson.FromJSONKeyValue f -> Aeson.withArray "Map" $ \arr ->
          fmap (MergeMap . Map.fromListWith (<>)) . zipWithM (parseIndexedJSONPair f Aeson.parseJSON) [0..] $ otoList arr
    where
      parseIndexedJSONPair :: (Aeson.Value -> Aeson.Parser a) -> (Aeson.Value -> Aeson.Parser b) -> Int -> Aeson.Value -> Aeson.Parser (a, b)
      parseIndexedJSONPair keyParser valParser idx value = p value Aeson.<?> Aeson.Index idx
        where
          p = Aeson.withArray "(k, v)" $ \ab ->
              let n = V.length ab
              in if n == 2
                   then (,) <$> parseJSONElemAtIndex keyParser 0 ab
                            <*> parseJSONElemAtIndex valParser 1 ab
                   else fail $ "cannot unpack array of length " ++
                               show n ++ " into a pair"

      parseJSONElemAtIndex :: (Aeson.Value -> Aeson.Parser a) -> Int -> Vector Aeson.Value -> Aeson.Parser a
      parseJSONElemAtIndex p idx ary = p (V.unsafeIndex ary idx) Aeson.<?> Aeson.Index idx
