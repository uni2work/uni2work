-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Cookies
  ( getCookiePath
  , siteApproot
  , cookiePath
  ) where

import ClassyPrelude.Yesod

import qualified Network.Wai as Wai


cookiePath :: Maybe Text -> ByteString
cookiePath = maybe "/" $ extractPath . encodeUtf8

siteApproot :: Yesod site => site -> Wai.Request -> Maybe Text
siteApproot master req = case approot of
  ApprootRelative   -> Nothing
  ApprootStatic   t -> Just t
  ApprootMaster   f -> Just $ f master
  ApprootRequest  f -> Just $ f master req

getCookiePath :: (MonadHandler m, Yesod (HandlerSite m)) => m ByteString
getCookiePath = do
  app <- getYesod
  req <- reqWaiRequest <$> getRequest

  return . cookiePath $ siteApproot app req
