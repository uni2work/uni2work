-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.Jobs
  ( insertJobContent
  ) where

import Import

import Jobs.Types


insertJobContent :: MonadIO m
                 => Job
                 -> SqlWriteT m JobContentId
insertJobContent job = withCompatibleBackend @SqlBackend $ do
  let
    jobContentHash    = toJobContentReference job
    jobContentContent = toJSON job
  unlessM (exists [JobContentHash ==. jobContentHash]) $ insert_ JobContent{..}
  return $ JobContentKey jobContentHash
