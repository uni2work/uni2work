-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.JSON
  ( DefaultMap(..)
  ) where

import ClassyPrelude.Yesod

import Data.Coerce

import Data.Aeson
import Data.Aeson.Types

import Control.Lens.TH

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set


newtype DefaultMap k v = DefaultMap { unDefaultMap :: Map k v }
  deriving newtype (Eq, Ord, ToJSON)
  deriving stock (Generic, Typeable)

instance (FromJSONKey k, Ord k, FromJSON v, Default v) => FromJSON (DefaultMap k v) where
  parseJSON v = fmap DefaultMap $
                      parseJSON v
                  <|> Map.fromSet (const def) <$> parseKeySet v
    where
      parseKeySet :: Value -> Parser (Set k)
      parseKeySet = case fromJSONKey of
        FromJSONKeyCoerce -> fmap (Set.fromList . coerce @[Text] @[k]) . parseJSON -- `Set` has nominal role to account for Ord instances differing among coercible types
        FromJSONKeyText f -> fmap (Set.fromList . map f) . parseJSON
        FromJSONKeyTextParser f -> fmap Set.fromList . mapM f <=< parseJSON
        FromJSONKeyValue p -> withArray "Set k" $ fmap Set.fromList . mapM p . otoList

makeWrapped ''DefaultMap
