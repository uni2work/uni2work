# Datei Index

Database,Esqueleto.*
  : Hilfsdefinitionen, welche Esqueleto anbieten könnte

Utils, Utils.*
  : Hilfsfunktionionen _unabhängig von Foundation_

Utils
  : Yesod Hilfsfunktionen und Instanzen, Crud, `NTop`, Utility-Funktionen für `MonadPlus`, `Maybe`,
    `MaybeT`, `Map`, und Attrs-Lists

Utils.TH
  : Template Haskell code-generatoren von unabhängigen Hilfsfunktionen (`deriveSimpleWith`)

Utils.DB
  : Derived persistent functions (`existsBy`, `getKeyBy404`, ...)

Utils.Form
  : `renderAForm`, Field-Settings helper, `FormIdentifier`, `Button`-Klasse,
    unabhängige konkrete Buttons

Utils.PathPiece
  : (Template-Haskell)-Hilfsfunktionen für Formulierung von PathPiece-Instanzen

Utils.Message
  : redefines addMessage, addMessageI, defines MessageClass

Utils.Lens
  : Automatisch erzeugt Linsen für eigene und Yesod-Typen, `Control.Lens`-Export

Utils.DateTime
  : Template Haskell code-generatoren zum compile-time einbinden von Zeitzone
    und `TimeLocale`

Handler.Utils, Handler.Utils.*
  : Hilfsfunktionien, importieren `Import`

Handler.Utils
  : `Handler.Utils.*`, Unsortierte _Foundation-abhängige_ Hilfsfunktionen

Handler.Utils.DateTime
  : Nutzer-spezifisches `DateTime`-Formatieren

Handler.Utils.Form
  : Konkrete Buttons, spezielle Felder (inkl. Datei-Upload-Felder),
    Optionslisten (`optionsPersistCryptoId`), `forced`-Felder (erzwungenes
    Ergebnis, deaktiviertes Feld), `multiAction`

Handler.Utils.Rating
  : `Rating` (kodiert eine Rating-Datei), Parsen und PrettyPrinten von
    Rating-Dateien

Handler.Utils.Sheet
  : `fetchSheet`

Handler.Utils.StudyFeatures
  : Parsen von LDAP StudyFeatures-Strings

Handler.Utils.Submission
  : `assignSubmissions`, `sinkSubmission` State-Maschinen die (bereits geparste)
    ZIP-Archive auseinandernehmen und (in einer Transaction) in die Datenbank
    speichern

Handler.Utils.Submission.TH
  : Template Haskell zum parsen und einkompilieren von Dateiname-Blacklist für
    `sinkSubmission`; Patterns in `config/submission-blacklist`

Handler.Utils.Table
  : Hilfsfunktion zum direkten Benutzen von Colonnade (kein `dbTable`)

Handler.Utils.Table.Pagination
  : Here be Dragons

    Paginated database-backed tables with support for sorting, filtering,
    numbering, forms, further database-requests within cells

    Includes helper functions for mangling pagination-, sorting-, and filter-settings

    Includes helper functions for constructing common types of cells

Handler.Utils.Table.Pagination.Types
  : `Sortable`-Headedness for colonnade

Handler.Utils.Table.Cells
  : extends dbTable with UniWorX specific functions, such as special courseCell

Utils.Modal
  : Modals

Handler.Utils.Zip
  : Conduit-basiertes ZIP Parsen und Erstellen

Handler.Common
  : Handler aus dem Scaffolding; Implementierungen von Handlern, die _jede
    Website_ irgendwann braucht

CryptoID
  : Definiert CryptoIDs für custom Typen (aus Model)

Model.Migration
  : Manuelle Datenbank-Migration

Model.Rating
  : Types for Submission Ratings that the Database does not depend on, but needed in Foundation

Jobs
  : `handleJobs` worker thread handling background jobs
    `JobQueueException`

Jobs.Types
  : `Job`, `Notification`, `JobCtl` Types of Jobs

Cron.Types
  : Datentypen zur Spezifikation von Intervallen zu denen Jobs ausgeführt werden
    können:

    `Cron`, `CronMatch`, `CronAbsolute`, `CronRepeat`, `Crontab`

Cron
  : Seiteneffektfreie Berechnungen auf Typen aus `Cron.Types`: `nextCronMatch`

Jobs.Queue
  : Funktionen zum _anstoßen_ von Jobs und zur Kommunikation mit den
    Worker-Threads

    `writeJobCtl` schickt Nachricht an einen pseudo-Zufälligen worker-thread der
    lokalen Instanz

    `queueJob` und `queueJob'` schreiben neue Jobs in die Instanz-übergreifende
    Job-Queue, `queueJob'` stößt außerdem einen lokalen worker-thread an sich
    des Jobs anzunehmen

    `runDBJobs` ersetzt `runDB` und erlaubt `queueDBJob` zu
    benutzen. `queueDBJob` schreibt einen Job in die Queue; am Ende stößt
    `runDBJobs` lokale worker-threads für alle mit `queueDBJobs` eingetragenen
    Jobs an.

Jobs.TH
  : Templatehaskell für den dispatch mechanismus für `Jobs`

Jobs.Crontab
  : Generiert `Crontab JobCtl` aus der Datenbank (sammelt alle in den Daten aus
    der Datenbank impliziten Jobs (notifications zu bestimmten zeiten,
    aufräumaktionen, ...) ein)

Jobs.Handler.**
  : Via `Jobs.TH` delegiert `Jobs` das Interpretieren und Ausführen eines Werts
    aus `Jobs.Types` an einen dieser Handler

Mail
  : Monadically constructing MIME emails
