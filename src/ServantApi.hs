-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module ServantApi
  ( module ServantApi
  ) where

import Import.Servant


import ServantApi.ExternalApis as ServantApi


mkYesodApi ''UniWorX uniworxRoutes

uniworxApi :: Proxy UniWorXApi
uniworxApi = Proxy
