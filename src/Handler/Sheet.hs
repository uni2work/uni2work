-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Handler.Sheet
  ( module Handler.Sheet
  ) where

import Import


import Handler.Sheet.CorrectorInvite as Handler.Sheet (getSCorrInviteR, postSCorrInviteR)
import Handler.Sheet.Delete as Handler.Sheet
import Handler.Sheet.Edit as Handler.Sheet (getSEditR, postSEditR)
import Handler.Sheet.List as Handler.Sheet
import Handler.Sheet.Pseudonym as Handler.Sheet (getSPseudonymR, postSPseudonymR)
import Handler.Sheet.Current as Handler.Sheet
import Handler.Sheet.Download as Handler.Sheet
import Handler.Sheet.New as Handler.Sheet
import Handler.Sheet.Show as Handler.Sheet
import Handler.Sheet.PersonalisedFiles as Handler.Sheet (getSPersonalFilesR)


getSIsCorrR :: TermId -> SchoolId -> CourseShorthand -> SheetName -> Handler Html
-- NOTE: The route SIsCorrR is only used to verfify corrector access rights to given sheet!
getSIsCorrR _ _ _ shn =
  defaultLayout . i18n $ MsgHaveCorrectorAccess shn
