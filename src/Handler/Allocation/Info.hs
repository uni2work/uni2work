-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Allocation.Info
  ( getInfoAllocationR
  ) where

import Import
import Handler.Utils

import Handler.Info (FAQItem(..))


getInfoAllocationR :: Handler Html
getInfoAllocationR =
  siteLayoutMsg MsgHeadingAllocationInfo $ do
    setTitleI MsgHeadingAllocationInfo
    faqItemUrlAllocationNoPlaces <- toTextUrl $ FaqR :#: FAQAllocationNoPlaces
    $(i18nWidgetFile "allocation-info")
