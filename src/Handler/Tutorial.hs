-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Tutorial
  ( module Handler.Tutorial
  ) where

import Handler.Tutorial.Communication as Handler.Tutorial
import Handler.Tutorial.Delete        as Handler.Tutorial
import Handler.Tutorial.Edit          as Handler.Tutorial
import Handler.Tutorial.Form          as Handler.Tutorial
import Handler.Tutorial.List          as Handler.Tutorial
import Handler.Tutorial.New           as Handler.Tutorial
import Handler.Tutorial.Register      as Handler.Tutorial
import Handler.Tutorial.TutorInvite   as Handler.Tutorial
import Handler.Tutorial.Users         as Handler.Tutorial
