-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Admin.ErrorMessage
  ( getAdminErrMsgR, postAdminErrMsgR
  ) where

import Import
import Handler.Utils
import Data.Aeson.Encode.Pretty (encodePrettyToTextBuilder)


getAdminErrMsgR, postAdminErrMsgR :: Handler Html
getAdminErrMsgR = postAdminErrMsgR
postAdminErrMsgR = do
  ((ctResult, ctView), ctEncoding) <- runFormPost . renderAForm FormStandard $
        unTextarea <$> areq textareaField (fslpI MsgErrMsgCiphertext "Ciphertext") Nothing

  plaintext <- formResultMaybe ctResult $ exceptT (\err -> Nothing <$ addMessageI Error err) (return . Just) . (encodedSecretBoxOpen :: Text -> ExceptT EncodedSecretBoxException Handler Value)

  let ctView' = wrapForm ctView def{ formAction = Just . SomeRoute $ AdminErrMsgR, formEncoding = ctEncoding }
  defaultLayout
    [whamlet|
      $newline never
      $maybe t <- plaintext
        <pre .literal-error>
          $case t
            $of String t'
              #{t'}
            $of t'
              #{encodePrettyToTextBuilder t'}

      ^{ctView'}
    |]
