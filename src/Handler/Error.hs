-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Error
  ( getErrorR
  ) where

import Import
import Yesod.Core.Types (HandlerContents(HCError))

  
getErrorR :: Handler Void
getErrorR = do
  encodedErrResponse <- maybe (redirect NewsR) return =<< lookupGlobalGetParam GetError
  errResponse <- throwExceptT (encodedAuthVerify encodedErrResponse)

  isAuthed <- is _Just <$> maybeAuthId
  case errResponse of
    NotAuthenticated | isAuthed -> permissionDeniedI MsgUnauthorizedNotAuthenticatedInDifferentApproot
    _ -> throwM $ HCError errResponse
