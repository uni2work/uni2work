-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Sheet.Current
  ( getSheetCurrentR
  , getSheetOldUnassignedR
  ) where

import Import

import Handler.Utils
import Utils.Sheet


getSheetCurrentR :: TermId -> SchoolId -> CourseShorthand -> Handler Void
getSheetCurrentR tid ssh csh = do
  mbShn <- runDB $ sheetCurrent tid ssh csh
  case mbShn of
    Just shn -> redirectAccess $ CSheetR tid ssh csh shn SShowR
    Nothing  -> do -- no current sheet exists
      addMessageI Error MsgSheetNoCurrent
      redirect $ CourseR tid ssh csh SheetListR

getSheetOldUnassignedR :: TermId -> SchoolId -> CourseShorthand -> Handler Void
getSheetOldUnassignedR tid ssh csh = do
    mbShn <- runDB $ sheetOldUnassigned tid ssh csh
    case mbShn of
      Just shn -> redirectAccess $ CSheetR tid ssh csh shn SSubsR
      Nothing  -> do -- no unassigned submissions in any inactive sheet
        addMessageI Error MsgSheetNoOldUnassigned
        redirect $ CourseR tid ssh csh SheetListR
