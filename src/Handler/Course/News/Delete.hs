-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Course.News.Delete
  ( getCNDeleteR, postCNDeleteR
  ) where

import Import
import Handler.Utils.Delete

import qualified Data.Set as Set


getCNDeleteR, postCNDeleteR :: TermId -> SchoolId -> CourseShorthand -> CryptoUUIDCourseNews -> Handler Html
getCNDeleteR = postCNDeleteR
postCNDeleteR tid ssh csh cID = do
  nId <- decrypt cID

  let
    drRecords :: Set (Key CourseNews)
    drRecords = Set.singleton nId

    drGetInfo = return
    drUnjoin = id

    drRenderRecord :: Entity CourseNews -> DB Widget
    drRenderRecord (Entity _ CourseNews{..})
      = return . fromMaybe (toWidget courseNewsContent) $ asum
        [ toWidget <$> courseNewsTitle
        , toWidget <$> courseNewsSummary
        ]

    drRecordConfirmString :: Entity CourseNews -> DB Text
    drRecordConfirmString _ = return ""

    drCaption, drSuccessMessage :: SomeMessage UniWorX
    drCaption = SomeMessage MsgCourseNewsDeleteQuestion
    drSuccessMessage = SomeMessage MsgCourseNewsDeleted

    drAbort, drSuccess :: SomeRoute UniWorX
    drAbort = SomeRoute $ CourseR tid ssh csh CShowR :#: [st|news-#{toPathPiece cID}|]
    drSuccess = SomeRoute $ CourseR tid ssh csh CShowR

    drFormMessage :: [Entity CourseNews] -> DB (Maybe Message)
    drFormMessage _ = return Nothing

    drDelete :: forall a. CourseNewsId -> JobDB a -> JobDB a
    drDelete _ = id

  deleteR DeleteRoute{..}
