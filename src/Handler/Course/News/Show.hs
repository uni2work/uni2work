-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Course.News.Show
  ( getCNShowR
  ) where

import Import
import Handler.Utils

{-# ANN module ("HLint: ignore Too strict maybe"::String) #-}


getCNShowR :: TermId -> SchoolId -> CourseShorthand -> CryptoUUIDCourseNews -> Handler Html
getCNShowR tid ssh csh cID = do
  nId <- decrypt cID
  CourseNews{..} <- runDB $ get404 nId

  siteLayout' (toWidget <$> courseNewsTitle) $ do
    setTitleI . prependCourseTitle tid ssh csh $ maybe (SomeMessage MsgCourseNews) SomeMessage courseNewsTitle

    $(widgetFile "course-news")
