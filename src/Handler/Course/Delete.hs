-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Course.Delete
  ( getCDeleteR, postCDeleteR
  ) where

import Import

import Handler.Utils.Course
import Handler.Utils.Delete

import qualified Data.Set as Set


getCDeleteR, postCDeleteR :: TermId -> SchoolId -> CourseShorthand -> Handler Html
getCDeleteR = postCDeleteR
postCDeleteR tid ssh csh = do
  Entity cId _ <- runDB . getBy404 $ TermSchoolCourseShort tid ssh csh
  deleteR $ (courseDeleteRoute $ Set.singleton cId)
    { drAbort = SomeRoute $ CourseR tid ssh csh CShowR
    , drSuccess = SomeRoute . TermR tid $ TermSchoolR ssh TermSchoolCourseListR
    }
