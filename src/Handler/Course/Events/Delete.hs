-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Course.Events.Delete
  ( getCEvDeleteR, postCEvDeleteR
  ) where

import Import

import Handler.Utils.Occurrences
import Handler.Utils.Delete

import qualified Data.Set as Set


getCEvDeleteR, postCEvDeleteR :: TermId -> SchoolId -> CourseShorthand -> CryptoUUIDCourseEvent -> Handler Html
getCEvDeleteR = postCEvDeleteR
postCEvDeleteR tid ssh csh cID = do
  nId <- decrypt cID

  let
    drRecords :: Set (Key CourseEvent)
    drRecords = Set.singleton nId

    drGetInfo = return
    drUnjoin = id

    drRenderRecord :: Entity CourseEvent -> DB Widget
    drRenderRecord (Entity _ CourseEvent{..}) = return
      [whamlet|
        $newline never
        #{courseEventType}
        $maybe room <- courseEventRoom
          , #{roomReferenceText room}
        :
        ^{occurrencesWidget courseEventTime}
      |]

    drRecordConfirmString :: Entity CourseEvent -> DB Text
    drRecordConfirmString _ = return ""

    drCaption, drSuccessMessage :: SomeMessage UniWorX
    drCaption = SomeMessage MsgCourseEventDeleteQuestion
    drSuccessMessage = SomeMessage MsgCourseEventDeleted

    drAbort, drSuccess :: SomeRoute UniWorX
    drAbort = SomeRoute $ CourseR tid ssh csh CShowR :#: [st|event-#{toPathPiece cID}|]
    drSuccess = SomeRoute $ CourseR tid ssh csh CShowR

    drFormMessage :: [Entity CourseEvent] -> DB (Maybe Message)
    drFormMessage _ = return Nothing

    drDelete :: forall a. CourseEventId -> JobDB a -> JobDB a
    drDelete _ = id

  deleteR DeleteRoute{..}

