-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Course.Events
  ( module Handler.Course.Events
  ) where

import Handler.Course.Events.New as Handler.Course.Events
import Handler.Course.Events.Edit as Handler.Course.Events
import Handler.Course.Events.Delete as Handler.Course.Events
