-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.ApiDocs
  ( getApiDocsR
  ) where

import Import
import ServantApi

import qualified Servant.Docs as Servant

import Servant.Docs.Internal.Pretty

import Handler.Utils.Pandoc


getApiDocsR :: Handler TypedContent
getApiDocsR = selectRep $ do
  case htmlDocs of
    Right html -> provideRep . siteLayoutMsg MsgBreadcrumbApiDocs $ do
      setTitleI MsgBreadcrumbApiDocs

      [whamlet|
        $newline never
        <div .apidocs>
          ^{html}
      |]
    Left  _err -> return ()
  provideRepType "text/markdown" $ return mdDocs
  where
    mdDocs = pack . Servant.markdown $ Servant.docsWith Servant.defaultDocOptions docIntros docExtra (Proxy @(Pretty UniWorXApi))
    htmlDocs = parseMarkdownWith markdownReaderOptions htmlWriterOptions mdDocs

    docIntros = mempty
    docExtra = mconcat
      [
      ]
