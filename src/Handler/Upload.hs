-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Upload
  ( putUploadR
  ) where

import Import


data UploadResponse
  = UploadResponseNoToken
  deriving (Eq, Ord, Show, Generic, Typeable)

deriveJSON defaultOptions
  { tagSingleConstructors = True
  , allNullaryToStringTag = False
  , constructorTagModifier = camelToPathPiece' 2
  } ''UploadResponse
  

putUploadR :: Handler TypedContent
putUploadR = do
  resp <- exceptT return return $ do
    _uploadToken <- decodeUploadToken <=< maybeExceptT UploadResponseNoToken $ lookupCustomHeader HeaderUploadToken
    
    error "not implemented"

  selectRep $ do
    provideRep . return $ toPrettyJSON resp
    provideRep . return $ toJSON resp
    provideRep . return $ toYAML resp
