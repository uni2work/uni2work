-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.ExternalExam
  ( fetchExternalExam
  ) where

import Import


fetchExternalExam :: MonadHandler m => TermId -> SchoolId -> CourseName -> ExamName -> ReaderT SqlBackend m (Entity ExternalExam)
fetchExternalExam tid ssh coursen examn = 
  let cachId = encodeUtf8 $ tshow (tid, ssh, coursen, examn)
  in  cachedBy cachId $ do
      mExtEx <- getBy $ UniqueExternalExam tid ssh coursen examn
      case mExtEx of
        Just extEx -> return extEx
        _          -> notFound
