-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Form.Types where

import Import

data FileUploadInfo = FileUploadInfo
  { fuiTitle :: FilePath
  , fuiHtmlId :: Text
  , fuiChecked, fuiSession, fuiForced :: Bool
  }
