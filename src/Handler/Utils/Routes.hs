-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Routes
  ( classifyHandler
  ) where

import Import.NoFoundation
import Foundation.Routes
import Foundation.Type

import Utils.TH.Routes


classifyHandler :: Route UniWorX -> String
classifyHandler = $(classifyHandler' uniworxRoutes)
