-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.ContentDisposition
  ( downloadFiles
  , setContentDisposition'
  ) where

import Import.NoFoundation
import Foundation.Type

-- | Check whether the user's preference for files is inline-viewing or downloading
downloadFiles :: (MonadHandler m, HandlerSite m ~ UniWorX, YesodAuthPersist UniWorX, AuthEntity UniWorX ~ User, AuthId UniWorX ~ UserId) => m Bool
downloadFiles = do
  mauth <- liftHandler maybeAuth
  case mauth of
    Just (Entity _ User{..}) -> return userDownloadFiles
    Nothing -> do
      UserDefaultConf{..} <- getsYesod $ view _appUserDefaults
      return userDefaultDownloadFiles

setContentDisposition' :: (MonadHandler m, HandlerSite m ~ UniWorX, YesodAuthPersist UniWorX, AuthEntity UniWorX ~ User, AuthId UniWorX ~ UserId) => Maybe FilePath -> m ()
setContentDisposition' mFileName = do
  wantsDownload <- maybeT downloadFiles . MaybeT $ lookupGlobalGetParam GetDownload
  setContentDisposition (bool ContentInline ContentAttachment wantsDownload) mFileName

