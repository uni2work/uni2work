-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow
  ( workflowsDisabledWarning
  , module Reexport
  ) where

import Import

import Handler.Utils.I18n

import Handler.Utils.Workflow.Form as Reexport
import Handler.Utils.Workflow.EdgeForm as Reexport
import Handler.Utils.Workflow.Restriction as Reexport
import Handler.Utils.Workflow.CanonicalRoute as Reexport
import Handler.Utils.Workflow.Workflow as Reexport
import Handler.Utils.Workflow.Auth as Reexport
import Handler.Utils.Workflow.Types as Reexport


workflowsDisabledWarning :: ( MonadHandler m
                            , HandlerSite m ~ UniWorX
                            , RenderMessage UniWorX titleMsg, RenderMessage UniWorX headingMsg
                            )
                         => titleMsg -> headingMsg
                         -> m Html
                         -> m Html
workflowsDisabledWarning tMsg hMsg = volatileBool clusterVolatileWorkflowsEnabled warningHtml
  where
    warningHtml = liftHandler . siteLayoutMsg hMsg $ do
      setTitleI tMsg

      notificationWidget NotificationBroad Warning $(i18nWidgetFile "workflows-disabled")
