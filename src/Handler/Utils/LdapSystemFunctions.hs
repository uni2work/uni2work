-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.LdapSystemFunctions
  ( determineSystemFunctions
  ) where

import Import.NoFoundation

import qualified Data.Set as Set


determineSystemFunctions :: Set (CI Text) -> (SystemFunction -> Bool)
determineSystemFunctions ldapFuncs = \case
  SystemExamOffice -> False
  SystemFaculty -> "faculty" `Set.member` ldapFuncs
  SystemStudent -> "student" `Set.member` ldapFuncs
