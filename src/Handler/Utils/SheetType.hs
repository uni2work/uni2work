-- SPDX-FileCopyrightText: 2022 Felix Hamann <felix.hamann@campus.lmu.de>,Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.SheetType
  (
    gradeSummaryWidget
  ) where

import Import

addBonusToPoints :: SheetTypeSummary a -> SheetTypeSummary a
addBonusToPoints sts =
    sts & _normalSummary . _achievedPasses %~ (min passmax . (passbonus +))
        & _normalSummary . _achievedPoints %~ (min ptsmax  . (ptsbonus  +))
  where
    passmax   = sts ^. _normalSummary . _numMarkedPasses
    passbonus = sts ^. _bonusSummary  . _achievedPasses
    ptsmax    = sts ^. _normalSummary . _sumMarkedPoints
    ptsbonus  = sts ^. _bonusSummary  . _achievedPoints

gradeSummaryWidget ::  RenderMessage UniWorX msg => (Integer -> msg) -> SheetTypeSummary a -> Widget
gradeSummaryWidget title sts =
  let SheetTypeSummary{..} = addBonusToPoints sts
      sumSummaries    = normalSummary <> bonusSummary <> informationalSummary & _numSheets %~ (<> numNotGraded)
      hasPasses       = positiveSum $ numSheetsPasses sumSummaries
      hasMarkedPasses = positiveSum $ numMarkedPasses sumSummaries
      hasPoints       = positiveSum $ numSheetsPoints sumSummaries
      hasMarkedPoints = positiveSum $ numMarkedPoints sumSummaries
      rowWgt (sumHeader, summary) =  $(widgetFile "widgets/grading-summary/grading-summary-row") -- diese Funktonsdefinition darf leider nicht im .hamlet stehen!
      rowsShown = [ (MsgSheetTypeNormal'        ,normalSummary)
                  , (MsgSheetTypeBonus'         ,bonusSummary)
                  , (MsgSheetTypeInformational' ,informationalSummary)
                  ] -- diese Liste könnte auch im .hamlet definiert werden
  in if 0 == numSheets sumSummaries
        then mempty
        else $(widgetFile "widgets/grading-summary/grading-summary")
