-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.Types
  ( WorkflowStatePayload, IdWorkflowStatePayload, DBWorkflowStatePayload, _DBWorkflowStatePayload
  , WorkflowStatePayloadAuthorized, IdWorkflowStatePayloadAuthorized, JSONIdWorkflowStatePayloadAuthorized, DBWorkflowStatePayloadAuthorized, _DBWorkflowStatePayloadAuthorized
  , WorkflowRestrictionState(..), IdWorkflowRestrictionState, JSONIdWorkflowRestrictionState, DBWorkflowRestrictionState
  , WorkflowWorkflowActionInfo(..), IdWorkflowWorkflowActionInfo, DBWorkflowWorkflowActionInfo, _DBWorkflowWorkflowActionInfo
  , WorkflowWorkflowActionInfoAuthorized(..), IdWorkflowWorkflowActionInfoAuthorized, JSONIdWorkflowWorkflowActionInfoAuthorized, DBWorkflowWorkflowActionInfoAuthorized, _DBWorkflowWorkflowActionInfoAuthorized, _wwaiaUnauthorized
  , WorkflowWorkflowEdgeForm(..), IdWorkflowWorkflowEdgeForm, JsonWorkflowWorkflowEdgeForm
  , _wwefTo, _wwefVia, _wwefPayload
  ) where

import Import

import Utils.Workflow (_DBWorkflowActionUser)

import Data.Aeson


type WorkflowStatePayload fileid userid = Map WorkflowPayloadLabel (Set (WorkflowFieldPayloadW fileid userid))
type IdWorkflowStatePayload = WorkflowStatePayload FileReference UserId
type DBWorkflowStatePayload = WorkflowStatePayload FileReference SqlBackendKey

_DBWorkflowStatePayload :: Iso' IdWorkflowStatePayload DBWorkflowStatePayload
_DBWorkflowStatePayload = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowStatePayload FileReference UserId) @(WorkflowStatePayload FileReference SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowStatePayload FileReference SqlBackendKey) @(WorkflowStatePayload FileReference UserId) @SqlBackendKey @UserId) (review _SqlKey)

type WorkflowStatePayloadAuthorized fileid userid = Map WorkflowPayloadLabel (Bool, Set (WorkflowFieldPayloadW fileid userid))
type IdWorkflowStatePayloadAuthorized = WorkflowStatePayloadAuthorized FileReference UserId
type JSONIdWorkflowStatePayloadAuthorized = WorkflowStatePayloadAuthorized Any UserId
type DBWorkflowStatePayloadAuthorized = WorkflowStatePayloadAuthorized FileReference SqlBackendKey

_DBWorkflowStatePayloadAuthorized :: Iso' IdWorkflowStatePayloadAuthorized DBWorkflowStatePayloadAuthorized
_DBWorkflowStatePayloadAuthorized = iso toDB fromDB
  where
    toDB = over (typesCustom @WorkflowChildren @(WorkflowStatePayloadAuthorized FileReference UserId) @(WorkflowStatePayloadAuthorized FileReference SqlBackendKey) @UserId @SqlBackendKey) (view _SqlKey)
    fromDB = over (typesCustom @WorkflowChildren @(WorkflowStatePayloadAuthorized FileReference SqlBackendKey) @(WorkflowStatePayloadAuthorized FileReference UserId) @SqlBackendKey @UserId) (review _SqlKey)


data WorkflowRestrictionState fileid userid = WorkflowRestrictionState
  { wwrCurrentNode :: WorkflowGraphNodeLabel
  , wwrCurrentPayload :: WorkflowStatePayload fileid userid
  , wwrNodeHistory :: Seq WorkflowGraphNodeLabel
  , wwrEdgeHistory :: Seq WorkflowGraphEdgeLabel
  } deriving stock (Eq, Ord, Show, Generic, Typeable)

type IdWorkflowRestrictionState = WorkflowRestrictionState FileReference UserId
type JSONIdWorkflowRestrictionState = WorkflowRestrictionState Any UserId
type DBWorkflowRestrictionState = WorkflowRestrictionState FileReference SqlBackendKey


data WorkflowWorkflowActionInfo fileid userid = WorkflowWorkflowActionInfo
  { wwaiTo :: WorkflowGraphNodeLabel
  , wwaiVia :: WorkflowGraphEdgeLabel
  , wwaiUser :: WorkflowActionUser userid
  , wwaiTime :: UTCTime
  , wwaiPayload :: WorkflowStatePayload fileid userid
  } deriving stock (Eq, Ord, Show, Generic, Typeable)

type IdWorkflowWorkflowActionInfo = WorkflowWorkflowActionInfo FileReference UserId
type DBWorkflowWorkflowActionInfo = WorkflowWorkflowActionInfo FileReference SqlBackendKey

_DBWorkflowWorkflowActionInfo :: Iso' IdWorkflowWorkflowActionInfo DBWorkflowWorkflowActionInfo
_DBWorkflowWorkflowActionInfo = iso toDB fromDB
  where
    -- No idea why using `typesCustom` doesn't work here, GK 2022-04-01
    toDB WorkflowWorkflowActionInfo{..} = WorkflowWorkflowActionInfo
      { wwaiUser = wwaiUser ^. _DBWorkflowActionUser
      , wwaiPayload = wwaiPayload ^. _DBWorkflowStatePayload
      , ..
      }
    fromDB WorkflowWorkflowActionInfo{..} = WorkflowWorkflowActionInfo
      { wwaiUser = wwaiUser ^. from _DBWorkflowActionUser
      , wwaiPayload = wwaiPayload ^. from _DBWorkflowStatePayload
      , ..
      }

data WorkflowWorkflowActionInfoAuthorized fileid userid = WorkflowWorkflowActionInfoAuthorized
  { wwaiaTo :: (Bool, WorkflowGraphNodeLabel)
  , wwaiaVia :: WorkflowGraphEdgeLabel
  , wwaiaUser :: (Bool, WorkflowActionUser userid)
  , wwaiaTime :: UTCTime
  , wwaiaPayload :: WorkflowStatePayloadAuthorized fileid userid
  } deriving stock (Eq, Ord, Show, Generic, Typeable)

type IdWorkflowWorkflowActionInfoAuthorized = WorkflowWorkflowActionInfoAuthorized FileReference UserId
type JSONIdWorkflowWorkflowActionInfoAuthorized = WorkflowWorkflowActionInfoAuthorized Any UserId
type DBWorkflowWorkflowActionInfoAuthorized = WorkflowWorkflowActionInfoAuthorized FileReference SqlBackendKey

_DBWorkflowWorkflowActionInfoAuthorized :: Iso' IdWorkflowWorkflowActionInfoAuthorized DBWorkflowWorkflowActionInfoAuthorized
_DBWorkflowWorkflowActionInfoAuthorized = iso toDB fromDB
  where
    -- No idea why using `typesCustom` doesn't work here, GK 2023-02-16
    toDB WorkflowWorkflowActionInfoAuthorized{..} = WorkflowWorkflowActionInfoAuthorized
      { wwaiaUser = wwaiaUser & _2 %~ view _DBWorkflowActionUser
      , wwaiaPayload = wwaiaPayload ^. _DBWorkflowStatePayloadAuthorized
      , ..
      }
    fromDB WorkflowWorkflowActionInfoAuthorized{..} = WorkflowWorkflowActionInfoAuthorized
      { wwaiaUser = wwaiaUser & _2 %~ review _DBWorkflowActionUser
      , wwaiaPayload = wwaiaPayload ^. from _DBWorkflowStatePayloadAuthorized
      , ..
      }

_wwaiaUnauthorized :: (Profunctor p, Contravariant f) => Optic' p f (WorkflowWorkflowActionInfoAuthorized fileid userid) (WorkflowWorkflowActionInfo fileid userid)
_wwaiaUnauthorized = to $ \WorkflowWorkflowActionInfoAuthorized{..} -> WorkflowWorkflowActionInfo
  { wwaiTo = wwaiaTo ^. _2
  , wwaiVia = wwaiaVia
  , wwaiUser = wwaiaUser ^. _2
  , wwaiTime = wwaiaTime
  , wwaiPayload = wwaiaPayload <&> view _2
  }

data WorkflowWorkflowEdgeForm fileid userid = WorkflowWorkflowEdgeForm
  { wwefTo :: WorkflowGraphNodeLabel
  , wwefVia :: WorkflowGraphEdgeLabel
  , wwefPayload :: WorkflowStatePayload fileid userid
  } deriving (Eq, Ord, Show, Generic, Typeable)
type IdWorkflowWorkflowEdgeForm = WorkflowWorkflowEdgeForm FileReference UserId
type JsonWorkflowWorkflowEdgeForm = WorkflowWorkflowEdgeForm PureFile UserEmail

makeLenses_ ''WorkflowWorkflowEdgeForm

workflowWorkflowEdgeFormAesonOptions :: _
workflowWorkflowEdgeFormAesonOptions = defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  }

instance (ToJSON fileid, ToJSON userid) => ToJSON (WorkflowWorkflowEdgeForm fileid userid) where
  toJSON = genericToJSON workflowWorkflowEdgeFormAesonOptions

instance ( FromJSON fileid, FromJSON userid
         , Ord fileid, Ord userid
         , Typeable fileid, Typeable userid
         ) => FromJSON (WorkflowWorkflowEdgeForm fileid userid) where
  parseJSON = genericParseJSON workflowWorkflowEdgeFormAesonOptions
