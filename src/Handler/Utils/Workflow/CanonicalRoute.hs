-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.CanonicalRoute where

import Import.NoFoundation
import Foundation.Type
import Foundation.Routes

import Utils.Workflow (RouteWorkflowScope)


data WorkflowScopeRoute
  = WorkflowInstanceListR
  | WorkflowInstanceNewR
  | WorkflowInstanceR WorkflowInstanceName WorkflowInstanceR
  | WorkflowWorkflowListR WorkflowWorkflowListType
  | WorkflowWorkflowR CryptoFileNameWorkflowWorkflow WorkflowWorkflowR
  deriving (Eq, Ord, Read, Show, Generic, Typeable)

data WorkflowInstanceR
  = WIEditR | WIDeleteR | WIWorkflowsR WorkflowWorkflowListType | WIInitiateR | WIUpdateR | WIOverviewR WorkflowOverviewName
  deriving (Eq, Ord, Read, Show, Generic, Typeable)

data WorkflowWorkflowR
  = WWWorkflowR | WWFilesR WorkflowPayloadLabel CryptoUUIDWorkflowStateIndex | WWEditR | WWDeleteR
  deriving (Eq, Ord, Read, Show, Generic, Typeable)


_WorkflowScopeRoute :: Prism' (Route UniWorX) (RouteWorkflowScope, WorkflowScopeRoute)
_WorkflowScopeRoute = prism' (uncurry toRoute) toWorkflowScopeRoute
  where
    toRoute = \case
      WSGlobal -> \case
        WorkflowInstanceListR            -> GlobalWorkflowInstanceListR
        WorkflowInstanceNewR             -> GlobalWorkflowInstanceNewR
        WorkflowInstanceR win subRoute   -> GlobalWorkflowInstanceR win $ case subRoute of
          WIEditR             -> GWIEditR
          WIDeleteR           -> GWIDeleteR
          WIWorkflowsR lState -> GWIWorkflowsR lState
          WIInitiateR         -> GWIInitiateR
          WIUpdateR           -> GWIUpdateR
          WIOverviewR won     -> GWIOverviewR won
        WorkflowWorkflowListR lState     -> GlobalWorkflowWorkflowListR lState
        WorkflowWorkflowR wwCID subRoute -> GlobalWorkflowWorkflowR wwCID $ case subRoute of
          WWWorkflowR        -> GWWWorkflowR
          WWFilesR wpl stCID -> GWWFilesR wpl stCID
          WWEditR            -> GWWEditR
          WWDeleteR          -> GWWDeleteR
      WSSchool ssh -> SchoolR ssh . \case
        WorkflowInstanceListR            -> SchoolWorkflowInstanceListR
        WorkflowInstanceNewR             -> SchoolWorkflowInstanceNewR
        WorkflowInstanceR win subRoute   -> SchoolWorkflowInstanceR win $ case subRoute of
          WIEditR             -> SWIEditR
          WIDeleteR           -> SWIDeleteR
          WIWorkflowsR lState -> SWIWorkflowsR lState
          WIInitiateR         -> SWIInitiateR
          WIUpdateR           -> SWIUpdateR
          WIOverviewR won     -> SWIOverviewR won
        WorkflowWorkflowListR lState     -> SchoolWorkflowWorkflowListR lState
        WorkflowWorkflowR wwCID subRoute -> SchoolWorkflowWorkflowR wwCID $ case subRoute of
          WWWorkflowR        -> SWWWorkflowR
          WWFilesR wpl stCID -> SWWFilesR wpl stCID
          WWEditR            -> SWWEditR
          WWDeleteR          -> SWWDeleteR
      WSTerm tid -> TermR tid . \case
        WorkflowInstanceListR            -> TermWorkflowInstanceListR
        WorkflowInstanceNewR             -> TermWorkflowInstanceNewR
        WorkflowInstanceR win subRoute   -> TermWorkflowInstanceR win $ case subRoute of
          WIEditR             -> TWIEditR
          WIDeleteR           -> TWIDeleteR
          WIWorkflowsR lState -> TWIWorkflowsR lState
          WIInitiateR         -> TWIInitiateR
          WIUpdateR           -> TWIUpdateR
          WIOverviewR won     -> TWIOverviewR won
        WorkflowWorkflowListR lState     -> TermWorkflowWorkflowListR lState
        WorkflowWorkflowR wwCID subRoute -> TermWorkflowWorkflowR wwCID $ case subRoute of
          WWWorkflowR        -> TWWWorkflowR
          WWFilesR wpl stCID -> TWWFilesR wpl stCID
          WWEditR            -> TWWEditR
          WWDeleteR          -> TWWDeleteR
      WSTermSchool tid ssh -> TermR tid . TermSchoolR ssh . \case
        WorkflowInstanceListR            -> TermSchoolWorkflowInstanceListR
        WorkflowInstanceNewR             -> TermSchoolWorkflowInstanceNewR
        WorkflowInstanceR win subRoute   -> TermSchoolWorkflowInstanceR win $ case subRoute of
          WIEditR             -> TSWIEditR
          WIDeleteR           -> TSWIDeleteR
          WIWorkflowsR lState -> TSWIWorkflowsR lState
          WIInitiateR         -> TSWIInitiateR
          WIUpdateR           -> TSWIUpdateR
          WIOverviewR won     -> TSWIOverviewR won
        WorkflowWorkflowListR lState     -> TermSchoolWorkflowWorkflowListR lState
        WorkflowWorkflowR wwCID subRoute -> TermSchoolWorkflowWorkflowR wwCID $ case subRoute of
          WWWorkflowR        -> TSWWWorkflowR
          WWFilesR wpl stCID -> TSWWFilesR wpl stCID
          WWEditR            -> TSWWEditR
          WWDeleteR          -> TSWWDeleteR
      other -> error $ "not implemented _WorkflowScopeRoute for: " <> show other
    toWorkflowScopeRoute = \case
      GlobalWorkflowInstanceListR            -> Just   ( WSGlobal, WorkflowInstanceListR )
      GlobalWorkflowInstanceNewR             -> Just   ( WSGlobal, WorkflowInstanceNewR )
      GlobalWorkflowInstanceR win subRoute   -> Just . ( WSGlobal, ) . WorkflowInstanceR win $ case subRoute of
        GWIEditR             -> WIEditR
        GWIDeleteR           -> WIDeleteR
        GWIWorkflowsR lState -> WIWorkflowsR lState
        GWIInitiateR         -> WIInitiateR
        GWIUpdateR           -> WIUpdateR
        GWIOverviewR won     -> WIOverviewR won
      GlobalWorkflowWorkflowListR lState     -> Just   ( WSGlobal, WorkflowWorkflowListR lState )
      GlobalWorkflowWorkflowR wwCID subRoute -> Just . ( WSGlobal, ) . WorkflowWorkflowR wwCID $ case subRoute of
        GWWWorkflowR        -> WWWorkflowR
        GWWFilesR wpl stCID -> WWFilesR wpl stCID
        GWWEditR            -> WWEditR
        GWWDeleteR          -> WWDeleteR
      SchoolR ssh sRoute -> case sRoute of
        SchoolWorkflowInstanceListR            -> Just   ( WSSchool ssh, WorkflowInstanceListR )
        SchoolWorkflowInstanceNewR             -> Just   ( WSSchool ssh, WorkflowInstanceNewR )
        SchoolWorkflowInstanceR win subRoute   -> Just . ( WSSchool ssh, ) . WorkflowInstanceR win $ case subRoute of
          SWIEditR             -> WIEditR
          SWIDeleteR           -> WIDeleteR
          SWIWorkflowsR lState -> WIWorkflowsR lState
          SWIInitiateR         -> WIInitiateR
          SWIUpdateR           -> WIUpdateR
          SWIOverviewR won     -> WIOverviewR won
        SchoolWorkflowWorkflowListR lState     -> Just   ( WSSchool ssh, WorkflowWorkflowListR lState )
        SchoolWorkflowWorkflowR wwCID subRoute -> Just . ( WSSchool ssh, ) . WorkflowWorkflowR wwCID $ case subRoute of
          SWWWorkflowR        -> WWWorkflowR
          SWWFilesR wpl stCID -> WWFilesR wpl stCID
          SWWEditR            -> WWEditR
          SWWDeleteR          -> WWDeleteR
        _other -> Nothing
      TermR tid sRoute -> case sRoute of
        TermWorkflowInstanceListR            -> Just   ( WSTerm tid, WorkflowInstanceListR )
        TermWorkflowInstanceNewR             -> Just   ( WSTerm tid, WorkflowInstanceNewR )
        TermWorkflowInstanceR win subRoute   -> Just . ( WSTerm tid, ) . WorkflowInstanceR win $ case subRoute of
          TWIEditR             -> WIEditR
          TWIDeleteR           -> WIDeleteR
          TWIWorkflowsR lState -> WIWorkflowsR lState
          TWIInitiateR         -> WIInitiateR
          TWIUpdateR           -> WIUpdateR
          TWIOverviewR won     -> WIOverviewR won
        TermWorkflowWorkflowListR lState     -> Just   ( WSTerm tid, WorkflowWorkflowListR lState )
        TermWorkflowWorkflowR wwCID subRoute -> Just . ( WSTerm tid, ) . WorkflowWorkflowR wwCID $ case subRoute of
          TWWWorkflowR        -> WWWorkflowR
          TWWFilesR wpl stCID -> WWFilesR wpl stCID
          TWWEditR            -> WWEditR
          TWWDeleteR          -> WWDeleteR

        TermSchoolR ssh sRoute' -> case sRoute' of
          TermSchoolWorkflowInstanceNewR             -> Just   ( WSTermSchool tid ssh, WorkflowInstanceNewR )
          TermSchoolWorkflowInstanceR win subRoute   -> Just . ( WSTermSchool tid ssh, ) . WorkflowInstanceR win $ case subRoute of
            TSWIEditR             -> WIEditR
            TSWIDeleteR           -> WIDeleteR
            TSWIWorkflowsR lState -> WIWorkflowsR lState
            TSWIInitiateR         -> WIInitiateR
            TSWIUpdateR           -> WIUpdateR
            TSWIOverviewR won     -> WIOverviewR won
          TermSchoolWorkflowWorkflowListR lState     -> Just   ( WSTermSchool tid ssh, WorkflowWorkflowListR lState )
          TermSchoolWorkflowWorkflowR wwCID subRoute -> Just . ( WSTermSchool tid ssh, ) . WorkflowWorkflowR wwCID $ case subRoute of
            TSWWWorkflowR        -> WWWorkflowR
            TSWWFilesR wpl stCID -> WWFilesR wpl stCID
            TSWWEditR            -> WWEditR
            TSWWDeleteR          -> WWDeleteR
          _other -> Nothing

        _other -> Nothing
      _other -> Nothing
