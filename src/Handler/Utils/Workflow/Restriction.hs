-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.Restriction
  ( checkWorkflowRestriction
  ) where

import Import

import Handler.Utils.Workflow.Types

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Sequence as Seq


checkWorkflowRestriction :: Maybe IdWorkflowRestrictionState
                         -> PredDNF WorkflowGraphRestriction
                         -> Bool
checkWorkflowRestriction mHistory = evalPredDNF evalPred
  where
    evalPred PLVariable{ plVar = WorkflowGraphRestrictionPayloadFilled{..} } = wgrPayloadFilled `Map.member`    filledPayloads
    evalPred PLNegated{  plVar = WorkflowGraphRestrictionPayloadFilled{..} } = wgrPayloadFilled `Map.notMember` filledPayloads

    evalPred PLVariable{ plVar = WorkflowGraphRestrictionPreviousNode{..}  } = maybe False (wgrPreviousNode ==) cState
    evalPred PLNegated{  plVar = WorkflowGraphRestrictionPreviousNode{..}  } = maybe True  (wgrPreviousNode /=) cState

    evalPred PLVariable{ plVar = WorkflowGraphRestrictionPreviousEdge{..}  } = maybe False (wgrPreviousEdge ==) prevEdge
    evalPred PLNegated{  plVar = WorkflowGraphRestrictionPreviousEdge{..}  } = maybe True  (wgrPreviousEdge /=) prevEdge

    evalPred PLVariable{ plVar = WorkflowGraphRestrictionNodeInHistory{..} } = wgrNodeInHistory `Set.member`    nodesInHistory
    evalPred PLNegated{  plVar = WorkflowGraphRestrictionNodeInHistory{..} } = wgrNodeInHistory `Set.notMember` nodesInHistory

    evalPred PLVariable{ plVar = WorkflowGraphRestrictionEdgeInHistory{..} } = wgrEdgeInHistory `Set.member`    edgesInHistory
    evalPred PLNegated{  plVar = WorkflowGraphRestrictionEdgeInHistory{..} } = wgrEdgeInHistory `Set.notMember` edgesInHistory

    evalPred PLVariable{ plVar = WorkflowGraphRestrictionInitial           } = is    _Nothing mHistory
    evalPred PLNegated{  plVar = WorkflowGraphRestrictionInitial           } = isn't _Nothing mHistory

    filledPayloads = maybe Map.empty (Map.mapMaybe fromNullable . wwrCurrentPayload) mHistory
    cState = wwrCurrentNode <$> mHistory
    prevEdge = case Seq.viewr . wwrEdgeHistory <$> mHistory of
      Just (_ Seq.:> prev) -> Just prev
      _                    -> Nothing
    nodesInHistory = maybe Set.empty (Set.fromList . toList . wwrNodeHistory) mHistory
    edgesInHistory = maybe Set.empty (Set.fromList . toList . wwrEdgeHistory) mHistory
