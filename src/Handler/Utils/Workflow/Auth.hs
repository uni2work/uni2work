-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.Auth
  ( getSharedDBWorkflowGraphAuth, getSharedIdWorkflowGraphAuth
  ) where

import Import.NoFoundation
import Foundation.Type

import Utils.Workflow

import Handler.Utils.Memcached

{-# ANN module ("HLint: ignore Use newtype instead of data" :: String) #-}


data SharedDBWorkflowGraphAuthCacheKey
  = SharedDBWorkflowGraphAuthFor SharedWorkflowGraphId
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Hashable, Binary)
  
getSharedDBWorkflowGraphAuth :: ( MonadHandler m, HandlerSite m ~ UniWorX
                                , MonadThrow m
                                , BackendCompatible SqlReadBackend backend
                                )
                             => SharedWorkflowGraphId
                             -> ReaderT backend m DBWorkflowGraphAuth
getSharedDBWorkflowGraphAuth swgId = memcachedBy Nothing (SharedDBWorkflowGraphAuthFor swgId) $ mkWorkflowGraphAuth <$> getSharedDBWorkflowGraph swgId

getSharedIdWorkflowGraphAuth :: ( MonadHandler m, HandlerSite m ~ UniWorX
                                , MonadThrow m
                                , BackendCompatible SqlReadBackend backend
                                )
                             => SharedWorkflowGraphId
                             -> ReaderT backend m IdWorkflowGraphAuth
getSharedIdWorkflowGraphAuth = fmap (review _DBWorkflowGraphAuth) . getSharedDBWorkflowGraphAuth
