-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.Workflow
  ( ensureScope
  , appendWorkflowRestrictionState, accumWorkflowWorkflowRestrictionState
  , followEdge
  , followAutomaticEdges, WorkflowAutomaticEdgeException(..)
  , sourceWorkflowWorkflowActionInfos, sinkWorkflowWorkflowActionInfos
  , sourceWorkflowWorkflowActionInfosAuthorized, sourceWorkflowWorkflowActionInfosAuthorizedForJSON
  , getWorkflowWorkflowRestrictionState
  , module Handler.Utils.Workflow.Restriction
  , module Handler.Utils.Workflow.Payload
  ) where

import Import

import Utils.Workflow
import Handler.Utils.Workflow.Types
import Handler.Utils.Workflow.Restriction
import Handler.Utils.Workflow.Payload
import Handler.Utils.Workflow.EdgeForm
import Handler.Utils.Workflow.Auth
import Handler.Utils.Workflow.CanonicalRoute

import qualified Data.Map as Map
import qualified Data.Map.Merge.Strict as Map
import qualified Data.Set as Set
import qualified Data.Sequence as Seq
import qualified Data.List.NonEmpty as NonEmpty

import qualified Data.Conduit.Combinators as C
import qualified Data.Conduit.List as C (unfoldM)

import qualified Control.Monad.State.Class as State

import qualified Database.Esqueleto.Legacy as E
import qualified Database.Esqueleto.Utils as E
import qualified Database.Esqueleto.Internal.Internal as E (SqlSelect)
import Database.Persist.Sql.Raw.QQ (executeQQ)

import Data.Coerce (coerce)


ensureScope :: IdWorkflowScope -> CryptoFileNameWorkflowWorkflow -> MaybeT DB WorkflowWorkflowId
ensureScope wiScope cID = do
  wId <- catchMaybeT (Proxy @CryptoIDError) $ decrypt cID
  WorkflowWorkflow{..} <- MaybeT $ get wId
  let wiScope' = wiScope
        & _wisTerm   %~ unTermKey
        & _wisSchool %~ unSchoolKey
        & _wisCourse %~ view _SqlKey
  guard $ workflowWorkflowScope == wiScope'
  return wId


appendWorkflowRestrictionState :: Maybe (WorkflowRestrictionState fileid userid) -> WorkflowWorkflowActionInfo fileid userid -> WorkflowRestrictionState fileid userid
appendWorkflowRestrictionState Nothing WorkflowWorkflowActionInfo{..} = WorkflowRestrictionState
  { wwrCurrentNode = wwaiTo
  , wwrCurrentPayload = wwaiPayload
  , wwrNodeHistory = Seq.singleton wwaiTo
  , wwrEdgeHistory = Seq.singleton wwaiVia
  }
appendWorkflowRestrictionState (Just WorkflowRestrictionState{..}) WorkflowWorkflowActionInfo{..} = WorkflowRestrictionState
  { wwrCurrentNode = wwaiTo
  , wwrCurrentPayload = Map.unionWith (\_ v -> v) wwrCurrentPayload wwaiPayload
  , wwrNodeHistory = wwrNodeHistory Seq.|> wwaiTo
  , wwrEdgeHistory = wwrEdgeHistory Seq.|> wwaiVia
  }

accumWorkflowWorkflowRestrictionState :: Monad m => ConduitT (WorkflowWorkflowActionInfo fileid userid) o m (Maybe (WorkflowRestrictionState fileid userid))
accumWorkflowWorkflowRestrictionState = C.foldl ((Just .) . appendWorkflowRestrictionState) Nothing

followEdge :: ( MonadHandler m
              , HandlerSite m ~ UniWorX
              , MonadThrow m
              )
           => IdWorkflowGraph -> IdWorkflowWorkflowEdgeFormAligned -> Maybe IdWorkflowRestrictionState -> ConduitT () IdWorkflowWorkflowActionInfo m ()
-- | Remember to invalidate auth cache
followEdge graph edgeRes cState = do
  act <- workflowEdgeFormToAction edgeRes
  yield act
  followAutomaticEdges graph $ appendWorkflowRestrictionState cState act

data WorkflowAutomaticEdgeException
  = WorkflowAutomaticEdgeCycle [(WorkflowGraphEdgeLabel, WorkflowGraphNodeLabel)]
  | WorkflowAutomaticEdgeAmbiguity (Set (WorkflowGraphEdgeLabel, WorkflowGraphNodeLabel))
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

followAutomaticEdges :: forall m.
                        ( MonadIO m
                        , MonadThrow m
                        )
                     => IdWorkflowGraph -> IdWorkflowRestrictionState -> ConduitT () IdWorkflowWorkflowActionInfo m ()
followAutomaticEdges WorkflowGraph{..} restrState = C.unfoldM go (restrState, [])
  where
    go :: ( IdWorkflowRestrictionState
          , [(IdWorkflowRestrictionState, (WorkflowGraphEdgeLabel, WorkflowGraphNodeLabel))]
          )
       -> m (Maybe ( IdWorkflowWorkflowActionInfo
                   , ( IdWorkflowRestrictionState
                     , [(IdWorkflowRestrictionState, (WorkflowGraphEdgeLabel, WorkflowGraphNodeLabel))]
                     )
                   )
            )
    go (restrState', edgesTaken)
      | null automaticEdgeOptions = return Nothing
      | [edge@(edgeLbl, nodeLbl)] <- automaticEdgeOptions = if
          | (restrState', edge) `elem` edgesTaken
            -> throwM . WorkflowAutomaticEdgeCycle . reverse $ map (view _2) edgesTaken
          | otherwise -> do
              wwaiTime <- liftIO getCurrentTime
              let wwai = WorkflowWorkflowActionInfo
                    { wwaiTo = nodeLbl
                    , wwaiVia = edgeLbl
                    , wwaiUser = WorkflowActionAutomatic
                    , wwaiTime
                    , wwaiPayload = mempty
                    }
              return $ Just
                ( wwai
                , ( appendWorkflowRestrictionState (Just restrState') wwai
                  , (restrState', edge) : edgesTaken
                  )
                )
      | otherwise = throwM . WorkflowAutomaticEdgeAmbiguity $ Set.fromList automaticEdgeOptions
      where
        automaticEdgeOptions = nubOrd $ do
          (nodeLbl, WGN{..}) <- Map.toList wgNodes
          (edgeLbl, WorkflowGraphEdgeAutomatic{..}) <- Map.toList wgnEdges
          guard $ wgeSource == wwrCurrentNode restrState'
          whenIsJust wgeRestriction $ guard . checkWorkflowRestriction (Just restrState')
          return (edgeLbl, nodeLbl)


sinkWorkflowWorkflowActionInfos :: (MonadHandler m, HandlerSite m ~ UniWorX)
                                => WorkflowWorkflowId
                                -> ConduitT IdWorkflowWorkflowActionInfo o (SqlPersistT m) ()
sinkWorkflowWorkflowActionInfos wwId = do
  maxIx <- lift . E.selectMaybe . E.from $ \workflowWorkflowAction -> do
    E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
    return . E.max_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionIx

  let accumCurrentPayload = fmap unMergeMap . C.foldMap $ \(E.Value lbl, E.Value val) -> MergeMap . Map.singleton lbl $ opoint val
  currentPayload <- lift . runConduit . (.| accumCurrentPayload) . E.selectSource . E.from $ \(workflowWorkflowPayload `E.InnerJoin` workflowWorkflowAction) -> do
    E.on $ workflowWorkflowPayload E.^. WorkflowWorkflowPayloadAction E.==. workflowWorkflowAction E.^. WorkflowWorkflowActionId
    E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
    E.where_ $ workflowWorkflowPayload E.^. WorkflowWorkflowPayloadCurrent
    payload <- E.guardMaybe $ workflowWorkflowPayload E.^. WorkflowWorkflowPayloadPayload
    return ( workflowWorkflowPayload E.^. WorkflowWorkflowPayloadLabel
           , payload
           )

  let withIx = evalStateC (maybe 0 succ $ E.unValue =<< maxIx) . C.mapM $ \x -> State.state (\currIx -> ((currIx, x), succ currIx))
      doInsert (workflowWorkflowActionIx, view _DBWorkflowWorkflowActionInfo -> WorkflowWorkflowActionInfo{..}) = do
        wwaId <- lift $ insert WorkflowWorkflowAction
          { workflowWorkflowActionWorkflow = wwId
          , workflowWorkflowActionIx
          , workflowWorkflowActionTo = wwaiTo
          , workflowWorkflowActionVia = wwaiVia
          , workflowWorkflowActionUser = wwaiUser
          , workflowWorkflowActionTime = wwaiTime
          }
        payload' <- iforM wwaiPayload $ \label payloads ->
          fmap (, payloads) . State.state $ \prevPayloads ->
            ( Map.lookup label prevPayloads /= fromNullable payloads
            , Map.update (const $ fromNullable payloads) label prevPayloads
            )
        lift $ case Map.keysSet $ Map.filter (view _1) payload' of
          (fromNullable -> Just (toNonEmpty -> changedPayloads))
            -> [executeQQ|
                 UPDATE ^{WorkflowWorkflowPayload} wwp
                   SET @{WorkflowWorkflowPayloadCurrent} = #{False}
                   FROM ^{WorkflowWorkflowAction} wwa
                   WHERE     wwa.@{WorkflowWorkflowActionId} = wwp.@{WorkflowWorkflowPayloadAction}
                         AND wwa.@{WorkflowWorkflowActionWorkflow} = #{wwId}
                         AND wwa.@{WorkflowWorkflowActionIx} < #{workflowWorkflowActionIx}
                         AND wwp.@{WorkflowWorkflowPayloadLabel} IN %{changedPayloads};
               |]
          _other -> return ()
        iforM_ payload' $ \workflowWorkflowPayloadLabel (didChange, payloads) -> do
          if | not didChange
               -> return ()
             | Set.null payloads -> lift $ insert_ WorkflowWorkflowPayload
                 { workflowWorkflowPayloadAction = wwaId
                 , workflowWorkflowPayloadLabel
                 , workflowWorkflowPayloadCurrent = True
                 , workflowWorkflowPayloadPayload = Nothing
                 }
             | otherwise -> lift $ insertMany_
                 [ WorkflowWorkflowPayload
                     { workflowWorkflowPayloadAction = wwaId
                     , workflowWorkflowPayloadLabel
                     , workflowWorkflowPayloadCurrent = True
                     , workflowWorkflowPayloadPayload = Just payload
                     }
                 | payload <- Set.toList payloads
                 ]
      updateArchived WorkflowWorkflowActionInfo{wwaiTo, wwaiTime} = do
        WorkflowGraph{wgNodes} <- lift . getSharedIdWorkflowGraph . workflowWorkflowGraph =<< MaybeT (get wwId)
        WGN{wgnFinal} <- hoistMaybe $ Map.lookup wwaiTo wgNodes
        archiveAfter <- getsYesod $ view _appWorkflowWorkflowArchiveAfter
        let wwArchived = (`addUTCTime` wwaiTime) <$> archiveAfter <* wgnFinal
        lift $ update wwId [WorkflowWorkflowArchived =. wwArchived]

  toConsumer . getZipSink $
       ZipSink (withIx .| evalStateC currentPayload (C.mapM_ doInsert))
    *> ZipSink (maybeT_ $ MaybeT C.last >>= hoist lift . updateArchived)

sourceWorkflowWorkflowActionInfos' :: forall m backend r b b'.
                                      ( MonadResource m
                                      , BackendCompatible SqlReadBackend backend
                                      , E.SqlSelect b b'
                                      )
                                   => (E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlExpr (E.Value Bool) -> E.SqlExpr (E.Value Bool))
                                   -> (forall a. (E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery (a, b)))
                                   -> (NonEmpty b' -> IdWorkflowWorkflowActionInfo -> r)
                                   -> ConduitT () (WorkflowStateIndex, r) (ReaderT backend m) ()
sourceWorkflowWorkflowActionInfos' extraOn cont fin = transPipe (withReaderT $ projectBackend @SqlBackend . projectBackend @SqlReadBackend) $ sourcePayloads .| accumActionInfo
  where
    sourcePayloads = E.selectSource . E.from $ \x@(workflowWorkflowAction `E.LeftOuterJoin` workflowWorkflowPayload) -> do
      E.on . extraOn x $ E.just (workflowWorkflowAction E.^. WorkflowWorkflowActionId) E.==. workflowWorkflowPayload E.?. WorkflowWorkflowPayloadAction
      cont x $ do
        E.orderBy [ E.asc $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow
                  , E.asc $ workflowWorkflowAction E.^. WorkflowWorkflowActionIx
                  ]
        return ( workflowWorkflowAction
               , workflowWorkflowPayload
               )
    accumActionInfo = awaitForever $ \((Entity _ wwA, mPayload), b) -> do
      let
        accumPayloads firstPayload = sourcePayloads' .| injectFirstPayload .| accumPayloads'
          where sourcePayloads' = C.mapWhile $ \((Entity _ wwA', mPayload'), b') -> do
                  guard $ workflowWorkflowActionIx wwA' == workflowWorkflowActionIx wwA
                  (, Just b') <$> mPayload'
                injectFirstPayload = yield (firstPayload, Nothing) >> C.map id
                accumPayloads' = over _2 (b :|) . over _1 unMergeMap <$> execWriterC (C.mapM_ tellPayload)
                  where tellPayload (Entity _ WorkflowWorkflowPayload{..}, b') = tell . (, hoistMaybe b') . MergeMap . Map.singleton workflowWorkflowPayloadLabel $ workflowWorkflowPayloadPayload ^. _Just . re _DBWorkflowFieldPayloadW . to Set.singleton
      (wwaiPayload, bs) <- maybe (return (mempty, b :| [])) accumPayloads mPayload
      yield $ let WorkflowWorkflowAction{..} = wwA
               in ( workflowWorkflowActionIx
                  , fin bs WorkflowWorkflowActionInfo
                    { wwaiTo = workflowWorkflowActionTo
                    , wwaiVia = workflowWorkflowActionVia
                    , wwaiUser = _DBWorkflowActionUser # workflowWorkflowActionUser
                    , wwaiTime = workflowWorkflowActionTime
                    , wwaiPayload
                    }
                  )

sourceWorkflowWorkflowActionInfos :: forall m backend.
                                     ( MonadResource m
                                     , BackendCompatible SqlReadBackend backend
                                     )
                                  => (E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlExpr (E.Value Bool) -> E.SqlExpr (E.Value Bool))
                                  -> (forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery a)
                                  -> ConduitT () (WorkflowStateIndex, IdWorkflowWorkflowActionInfo) (ReaderT backend m) ()
sourceWorkflowWorkflowActionInfos extraOn cont = sourceWorkflowWorkflowActionInfos' extraOn (\x -> fmap (, ()) . cont x) (const id)


sourceWorkflowWorkflowActionInfosAuthorized
  :: forall m backend.
     ( MonadHandler m, HandlerSite m ~ UniWorX
     , MonadUnliftIO m
     , MonadCatch m
     , BackendCompatible SqlReadBackend backend
     )
  => (E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlExpr (E.Value Bool) -> E.SqlExpr (E.Value Bool))
  -> (forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery a)
  -> Entity WorkflowWorkflow
  -> ConduitT () (WorkflowStateIndex, IdWorkflowWorkflowActionInfoAuthorized) (ReaderT backend m) ()
sourceWorkflowWorkflowActionInfosAuthorized extraOn' cont' (Entity wwId WorkflowWorkflow{..}) = maybeT mempty $ do
  -- cID <- encrypt wwId
  scope <- hoist lift . toRouteWorkflowScope $ _DBWorkflowScope # workflowWorkflowScope
  -- let route = _WorkflowScopeRoute # (scope, WorkflowWorkflowR cID WWWorkflowR)
  let route = _WorkflowScopeRoute # (scope, WorkflowWorkflowListR WorkflowWorkflowListAll)

  graphAuth'@(WorkflowGraphAuth graphAuth) <- lift . lift $ getSharedIdWorkflowGraphAuth workflowWorkflowGraph
  authWhere <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowAction) route False
  payloadAuthWhere <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowPayload) route False
  actorAuth <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowActor) route False
  nodeLabelAuth <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowNodeLabel) route False
  payloadLabelAuth <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowPayloadLabel) route False

  let
    cont :: forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery (a, (E.SqlExpr (E.Value Bool), E.SqlExpr (E.Value Bool), (E.SqlExpr (E.Value (Maybe WorkflowPayloadLabel)), E.SqlExpr (E.Value Bool))))
    cont x@(workflowWorkflowAction `E.LeftOuterJoin` workflowWorkflowPayload) act = do
      E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
           E.&&. (       E.isJust (workflowWorkflowPayload E.?. WorkflowWorkflowPayloadId)
                   E.||. $$(E.annSqlExpr) (authWhere $ WorkflowAuthWhereAction workflowWorkflowAction)
                 )
      let viewActor = $$(E.annSqlExpr) (actorAuth $ WorkflowAuthWhereAction workflowWorkflowAction)
          viewNodeLabel = $$(E.annSqlExpr) (nodeLabelAuth $ WorkflowAuthWhereAction workflowWorkflowAction)
          viewPayloadLabel = E.maybeEntity E.false ($$(E.annSqlExpr) . nodeLabelAuth . WorkflowAuthWherePayload workflowWorkflowAction) workflowWorkflowPayload
      (, (viewActor, viewNodeLabel, (workflowWorkflowPayload E.?. WorkflowWorkflowPayloadLabel, viewPayloadLabel))) <$> cont' x act
    extraOn x@(workflowWorkflowAction `E.LeftOuterJoin` workflowWorkflowPayload) = (extraOn' x .) . (E.&&.) .
      $$(E.annSqlExpr) . payloadAuthWhere $ WorkflowAuthWherePayload workflowWorkflowAction (coerce workflowWorkflowPayload) -- coerce should be safe here since we're "only" dropping the Maybe within E.SqlExpr and using this expression *only within an ON-clause*
    fin :: NonEmpty (E.Value Bool, E.Value Bool, (E.Value (Maybe WorkflowPayloadLabel), E.Value Bool)) -> IdWorkflowWorkflowActionInfo
        -> IdWorkflowWorkflowActionInfoAuthorized
    fin bs@((E.Value viewActor, E.Value viewNodeLabel, _) :| _) WorkflowWorkflowActionInfo{..}
        = WorkflowWorkflowActionInfoAuthorized
            { wwaiaTo = (viewNodeLabel, wwaiTo)
            , wwaiaVia = wwaiVia
            , wwaiaUser = (viewActor, wwaiUser)
            , wwaiaTime = wwaiTime
            , wwaiaPayload
                = Map.merge
                    Map.dropMissing
                    (Map.mapMissing $ const (False, ))
                    (Map.zipWithMatched $ const (,))
                    viewPayloadLabels
                    wwaiPayload
            }
      where
        viewPayloadLabels = Map.fromList . mapMaybe (views _3 $ \(E.Value mKey, E.Value v) -> (, v) <$> mKey) $ NonEmpty.toList bs

  lift $ sourceWorkflowWorkflowActionInfos' extraOn cont fin

sourceWorkflowWorkflowActionInfosAuthorizedForJSON
  :: forall m backend.
     ( MonadHandler m, HandlerSite m ~ UniWorX
     , MonadUnliftIO m
     , MonadCatch m
     , BackendCompatible SqlReadBackend backend
     )
  => (E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlExpr (E.Value Bool) -> E.SqlExpr (E.Value Bool))
  -> (forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery a)
  -> Entity WorkflowWorkflow
  -> ConduitT () (WorkflowStateIndex, JSONIdWorkflowWorkflowActionInfoAuthorized) (ReaderT backend m) ()
sourceWorkflowWorkflowActionInfosAuthorizedForJSON extraOn' cont' (Entity wwId WorkflowWorkflow{..}) = maybeT mempty $ do
  -- cID <- encrypt wwId
  scope <- hoist lift . toRouteWorkflowScope $ _DBWorkflowScope # workflowWorkflowScope
  -- let route = _WorkflowScopeRoute # (scope, WorkflowWorkflowR cID WWWorkflowR)
  let route = _WorkflowScopeRoute # (scope, WorkflowWorkflowListR WorkflowWorkflowListAll)

  graphAuth'@(WorkflowGraphAuth graphAuth) <- lift . lift $ getSharedIdWorkflowGraphAuth workflowWorkflowGraph
  authWhere <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowAction) route False
  payloadAuthWhere <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowPayload) route False
  actorAuth <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowActor) route False
  nodeLabelAuth <- lift . lift $ workflowAuthWhere'' (Right graphAuth') (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowNodeLabel) route False

  let
    cont :: forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery (a, (E.SqlExpr (E.Value Bool), E.SqlExpr (E.Value Bool)))
    cont x@(workflowWorkflowAction `E.LeftOuterJoin` workflowWorkflowPayload) act = E.distinctOnOrderBy
      [ E.asc $ workflowWorkflowAction E.^. WorkflowWorkflowActionId
      , E.asc $ workflowWorkflowPayload E.?. WorkflowWorkflowPayloadLabel
      , E.asc $ E.case_
        [ E.when_ (E.maybe E.false ((E.==. E.val WFPFile') . (E.->. "tag")) $ workflowWorkflowPayload E.?. WorkflowWorkflowPayloadPayload)
          E.then_ E.nothing
        ] (E.else_ $ workflowWorkflowPayload E.?. WorkflowWorkflowPayloadId)
      ] $ do
        E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
             E.&&. (       E.isJust (workflowWorkflowPayload E.?. WorkflowWorkflowPayloadId)
                     E.||. $$(E.annSqlExpr) (authWhere $ WorkflowAuthWhereAction workflowWorkflowAction)
                   )
        let viewActor = $$(E.annSqlExpr) (actorAuth $ WorkflowAuthWhereAction workflowWorkflowAction)
            viewNodeLabel = $$(E.annSqlExpr) (nodeLabelAuth $ WorkflowAuthWhereAction workflowWorkflowAction)
        (, (viewActor, viewNodeLabel)) <$> cont' x act
    extraOn x@(workflowWorkflowAction `E.LeftOuterJoin` workflowWorkflowPayload) = (extraOn' x .) . (E.&&.) .
      $$(E.annSqlExpr) . payloadAuthWhere $ WorkflowAuthWherePayload workflowWorkflowAction (coerce workflowWorkflowPayload) -- coerce should be safe here since we're "only" dropping the Maybe within E.SqlExpr and using this expression *only within an ON-clause*
    fin :: NonEmpty (E.Value Bool, E.Value Bool) -> IdWorkflowWorkflowActionInfo
        -> JSONIdWorkflowWorkflowActionInfoAuthorized
    fin ((E.Value viewActor, E.Value viewNodeLabel) :| _) WorkflowWorkflowActionInfo{..}
        = WorkflowWorkflowActionInfoAuthorized
            { wwaiaTo = (viewNodeLabel, wwaiTo)
            , wwaiaVia = wwaiVia
            , wwaiaUser = (viewActor, wwaiUser)
            , wwaiaTime = wwaiTime
            , wwaiaPayload = wwaiPayload
                & fmap (False, )
                & typesCustom @WorkflowChildren @IdWorkflowStatePayloadAuthorized @JSONIdWorkflowStatePayloadAuthorized @FileReference @Any .~ Any True
            }

  lift $ sourceWorkflowWorkflowActionInfos' extraOn cont fin


getWorkflowWorkflowRestrictionState :: ( MonadIO m, MonadResource m
                                       , BackendCompatible SqlReadBackend backend
                                       )
                                    => WorkflowWorkflowId
                                    -> ReaderT backend m (Maybe IdWorkflowRestrictionState)
getWorkflowWorkflowRestrictionState wwId = runConduit $ sourceWorkflowWorkflowActionInfos (const id) cont .| C.map (view _2) .| accumWorkflowWorkflowRestrictionState
  where
    cont :: forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowWorkflowPayload)) -> E.SqlQuery a -> E.SqlQuery a
    cont (workflowWorkflowAction `E.LeftOuterJoin` _) act = (*> act) $
      E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
