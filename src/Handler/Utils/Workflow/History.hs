-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.History
  ( WorkflowHistoryItemActor'(..), WorkflowHistoryItemActor
  , _WHIAHidden, _WHIASelf, _WHIAOther, _WHIAGone
  , WorkflowHistoryItemUser', WorkflowHistoryItemUser
  , WorkflowHistoryItem(..)
  , WorkflowCurrentState(..)
  ) where

import Import


data WorkflowHistoryItemActor' user = WHIAHidden | WHIASelf | WHIAOther { whiaUser :: user } | WHIAGone
  deriving stock (Eq, Ord, Show, Read, Data, Generic, Typeable)
  deriving stock (Functor, Foldable, Traversable)
  deriving anyclass (NFData)

type WorkflowHistoryItemActor = WorkflowHistoryItemActor' (Entity User)


type WorkflowHistoryItemUser' userid = WorkflowActionUser (WorkflowHistoryItemActor' userid)

type WorkflowHistoryItemUser = WorkflowHistoryItemUser' (Entity User)


data WorkflowHistoryItem = WorkflowHistoryItem
  { whiUser :: WorkflowHistoryItemUser
  , whiTime :: UTCTime
  , whiPayloadChanges :: [(Text, ([WorkflowFieldPayloadW Void (Maybe (Entity User))], Maybe Text))]
  , whiFrom :: Maybe (Maybe (Text, Maybe Icon)) -- ^ outer maybe encodes existence, inner maybe encodes permission to view
  , whiVia :: Maybe (Maybe Text) -- ^ outer maybe encodes existence, inner maybe encodes permission to view
  , whiTo :: Maybe (Text, Maybe Icon)
  } deriving (Generic, Typeable)


data WorkflowCurrentState = WorkflowCurrentState
  { wcsState :: Maybe (Text, Maybe Icon)
  , wcsStages :: [(WorkflowGraphStageLabel, Text, [(Text, Icon, Bool)])]
  , wcsArchived :: Maybe UTCTime
  , wcsMessages :: Set Message
  , wcsPayload :: [(Text, ([WorkflowFieldPayloadW Void (Maybe (Entity User))], Maybe Text))]
  }


makeLenses_ ''WorkflowCurrentState
makePrisms ''WorkflowHistoryItemActor'
