-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.Payload
  ( workflowStateCurrentPayloads
  , workflowStateCurrentPayloadsAuthorized
  , workflowStateCurrentPayloads'
  , workflowStateCurrentPayloadsAuthorized'
  ) where

import Import

import Handler.Utils.Workflow.Types
import Handler.Utils.Workflow.CanonicalRoute
import Utils.Workflow

import qualified Data.Map as Map
import qualified Data.Set as Set

import qualified Data.Conduit.Combinators as C

import qualified Database.Esqueleto.Legacy as E
import qualified Database.Esqueleto.Utils as E


workflowStateCurrentPayloads' :: (forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.InnerJoin` E.SqlExpr (Entity WorkflowWorkflowPayload) -> E.SqlQuery a -> E.SqlQuery a)
                              -> DB DBWorkflowStatePayload
workflowStateCurrentPayloads' cont = runConduit . (.| accum) . E.selectSource . E.from $ \x@(workflowWorkflowAction `E.InnerJoin` workflowWorkflowPayload) -> do
  E.on $ workflowWorkflowAction E.^. WorkflowWorkflowActionId E.==. workflowWorkflowPayload E.^. WorkflowWorkflowPayloadAction
  cont x $ do
    E.where_ . E.notExists . E.from $ \x'@(workflowWorkflowAction' `E.InnerJoin` workflowWorkflowPayload') -> do
      E.on $ workflowWorkflowAction' E.^. WorkflowWorkflowActionId E.==. workflowWorkflowPayload' E.^. WorkflowWorkflowPayloadAction
      cont x' $
        E.where_ $ workflowWorkflowPayload' E.^. WorkflowWorkflowPayloadLabel E.==. workflowWorkflowPayload E.^. WorkflowWorkflowPayloadLabel
             E.&&. workflowWorkflowAction' E.^. WorkflowWorkflowActionIx E.>. workflowWorkflowAction E.^. WorkflowWorkflowActionIx
    return workflowWorkflowPayload
  where
    accum :: forall m o. Monad m => ConduitT (Entity WorkflowWorkflowPayload) o m DBWorkflowStatePayload 
    accum = fmap unMergeMap . C.foldMap $ \(Entity _ WorkflowWorkflowPayload{..}) -> MergeMap (Map.singleton workflowWorkflowPayloadLabel $ maybe Set.empty Set.singleton workflowWorkflowPayloadPayload)

workflowStateCurrentPayloads :: WorkflowWorkflowId -> DB DBWorkflowStatePayload
workflowStateCurrentPayloads wwId = workflowStateCurrentPayloads' cont
  where
    cont :: forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.InnerJoin` E.SqlExpr (Entity WorkflowWorkflowPayload) -> E.SqlQuery a -> E.SqlQuery a
    cont (workflowWorkflowAction `E.InnerJoin` _) act = (*> act) $
      E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId

workflowStateCurrentPayloadsAuthorized' :: (forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.InnerJoin` E.SqlExpr (Entity WorkflowWorkflowPayload) -> E.SqlQuery a -> E.SqlQuery a)
                                        -> WorkflowWorkflowId
                                        -> DB DBWorkflowStatePayload
workflowStateCurrentPayloadsAuthorized' cont' wwId = maybeT (return mempty) $ do
  WorkflowWorkflow{..} <- MaybeT $ get wwId
  rScope <- toRouteWorkflowScope (_DBWorkflowScope # workflowWorkflowScope)
  cID <- encrypt wwId
  let route = _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
  authWhere <- lift $ workflowAuthWhere'' (Left workflowWorkflowGraph) (Set.singleton WorkflowGraphAuthViewWorkflowWorkflowPayload) route False
  lift $
    let cont :: forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.InnerJoin` E.SqlExpr (Entity WorkflowWorkflowPayload) -> E.SqlQuery a -> E.SqlQuery a
        cont x@(workflowWorkflowAction `E.InnerJoin` workflowWorkflowPayload) act = cont' x . (*> act) $
          E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
               E.&&. $$(E.annSqlExpr) (authWhere $ WorkflowAuthWherePayload workflowWorkflowAction workflowWorkflowPayload)
     in workflowStateCurrentPayloads' cont

workflowStateCurrentPayloadsAuthorized :: WorkflowWorkflowId -> DB DBWorkflowStatePayload
workflowStateCurrentPayloadsAuthorized = workflowStateCurrentPayloadsAuthorized' $ const id
