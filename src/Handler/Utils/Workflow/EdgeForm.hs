-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.EdgeForm
  ( workflowEdgeForm, WorkflowEdgeReferenceException(..)
  , WorkflowWorkflowEdgeFormAligned(unWorkflowWorkflowEdgeFormAligned), IdWorkflowWorkflowEdgeFormAligned
  , assertWorkflowWorkflowEdgeFormAligned, WorkflowWorkflowEdgeFormAlignmentViolation(..), WorkflowWorkflowEdgeFormPayloadAlignmentViolation(..)
  , acceptJsonWorkflowWorkflowEdgeForm
  , workflowEdgeFormToAction
  ) where

import Import hiding (StateT)

import Utils.Form
import Utils.Workflow
import Handler.Utils.Form
import Handler.Utils.Workflow.CanonicalRoute
import Handler.Utils.Widgets
import Handler.Utils.Workflow.Restriction
import Handler.Utils.Workflow.Payload
import Handler.Utils.Workflow.Types
import Handler.Utils.DateTime
import Handler.Utils.Files (sourceFileSize)
import Handler.Utils.Users

import qualified ListT

import Data.RFC5051 (compareUnicode)
import qualified Data.Text as Text
import Text.Unidecode (unidecode)

import qualified Data.Map as Map
import Data.Map ((!), (!?))
import qualified Data.Set as Set
import qualified Data.HashMap.Strict.InsOrd as InsOrdHashMap

import qualified Crypto.MAC.KMAC as Crypto
import qualified Crypto.Saltine.Class as Saltine
import qualified Data.Binary as Binary
import qualified Data.ByteArray as BA
import Crypto.Hash.Algorithms (SHAKE128, SHAKE256)
import Crypto.MAC.KMAC (kmacGetDigest)

import qualified Control.Monad.State.Class as State
import qualified Control.Monad.Trans.RWS.Lazy as Lazy (runRWST, mapRWST)
import Control.Monad.Trans.State.Strict (evalStateT, execStateT)
import Control.Monad.Trans.RWS.Strict (RWST(..), evalRWST, execRWST, mapRWST)
import Control.Monad.Error.Class (MonadError(..))
import Control.Monad.Trans.Except (withExceptT, catchE)

import Data.Bitraversable

import Data.List (findIndex)
import qualified Data.List as List (delete)

import qualified Data.Aeson as Aeson
import qualified Data.Scientific as Scientific

import Numeric.Lens (subtracting)

import qualified Data.Conduit.Combinators as C

import qualified Database.Esqueleto.Legacy as E
import qualified Database.Esqueleto.Utils as E

import qualified Topograph

import qualified Text.Blaze as Blaze
import qualified Text.Blaze.Renderer.Text as Blaze

import qualified Text.Email.Validate as Email

import qualified Data.Text.Encoding as Text
import qualified Data.Text.Encoding.Error as Text
import Data.Text.Lens (unpacked)

import qualified Data.CaseInsensitive as CI


{-# ANN module ("HLint: ignore Use newtype instead of data"::String) #-}


newtype WorkflowWorkflowEdgeFormAligned fileid userid = WorkflowWorkflowEdgeFormAligned
  { unWorkflowWorkflowEdgeFormAligned :: WorkflowWorkflowEdgeForm fileid userid
  } deriving stock (Show, Generic, Typeable)
    deriving newtype (Eq, Ord, ToJSON, FromJSON)
type IdWorkflowWorkflowEdgeFormAligned = WorkflowWorkflowEdgeFormAligned FileReference UserId

data WorkflowEdgeReferenceException
  = WorkflowEdgeReferenceAmbiguousCycle (Set WorkflowPayloadLabel)
  | WorkflowEdgeReferenceUnresolved WorkflowPayloadLabel
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)


workflowEdgeResolveReferences :: forall m x'' x''' x' x.
                                 ( Eq x'
                                 , MonadError WorkflowEdgeReferenceException m
                                 )
                              => Lens' x'' [Either WorkflowPayloadLabel x']
                              -> Lens x'' x''' [Either WorkflowPayloadLabel x'] [x']
                              -> Lens' x''' [x']
                              -> Prism' x' x
                              -> Map WorkflowPayloadLabel x
                              -> Map WorkflowPayloadLabel x''
                              -> m (Map WorkflowPayloadLabel x''')
workflowEdgeResolveReferences valReferences valReferences' valReferences'' valContent prevValues = go
  where
    go :: Map WorkflowPayloadLabel x'' -> m (Map WorkflowPayloadLabel x''')
    go edgeValues = case Topograph.runG referenceAdjacency go' of
        Left (Set.fromList -> cycle)
          -> let cycleValues :: [x']
                 cycleValues | null cycleEdgeValues = cyclePrevValues
                             | otherwise = cycleEdgeValues
                   where
                     cyclePrevValues = fold $ toListOf (re valContent) <$> Map.restrictKeys prevValues cycle
                     cycleEdgeValues = fold $ toListOf (valReferences . folded . _Right) <$> Map.restrictKeys edgeValues cycle
              in if | (v : vs) <- cycleValues, all (== v) vs
                      -> go . flip imap edgeValues $ \l v' -> if
                           | l `Set.notMember` cycle -> v'
                           | otherwise -> v' & valReferences %~ ((Right v :) . filter (fromMaybe True . previews _Left (`Set.notMember` cycle)))
                    | otherwise -> throwError $ WorkflowEdgeReferenceAmbiguousCycle cycle
        Right x -> x
      where
        go' :: forall i. Topograph.G WorkflowPayloadLabel i -> m (Map WorkflowPayloadLabel x''')
        go' Topograph.G{gVertices, gFromVertex} = flip execStateT initState . forM_ (map gFromVertex gVertices) $
                \l@((edgeValues Map.!) -> v) -> forMOf_ (valReferences . traverse . _Left) v $ \refL -> do
                  mRefVal <- State.gets $ \s -> view valReferences'' <$> Map.lookup refL s
                                            <|> pure . review valContent <$> Map.lookup refL prevValues
                  case mRefVal of
                    Nothing -> throwError $ WorkflowEdgeReferenceUnresolved refL
                    Just refVal -> State.modify' . flip Map.alter l $ Just . maybe (v & valReferences' .~ refVal) (valReferences'' <>~ refVal)
          where
            initState :: Map WorkflowPayloadLabel x'''
            initState = edgeValues
              & traverse . valReferences' %~ toListOf (folded . _Right)

        referenceAdjacency :: Map WorkflowPayloadLabel (Set WorkflowPayloadLabel)
        -- ^ Maps each field to all fields that refer to it
        -- Topologically sorted we expect every field to occurr before all fields that have references to it
        referenceAdjacency = flip Map.fromSet (Map.keysSet edgeValues) $ \v -> Map.keysSet (Map.filter (has $ valReferences . folded . _Left . only v) edgeValues)

workflowEdgeCaptureTime :: forall fileid userid m.
                           ( MonadIO m
                           , WorkflowFieldPayloads fileid fileid userid userid
                           )
                        => WorkflowPayloadTimeCapturePrecision
                        -> m (WorkflowFieldPayloadW fileid userid)
workflowEdgeCaptureTime prec = do
  t <- liftIO getCurrentTime
  return $ case prec of
    WFCaptureDate -> _WorkflowFieldPayloadW . _WorkflowFieldPayload # localDay (utcToLocalTime t)
    WFCaptureTime -> _WorkflowFieldPayloadW . _WorkflowFieldPayload # localTimeOfDay (utcToLocalTime t)
    WFCaptureDateTime -> _WorkflowFieldPayloadW . _WorkflowFieldPayload # t


workflowEdgeForm :: forall m m'.
                    ( MonadHandler m
                    , HandlerSite m ~ UniWorX
                    , MonadUnliftIO m
                    , MonadThrow m
                    , MonadHandler m'
                    , HandlerSite m' ~ UniWorX
                    , MonadUnliftIO m'
                    , MonadThrow m'
                    )
                 => Either WorkflowInstanceId WorkflowWorkflowId
                 -> Maybe IdWorkflowWorkflowEdgeForm
                 -> SqlPersistT m' (Maybe (AForm (SqlPersistT m) IdWorkflowWorkflowEdgeFormAligned))
workflowEdgeForm mwwId mPrev = runMaybeT $ do
  MsgRenderer mr <- getMsgRenderer

  ctx' <- bitraverse (MaybeT . getEntity) (MaybeT . getEntity) mwwId
  let (scope, sharedGraphId) = case ctx' of
        Left  (Entity _ WorkflowInstance{..}) -> ( _DBWorkflowScope # workflowInstanceScope
                                                 , workflowInstanceGraph
                                                 )
        Right (Entity _ WorkflowWorkflow{..}) -> ( _DBWorkflowScope # workflowWorkflowScope
                                                 , workflowWorkflowGraph
                                                 )
  WorkflowGraph{..} <- lift $ getSharedIdWorkflowGraph sharedGraphId
  let ctx = bimap entityVal entityVal ctx'
  wState <- lift . runMaybeT $ do
    wwId <- hoistMaybe $ ctx' ^? _Right . _entityKey
    fmap E.unValue . MaybeT . E.selectMaybe . E.from $ \workflowWorkflowAction -> do
      E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
      E.orderBy [E.desc $ workflowWorkflowAction E.^. WorkflowWorkflowActionIx]
      return $ workflowWorkflowAction E.^. WorkflowWorkflowActionTo
  wPayload' <- fmap (fmap $ review _DBWorkflowStatePayload) . lift . hoist liftHandler . traverse workflowStateCurrentPayloads $ ctx' ^? _Right . _entityKey

  rScope <- toRouteWorkflowScope scope

  wPayload <- do
    unfiltered <- fmap (view $ _Just . re _DBWorkflowStatePayload) . lift . hoist liftHandler . traverse workflowStateCurrentPayloadsAuthorized $ ctx' ^? _Right . _entityKey
    flip mapFilterWithKeyM unfiltered $ \payloadLbl _ -> lift . maybeT (return False) $ do
      wState' <- hoistMaybe wState
      wwId <- hoistMaybe $ mwwId ^? _Right
      cID <- lift $ encrypt wwId
      let canonRoute = _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
          hasWorkflowRole' role = $cachedHereBinary (rScope, wwId, role) $ is _Authorized <$> hasWorkflowRole (Just wwId) role canonRoute True
      WorkflowPayloadView{..} <- hoistMaybe . Map.lookup payloadLbl $ Map.findWithDefault Map.empty wState' (wgnPayloadView <$> wgNodes)
      guardM . anyM (otoList wpvViewers) $ lift . hasWorkflowRole'
      return True

  -- edges :: [((WorkflowGraphNodeLabel, WorkflowGraphEdgeLabel), (I18nText, Map WorkflowPayloadLabel (NonNull (Set (WorkflowPayloadSpec FileReference UserId)))))]
  edges <- ListT.toList $ do
    (nodeLabel, WGN{..}) <- ListT.fromFoldable $ Map.toList wgNodes
    (edgeLabel, edge) <- ListT.fromFoldable $ Map.toList wgnEdges
    ((nodeLabel, edgeLabel), ) <$> case edge of
      WorkflowGraphEdgeManual{..} -> do
        guard $ Just wgeSource == wState
        wwId <- hoistMaybe $ mwwId ^? _Right
        cID <- lift $ encrypt wwId
        guardM . anyM (Set.toList wgeActors) $ \role ->
          lift . lift $ is _Authorized <$> hasWorkflowRole (Just wwId) role (_WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)) True
        return (wgeDisplayLabel, (wgeForm, wgeMessages))
      WorkflowGraphEdgeInitial{..}  -> do
        guard $ is _Nothing wState
        win <- hoistMaybe $ ctx ^? _Left . _workflowInstanceName
        guardM . anyM (Set.toList wgeActors) $ \role ->
          lift . lift $ is _Authorized <$> hasWorkflowRole Nothing role (_WorkflowScopeRoute # (rScope, WorkflowInstanceR win WIInitiateR)) True
        return (wgeDisplayLabel, (wgeForm, wgeMessages))
      _other -> mzero

  guard . not $ null edges

  -- edgesOptList :: OptionList (WorkflowGraphNodeLabel, WorkflowGraphEdgeLabel)
  edgesOptList <- do
    sBoxKey <- secretBoxKey

    let olReadExternal ciphertext = do
          edgeIdent <- fromMaybeT . exceptTMaybe $ encodedSecretBoxOpen' sBoxKey ciphertext
          guard $ any (\(edgeIdent', _) -> edgeIdent == edgeIdent') edges
          return edgeIdent
    olOptions' <- ListT.toList $ do
      (edgeIdent, (edgeLabel, _)) <- ListT.fromFoldable edges
      optionDisplay <- lift $ selectLanguageI18n edgeLabel
      let optionInternalValue = edgeIdent
      optionExternalValue <- encodedSecretBox' sBoxKey SecretBoxShort edgeIdent
      return Option{..}
    let olOptions = concat $ do
          let optSort = (compareUnicode `on` (Text.toLower . optionDisplay))
                     <>  comparing (fallbackSortKey . optionInternalValue)
                where fallbackSortKey = toDigest . kmaclazy ("workflow-edge-sorting" :: ByteString) (Saltine.encode sBoxKey) . Binary.encode . (mwwId, )
                        where toDigest :: Crypto.KMAC (SHAKE256 256) -> ByteString
                              toDigest = BA.convert
          opts <- sortBy optSort olOptions'
            & foldr (\opt@Option{..} -> InsOrdHashMap.insertWith (<>) (Text.concatMap (pack . unidecode) optionDisplay) [opt]) InsOrdHashMap.empty
            & InsOrdHashMap.elems

          if
            | [_] <- opts
              -> return opts
            | otherwise -> do
                return $ zipWith (\Option{..} i -> Option{ optionDisplay = mr $ MsgWorkflowEdgeNumberedVariant optionDisplay i, ..}) opts [1..]
    return OptionList{..}

  let edges' = flip sortOn edges $ \(edgeIdent, _) -> flip findIndex (olOptions edgesOptList) $ (== edgeIdent) . optionInternalValue

  checkAlignment <- getsYesod $ views _appWorkflowEdgeFormCheckAlignment (bool (return . WorkflowWorkflowEdgeFormAligned) (assertWorkflowWorkflowEdgeFormAligned return return False mwwId) :: Bool -> IdWorkflowWorkflowEdgeForm -> SqlPersistT m IdWorkflowWorkflowEdgeFormAligned)
  let edgeForms :: Map (WorkflowGraphNodeLabel, WorkflowGraphEdgeLabel) (AForm (SqlPersistT m) IdWorkflowWorkflowEdgeFormAligned)
      edgeForms = Map.fromList . flip map edges' $ \(edgeIdent@(tState, edgeLbl), (_, (WorkflowGraphEdgeForm{..}, edgeMessages))) -> (edgeIdent, ) . bindAForm checkAlignment . fmap (WorkflowWorkflowEdgeForm tState edgeLbl) . wFormToAForm . fmap sequenceA $ do
        forM_ edgeMessages $ \WorkflowEdgeMessage{..} -> void . runMaybeT $ do
          let hasWorkflowRole' role = liftHandler . runDB $ case ctx' of
                Right (Entity wwId _) -> do
                  cID <- encrypt wwId
                  is _Authorized <$> hasWorkflowRole (Just wwId) role (_WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)) True
                Left (Entity _ WorkflowInstance{..})
                  -> is _Authorized <$> hasWorkflowRole Nothing role (_WorkflowScopeRoute # (rScope, WorkflowInstanceR workflowInstanceName WIInitiateR)) True
          guardM $ anyM (otoList wemViewers) hasWorkflowRole'
          whenIsJust wemRestriction $ \restr -> guard . flip checkWorkflowRestriction restr $ do
            wwrCurrentNode <- wState
            wwrCurrentPayload <- wPayload'
            return WorkflowRestrictionState{wwrNodeHistory=mempty,wwrEdgeHistory=mempty,..}
          let messageStatus = wemStatus
              messageIcon = Nothing
          messageContent <- selectLanguageI18n wemContent
          lift $ wformMessage Message{..}

        let fieldSort :: [(WorkflowPayloadLabel, [[(Either WorkflowGraphEdgeFormOrder ByteString, WorkflowPayloadSpec FileReference UserId)]])]
                      -> _
            fieldSort
              = sortOn ((,) <$> foldOf (_2 . folded . folded . _1 . _Left) <*> foldMapOf (_2 . folded . folded . _1 . _Right) (Just . Min))
              . over (traverse . _2) (sortOn $ (,) <$> foldOf (folded . _1 . _Left) <*> foldMapOf (folded . _1 . _Right) (Just . Min))
              . over (traverse . _2 . traverse) (sortOn $ (,) <$> preview (_1 . _Left) <*> preview (_1 . _Right))
        orderedFields <- lift . lift . fmap fieldSort . for (Map.toList wgefFields) $ \(payloadLabel, Set.toList . toNullable -> payloadSpecs) -> fmap (payloadLabel, ) . for payloadSpecs $ \(Map.toList . toNullable -> payloadSpecs') -> for payloadSpecs' $ \(payloadOrder, payloadSpec) -> if
          | payloadOrder /= mempty -> return (Left payloadOrder, payloadSpec)
          | otherwise -> do
              sBoxKey <- liftHandler secretBoxKey
              payloadSpec' <- traverseOf (typesCustom @WorkflowChildren @(WorkflowPayloadSpec FileReference UserId) @(WorkflowPayloadSpec FileReference CryptoUUIDUser) @UserId @CryptoUUIDUser) encrypt payloadSpec
              let toDigest :: Crypto.KMAC (SHAKE256 256) -> ByteString
                  toDigest = BA.convert
                  fallbackSortKey = toDigest . kmaclazy ("workflow-edge-form-payload-field-sorting" :: ByteString) (Saltine.encode sBoxKey) $ Aeson.encode (mwwId, payloadSpec')
              return (Right fallbackSortKey, payloadSpec)

        orderedFields' <- flip evalStateT 1 . for orderedFields $ \x@(payloadLabel, _) -> do
          let generateDisplayLabel = State.state $ \n -> (mr $ MsgWorkflowEdgeFormHiddenPayload n, succ n)
          (mayView, payloadDisplayLabel) <- maybeT ((False, ) <$> generateDisplayLabel) $
            let
              displayNameFromState s = do
                WorkflowPayloadView{..} <- hoistMaybe . Map.lookup payloadLabel $ Map.findWithDefault Map.empty s (wgnPayloadView <$> wgNodes)
                wRoute <- case ctx' of
                  Right (Entity wwId _) -> do
                    cID <- encrypt wwId
                    return $ _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
                  Left (Entity _ WorkflowInstance{..})
                    -> return $ _WorkflowScopeRoute # (rScope, WorkflowInstanceR workflowInstanceName WIInitiateR)
                guardM . anyM (Set.toList $ toNullable wpvViewers) $ \role ->
                  lift . lift . lift . lift $ is _Authorized <$> hasWorkflowRole (mwwId ^? _Right) role wRoute False
                (True, ) <$> selectLanguageI18n wpvDisplayLabel
             in displayNameFromState tState <|> maybe mzero displayNameFromState wState
          return ((mayView, payloadDisplayLabel), x)

        fields <- for orderedFields' $ \((mayView, payloadDisplayLabel), (payloadLabel, payloadSpecs)) -> (payloadLabel, ) <$> do
          let payloadSpecs' = payloadSpecs ^.. folded . folded . _2
              payloadFields = workflowEdgePayloadFields payloadSpecs' $ fmap otoList . Map.lookup payloadLabel =<< prevSrc
                where prevSrc = asum
                        [ wwefPayload <$> assertM ((== edgeIdent) . (wwefTo &&& wwefVia)) mPrev
                        , guardOn mayView wPayload
                        ]
          ((payloadRes, isOptional), payloadFieldViews) <- hoist (hoist liftHandler) $ wFormFields payloadFields
          return ((payloadDisplayLabel, getAll isOptional), (payloadRes, payloadFieldViews))

        fields' <-
          let resort = sortOn $ \(payloadLabel, _) -> findIndex (views _1 (== payloadLabel)) fields
           in fmap (resort . Map.toList) . throwExceptT $ workflowEdgeResolveReferences
                @_
                @((Text, Bool), ([Either WorkflowPayloadLabel (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))], [FieldView UniWorX]))
                @((Text, Bool), ([(Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))], [FieldView UniWorX]))
                @(Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
                @(Set (WorkflowFieldPayloadW FileReference UserId))
                (_2 . _1) (_2 . _1) (_2 . _1)
                (iso (view _2) (True ,) . _FormSuccess . maybePrism _NonEmpty . _NonNull . iso Set.fromList Set.toList)
                (fromMaybe Map.empty wPayload')
                (Map.fromList fields)

        -- fields' <-
        --   let payloadReferenceAdjacency = fieldsMap <&> setOf (_2 . _1 . folded . _Left)
        --       fieldsMap :: Map WorkflowPayloadLabel ((Text, Bool), ([Either WorkflowPayloadLabel (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))], [FieldView UniWorX]))
        --       fieldsMap = Map.fromList fields
        --       resolveReferences :: forall i. Topograph.G WorkflowPayloadLabel i -> [(WorkflowPayloadLabel, ((Text, Bool), ([(Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))], [FieldView UniWorX])))]
        --       resolveReferences Topograph.G{gVertices, gFromVertex} = resort . Map.toList . flip execState Map.empty . for topoOrder $ \payloadLabel -> whenIsJust (Map.lookup payloadLabel fieldsMap) $ \(payloadDisplay, (payloadRes, payloadFieldViews)) -> State.modify' $ \oldState ->
        --         let payloadRes' = flip concatMap payloadRes $ \case
        --               Right res -> pure res
        --               Left  ref -> Map.lookup ref oldState ^. _Just . _2 . _1
        --          in Map.insert payloadLabel (payloadDisplay, (payloadRes', payloadFieldViews)) oldState
        --         where
        --           topoOrder = map gFromVertex gVertices
        --           resort = sortOn $ \(payloadLabel, _) -> findIndex (views _1 (== payloadLabel)) fields
        --    in either (throwM . WorkflowWorkflowEdgeFormPayloadFieldReferenceCycle) return $ Topograph.runG payloadReferenceAdjacency resolveReferences

        fmap Map.fromList . for fields' $ \(payloadLabel, ((payloadDisplayLabel, isOptional), (payloadRes, payloadFieldViews))) -> (payloadLabel, ) <$> do
          $logWarnS "WorkflowEdgeForm" $ toPathPiece payloadLabel <> ": " <> tshow payloadRes
          let payloadRes' = let res = foldMap (views _2 . fmap $ maybe Set.empty (Set.fromList . otoList)) payloadRes
                             in if | doErrMsg -> FormFailure $ view _FormFailure res ++ pure (mr $ MsgWorkflowEdgeFormPayloadOneFieldRequiredFor payloadDisplayLabel)
                                   | otherwise -> res
              doErrMsg = flip none payloadRes $ \res -> view _1 res || hasn't (_2 . _FormSuccess) res
              addErrMsg pErrs = Just
                [shamlet|
                  $newline never
                  $maybe errs <- pErrs
                    #{errs}
                    <br />
                  #{mr MsgWorkflowEdgeFormPayloadOneFieldRequired}
                |]
          case payloadFieldViews of
            [] -> return ()
            [fv] -> lift . tell . pure $ fv
                      & _fvRequired .~ not isOptional
                      & _fvErrors   %~ bool id addErrMsg doErrMsg
            _other -> do
              fvId <- newIdent
              let fvLabel = toHtml payloadDisplayLabel
                  fvTooltip = Nothing
                  fvInput = renderFieldViews FormWorkflowDataset $ payloadFieldViews
                              & traverse . _fvRequired .~ not isOptional
                  fvErrors = bool id addErrMsg doErrMsg Nothing
                  fvRequired = not isOptional
               in lift . tell $ pure FieldView{..}
          return payloadRes'

  return . multiActionAOpts edgeForms (return edgesOptList) actFS $ (wwefTo &&& wwefVia) <$> mPrev
  where
    actFS = fslI MsgWorkflowEdgeFormEdge

workflowEdgePayloadFields :: [WorkflowPayloadSpec FileReference UserId]
                          -> Maybe [WorkflowFieldPayloadW FileReference UserId]
                          -> WForm Handler ([Either WorkflowPayloadLabel (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))], All) -- ^ @isFilled@, @foldMap ala All . map isOptional@
workflowEdgePayloadFields specs = evalRWST (forM specs $ runExceptT . renderSpecField) Nothing . fromMaybe []
  where
    renderSpecField :: WorkflowPayloadSpec FileReference UserId
                    -> ExceptT WorkflowPayloadLabel (RWST (Maybe (Text -> Text)) All [WorkflowFieldPayloadW FileReference UserId] (MForm (WriterT [FieldView UniWorX] Handler))) (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
    renderSpecField (WorkflowPayloadSpec (specField :: WorkflowPayloadField FileReference UserId payload)) = do
      let f' :: forall payload' payload''.
               _
             => (payload' -> Maybe (NonEmpty payload''))
             -> Bool -- ^ @isOpt@
             -> Field Handler payload'
             -> FieldSettings UniWorX
             -> Maybe payload'
             -> _ (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
          f' toNonEmpty' isOpt fld fs mx = lift . (<* tell (All isOpt)) . lift $ over (_2 . mapped) (fmap (fmap . review $ _WorkflowFieldPayloadW . _WorkflowFieldPayload) . toNonEmpty' =<<) . bool (is (_FormSuccess . _Just) &&& id) (True, ) isOpt <$> wopt fld fs (Just <$> mx)
          f :: forall payload'.
               _
            => Bool -- ^ @isOpt@
            -> Field Handler payload'
            -> FieldSettings UniWorX
            -> Maybe payload'
            -> _ (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
          f = f' (nonEmpty . pure)
          extractPrevs :: forall payload' m xs.
                          ( IsWorkflowFieldPayload' FileReference UserId payload'
                          , State.MonadState [WorkflowFieldPayloadW FileReference UserId] m
                          )
                       => (payload' -> Maybe xs -> Maybe xs)
                       -> m (Maybe xs)
          extractPrevs accum = State.state $ foldl' go (Nothing, []) . map (matching $ _WorkflowFieldPayloadW @payload' @FileReference @UserId . _WorkflowFieldPayload)
            where go (mPrev', xs) (Left  x) = (mPrev', xs ++ [x])
                  go (acc,    xs) (Right p) = case accum p acc of
                    acc'@(Just _) -> (acc', xs)
                    Nothing -> (acc, xs ++ [_WorkflowFieldPayloadW @payload' @FileReference @UserId . _WorkflowFieldPayload # p])
          extractPrev :: forall payload' m.
                         ( IsWorkflowFieldPayload' FileReference UserId payload'
                         , State.MonadState [WorkflowFieldPayloadW FileReference UserId] m
                         )
                      => m (Maybe payload')
          extractPrev = extractPrevs $ \p -> \case
            Nothing -> Just p
            Just _  -> Nothing

          delTyp :: forall payload' m.
                    ( State.MonadState [WorkflowFieldPayloadW FileReference UserId] m
                    , IsWorkflowFieldPayload' FileReference UserId payload'
                    ) => Proxy payload' -> m ()
          delTyp _ = State.modify $ \xs' ->
            let go [] = []
                go (x:xs) | is (_WorkflowFieldPayloadW @payload') x = xs
                          | otherwise = x : go xs
             in go xs'

          wSetTooltip' :: _ => Maybe Html -> t' (t (MForm (WriterT [FieldView UniWorX] Handler))) a -> t' (t (MForm (WriterT [FieldView UniWorX] Handler))) a
          wSetTooltip' tip = hoist (hoist (wSetTooltip tip))


      MsgRenderer mr <- getMsgRenderer
      LanguageSelectI18n{..} <- getLanguageSelectI18n
      mNudge <- ask

      case specField of
        -- Ensure this stays in sync with @assertWorkflowEdgePayloadFieldAligned@
        WorkflowPayloadFieldText{..} | Nothing <- wpftPresets -> do
          prev <- extractPrev @Text
          let field = case wpftType of
                WorkflowPayloadTextTypeText -> textField & cfStrip
                WorkflowPayloadTextTypeLarge -> textareaField & isoField _Wrapped & cfStrip
                WorkflowPayloadTextTypeEmail -> emailField
                WorkflowPayloadTextTypeUrl -> urlFieldText
                WorkflowPayloadTextTypePassword -> passwordField
              addFieldSettings = case wpftType of
                WorkflowPayloadTextTypePassword -> addAttr "autofill" "new-password"
                _other -> id
          wSetTooltip' (fmap slI18n wpftTooltip) $
            f wpftOptional
              field
              ( fsl (slI18n wpftLabel)
                  & maybe id (addPlaceholder . slI18n) wpftPlaceholder
                  & maybe id (addName . ($ "text")) mNudge
                  & addFieldSettings
              )
              (prev <|> wpftDefault)
        WorkflowPayloadFieldText{..} | Just (otoList -> opts) <- wpftPresets -> do
          prev <- extractPrev @Text
          sBoxKey <- secretBoxKey
          let offerNothing = wpftOptional || minLength 2 specs
              optList = do
                WorkflowPayloadTextPreset{..} <- opts
                let optionExternalValue = toPathPiece @(Digest (SHAKE128 128)) . kmacGetDigest . kmaclazy ("payload-field-text-enum" :: ByteString) (Saltine.encode sBoxKey) $ Binary.encode optionInternalValue
                    optionInternalValue = wptpText
                return ( Option
                           { optionDisplay = slI18n wptpLabel
                           , ..
                           }
                       , toWidget . slI18n <$> wptpTooltip
                       )
              readExternal = flip Map.lookup . Map.fromList $ map (views _1 (optionExternalValue &&& optionInternalValue)) optList
              doExplainedSelectionField = has (folded . _wptpTooltip . _Just) opts
          wSetTooltip' (fmap slI18n wpftTooltip) $
            f wpftOptional
              (bool (selectField' (guardOn offerNothing $ SomeMessage MsgWorkflowEdgeFormEnumFieldNothing) . return $ OptionList (optList ^.. folded . _1) readExternal)
                    (explainedSelectionField (guardOn offerNothing (SomeMessage MsgWorkflowEdgeFormEnumFieldNothing, Nothing)) $ return (optList, readExternal))
                    doExplainedSelectionField
              )
              ( fsl (slI18n wpftLabel)
                  & maybe id (addName . ($ "text")) mNudge
              )
              (prev <|> wpftDefault <|> preview (_head . _1 . to optionInternalValue) optList)
        WorkflowPayloadFieldNumber{..} -> do
          prev <- extractPrev @Scientific
          wSetTooltip' (fmap slI18n wpfnTooltip) $
            f wpfnOptional
              ( fractionalField
                  & maybe id (\wpfnMin' -> checkBool (>= wpfnMin') $ MsgWorkflowEdgeFormFieldNumberTooSmall wpfnMin') wpfnMin
                  & maybe id (\wpfnMax' -> checkBool (>= wpfnMax') $ MsgWorkflowEdgeFormFieldNumberTooSmall wpfnMax') wpfnMax
                  & maybe id (\wpfnStep' -> flip convertField id . over (maybe id subtracting wpfnMin) $ \n -> fromInteger (round $ n / wpfnStep') * wpfnStep') wpfnStep
              )
              ( fsl (slI18n wpfnLabel)
                  & maybe id (addPlaceholder . slI18n) wpfnPlaceholder
                  & maybe id (addAttr "min" . tshow . formatScientific Scientific.Fixed Nothing) wpfnMin
                  & maybe id (addAttr "max" . tshow . formatScientific Scientific.Fixed Nothing) wpfnMax
                  & maybe (addAttr "step" "any") (addAttr "step" . tshow . formatScientific Scientific.Fixed Nothing) wpfnStep
                  & maybe id (addName . ($ "number")) mNudge
              )
              (prev <|> wpfnDefault)
        WorkflowPayloadFieldBool{..} -> do
          prev <- extractPrev @Bool
          wSetTooltip' (fmap slI18n wpfbTooltip) $
            f (is _Just wpfbOptional)
              (maybe checkBoxField (boolField . Just . SomeMessage . slI18n) wpfbOptional)
              ( fsl (slI18n wpfbLabel)
                  & maybe id (addName . ($ "bool")) mNudge
              )
              (prev <|> wpfbDefault)
        WorkflowPayloadFieldDay{..} -> do
          dayValidationEnabled <- getVolatileClusterSetting clusterVolatileWorkflowEdgeFormDayValidationEnabled
          cDay <- localDay . utcToLocalTime <$> liftIO getCurrentTime
          let checkPast, checkFuture :: (MonadHandler m, HandlerSite m ~ UniWorX) => Field m Day -> Field m Day
              checkPast | Just offset <- wpfdMaxPast
                        , dayValidationEnabled
                        = checkBool ((<= offset) . (cDay `diffDays`)) $ MsgWorkflowEdgeFormFieldDayTooFarPast offset
                        | otherwise = id
              checkFuture | Just offset <- wpfdMaxFuture
                          , dayValidationEnabled
                          = checkBool ((<= offset) . (`diffDays` cDay)) $ MsgWorkflowEdgeFormFieldDayTooFarFuture offset
                          | otherwise = id
          prev <- extractPrev @Day
          wSetTooltip' (fmap slI18n wpfdTooltip) $
            f wpfdOptional
              ( dayField & checkPast & checkFuture )
              ( fsl (slI18n wpfdLabel)
                  & maybe id (addName . ($ "day")) mNudge
              )
              (prev <|> wpfdDefault)
        WorkflowPayloadFieldTime{..} -> do
          prev <- extractPrev @TimeOfDay
          wSetTooltip' (fmap slI18n wpfttTooltip) $
            f wpfttOptional
              timeFieldTypeTime
              ( fsl (slI18n wpfttLabel)
                  & maybe id (addName . ($ "timeofday")) mNudge
              )
              (prev <|> wpfttDefault)
        WorkflowPayloadFieldDateTime{..} -> do
          datetimeValidationEnabled <- getVolatileClusterSetting clusterVolatileWorkflowEdgeFormDayValidationEnabled
          now <- liftIO getCurrentTime
          let checkPast, checkFuture :: (MonadHandler m, HandlerSite m ~ UniWorX) => Field m UTCTime -> Field m UTCTime
              checkPast | Just offset <- wpfdtMaxPast
                        , datetimeValidationEnabled
                        = checkBool ((<= offset) . (now `diffUTCTime`)) . MsgWorkflowEdgeFormFieldDateTimeTooFarPast $ formatDiffDays offset
                        | otherwise = id
              checkFuture | Just offset <- wpfdtMaxFuture
                          , datetimeValidationEnabled
                          = checkBool ((<= offset) . (`diffUTCTime` now)) . MsgWorkflowEdgeFormFieldDateTimeTooFarFuture $ formatDiffDays offset
                          | otherwise = id
          prev <- extractPrev @UTCTime
          wSetTooltip' (fmap slI18n wpfdtTooltip) $
            f wpfdtOptional
              ( utcTimeField & checkPast & checkFuture )
              ( fsl (slI18n wpfdtLabel)
                  & maybe id (addName . ($ "datetime")) mNudge
              )
              (prev <|> wpfdtDefault)
        WorkflowPayloadFieldFile{..} -> do
          fRefs <- extractPrevs @FileReference $ \p -> if
            | fieldMultiple wpffConfig -> Just . maybe (Set.singleton p) (Set.insert p)
            | otherwise -> \case
                Nothing -> Just $ Set.singleton p
                Just _  -> Nothing
          let wpffConfig' = wpffConfig & _fieldAdditionalFiles %~ (fRefs' <>)
                where fRefs' = review _FileReferenceFileReferenceTitleMap . Map.fromList $ do
                        FileReference{..} <- Set.toList =<< hoistMaybe fRefs
                        return (fileReferenceTitle, (fileReferenceContent, fileReferenceModified, FileFieldUserOption False True))
          wSetTooltip' (fmap slI18n wpffTooltip) $
            f' (nonEmpty . Set.toList) wpffOptional
              (convertFieldM (\p -> runConduit $ transPipe liftHandler p .| C.foldMap Set.singleton) yieldMany . genericFileField $ return wpffConfig')
              ( fsl (slI18n wpffLabel)
                  & maybe id (addName . ($ "file")) mNudge
              )
               fRefs
        WorkflowPayloadFieldUser{..} -> do
          fRefs <- extractPrev @UserId
          let suggestions uid = E.from $ \user -> do
                E.where_ $ user E.^. UserId E.==. E.val uid
                return user
          wSetTooltip' (fmap slI18n wpfuTooltip) $
            f wpfuOptional
              (checkMap (first $ const MsgWorkflowEdgeFormFieldUserNotFound) Right . userField False $ suggestions <$> fRefs)
              ( fslI (slI18n wpfuLabel)
                  & maybe id (addName . ($ "user")) mNudge
              )
              (fRefs <|> wpfuDefault)
        WorkflowPayloadFieldCaptureUser -> do
          mAuthId <- liftHandler maybeAuth
          case mAuthId of
            Just (Entity uid User{userDisplayName, userSurname}) -> do
              fvId <- newIdent
              State.modify . List.delete $ _WorkflowFieldPayloadW . _WorkflowFieldPayload # uid
              lift . lift . lift . tell $ pure FieldView
                { fvLabel = [shamlet|#{mr MsgWorkflowEdgeFormFieldCaptureUserLabel}|]
                , fvTooltip = Nothing
                , fvId
                , fvInput = [whamlet|
                              $newline never
                              <span ##{fvId}>
                                ^{nameWidget userDisplayName userSurname}
                            |]
                , fvErrors = Nothing
                , fvRequired = False
                }
              (True,  FormSuccess . Just . (:| []) $ _WorkflowFieldPayloadW . _WorkflowFieldPayload # uid) <$ tell (All True)
            Nothing  -> (False, FormMissing) <$ tell (All False)
        WorkflowPayloadFieldCaptureDateTime{..} -> do
          let
            cLabel = case wpfcdtPrecision of
              WFCaptureDate -> MsgWorkflowEdgeFormFieldCaptureDateLabel
              WFCaptureTime -> MsgWorkflowEdgeFormFieldCaptureTimeLabel
              WFCaptureDateTime -> MsgWorkflowEdgeFormFieldCaptureDateTimeLabel

          fvId <- newIdent
          lift . lift . lift . tell $ pure FieldView
            { fvLabel = Blaze.toMarkup $ slI18n wpfcdtLabel
            , fvTooltip = slI18n <$> wpfcdtTooltip
            , fvId
            , fvInput = [whamlet|
                          $newline never
                          <span ##{fvId} .explanation>
                            _{cLabel}
                        |]
            , fvErrors = Nothing
            , fvRequired = False
            }

          case wpfcdtPrecision of
            WFCaptureDate -> do
              delTyp $ Proxy @Day
            WFCaptureTime -> do
              delTyp $ Proxy @TimeOfDay
            WFCaptureDateTime -> do
              delTyp $ Proxy @UTCTime
          tell $ All True
          (True, ) . FormSuccess . Just . (:| []) <$> workflowEdgeCaptureTime wpfcdtPrecision
        WorkflowPayloadFieldReference{..} -> throwE wpfrTarget
        WorkflowPayloadFieldMultiple{..} -> do
          fRefs <- nonEmpty <$> State.state (maybe (, []) (splitAt . fromIntegral) $ (+ wpfmMin) <$> wpfmRange)
          miIdent <- newIdent
          wSetTooltip' (fmap slI18n wpfmTooltip) $
            let mPrev' :: Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))
                mPrev' = fRefs <|> wpfmDefault
                mPrev :: Maybe (Map ListPosition (Maybe (WorkflowFieldPayloadW FileReference UserId), Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
                mPrev = Just . Map.fromList . zip [0..] . ensureLength . map (\x -> (Just x, Just $ x :| [])) $ mPrev' ^.. _Just . folded
                  where
                    ensureLength :: forall a b. [(Maybe a, Maybe b)] -> [(Maybe a, Maybe b)]
                    ensureLength = (\l -> (l ++) $ replicate (fromIntegral wpfmMin - length l) (Nothing, Nothing)) . maybe id (take . fromIntegral) ((+ wpfmMin) <$> wpfmRange)
                mangleResult :: forall a.
                                FormResult (Map ListPosition (a, Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
                             -> (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))))
                -- FieldMultiple are always filled since `massInput` ensures cardinality constraints (iff @mPrev'@ correctly initializes `massInput` with a list of fields of the appropriate length)
                mangleResult res = case matching _FormSuccess res of
                  Right ress
                    -> (True, FormSuccess . nonEmpty $ ress ^.. folded . _2 . _Just . folded)
                  Left res'
                    -> (False, res')
                runMI :: forall a.
                         WForm (ExceptT WorkflowPayloadLabel Handler) a
                      -> ExceptT WorkflowPayloadLabel (RWST (Maybe (Text -> Text)) All [WorkflowFieldPayloadW FileReference UserId] (MForm (WriterT [FieldView UniWorX] Handler))) a
                runMI mx = do
                  r <- lift $ lift ask
                  s <- lift $ lift State.get
                  ((a, s', w), w') <- ExceptT . lift . lift . lift . runExceptT . runWriterT $ Lazy.runRWST mx r s
                  lift . lift $ do
                    State.put s'
                    tell w
                    lift $ tell w'
                  lift . tell . All $ wpfmMin <= 0
                  return a

                miAdd :: ListPosition
                      -> Natural
                      -> ListLength
                      -> (Text -> Text)
                      -> FieldView UniWorX
                      -> Maybe (Html -> MForm (ExceptT WorkflowPayloadLabel Handler) (FormResult (Map ListPosition (Maybe (WorkflowFieldPayloadW FileReference UserId)) -> FormResult (Map ListPosition (Maybe (WorkflowFieldPayloadW FileReference UserId)))), Widget))
                miAdd pos dim liveliness nudge submitView = guardOn (miAllowAdd pos dim liveliness) $ over (mapped . _1 . _FormSuccess) tweakRes . miForm nudge (Left submitView)
                  where tweakRes :: Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))
                                 -> Map ListPosition (Maybe (WorkflowFieldPayloadW FileReference UserId))
                                 -> FormResult (Map ListPosition (Maybe (WorkflowFieldPayloadW FileReference UserId)))
                        tweakRes newDat prevData = pure . Map.fromList . zip [startKey..] . map Just $ newDat ^.. _Just . folded
                          where startKey = maybe 0 succ $ fst <$> Map.lookupMax prevData

                miCell :: ListPosition
                       -> Maybe (WorkflowFieldPayloadW FileReference UserId)
                       -> Maybe (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId)))
                       -> (Text -> Text)
                       -> (Html -> MForm (ExceptT WorkflowPayloadLabel Handler) (FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))), Widget))
                miCell _pos dat mPrev'' nudge = miForm nudge . Right $ fromMaybe (fmap (:| []) dat) mPrev''

                miForm :: (Text -> Text)
                       -> Either (FieldView UniWorX) (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId)))
                       -> (Html -> MForm (ExceptT WorkflowPayloadLabel Handler) (FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId))), Widget))
                miForm nudge mode csrf = do
                  let runSpecRender :: WriterT [FieldView UniWorX] Handler (Either WorkflowPayloadLabel (Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId)))), Ints, Enctype)
                                    -> ExceptT WorkflowPayloadLabel Handler (((Bool, FormResult (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId)))), [FieldView UniWorX]), Ints, Enctype)
                      runSpecRender mSR = do
                        ((eRes, s, w), fvs) <- lift $ runWriterT mSR
                        ExceptT . return $ (, s, w) . (, fvs) <$> eRes
                  ((fFilled, fmRes), fvs') <- Lazy.mapRWST runSpecRender . fmap (view _1) $ evalRWST (runExceptT $ renderSpecField wpfmSub) (Just $ fromMaybe id mNudge . nudge) (mode ^.. _Right . _Just . folded)

                  let fFilled' = fFilled || isn't _FormSuccess fmRes
                      fmRes' | not fFilled' = FormFailure . pure . maybe (mr MsgValueRequired) (mr . valueRequired) $ fvs ^? _head . to fvLabel'
                             | otherwise    = fmRes
                      fvLabel' = toStrict . Blaze.renderMarkup . Blaze.contents . fvLabel -- Dirty, but probably good enough; if not: `censor` writer with actual `Text` in `renderSpecField` and discard that information in `workflowEdgePayloadFields`
                      fvs | not fFilled' = fvs' <&> \fv -> fv { fvErrors = Just
                                                                  [shamlet|
                                                                    $newline never
                                                                    $maybe errs <- fvErrors fv
                                                                      #{errs}
                                                                      <br />
                                                                    #{mr (valueRequired (fvLabel' fv))}
                                                                  |]
                                                               }
                          | otherwise = fvs'
                      valueRequired :: forall msg. _ => msg -> ValueRequired UniWorX
                      valueRequired = ValueRequired

                  return ( fmRes'
                         , case mode of
                             Left  btn -> $(widgetFile "widgets/massinput/workflow-payload-field-multiple/add")
                             Right _   -> $(widgetFile "widgets/massinput/workflow-payload-field-multiple/cell")
                         )

                miDelete :: forall m.
                            Monad m
                         => Map ListPosition (Maybe (WorkflowFieldPayloadW FileReference UserId))
                         -> ListPosition
                         -> MaybeT m (Map ListPosition ListPosition)
                miDelete dat pos = do
                  ListLength l <- hoistMaybe . preview liveCoords $ Map.keysSet dat
                  guard $ l > wpfmMin
                  miDeleteList dat pos

                miAllowAdd :: ListPosition
                           -> Natural
                           -> ListLength
                           -> Bool
                miAllowAdd _ _ (ListLength l) = maybe True (l <) $ (+ wpfmMin) <$> wpfmRange

                miAddEmpty :: ListPosition
                           -> Natural
                           -> ListLength
                           -> Set ListPosition
                miAddEmpty _ _ _ = Set.empty

                miButtonAction :: forall p.
                                  p -> Maybe (SomeRoute UniWorX)
                miButtonAction _ = Nothing

                miLayout :: MassInputLayout ListLength (Maybe (WorkflowFieldPayloadW FileReference UserId)) (Maybe (NonEmpty (WorkflowFieldPayloadW FileReference UserId)))
                miLayout lLength _ cellWdgts delButtons addWdgts = $(widgetFile "widgets/massinput/workflow-payload-field-multiple/layout")
             in runMI . fmap mangleResult $ massInputW MassInput{..} (fslI $ slI18n wpfmLabel) False mPrev


data WorkflowWorkflowEdgeFormAlignmentViolation
  = WWEFAInvalidContext
  | WWEFAUnknownTo WorkflowGraphNodeLabel
  | WWEFAUnknownVia WorkflowGraphEdgeLabel
  | WWEFAUnauthorizedActor
  | WWEFAInvalidVia WorkflowGraphEdgeLabel
  | WWEFAInvalidInput WorkflowPayloadLabel WorkflowWorkflowEdgeFormPayloadAlignmentViolation
  | WWEFAExtraneousInput (Set WorkflowPayloadLabel)
  | WWEFAEdgeReferenceException WorkflowEdgeReferenceException
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

data WorkflowWorkflowEdgeFormPayloadAlignmentViolation
  = WWEFPAMissing
  | WWEFPAEmpty
  | WWEFPAInvalidEmail Text | WWEFPAInvalidUrl Text
  | WWEFPAInvalidTextPreset Text
  | WWEFPANumberBelowMin Scientific | WWEFPANumberAboveMax Scientific
  | WWEFPADayTooFarPast Day | WWEFPADayTooFarFuture Day
  | WWEFPADateTimeTooFarPast UTCTime | WWEFPADateTimeTooFarFuture UTCTime
  | WWEFPAInvalidFileExtension FilePath | WWEFPAFileTooLarge FilePath | WWEFPACumulativeSizeExceeded | WWEFPANonEmptyFileRequired
  | WWEFPAMultipleCardinalityViolation
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

assertWorkflowWorkflowEdgeFormAligned :: forall fileid userid m.
                                         ( MonadHandler m
                                         , HandlerSite m ~ UniWorX
                                         , MonadUnliftIO m
                                         , MonadThrow m
                                         , WorkflowFieldPayloads fileid fileid userid userid
                                         , HasFileReference fileid -- We expect `sourceFile` to work on the contained references
                                         , HasTypesCustom WorkflowChildren (WorkflowStatePayload FileReference UserId) (WorkflowStatePayload fileid UserId) FileReference fileid
                                         , HasTypesCustom WorkflowChildren (WorkflowStatePayload fileid UserId) (WorkflowStatePayload fileid userid) UserId userid
                                         , HasTypesCustom WorkflowChildren (WorkflowGraphEdgeForm FileReference UserId) (WorkflowGraphEdgeForm fileid UserId) FileReference fileid
                                         , HasTypesCustom WorkflowChildren (WorkflowGraphEdgeForm fileid UserId) (WorkflowGraphEdgeForm fileid userid) UserId userid
                                         , FileReferenceTitleMapConvertible (FileFieldUserOption Bool) fileid fileid, Ord (FileReferenceTitleMap fileid (FileFieldUserOption Bool))
                                         , Ord userid, Ord fileid
                                         , Show userid, Show fileid
                                         )
                                      => (FileReference -> SqlPersistT m fileid) -> (UserId -> SqlPersistT m userid)
                                      -> Bool -- ^ @doCapture@
                                      -> Either WorkflowInstanceId WorkflowWorkflowId
                                      -> WorkflowWorkflowEdgeForm fileid userid
                                      -> SqlPersistT m (WorkflowWorkflowEdgeFormAligned fileid userid)
assertWorkflowWorkflowEdgeFormAligned projFileReference projUserId doCapture mwwId WorkflowWorkflowEdgeForm{..} = throwExceptT $ do
  let projWPayload = traverseOf (typesCustom @WorkflowChildren @(WorkflowStatePayload fileid UserId) @(WorkflowStatePayload fileid userid) @UserId @userid) projUserId
                 <=< traverseOf (typesCustom @WorkflowChildren @(WorkflowStatePayload FileReference UserId) @(WorkflowStatePayload fileid UserId) @FileReference @fileid) projFileReference
      projForm = traverseOf (typesCustom @WorkflowChildren @(WorkflowGraphEdgeForm fileid UserId) @(WorkflowGraphEdgeForm fileid userid) @UserId @userid) projUserId
             <=< traverseOf (typesCustom @WorkflowChildren @(WorkflowGraphEdgeForm FileReference UserId) @(WorkflowGraphEdgeForm fileid UserId) @FileReference @fileid) projFileReference

  $logDebugS "assertWorkflowWorkflowEdgeFormAligned" $ tshow (wwefVia, wwefTo)

  ctx' <- bitraverse (maybeMExceptT' WWEFAInvalidContext . getEntity) (maybeMExceptT' WWEFAInvalidContext . getEntity) mwwId
  let (scope, sharedGraphId) = case ctx' of
        Left  (Entity _ WorkflowInstance{..}) -> ( _DBWorkflowScope # workflowInstanceScope
                                                 , workflowInstanceGraph
                                                 )
        Right (Entity _ WorkflowWorkflow{..}) -> ( _DBWorkflowScope # workflowWorkflowScope
                                                 , workflowWorkflowGraph
                                                 )
  WorkflowGraph{..} <- lift $ getSharedIdWorkflowGraph sharedGraphId
  let ctx = bimap entityVal entityVal ctx'
  wState <- lift . runMaybeT $ do
    wwId <- hoistMaybe $ ctx' ^? _Right . _entityKey
    fmap E.unValue . MaybeT . E.selectMaybe . E.from $ \workflowWorkflowAction -> do
      E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionWorkflow E.==. E.val wwId
      E.orderBy [E.desc $ workflowWorkflowAction E.^. WorkflowWorkflowActionIx]
      return $ workflowWorkflowAction E.^. WorkflowWorkflowActionTo
  wPayload' <- lift . traverse (projWPayload . review _DBWorkflowStatePayload <=< hoist liftHandler . workflowStateCurrentPayloads) $ ctx' ^? _Right . _entityKey

  rScope <- maybeTExceptT WWEFAInvalidContext $ toRouteWorkflowScope scope

  WGN{..} <- maybeExceptT' (WWEFAUnknownTo wwefTo) $ Map.lookup wwefTo wgNodes
  edge <- maybeExceptT' (WWEFAUnknownVia wwefVia) $ Map.lookup wwefVia wgnEdges
  WorkflowGraphEdgeForm{..} <- lift . projForm =<< case edge of
    WorkflowGraphEdgeManual{..} -> do
      flip guardExceptT WWEFAInvalidContext $ Just wgeSource == wState
      wwId <- maybeExceptT' WWEFAInvalidContext $ mwwId ^? _Right
      cID <- lift $ encrypt wwId
      guardExceptTM WWEFAUnauthorizedActor . anyM (Set.toList wgeActors) $ \role ->
        is _Authorized <$> hasWorkflowRole (Just wwId) role (_WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)) True
      return wgeForm
    WorkflowGraphEdgeInitial{..}  -> do
      flip guardExceptT WWEFAInvalidContext $ is _Nothing wState
      win <- maybeExceptT' WWEFAInvalidContext $ ctx ^? _Left . _workflowInstanceName
      guardExceptTM WWEFAUnauthorizedActor . anyM (Set.toList wgeActors) $ \role ->
        is _Authorized <$> hasWorkflowRole Nothing role (_WorkflowScopeRoute # (rScope, WorkflowInstanceR win WIInitiateR)) True
      return wgeForm
    _other -> throwE $ WWEFAInvalidVia wwefVia

  let
    runRWSExceptT :: forall r w s m' e a.
                     ( Monoid w, Monad m' )
                  => RWST r w s (ExceptT e m') a
                  -> RWST r w s m' (Either e a)
    runRWSExceptT (RWST act) = RWST $ \r s -> do
      res <- runExceptT $ act r s
      case res of
        Left err -> return (Left err, s, mempty)
        Right (res', s', w) -> return (Right res', s', w)
    assertWorkflowEdgePayloadFieldsAligned :: RWST
                                                ()
                                                (MergeMap WorkflowPayloadLabel (Set (Either WorkflowPayloadLabel (WorkflowFieldPayloadW fileid userid))))
                                                (Map WorkflowPayloadLabel (Set (WorkflowFieldPayloadW fileid userid)))
                                                (ExceptT WorkflowWorkflowEdgeFormAlignmentViolation (SqlPersistT m))
                                                ()
    assertWorkflowEdgePayloadFieldsAligned = iforM_ wgefFields $ \payloadLabel (toListOf $ re _nullable . folded . re _nullable . folded -> payloadSpecs) -> RWST $ \() s -> do
      let
        mergeResults ress
          | Just res <- ress ^? folded . _Right = return res
          | Just err <- ress ^? folded . _Left  = lift . throwE $ WWEFAInvalidInput payloadLabel err
          | otherwise = return ()
      ((), s', w) <- runRWST (mergeResults =<< mapM (hoist lift . runRWSExceptT . assertWorkflowEdgePayloadFieldAligned projUserId doCapture) payloadSpecs) () (Map.findWithDefault Set.empty payloadLabel s)
      return ( ()
             , Map.alter (const $ assertM' (not . Set.null) s') payloadLabel s
             , MergeMap $ Map.singleton payloadLabel w
             )

  (inputRemainder, unMergeMap -> protoPayload) <- execRWST assertWorkflowEdgePayloadFieldsAligned () wwefPayload

  $logDebugS "assertWorkflowWorkflowEdgeFormAligned" $ tshow wwefPayload
  $logDebugS "assertWorkflowWorkflowEdgeFormAligned" $ tshow inputRemainder
  guardExceptT (Map.null inputRemainder) . WWEFAExtraneousInput $ Map.keysSet inputRemainder

  wwefPayload' <- withExceptT WWEFAEdgeReferenceException $ workflowEdgeResolveReferences
    @_
    @(Set (Either WorkflowPayloadLabel (WorkflowFieldPayloadW fileid userid)))
    @(Set (WorkflowFieldPayloadW fileid userid))
    @(Set (WorkflowFieldPayloadW fileid userid))
    @(Set (WorkflowFieldPayloadW fileid userid))
    (iso (map (fmap Set.singleton) . Set.toList) (Set.fromList . (either (pure . Left) (map Right . Set.toList) =<<)))
    (iso (map (fmap Set.singleton) . Set.toList) mconcat)
    (iso (map Set.singleton . Set.toList) mconcat)
    id
    (maybeMonoid wPayload')
    protoPayload

  return $ WorkflowWorkflowEdgeFormAligned WorkflowWorkflowEdgeForm
    { wwefPayload = wwefPayload'
    , ..
    }

assertWorkflowEdgePayloadFieldAligned :: forall fileid' fileid userid' userid m r.
                                         ( MonadHandler m, HandlerSite m ~ UniWorX
                                         , WorkflowFieldPayloads fileid fileid userid userid
                                         , HasFileReference fileid -- We expect `sourceFile` to work on the contained references
                                         , FileReferenceTitleMapConvertible (FileFieldUserOption Bool) fileid' fileid'
                                         , fileid ~ fileid', userid ~ userid'
                                         , Ord fileid, Ord userid
                                         )
                                      => (UserId -> SqlPersistT m userid)
                                      -> Bool -- ^ @doCapture@
                                      -> WorkflowPayloadSpec fileid' userid'
                                      -> RWST
                                           r
                                           (Set (Either WorkflowPayloadLabel (WorkflowFieldPayloadW fileid userid))) -- prototype new payload and/or reference to other payload results
                                           (Set (WorkflowFieldPayloadW fileid userid)) -- input given, consume parts to determine new payload
                                           (ExceptT WorkflowWorkflowEdgeFormPayloadAlignmentViolation (SqlPersistT m))
                                           ()
assertWorkflowEdgePayloadFieldAligned projUserId doCapture (WorkflowPayloadSpec (specField :: WorkflowPayloadField fileid' userid' payload)) = case specField of
    -- Ensure this stays in sync with @workflowEdgePayloadFields@
    WorkflowPayloadFieldText{..} | Nothing <- wpftPresets -> withVal @Text wpftOptional $ \t -> do
      t' <- case wpftType of
        WorkflowPayloadTextTypeText -> return $ Text.strip t
        WorkflowPayloadTextTypeLarge -> return $ Text.strip t
        WorkflowPayloadTextTypeEmail -> case Email.canonicalizeEmail $ encodeUtf8 t of
          Just e -> return $ Text.decodeUtf8With Text.lenientDecode e
          Nothing -> throwError $ WWEFPAInvalidEmail t
        WorkflowPayloadTextTypeUrl -> case parseURI $ unpack t of
          Just u -> return . pack . ($ mempty) $ uriToString id u
          Nothing -> throwError $ WWEFPAInvalidUrl t
        WorkflowPayloadTextTypePassword -> return t
      when (null t') $ throwError WWEFPAEmpty
      tellVal t'
    WorkflowPayloadFieldText{..} | Just wpftPresets' <- wpftPresets -> withVal @Text wpftOptional $ \t -> do
      unless (t `Set.member` Set.fromList (wptpText <$> otoList wpftPresets')) $
        throwError $ WWEFPAInvalidTextPreset t
      tellVal t
    WorkflowPayloadFieldNumber{..} -> withVal @Scientific wpfnOptional $ \n -> do
      whenIsJust wpfnMin $ \wpfnMin' ->
        unless (n >= wpfnMin') . throwError $ WWEFPANumberBelowMin n
      whenIsJust wpfnMax $ \wpfnMax' ->
        unless (n <= wpfnMax') . throwError $ WWEFPANumberAboveMax n
      tellVal $ case wpfnStep of
        Nothing -> n
        Just wpfnStep' -> fromInteger (round $ n / wpfnStep') * wpfnStep'
    WorkflowPayloadFieldBool{..} -> withVal @Bool (is _Just wpfbOptional) tellVal
    WorkflowPayloadFieldDay{..} -> withVal @Day wpfdOptional $ \d -> do
      dayValidationEnabled <- getVolatileClusterSetting clusterVolatileWorkflowEdgeFormDayValidationEnabled
      cDay <- localDay . utcToLocalTime <$> liftIO getCurrentTime
      if | Just offset <- wpfdMaxPast
         , dayValidationEnabled
         , (cDay `diffDays` d) > offset
           -> throwError $ WWEFPADayTooFarPast d
         | otherwise -> return ()
      if | Just offset <- wpfdMaxFuture
         , dayValidationEnabled
         , (d `diffDays` cDay) > offset
           -> throwError $ WWEFPADayTooFarFuture d
         | otherwise -> return ()
      tellVal d
    WorkflowPayloadFieldTime{..} -> withVal @TimeOfDay wpfttOptional tellVal
    WorkflowPayloadFieldDateTime{..} -> withVal @UTCTime wpfdtOptional $ \dt -> do
      datetimeValidationEnabled <- getVolatileClusterSetting clusterVolatileWorkflowEdgeFormDayValidationEnabled
      now <- liftIO getCurrentTime
      if | Just offset <- wpfdtMaxPast
         , datetimeValidationEnabled
         , (now `diffUTCTime` dt) > offset
           -> throwError $ WWEFPADateTimeTooFarPast dt
         | otherwise -> return ()
      if | Just offset <- wpfdtMaxFuture
         , datetimeValidationEnabled
         , (dt `diffUTCTime` now) > offset
           -> throwError $ WWEFPADateTimeTooFarFuture dt
         | otherwise -> return ()
      tellVal dt
    WorkflowPayloadFieldFile{..} -> do
      let FileField
            { fieldIdent = _
            , fieldUnpackZips = _ -- Never unpack zips; we don't want double unpacking when validating form submissions and json users can be expected to send multiple files instead of an archive
            , fieldAdditionalFiles = toListOf (_FileReferenceTitleMap . filtered (views _2 $ (&&) <$> fieldOptionForce <*> fieldOptionDefault) . _1) -> addFiles
            , ..
            } = wpffConfig
      files <- if | fieldMultiple -> whileIsJust $ extractVal @fileid
                  | otherwise -> maybeToList <$> extractVal @fileid
      when (not wpffOptional && null files) $ throwError WWEFPAMissing
      if | Just allowedExts <- fieldRestrictExtensions
         , (fName : _) <- toListOf (folded . _FileReference . _1 . _fileReferenceTitle . filtered (\fTitle -> noneOf (re _nullable . folded . unpacked) ((flip isExtensionOf `on` CI.foldCase) $ unpack fTitle) allowedExts)) files
           -> throwError $ WWEFPAInvalidFileExtension fName
         | otherwise
           -> return ()
      let sizeCheck :: (Any, Sum Natural)
                    -> [fileid]
                    -> ExceptT WorkflowWorkflowEdgeFormPayloadAlignmentViolation (SqlPersistT m) ()
          sizeCheck (Any True, _) _
            | is _Nothing fieldMaxFileSize, is _Nothing fieldMaxCumulativeSize
            = return ()
          sizeCheck (haveNonEmpty, sizeSum) (f : fs) = do
            fSize <- lift $ fromMaybe 0 <$> sourceFileSize (f ^. _FileReference . _1)
            whenIsJust fieldMaxFileSize $ \fieldMaxFileSize' ->
              when (fieldMaxFileSize' < fSize) . throwError . WWEFPAFileTooLarge $ f ^. _FileReference . _1 . _fileReferenceTitle
            let sizeSum' = sizeSum <> Sum fSize
            whenIsJust fieldMaxCumulativeSize $ \fieldMaxCumulativeSize' ->
              when (fieldMaxCumulativeSize' < getSum sizeSum') $ throwError WWEFPACumulativeSizeExceeded
            sizeCheck (Any (fSize > 0) <> haveNonEmpty, sizeSum) fs
          sizeCheck (Any haveNonEmpty, _) []
            | haveNonEmpty || fieldAllEmptyOk = return ()
            | otherwise = throwError WWEFPANonEmptyFileRequired
       in void . lift $ sizeCheck mempty files
      -- TODO do @addFiles@ count againts @fieldMaxCumulativeSize@?
      mapM_ tellVal $ addFiles <> files
    WorkflowPayloadFieldUser{..} -> withVal @userid wpfuOptional tellVal
    WorkflowPayloadFieldCaptureUser | doCapture -> traverse_ (tellVal <=< lift . lift . projUserId) =<< liftHandler maybeAuthId
    WorkflowPayloadFieldCaptureUser -> assertWorkflowEdgePayloadFieldAligned projUserId doCapture $ WorkflowPayloadSpec WorkflowPayloadFieldUser{ wpfuLabel = opoint mempty, wpfuTooltip = Nothing, wpfuDefault = Nothing, wpfuOptional = False }
    WorkflowPayloadFieldCaptureDateTime{..} | doCapture -> tell . Set.singleton . Right =<< workflowEdgeCaptureTime wpfcdtPrecision
    WorkflowPayloadFieldCaptureDateTime{..} -> assertWorkflowEdgePayloadFieldAligned projUserId doCapture $ case wpfcdtPrecision of
      WFCaptureDate -> WorkflowPayloadSpec WorkflowPayloadFieldDay{ wpfdLabel = opoint mempty, wpfdTooltip = Nothing, wpfdDefault = Nothing, wpfdOptional = False, wpfdMaxPast = Nothing, wpfdMaxFuture = Nothing }
      WFCaptureTime -> WorkflowPayloadSpec WorkflowPayloadFieldTime{ wpfttLabel = opoint mempty, wpfttTooltip = Nothing, wpfttDefault = Nothing, wpfttOptional = False }
      WFCaptureDateTime -> WorkflowPayloadSpec WorkflowPayloadFieldDateTime{ wpfdtLabel = opoint mempty, wpfdtTooltip = Nothing, wpfdtDefault = Nothing, wpfdtOptional = False, wpfdtMaxPast = Nothing, wpfdtMaxFuture = Nothing }
    WorkflowPayloadFieldReference{..} -> tell . Set.singleton $ Left wpfrTarget
    WorkflowPayloadFieldMultiple{..}
      -> let go 0 (Just 0) = return ()
             go cMin cRange = do
                 pState <- State.get
                 isSuccessful <- bool id (mapRWST . flip catchE $ handleMissing pState) (cMin <= 0) $ True <$ subAct
                 isConsumptive <- State.gets $ (/=) pState
                 let goNext | cMin > 0  = go (pred cMin) cRange
                            | otherwise = go 0 (cRange <&> \cRange' -> bool id pred (cRange' > 0) cRange')
                 if | isSuccessful, isConsumptive || is _Just cRange -> goNext
                    -- | isConsumptive -> go cMin cRange -- impossible, consumptiveness implies success due to short-circuiting behaviour of `throwE` and how that is handled in the implementation of `handleMissing` (restoring the old state)
                    | cMin <= 0 -> return ()
                    | otherwise -> lift $ throwE WWEFPAMultipleCardinalityViolation
               where subAct = assertWorkflowEdgePayloadFieldAligned projUserId doCapture wpfmSub
                     handleMissing pState = \case
                       WWEFPAMissing -> return (False, pState, mempty)
                       otherE -> throwE otherE
          in go wpfmMin wpfmRange
  where
    extractVal :: forall payload' m'.
                  ( IsWorkflowFieldPayload' fileid userid payload'
                  , State.MonadState (Set (WorkflowFieldPayloadW fileid userid)) m'
                  )
               => m' (Maybe payload')
    extractVal = State.state $ foldl' go (Nothing, Set.empty) . map (matching $ _WorkflowFieldPayloadW @payload' @fileid @userid . _WorkflowFieldPayload) . Set.toList
      where go (mPrev, xs) (Left  x) = (mPrev, Set.insert x xs)
            go (mPrev, xs) (Right p) = case mPrev of
              Nothing -> (Just p, xs)
              Just _  -> (mPrev, Set.insert (_WorkflowFieldPayloadW @payload' @fileid @userid . _WorkflowFieldPayload # p) xs)
    withVal :: forall payload' m' a.
               ( IsWorkflowFieldPayload' fileid userid payload'
               , State.MonadState (Set (WorkflowFieldPayloadW fileid userid)) m'
               , MonadError WorkflowWorkflowEdgeFormPayloadAlignmentViolation m'
               )
            => Bool -- ^ @isOpt@
            -> (payload' -> m' a)
            -> m' ()
    withVal isOpt act = do
      val <- extractVal
      case val of
        Nothing | not isOpt -> throwError WWEFPAMissing
                | otherwise -> return ()
        Just val' -> void $ act val'
    tellVal :: forall m'.
             ( IsWorkflowFieldPayload' fileid userid payload
             , MonadWriter (Set (Either WorkflowPayloadLabel (WorkflowFieldPayloadW fileid userid))) m'
             )
            => payload
            -> m' ()
    tellVal = tell . Set.singleton . Right . review (_WorkflowFieldPayloadW @payload @fileid @userid . _WorkflowFieldPayload)


data JsonWorkflowWorkflowEdgeFormException
  = JWWEFUserAmibguous UserEmail
  | JWWEFAlignmentViolation WorkflowWorkflowEdgeFormAlignmentViolation
  deriving stock (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (Exception)

acceptJsonWorkflowWorkflowEdgeForm :: ( MonadHandler m
                                      , HandlerSite m ~ UniWorX
                                      , MonadCatch m
                                      , MonadUnliftIO m
                                      )
                                   => Either WorkflowInstanceId WorkflowWorkflowId
                                   -> SqlPersistT m (Maybe IdWorkflowWorkflowEdgeFormAligned)
acceptJsonWorkflowWorkflowEdgeForm mwwId = runMaybeT $ do
  edgeInput <- maybeCheckJsonBody @_ @JsonWorkflowWorkflowEdgeForm
  lift $ do
    edgeInput' <- forOf (typesCustom @WorkflowChildren @(WorkflowWorkflowEdgeForm PureFile UserEmail) @(WorkflowWorkflowEdgeForm PureFile UserId) @UserEmail @UserId) edgeInput $ \email -> hoist liftHandler (guessUser (predDNFSingleton (PLVariable $ GuessUserIdent email) `predDNFOr` predDNFSingleton (PLVariable $ GuessUserEmail email)) (Just 2)) >>= \case
      Just (Right (Entity uid _)) -> return uid
      _other -> throwM $ JWWEFUserAmibguous email
    edgeInput'' <- forOf (_wwefPayload . typesCustom @WorkflowChildren) (edgeInput' :: WorkflowWorkflowEdgeForm PureFile UserId) $ sinkFile . fromPureFile -- No idea why _wwefPayload is necessary here and not above GK 2022-10-17
    handle (throwM . JWWEFAlignmentViolation) $ assertWorkflowWorkflowEdgeFormAligned return return True mwwId edgeInput''


workflowEdgeFormToAction :: ( MonadHandler m
                            , HandlerSite m ~ UniWorX
                            )
                         => IdWorkflowWorkflowEdgeFormAligned
                         -> m IdWorkflowWorkflowActionInfo
workflowEdgeFormToAction (WorkflowWorkflowEdgeFormAligned WorkflowWorkflowEdgeForm{..}) = do
    wwaiUser <- maybe WorkflowActionUnauthenticated WorkflowActionUser <$> maybeAuthId
    wwaiTime <- liftIO getCurrentTime
    return WorkflowWorkflowActionInfo{..}
  where
    wwaiTo = wwefTo
    wwaiVia = wwefVia
    wwaiPayload = wwefPayload
