-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Table.Sorting
  ( getTableSorting
  ) where

import Import

import Handler.Utils.Routes

import qualified Data.Text as Text


getTableSorting :: Maybe (Route UniWorX) -> Text -> Handler (Maybe SortingSettings)
getTableSorting route dbtIdent = maybeT (return mempty) $ do
  uid <- MaybeT maybeAuthId
  tblHandler <- Text.pack . classifyHandler <$> hoistMaybe route
  lift . runDBRead . fmap ((userTableSortingSorting . entityVal) =<<) . getBy $ UniqueUserTableSorting uid tblHandler dbtIdent
