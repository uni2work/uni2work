-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Table.Pagination.Settings
  ( updateUserTableSorting
  , ButtonTableSorting(..)
  , TableSettingsForm(..)
  , mkTableSettingsForm
  , handleTableSettingsForm
  ) where

import Import

import Handler.Utils.Form
import Handler.Utils.Routes (classifyHandler)

import qualified Data.Text as Text


updateUserTableSorting :: Text -- ^ Table id (i.e. dbtIdent)
                       -> SortingSettings
                       -> Handler ()
updateUserTableSorting dbtIdent sorting = maybeT_ $ do
  uid <- MaybeT maybeAuthId
  currentHandler <- Text.pack . classifyHandler <$> MaybeT getCurrentRoute
  whenM (lift . fmap (maybe True ((\srt -> isn't _Nothing srt && srt /= Just sorting) . userTableSortingSorting . entityVal)) . runDBRead . getBy $ UniqueUserTableSorting uid currentHandler dbtIdent) $ do
    void . lift . runDB $ upsertBy
      (UniqueUserTableSorting uid currentHandler dbtIdent)
      UserTableSorting
        { userTableSortingUser    = uid
        , userTableSortingHandler = currentHandler
        , userTableSortingTable   = dbtIdent
        , userTableSortingSorting = Just sorting
        }
      [ UserTableSortingSorting =. Just sorting ]


data ButtonTableSorting = ButtonTableSortingReset
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Typeable)
instance Universe ButtonTableSorting
instance Finite ButtonTableSorting
nullaryPathPiece ''ButtonTableSorting $ camelToPathPiece' 1
embedRenderMessage ''UniWorX ''ButtonTableSorting id
instance Button UniWorX ButtonTableSorting where
  btnClasses ButtonTableSortingReset = [BCIsButton, BCPrimary]
  btnLabel ButtonTableSortingReset = [whamlet|_{MsgButtonTableSortingReset}|]

newtype TableSettingsForm = TableSettingsForm
  { tsfSorting :: Maybe [Maybe ButtonTableSorting]
  }

mkTableSettingsForm :: Text -> Handler (AForm Handler TableSettingsForm)
mkTableSettingsForm dbtIdent = do
  currentHandler <- maybe (error "mkTableSettingsForm called from 404-handler") (Text.pack . classifyHandler) <$> getCurrentRoute

  currentTableSortingSettings <- maybeM (return mempty) (\uid -> fmap (fmap $ userTableSortingSorting . entityVal) . runDBRead . getBy $ UniqueUserTableSorting uid currentHandler dbtIdent) maybeAuthId
  let template = TableSettingsForm <$> maybe (Just $ Just universeF) (fmap . const $ Just universeF) currentTableSortingSettings

  return $ TableSettingsForm
    -- <$> wformSection MsgTableSettingsSectionSorting
    <$> optionalActionA
        (combinedButtonFieldF "")
        (fslI MsgTableSettingsSortingDoSave & setTooltip MsgTableSettingsSortingDoSaveTip)
        (is _Just . tsfSorting <$> template)

handleTableSettingsForm :: Text
                        -> TableSettingsForm
                        -> Handler (Maybe (Maybe SortingSettings)) -- Outer Maybe ~ has changed, inner Maybe ~ new value
handleTableSettingsForm dbtIdent TableSettingsForm{..} = maybeT (return Nothing) $ do
  uid <- MaybeT maybeAuthId
  currentHandler <- Text.pack . classifyHandler <$> MaybeT getCurrentRoute

  if
    | is _Nothing tsfSorting -> do
      liftHandler . runDB . void $ upsert
        UserTableSorting
          { userTableSortingUser    = uid
          , userTableSortingHandler = currentHandler
          , userTableSortingTable   = dbtIdent
          , userTableSortingSorting = Nothing
          }
        [ UserTableSortingSorting =. Nothing ]
      return Nothing
    | Just srt <- tsfSorting, Just ButtonTableSortingReset `elem` srt -> do
      liftHandler . runDB . deleteBy $ UniqueUserTableSorting uid currentHandler dbtIdent
      return $ Just Nothing
    | Just (catMaybes -> []) <- tsfSorting -> do
      guardM . fmap (maybe False $ is _Nothing . userTableSortingSorting . entityVal) . liftHandler . runDBRead . getBy $ UniqueUserTableSorting uid currentHandler dbtIdent
      liftHandler . runDB . deleteBy $ UniqueUserTableSorting uid currentHandler dbtIdent
      return $ Just Nothing
    | otherwise -> return Nothing
