-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.News
  ( NewsGetParam(..)
  ) where

import Import


data NewsGetParam = GetHidden
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)
  deriving anyclass (Universe, Finite)
nullaryPathPiece ''NewsGetParam $ camelToPathPiece' 1
