-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Workflow.New
  ( getAdminWorkflowWorkflowNewR, postAdminWorkflowWorkflowNewR
  ) where

import Import


getAdminWorkflowWorkflowNewR, postAdminWorkflowWorkflowNewR :: Handler Html
getAdminWorkflowWorkflowNewR = postAdminWorkflowWorkflowNewR
postAdminWorkflowWorkflowNewR = error "not implemented"
