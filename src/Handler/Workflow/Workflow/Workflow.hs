-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Workflow.Workflow
  ( getGWWWorkflowR, postGWWWorkflowR, getGWWFilesR
  , getSWWWorkflowR, postSWWWorkflowR, getSWWFilesR
  , getTWWWorkflowR, postTWWWorkflowR, getTWWFilesR
  , getTSWWWorkflowR, postTSWWWorkflowR, getTSWWFilesR
  , workflowR
  ) where

import Import hiding (Last(..), Encoding(None))

import Utils.Form
import Utils.Workflow

import Data.Semigroup (Last(..))

import Handler.Utils
import Handler.Utils.Workflow
import Handler.Utils.Workflow.History

import qualified Data.Map as Map
import qualified Data.Set as Set

import qualified Control.Monad.State.Class as State
import Control.Monad.Trans.RWS.Strict (RWST)

import qualified Crypto.Saltine.Class as Saltine
import qualified Data.Binary as Binary
import qualified Data.ByteArray as BA
import Crypto.Hash.Algorithms (SHAKE256)

import qualified Data.Text as Text
import Data.RFC5051 (compareUnicode)

import qualified Data.Scientific as Scientific
import Text.Blaze (toMarkup)
import Data.Void (absurd)

import qualified Data.Conduit.Combinators as C

import qualified Database.Esqueleto.Legacy as E


getGWWWorkflowR, postGWWWorkflowR :: CryptoFileNameWorkflowWorkflow -> Handler Html
getGWWWorkflowR = postGWWWorkflowR
postGWWWorkflowR = workflowR WSGlobal

getGWWFilesR :: CryptoFileNameWorkflowWorkflow -> WorkflowPayloadLabel -> CryptoUUIDWorkflowStateIndex -> Handler TypedContent
getGWWFilesR = getWorkflowFilesR WSGlobal

getSWWWorkflowR, postSWWWorkflowR :: SchoolId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getSWWWorkflowR = postSWWWorkflowR
postSWWWorkflowR ssh = workflowR $ WSSchool ssh

getSWWFilesR :: SchoolId -> CryptoFileNameWorkflowWorkflow -> WorkflowPayloadLabel -> CryptoUUIDWorkflowStateIndex -> Handler TypedContent
getSWWFilesR ssh = getWorkflowFilesR $ WSSchool ssh

getTWWWorkflowR, postTWWWorkflowR :: TermId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getTWWWorkflowR = postTWWWorkflowR
postTWWWorkflowR tid = workflowR $ WSTerm tid

getTWWFilesR :: TermId -> CryptoFileNameWorkflowWorkflow -> WorkflowPayloadLabel -> CryptoUUIDWorkflowStateIndex -> Handler TypedContent
getTWWFilesR tid = getWorkflowFilesR $ WSTerm tid

getTSWWWorkflowR, postTSWWWorkflowR :: TermId -> SchoolId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getTSWWWorkflowR = postTSWWWorkflowR
postTSWWWorkflowR tid ssh = workflowR $ WSTermSchool tid ssh

getTSWWFilesR :: TermId -> SchoolId -> CryptoFileNameWorkflowWorkflow -> WorkflowPayloadLabel -> CryptoUUIDWorkflowStateIndex -> Handler TypedContent
getTSWWFilesR tid ssh = getWorkflowFilesR $ WSTermSchool tid ssh


workflowR :: RouteWorkflowScope -> CryptoFileNameWorkflowWorkflow -> Handler Html
workflowR rScope cID = workflowsDisabledWarning title heading $ do
  now <- liftIO getCurrentTime
  (mEdge, (workflowState, workflowHistory)) <- runDB $ do
    wwId <- decrypt cID
    ww@WorkflowWorkflow{..} <- get404 wwId
    maybeT notFound . void . assertM (== review _DBWorkflowScope workflowWorkflowScope) $ fromRouteWorkflowScope rScope
    mEdgeForm <- workflowEdgeForm (Right wwId) Nothing
    wGraph@WorkflowGraph{..} <- getSharedIdWorkflowGraph workflowWorkflowGraph
    let canonRoute = _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)

    mEdge <- for mEdgeForm $ \edgeForm -> do
      ((edgeRes, edgeView), edgeEnc) <- runFormPost $ renderAForm FormStandard edgeForm
      edgeRes' <- runMaybeT $ formResultMaybeT' edgeRes
                          <|> assertStdMethod POST *> MaybeT (acceptJsonWorkflowWorkflowEdgeForm $ Right wwId)
      edgeAct <- for edgeRes' $ \edgeRes'' -> do
        $logWarnS "WWWorkflowR" $ tshow edgeRes''
        restrState <- maybeT notFound . MaybeT $ getWorkflowWorkflowRestrictionState wwId
        runConduit $
          followEdge wGraph edgeRes'' (Just restrState) .| sinkWorkflowWorkflowActionInfos wwId

        return $ do
          addMessageI Success MsgWorkflowWorkflowWorkflowEdgeSuccess

          redirect canonRoute
      return ((edgeAct, edgeView), edgeEnc)

    (fmap getLast -> workflowState, workflowHistory) <-
      let go :: forall m.
                ( MonadHandler m
                , HandlerSite m ~ UniWorX
                , MonadCatch m
                , MonadUnliftIO m
                )
             => (WorkflowStateIndex, IdWorkflowWorkflowActionInfoAuthorized)
             -> RWST
                  ()
                  (Maybe (Last WorkflowCurrentState), [WorkflowHistoryItem])
                  (Maybe (WorkflowStateIndex, IdWorkflowRestrictionState))
                  (SqlPersistT m)
                  ()
          go (stIx, wwaia@WorkflowWorkflowActionInfoAuthorized{ wwaiaPayload = payloadChanges, .. }) = maybeT_ $ do
            mAuthId <- maybeAuthId

            stCID <- encryptWorkflowStateIndex wwId stIx

            let
              nodeView (mMayView, nodeLbl) = do
                WGN{..} <- hoistMaybe $ Map.lookup nodeLbl wgNodes
                WorkflowNodeView{..} <- hoistMaybe wgnViewers
                case mMayView of
                  Nothing -> guardM $ anyM (otoList wnvViewers) hasWorkflowRole'
                  Just mayView -> guard mayView
                (, wgnFinal) <$> selectLanguageI18n wnvDisplayLabel

              whiTime = wwaiaTime
              mVia = Map.lookup wwaiaVia . wgnEdges =<< Map.lookup (wwaiaTo ^. _2) wgNodes
              hasWorkflowRole' role = $cachedHereBinary (rScope, wwId, role) . lift . lift $ is _Authorized <$> hasWorkflowRole (Just wwId) role canonRoute False

            whiTo <- lift . runMaybeT . nodeView $ over _1 Just wwaiaTo
            let wcsState = whiTo

            whiUser <-
              let (mayView, uid) = wwaiaUser
               in for uid $ \uid' -> if
                    | Just uid' == mAuthId -> return WHIASelf
                    | otherwise -> lift . maybeT (return WHIAHidden) $ do
                        guard mayView
                        fmap (maybe WHIAGone WHIAOther) . lift . lift $ getEntity uid'

            whiVia <- lift . runMaybeT $ do
              wge <- hoistMaybe mVia
              edgeDisplayLbl <- hoistMaybe $ wge ^? _wgeDisplayLabel
              mapM (const $ selectLanguageI18n edgeDisplayLbl) whiTo

            mFrom <- State.gets $ \prevSt -> do
              (prevIx, WorkflowRestrictionState{wwrCurrentNode}) <- prevSt
              guard $ prevIx == pred stIx
              return wwrCurrentNode
            whiFrom <- for mFrom $ lift . runMaybeT . nodeView . (Nothing, )

            let
              renderPayload :: Monoid w
                            => IdWorkflowStatePayload
                            -> MaybeT (RWST r w s (SqlPersistT m)) [(Text, ([WorkflowFieldPayloadW Void (Maybe (Entity User))], Maybe Text))]
              renderPayload payload = do
                sBoxKey <- secretBoxKey
                let payloadLabelToDigest :: WorkflowPayloadLabel -> ByteString
                    payloadLabelToDigest = BA.convert . kmaclazy @(SHAKE256 256) ("workflow-workflow-payload-sorting" :: ByteString) (Saltine.encode sBoxKey) . Binary.encode . (wwId, )
                    payloadLabelSort = (compareUnicode `on` views (_2 . _1) Text.toLower)
                                    <>  comparing (views _1 payloadLabelToDigest)
                payload' <- fmap (map (view _2) . sortBy payloadLabelSort) . forMaybeM (Map.toList payload) $ \(payloadLbl, newPayload) -> do
                  WorkflowPayloadView{..} <- hoistMaybe . Map.lookup payloadLbl $ Map.findWithDefault Map.empty (wwaiaTo ^. _2) (wgnPayloadView <$> wgNodes)
                  -- Authorization against `wpvViewers` is already part of the checks performed by `sourceWorkflowWorkflowActionInfosAuthorized` (via `WorkflowGraphAuthViewWorkflowWorkflowPayload`)
                  -- guardM . $cachedHereBinary payloadLbl . anyM (otoList wpvViewers) $ lift . hasWorkflowRole'
                  let fRoute = _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID (WWFilesR payloadLbl stCID))
                  (payloadLbl, ) . (, (newPayload, fRoute)) <$> selectLanguageI18n wpvDisplayLabel
                let
                  payloadSort :: WorkflowFieldPayloadW Void (Maybe (Entity User))
                              -> WorkflowFieldPayloadW Void (Maybe (Entity User))
                              -> Ordering
                  payloadSort = workflowPayloadSort ordFiles ordUsers
                    where
                      ordFiles = absurd
                      ordUsers a' b' = case (a', b') of
                        (Nothing, _) -> GT
                        (_, Nothing) -> LT
                        (Just (Entity _ uA), Just (Entity _ uB))
                          -> (compareUnicode `on` userSurname) uA uB
                          <> (compareUnicode `on` userDisplayName) uA uB
                          <> comparing userIdent uA uB

                forM payload' $ \(lblText, (otoList -> payloads, fRoute)) -> fmap ((lblText, ) . over _1 (sortBy payloadSort)) . mapMOf _2 (traverse toTextUrl . bool Nothing (Just fRoute) . getAny) <=< execWriterT @_ @(_, Any) . forM_ payloads $ \case
                  WorkflowFieldPayloadW (WFPText   t  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPText t)
                  WorkflowFieldPayloadW (WFPNumber n  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPNumber n)
                  WorkflowFieldPayloadW (WFPBool   b  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPBool b)
                  WorkflowFieldPayloadW (WFPDay    d  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPDay d)
                  WorkflowFieldPayloadW (WFPTime   t  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPTime t)
                  WorkflowFieldPayloadW (WFPDateTime t) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPDateTime t)
                  WorkflowFieldPayloadW (WFPFile   _  ) -> tell (mempty, Any True)
                  WorkflowFieldPayloadW (WFPUser   uid) -> tell . (, mempty) . pure . review (_WorkflowFieldPayloadW . _WorkflowFieldPayload) =<< lift (lift . lift $ getEntity uid)

            currentPayload <- State.state $ \prevSt ->
              let currentRestr = appendWorkflowRestrictionState (view _2 <$> prevSt) $ wwaia ^. _wwaiaUnauthorized
               in ( wwrCurrentPayload currentRestr
                  , Just (stIx, currentRestr)
                  )
            whiPayloadChanges <- renderPayload $ payloadChanges <&> view _2
            wcsPayload <- renderPayload currentPayload

            wcsMessages <- do
              let msgs = maybe Set.empty wgnMessages $ Map.lookup (wwaiaTo ^. _2) wgNodes
              flip foldMapM msgs $ \WorkflowNodeMessage{..} -> lift . maybeT (return Set.empty) . fmap Set.singleton $ do
                guardM $ anyM (otoList wnmViewers) hasWorkflowRole'
                history <- hoistMaybe =<< State.gets (fmap $ view _2)
                whenIsJust wnmRestriction $ guard . checkWorkflowRestriction (Just history)
                let messageStatus = wnmStatus
                    messageIcon = Nothing
                messageContent <- selectLanguageI18n wnmContent
                return Message{..}

            wcsStages <- forM wgStages $ \WorkflowGraphStage{..} -> do
              stageDisplayLabel <- selectLanguageI18n wgsDisplayLabel
              substages <- forMaybeM wgsSubstages $ \WorkflowGraphSubstage{..} -> do
                substageDisplayLbl <- lift $ selectLanguageI18n wgssDisplayLabel
                history <- lift $ State.gets (fmap $ view _2)
                let
                  predicateFulfilled = checkWorkflowRestriction history wgssPredicate
                  substageIcn = case wgssMode of
                    WorkflowSubstageRequired -> IconWorkflowTaskRequired
                    WorkflowSubstageOptional -> IconWorkflowTaskOptional
                guard $ wgssShowWhen == WorkflowSubstageShowAlways
                     || wgssShowWhen == WorkflowSubstageShowFulfilled   &&     predicateFulfilled
                     || wgssShowWhen == WorkflowSubstageShowUnfulfilled && not predicateFulfilled
                return (substageDisplayLbl, substageIcn, predicateFulfilled)
              return (wgsLabel, stageDisplayLabel, substages)

            let wcsArchived = workflowWorkflowArchived

            tell ( Just $ Last WorkflowCurrentState{..}
                 , pure WorkflowHistoryItem{..}
                 )

          sourceWorkflowWorkflowActionInfos' = sourceWorkflowWorkflowActionInfosAuthorized (const id) (const id) $ Entity wwId ww
       in fmap (over _2 (sortOn (Down . whiTime) . reverse) . view _2) . runConduit $ sourceWorkflowWorkflowActionInfos' .| execRWSC () Nothing (C.mapM_ go)

    return (mEdge, (workflowState, workflowHistory))

  sequenceOf_ (_Just . _1 . _1 . _Just) mEdge

  let headingWgt
        | Just WorkflowCurrentState{..} <- workflowState
        , Just (_, Just icn) <- wcsState
        = [whamlet|_{heading}&nbsp;#{icon icn}|]
        | otherwise = i18n heading

  siteLayout headingWgt $ do
    setTitleI title
    let mEdgeView = mEdge <&> \((_, edgeView'), edgeEnc) -> wrapForm edgeView' FormSettings
          { formMethod = POST
          , formAction = Just . SomeRoute $ _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
          , formEncoding = edgeEnc
          , formAttrs = []
          , formSubmit = FormSubmit
          , formAnchor = Nothing :: Maybe Text
          }
        historyToWidget WorkflowHistoryItem{..} = $(widgetFile "workflows/workflow/history-item")
        payloadToWidget :: WorkflowFieldPayloadW Void (Maybe (Entity User)) -> Widget
        payloadToWidget = \case
          WorkflowFieldPayloadW (WFPText   t       )
            -> [whamlet|
                 $newline never
                 <p .workflow-payload--text>
                   #{t}
               |]
          WorkflowFieldPayloadW (WFPNumber   n       ) -> toWidget . toMarkup $ formatScientific Scientific.Fixed Nothing n
          WorkflowFieldPayloadW (WFPBool     b       ) -> i18n $ WorkflowPayloadBool b
          WorkflowFieldPayloadW (WFPDay      d       ) -> formatTimeW SelFormatDate d
          WorkflowFieldPayloadW (WFPTime     t       ) -> formatTimeW SelFormatTime t
          WorkflowFieldPayloadW (WFPDateTime t       ) -> formatTimeW SelFormatDateTime t
          WorkflowFieldPayloadW (WFPUser     mUserEnt) -> case mUserEnt of
            Nothing                  -> i18n MsgWorkflowPayloadUserGone
            Just (Entity _ User{..}) -> nameEmailWidget userDisplayEmail userDisplayName userSurname
          WorkflowFieldPayloadW (WFPFile     v       ) -> absurd v
        archivationScheduled archived = $(i18nWidgetFile "workflow-archivation-scheduled")
    $(widgetFile "workflows/workflow")
  where
    (heading, title) = case rScope of
      WSGlobal -> (MsgGlobalWorkflowWorkflowWorkflowHeading cID, MsgGlobalWorkflowWorkflowWorkflowTitle cID)
      WSSchool ssh -> (MsgSchoolWorkflowWorkflowWorkflowHeading ssh cID, MsgSchoolWorkflowWorkflowWorkflowTitle ssh cID)
      WSTerm tid -> (MsgTermWorkflowWorkflowWorkflowHeading tid cID, MsgTermWorkflowWorkflowWorkflowTitle tid cID)
      WSTermSchool tid ssh -> (MsgTermSchoolWorkflowWorkflowWorkflowHeading tid ssh cID, MsgTermSchoolWorkflowWorkflowWorkflowTitle tid ssh cID)
      _other -> error "not implemented"

getWorkflowFilesR :: RouteWorkflowScope
                  -> CryptoFileNameWorkflowWorkflow
                  -> WorkflowPayloadLabel
                  -> CryptoUUIDWorkflowStateIndex
                  -> Handler TypedContent
getWorkflowFilesR rScope wwCID wpl stCID = do
  fRefs <- runDB . maybeT notFound $ do
    scope <- fromRouteWorkflowScope rScope
    wwId <- ensureScope scope wwCID
    stIx <- decryptWorkflowStateIndex wwId stCID
    let
      cont :: forall a. E.SqlExpr (Entity WorkflowWorkflowAction) `E.InnerJoin` E.SqlExpr (Entity WorkflowWorkflowPayload) -> E.SqlQuery a -> E.SqlQuery a
      cont (workflowWorkflowAction `E.InnerJoin` workflowWorkflowPayload) act = (*> act) $
        E.where_ $ workflowWorkflowAction E.^. WorkflowWorkflowActionIx E.<=. E.val stIx
             E.&&. workflowWorkflowPayload E.^. WorkflowWorkflowPayloadLabel E.==. E.val wpl
    payloads <- lift $ workflowStateCurrentPayloadsAuthorized' cont wwId
    let
      payloads' :: [FileReference]
      payloads' = payloads ^.. ix wpl . folded . _WorkflowFieldPayloadW . _WorkflowFieldPayload
    assertM' (not . null) payloads'

  archiveName <- fmap (flip addExtension (unpack extensionZip) . unpack) . ap getMessageRender . pure $ MsgWorkflowWorkflowFilesArchiveName wwCID wpl stCID

  serveSomeFiles archiveName $ yieldMany fRefs
