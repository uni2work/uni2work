-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}

module Handler.Workflow.Workflow.Overview
  ( getGWIOverviewR
  , getSWIOverviewR, getTWIOverviewR, getTSWIOverviewR
  ) where

import Import hiding (Last(..), WriterT)

import Utils.Workflow
import Handler.Utils.Workflow
import Handler.Utils.Workflow.History
import Handler.Utils

import qualified Database.Esqueleto.Legacy as E
import qualified Database.Esqueleto.Utils as E

import qualified Data.CaseInsensitive as CI
import qualified Data.Scientific as Scientific
import Text.Blaze (toMarkup)
import Data.Void (absurd)

import qualified Data.Set as Set
import qualified Data.Map as Map

import qualified Data.Conduit.Combinators as C

import qualified Data.Monoid as Monoid

import Control.Monad.Trans.Writer.Strict (WriterT)


data WorkflowWorkflowOverviewFilterProj = WorkflowWorkflowOverviewFilterProj
  { wwoProjFilterWorkflowWorkflow :: Maybe [[CI Char]]
  , wwoProjFilterCurrentState :: Maybe [[CI Char]]
  , wwoProjFilterFinal :: Maybe Bool
  }

instance Default WorkflowWorkflowOverviewFilterProj where
  def = WorkflowWorkflowOverviewFilterProj
    { wwoProjFilterWorkflowWorkflow = Nothing
    , wwoProjFilterCurrentState = Nothing
    , wwoProjFilterFinal = Nothing
    }

makeLenses_ ''WorkflowWorkflowOverviewFilterProj


getGWIOverviewR :: WorkflowInstanceName -> WorkflowOverviewName -> Handler Html
getGWIOverviewR = workflowInstanceOverviewR WSGlobal

getSWIOverviewR :: SchoolId -> WorkflowInstanceName -> WorkflowOverviewName -> Handler Html
getSWIOverviewR = workflowInstanceOverviewR . WSSchool

getTWIOverviewR :: TermId -> WorkflowInstanceName -> WorkflowOverviewName -> Handler Html
getTWIOverviewR = workflowInstanceOverviewR . WSTerm

getTSWIOverviewR :: TermId -> SchoolId -> WorkflowInstanceName -> WorkflowOverviewName -> Handler Html
getTSWIOverviewR tid ssh = workflowInstanceOverviewR $ WSTermSchool tid ssh


type WorkflowWorkflowOverviewTableExpr = E.SqlExpr (Entity WorkflowWorkflow)

queryWorkflowWorkflow :: Getter WorkflowWorkflowOverviewTableExpr (E.SqlExpr (Entity WorkflowWorkflow))
queryWorkflowWorkflow = id

type WorkflowWorkflowData = DBRow
  ( CryptoFileNameWorkflowWorkflow
  , Entity WorkflowWorkflow
  , Maybe WorkflowWorkflowActionData
  , Map WorkflowPayloadLabel (Set ResultWorkflowFieldPayloadW, Maybe (Route UniWorX))
  )

type WorkflowWorkflowActionData = ( Maybe Text
                                  , UTCTime
                                  , WorkflowHistoryItemUser
                                  , Maybe Icon
                                  )

type ResultWorkflowFieldPayloadW = WorkflowFieldPayloadW Void (Maybe (Entity User))

resultWorkflowWorkflowId :: Lens' WorkflowWorkflowData CryptoFileNameWorkflowWorkflow
resultWorkflowWorkflowId = _dbrOutput . _1

resultWorkflowWorkflow :: Lens' WorkflowWorkflowData (Entity WorkflowWorkflow)
resultWorkflowWorkflow = _dbrOutput . _2

resultLastAction :: Lens' WorkflowWorkflowData (Maybe WorkflowWorkflowActionData)
resultLastAction = _dbrOutput . _3

resultPayload :: WorkflowPayloadLabel -> Lens' WorkflowWorkflowData (Maybe (Set ResultWorkflowFieldPayloadW, Maybe (Route UniWorX)))
resultPayload pLbl = _dbrOutput . _4 . at pLbl

actionTo :: Lens' WorkflowWorkflowActionData (Maybe Text)
actionTo = _1

actionTime :: Lens' WorkflowWorkflowActionData UTCTime
actionTime = _2

actionActor :: Lens' WorkflowWorkflowActionData WorkflowHistoryItemUser
actionActor = _3

actionFinal :: Lens' WorkflowWorkflowActionData (Maybe Icon)
actionFinal = _4

workflowInstanceOverviewR :: RouteWorkflowScope
                          -> WorkflowInstanceName
                          -> WorkflowOverviewName
                          -> Handler Html
workflowInstanceOverviewR rScope win won = do
  mAuthId <- maybeAuthId

  (WorkflowInstanceOverview{..}, workflowTable) <- runDB $ do
    idScope <- maybeT notFound $ fromRouteWorkflowScope rScope
    wiId <- getKeyBy404 . UniqueWorkflowInstance win $ idScope ^. _DBWorkflowScope

    Entity _ wio@WorkflowInstanceOverview{..} <- getBy404 $ UniqueWorkflowInstanceOverview wiId won
    WorkflowOverviewSpec{..} <- getSharedIdWorkflowOverviewSpec workflowInstanceOverviewOverview

    relevantGraphs <- fmap (map E.unValue) . E.select . E.from . runReaderT $ do
      workflowWorkflow <- view queryWorkflowWorkflow
      let distinct =
            [ E.asc $ workflowWorkflow E.^. WorkflowWorkflowGraph
            ]
      hoist (E.distinctOnOrderBy distinct) $ do
        lift . E.where_ $ workflowWorkflow E.^. WorkflowWorkflowInstance E.==. E.justVal wiId
                    E.&&. workflowWorkflow E.^. WorkflowWorkflowScope E.==. E.val (idScope ^. _DBWorkflowScope)
        return $ workflowWorkflow E.^. WorkflowWorkflowGraph
    graphAuths <- fmap Map.fromList . forM relevantGraphs $ \wwGraph -> (wwGraph, ) <$>
      let route = _WorkflowScopeRoute # (rScope, WorkflowInstanceR win $ WIOverviewR won)
       in workflowAuthWhere'' (Left wwGraph) (Set.singleton WorkflowGraphAuthViewWorkflowWorkflow) route False

    let
      workflowWorkflowOverviewDBTable = DBTable{..}
        where
          dbtSQLQuery = runReaderT $ do
            workflowWorkflow <- view queryWorkflowWorkflow
            lift . E.where_ $ workflowWorkflow E.^. WorkflowWorkflowInstance E.==. E.justVal wiId
                        E.&&. workflowWorkflow E.^. WorkflowWorkflowScope E.==. E.val (idScope ^. _DBWorkflowScope)
            lift . E.where_ $ if
              | Map.null graphAuths -> E.false
              | otherwise -> E.case_
                  [ ( workflowWorkflow E.^. WorkflowWorkflowGraph E.==. E.val wwGraph
                    , $$(E.annSqlExpr) . authWhere . WorkflowAuthWhereWorkflow $ workflowWorkflow E.^. WorkflowWorkflowId
                    )
                  | (wwGraph, authWhere) <- Map.toList graphAuths
                  ]
                  E.false
            return workflowWorkflow
          dbtRowKey = views queryWorkflowWorkflow (E.^. WorkflowWorkflowId)
          dbtProj = (views _dbtProjRow . set _dbrOutput) =<< do
            ww@(Entity wwId WorkflowWorkflow{}) <- view $ _dbtProjRow . _dbrOutput

            cID <- encrypt @WorkflowWorkflowId @(CI FilePath) wwId
            forMM_ (view $ _dbtProjFilter . _wwoProjFilterWorkflowWorkflow) $ \criteria ->
              let haystack = map CI.mk . unpack $ toPathPiece cID
               in guard $ any (`isInfixOf` haystack) criteria

            WorkflowGraph{..} <- lift . lift . getSharedIdWorkflowGraph $ ww ^. _entityVal . _workflowWorkflowGraph

            let
              sourceWorkflowWorkflowActionInfos' :: ConduitT () (WorkflowStateIndex, IdWorkflowWorkflowActionInfoAuthorized) DB ()
              sourceWorkflowWorkflowActionInfos' = sourceWorkflowWorkflowActionInfosAuthorized onlyRelevant (const id) ww
                where onlyRelevant (_ `E.LeftOuterJoin` workflowWorkflowPayload) = (E.&&.) $
                        E.maybe E.true (`E.in_` E.valList (wosColumns ^.. folded . _wocsPayloadLabel)) (workflowWorkflowPayload E.?. WorkflowWorkflowPayloadLabel)
              toRes :: forall m. (MonadHandler m, HandlerSite m ~ UniWorX, MonadThrow m)
                    => ( Monoid.Last (ReaderT SqlBackend (HandlerFor UniWorX) WorkflowWorkflowActionData)
                       , Dual (Map WorkflowPayloadLabel (Set IdWorkflowFieldPayloadW, WorkflowStateIndex))
                       )
                    -> ReaderT SqlBackend m (Maybe WorkflowWorkflowActionData, Map WorkflowPayloadLabel (Set ResultWorkflowFieldPayloadW, Maybe (Route UniWorX)))
              toRes (Monoid.Last mActionData, Dual payloads) = (,)
                  <$> hoist liftHandler (sequenceA mActionData)
                  <*> imapM payloadsToRes payloads
                where
                  payloadsToRes :: WorkflowPayloadLabel
                                -> (Set IdWorkflowFieldPayloadW, WorkflowStateIndex)
                                -> ReaderT SqlBackend m (Set ResultWorkflowFieldPayloadW, Maybe (Route UniWorX))
                  payloadsToRes payloadLbl (payloads', stIx) = fmap (over _2 Monoid.getLast) . execWriterT @_ @(Set ResultWorkflowFieldPayloadW, Monoid.Last (Route UniWorX)) $ forM_ payloads' $ \case
                        WorkflowFieldPayloadW (WFPText   t  ) -> tell . (, mempty) . Set.singleton $ WorkflowFieldPayloadW (WFPText t)
                        WorkflowFieldPayloadW (WFPNumber n  ) -> tell . (, mempty) . Set.singleton $ WorkflowFieldPayloadW (WFPNumber n)
                        WorkflowFieldPayloadW (WFPBool   b  ) -> tell . (, mempty) . Set.singleton $ WorkflowFieldPayloadW (WFPBool b)
                        WorkflowFieldPayloadW (WFPDay    d  ) -> tell . (, mempty) . Set.singleton $ WorkflowFieldPayloadW (WFPDay d)
                        WorkflowFieldPayloadW (WFPTime   t  ) -> tell . (, mempty) . Set.singleton $ WorkflowFieldPayloadW (WFPTime t)
                        WorkflowFieldPayloadW (WFPDateTime t) -> tell . (, mempty) . Set.singleton $ WorkflowFieldPayloadW (WFPDateTime t)
                        WorkflowFieldPayloadW (WFPFile   _  ) -> do
                          stCID <- encryptWorkflowStateIndex wwId stIx
                          let fRoute :: Route UniWorX
                              fRoute = _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID $ WWFilesR payloadLbl stCID)
                           in tell (mempty, Monoid.Last $ Just fRoute)
                        WorkflowFieldPayloadW (WFPUser   uid) -> tell . (, mempty) . Set.singleton . review (_WorkflowFieldPayloadW . _WorkflowFieldPayload) =<< lift (getEntity uid)
              accActionInfos :: (WorkflowStateIndex, IdWorkflowWorkflowActionInfoAuthorized)
                             -> WriterT ( Monoid.Last (ReaderT SqlBackend (HandlerFor UniWorX) WorkflowWorkflowActionData)
                                        , Dual (Map WorkflowPayloadLabel (Set IdWorkflowFieldPayloadW, WorkflowStateIndex))
                                        ) (ReaderT SqlBackend (HandlerFor UniWorX)) ()
              accActionInfos (stIx, WorkflowWorkflowActionInfoAuthorized{..}) = do
                let
                  mTo = Map.lookup (wwaiaTo ^. _2) wgNodes
                  actName = runMaybeT $ do
                    WorkflowNodeView{..} <- hoistMaybe $ mTo >>= wgnViewers
                    guard $ wwaiaTo ^. _1
                    selectLanguageI18n wnvDisplayLabel
                  actUser = for (wwaiaUser ^. _2) $ \uid -> if
                    | Just uid == mAuthId -> return WHIASelf
                    | otherwise -> maybeT (return WHIAHidden) $ do
                        guard $ wwaiaUser ^. _1
                        fmap (maybe WHIAGone WHIAOther) . lift $ getEntity uid
                  actFinal = wgnFinal =<< mTo
                scribe (simple . _1) . Monoid.Last . Just $ do
                  actName' <- actName
                  actUser' <- actUser
                  return ( actName'
                         , wwaiaTime
                         , actUser'
                         , actName' *> actFinal
                         )

                scribe (simple . _2) . Dual $ (, stIx) . view _2 <$> wwaiaPayload
            (lastAct, payload) <- lift . lift $ toRes =<< runConduit (sourceWorkflowWorkflowActionInfos' .| execWriterC (C.mapM_ accActionInfos))

            whenIsJust (lastAct ^? _Just . actionTo . _Just) $ \lastActTo ->
              forMM_ (view $ _dbtProjFilter . _wwoProjFilterCurrentState) $ \criteria ->
                let haystack = map CI.mk $ unpack lastActTo
                 in guard $ any (`isInfixOf` haystack) criteria
            whenIsJust (lastAct ^? _Just . actionFinal . to (is _Just)) $ \isFinal ->
              forMM_ (view $ _dbtProjFilter . _wwoProjFilterFinal) $ \criterion ->
                guard $ criterion == isFinal

            return (cID, ww, lastAct, payload)
          dbtColonnade :: Colonnade Sortable _ _
          dbtColonnade = foldMap toColonnade wosColumns
            where
              toColonnade = \case
                WOCSWorkflowWorkflowId -> sortable (Just "workflow-workflow") (i18nCell MsgWorkflowWorkflowListNumber) . (addCellClass ("cryptoid" :: Text) .) . anchorWorkflowWorkflow . views resultWorkflowWorkflowId $ toWidget . (toPathPiece :: CryptoFileNameWorkflowWorkflow -> Text)
                WOCSWorkflowWorkflowState -> sortable (Just "current-state") (i18nCell MsgWorkflowWorkflowListCurrentState) $ fromMaybe mempty . previews (resultLastAction . _Just . $(multifocusL 2) actionTo actionFinal) stateCell
                WOCSWorkflowWorkflowLastActionTime -> sortable (Just "last-action-time") (i18nCell MsgWorkflowWorkflowListLastActionTime) $ fromMaybe mempty . previews (resultLastAction . _Just . actionTime) dateTimeCell
                WOCSWorkflowWorkflowLastActionUser -> sortable (Just "last-action-user") (i18nCell MsgWorkflowWorkflowListLastActionUser) $ fromMaybe mempty . previews (resultLastAction . _Just . actionActor) actorCell
                WOCSWorkflowWorkflowCurrentPayload{..} -> sortable Nothing (i18nCell wocsHeader) $ maybe mempty payloadCell . preview (resultPayload wocsPayloadLabel . _Just)

              anchorWorkflowWorkflow :: (WorkflowWorkflowData -> Widget) -> _
              anchorWorkflowWorkflow f x = cell $ do
                let cID = x ^. resultWorkflowWorkflowId
                linkUrl <- toTextUrl $ _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
                -- No authorization check for performance; sufficient auth already happens in sql
                let widget = f x
                $(widgetFile "table/cell/link")

              stateCell = \case
                (Nothing, _) -> i18nCell MsgWorkflowWorkflowWorkflowHistoryStateHidden & addCellClass ("explanation" :: Text)
                (Just n, Nothing) -> textCell n
                (Just n, Just fin) -> cell [whamlet|#{icon fin}&nbsp;#{n}|]
              actorCell = \case
                WorkflowActionAutomatic -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserAutomatic & addCellClass ("explanation" :: Text)
                WorkflowActionUser WHIASelf -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserSelf & addCellClass ("explanation" :: Text)
                WorkflowActionUser WHIAGone -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserGone & addCellClass ("explanation" :: Text)
                WorkflowActionUser WHIAHidden -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserHidden & addCellClass ("explanation" :: Text)
                WorkflowActionUnauthenticated -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserNotLoggedIn & addCellClass ("explanation" :: Text)
                WorkflowActionUser (WHIAOther (Entity _ User{..})) -> userCell userDisplayName userSurname

              payloadCell :: (Set ResultWorkflowFieldPayloadW, Maybe (Route UniWorX)) -> DBCell _ _
              payloadCell (payloads, mFileRoute) = cell $ do
                  mTextRoute <- traverse toTextUrl mFileRoute
                  [whamlet|
                    $newline never
                    <ul .list--iconless>
                      $maybe fileRoute <- mTextRoute
                        <li>
                          <a href=#{fileRoute}>
                            _{MsgWorkflowPayloadFiles}
                      $forall pItem <- payloads
                        <li>
                          ^{payloadToWidget pItem}
                  |]
                where
                  payloadToWidget :: WorkflowFieldPayloadW Void (Maybe (Entity User)) -> Widget
                  payloadToWidget = \case
                    WorkflowFieldPayloadW (WFPText   t       )
                      -> [whamlet|
                           $newline never
                           <p .workflow-payload--text>
                             #{t}
                         |]
                    WorkflowFieldPayloadW (WFPNumber   n       ) -> toWidget . toMarkup $ formatScientific Scientific.Fixed Nothing n
                    WorkflowFieldPayloadW (WFPBool     b       ) -> i18n $ WorkflowPayloadBool b
                    WorkflowFieldPayloadW (WFPDay      d       ) -> formatTimeW SelFormatDate d
                    WorkflowFieldPayloadW (WFPTime     t       ) -> formatTimeW SelFormatTime t
                    WorkflowFieldPayloadW (WFPDateTime t       ) -> formatTimeW SelFormatDateTime t
                    WorkflowFieldPayloadW (WFPUser     mUserEnt) -> case mUserEnt of
                      Nothing                  -> i18n MsgWorkflowPayloadUserGone
                      Just (Entity _ User{..}) -> nameEmailWidget userDisplayEmail userDisplayName userSurname
                    WorkflowFieldPayloadW (WFPFile     v       ) -> absurd v

          dbtSorting = mconcat
            [ singletonMap "workflow-workflow" . SortProjected . comparing $ view resultWorkflowWorkflowId
            , singletonMap "current-state" . SortProjected . comparing . preview $ resultLastAction . _Just . actionTo . _Just
            , singletonMap "last-action-time" . SortProjected . comparing . preview $ resultLastAction . _Just . actionTime
            , singletonMap "last-action-user" . SortProjected . comparing . preview $ resultLastAction . _Just . actionActor . to (over (mapped . mapped) $ \(Entity _ User{..}) -> (userSurname, userDisplayName))
            , singletonMap "final" . SortProjected . comparing $ \x -> guardOnM (has (resultLastAction . _Just . actionTo . _Just) x) (x ^? resultLastAction . _Just . actionFinal . _Just)
            ]
          dbtFilter = mconcat
            [ singletonMap "workflow-workflow" . FilterProjected $ \(criteria :: Set Text) ->
                let criteria' = map CI.mk . unpack <$> Set.toList criteria
                 in _wwoProjFilterWorkflowWorkflow ?~ criteria'
            , singletonMap "current-state" . FilterProjected $ \(criteria :: Set Text) ->
                let criteria' = map CI.mk . unpack <$> Set.toList criteria
                 in _wwoProjFilterCurrentState ?~ criteria'
            , singletonMap "final" . FilterProjected $ \(criterion :: Monoid.Last Bool) -> case Monoid.getLast criterion of
                Nothing -> id
                Just needle -> _wwoProjFilterFinal ?~ needle
            ]
          dbtFilterUI = mconcat
            [ flip (prismAForm $ singletonFilter "workflow-workflow") $ aopt textField (fslI MsgWorkflowWorkflowListNumber)
            , flip (prismAForm $ singletonFilter "current-state") $ aopt textField (fslI MsgWorkflowWorkflowListCurrentState)

            , flip (prismAForm (singletonFilter "final" . maybePrism _PathPiece)) $ aopt (boolField . Just $ SomeMessage MsgBoolIrrelevant) (fslI MsgWorkflowWorkflowListIsFinal)
            ]
          dbtStyle = def { dbsFilterLayout = defaultDBSFilterLayout }
          dbtParams = def
          dbtIdent :: Text
          dbtIdent = "workflow-overview--" <> CI.foldedCase won
          dbtCsvEncode = noCsvEncode
          dbtCsvDecode = Nothing
          dbtExtraReps = []
      workflowWorkflowOverviewDBTableValidator = def
        & defaultSorting [SortAscBy "final", SortAscBy "current-state", SortDescBy "last-action-time"]
     in (wio, ) <$> dbTableDB' workflowWorkflowOverviewDBTableValidator workflowWorkflowOverviewDBTable

  siteLayoutMsg workflowInstanceOverviewTitle $ do
    setTitleI workflowInstanceOverviewTitle
    $(widgetFile "workflows/workflow-overview")
