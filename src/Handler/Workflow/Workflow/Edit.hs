-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Workflow.Edit
  ( getGWWEditR, postGWWEditR
  , getSWWEditR, postSWWEditR
  , getTWWEditR, postTWWEditR
  , getTSWWEditR, postTSWWEditR
  , workflowEditR
  ) where

import Import

import Utils.Workflow


getGWWEditR, postGWWEditR :: CryptoFileNameWorkflowWorkflow -> Handler Html
getGWWEditR = postGWWEditR
postGWWEditR = workflowEditR WSGlobal

getSWWEditR, postSWWEditR :: SchoolId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getSWWEditR = postSWWEditR
postSWWEditR ssh = workflowEditR $ WSSchool ssh

getTWWEditR, postTWWEditR :: TermId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getTWWEditR = postTWWEditR
postTWWEditR tid = workflowEditR $ WSTerm tid

getTSWWEditR, postTSWWEditR :: TermId -> SchoolId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getTSWWEditR = postTSWWEditR
postTSWWEditR tid ssh = workflowEditR $ WSTermSchool tid ssh

workflowEditR :: RouteWorkflowScope -> CryptoFileNameWorkflowWorkflow -> Handler Html
workflowEditR = error "not implemented"
