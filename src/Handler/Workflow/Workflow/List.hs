-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}

module Handler.Workflow.Workflow.List
  ( getGlobalWorkflowWorkflowListR
  , getSchoolWorkflowWorkflowListR, getTermWorkflowWorkflowListR, getTermSchoolWorkflowWorkflowListR
  , workflowWorkflowListR
  , getGWIWorkflowsR
  , getSWIWorkflowsR, getTWIWorkflowsR, getTSWIWorkflowsR
  , workflowInstanceWorkflowsR
  , getAdminWorkflowWorkflowListR
  , getTopWorkflowWorkflowListR
  ) where

import Import hiding (Last(..), WriterT)

import Utils.Workflow
import Handler.Utils.Workflow
import Handler.Utils.Workflow.History

import qualified Database.Esqueleto.Legacy as E
import qualified Database.Esqueleto.Utils as E

import Utils.Form
import Handler.Utils hiding (columns)
import qualified Data.CaseInsensitive as CI

import qualified Data.Set as Set
import qualified Data.Map as Map

import qualified Data.Conduit.Combinators as C

import Data.Semigroup (Last(..))
import qualified Data.Monoid as Monoid (Last(..))

import Control.Monad.Trans.RWS.Strict (RWST)
import qualified Control.Monad.State.Class as State

import Control.Monad.Trans.Writer.Strict (WriterT)

import qualified Data.RFC5051 as RFC5051


data WorkflowWorkflowListFilterProj = WorkflowWorkflowListFilterProj
  { wwProjFilterWorkflowWorkflow :: Maybe [[CI Char]]
  , wwProjFilterCurrentState :: Maybe [[CI Char]]
  , wwProjFilterFinal :: Maybe Bool
  }

instance Default WorkflowWorkflowListFilterProj where
  def = WorkflowWorkflowListFilterProj
    { wwProjFilterWorkflowWorkflow = Nothing
    , wwProjFilterCurrentState = Nothing
    , wwProjFilterFinal = Nothing
    }

makeLenses_ ''WorkflowWorkflowListFilterProj


getGlobalWorkflowWorkflowListR :: WorkflowWorkflowListType -> Handler Html
getGlobalWorkflowWorkflowListR = workflowWorkflowListR WSGlobal

getSchoolWorkflowWorkflowListR :: SchoolId -> WorkflowWorkflowListType -> Handler Html
getSchoolWorkflowWorkflowListR = workflowWorkflowListR . WSSchool

getTermWorkflowWorkflowListR :: TermId -> WorkflowWorkflowListType -> Handler Html
getTermWorkflowWorkflowListR = workflowWorkflowListR . WSTerm

getTermSchoolWorkflowWorkflowListR :: TermId -> SchoolId -> WorkflowWorkflowListType -> Handler Html
getTermSchoolWorkflowWorkflowListR tid ssh = workflowWorkflowListR $ WSTermSchool tid ssh

workflowWorkflowListR :: RouteWorkflowScope
                      -> WorkflowWorkflowListType
                      -> Handler Html
workflowWorkflowListR rScope wwListType = workflowsDisabledWarning (headings ^. _1) (headings ^. _2) $ do
  now <- liftIO getCurrentTime
  scope <- runDB . maybeT notFound $ fromRouteWorkflowScope rScope
  workflowWorkflowList headings columns . runReader $ do
    workflowWorkflow <- view queryWorkflowWorkflow
    return $
            workflowWorkflow E.^. WorkflowWorkflowScope E.==. E.val (scope ^. _DBWorkflowScope)
      E.&&. matchesWorkflowWorkflowListType wwListType (E.val now) workflowWorkflow
  where
     columns = def
        { wwListColumnScope = False
        }
     headings = (MsgWorkflowWorkflowListScopeTitle rScope wwListType, MsgWorkflowWorkflowListScopeHeading rScope wwListType)


getGWIWorkflowsR :: WorkflowInstanceName -> WorkflowWorkflowListType -> Handler Html
getGWIWorkflowsR = workflowInstanceWorkflowsR WSGlobal

getSWIWorkflowsR :: SchoolId -> WorkflowInstanceName -> WorkflowWorkflowListType -> Handler Html
getSWIWorkflowsR = workflowInstanceWorkflowsR . WSSchool

getTWIWorkflowsR :: TermId -> WorkflowInstanceName -> WorkflowWorkflowListType -> Handler Html
getTWIWorkflowsR = workflowInstanceWorkflowsR . WSTerm

getTSWIWorkflowsR :: TermId -> SchoolId -> WorkflowInstanceName -> WorkflowWorkflowListType -> Handler Html
getTSWIWorkflowsR tid ssh = workflowInstanceWorkflowsR $ WSTermSchool tid ssh

workflowInstanceWorkflowsR :: RouteWorkflowScope
                           -> WorkflowInstanceName
                           -> WorkflowWorkflowListType
                           -> Handler Html
workflowInstanceWorkflowsR rScope win wwListType = workflowsDisabledWarning (MsgWorkflowWorkflowListNamedInstanceTitleDisabled rScope wwListType) (MsgWorkflowWorkflowListNamedInstanceHeadingDisabled rScope wwListType) $ do
  now <- liftIO getCurrentTime
  (scope, desc) <- runDB $ do
    scope <- maybeT notFound $ fromRouteWorkflowScope rScope
    wiId <- getKeyBy404 . UniqueWorkflowInstance win $ scope ^. _DBWorkflowScope
    desc <- selectWorkflowInstanceDescription wiId
    return (scope, desc)
  let headings = case desc of
        Nothing -> (MsgWorkflowWorkflowListInstanceTitle wwListType, MsgWorkflowWorkflowListInstanceHeading wwListType)
        Just (Entity _ WorkflowInstanceDescription{..})
          -> ( MsgWorkflowWorkflowListNamedInstanceTitle rScope workflowInstanceDescriptionTitle wwListType
             , MsgWorkflowWorkflowListNamedInstanceHeading rScope workflowInstanceDescriptionTitle wwListType
             )
  workflowWorkflowList headings columns . runReader $ do
    workflowWorkflow <- view queryWorkflowWorkflow
    return . E.exists . E.from $ \workflowInstance ->
      E.where_ $ workflowInstance E.^. WorkflowInstanceName E.==. E.val win
           E.&&. workflowInstance E.^. WorkflowInstanceScope E.==. E.val (scope ^. _DBWorkflowScope)
           E.&&. workflowWorkflow E.^. WorkflowWorkflowInstance E.==. E.just (workflowInstance E.^. WorkflowInstanceId)
           E.&&. matchesWorkflowWorkflowListType wwListType (E.val now) workflowWorkflow
  where
    columns = def
        { wwListColumnInstance = False
        , wwListColumnScope    = False
        }


getAdminWorkflowWorkflowListR :: Handler Html
getAdminWorkflowWorkflowListR = workflowWorkflowList headings def $ const E.true
  where headings = (MsgAdminWorkflowWorkflowListTitle, MsgAdminWorkflowWorkflowListHeading)

getTopWorkflowWorkflowListR :: WorkflowWorkflowListType -> Handler Html
getTopWorkflowWorkflowListR = topWorkflowWorkflowListR

topWorkflowWorkflowListR :: WorkflowWorkflowListType -> Handler Html
topWorkflowWorkflowListR wwListType = do
  now <- liftIO getCurrentTime
  workflowsDisabledWarning (headings ^. _1) (headings ^. _2) . workflowWorkflowList headings def . views queryWorkflowWorkflow $ \workflowWorkflow -> isTopWorkflowScopeSql (workflowWorkflow E.^. WorkflowWorkflowScope) E.&&. matchesWorkflowWorkflowListType wwListType (E.val now) workflowWorkflow
  where headings = (MsgWorkflowWorkflowListTopTitle wwListType, MsgWorkflowWorkflowListTopHeading wwListType)

type WorkflowWorkflowTableExpr = E.SqlExpr (Entity WorkflowWorkflow)
               `E.LeftOuterJoin` E.SqlExpr (Maybe (Entity WorkflowInstance))

queryWorkflowWorkflow :: Getter WorkflowWorkflowTableExpr (E.SqlExpr (Entity WorkflowWorkflow))
queryWorkflowWorkflow = to $(E.sqlLOJproj 2 1)

queryWorkflowInstance :: Getter WorkflowWorkflowTableExpr (E.SqlExpr (Maybe (Entity WorkflowInstance)))
queryWorkflowInstance = to $(E.sqlLOJproj 2 2)

type WorkflowWorkflowData = DBRow
  ( CryptoFileNameWorkflowWorkflow
  , Maybe RouteWorkflowScope
  , Entity WorkflowWorkflow
  , Maybe (Entity WorkflowInstance)
  , Maybe (Entity WorkflowInstanceDescription)
  , Maybe WorkflowWorkflowActionData
  , [Entity User]
  )
-- ^ @Maybe `WorkflowWorkflowActionData`@ corresponds to last action

type WorkflowWorkflowActionData = ( Maybe Text
                                  , UTCTime
                                  , WorkflowHistoryItemUser
                                  , Maybe Icon
                                  )

data JsonWorkflowWorkflow = JsonWorkflowWorkflow
  { jwwScope          :: Maybe RouteWorkflowScope
  , jwwInstance       :: Maybe JsonWorkflowInstance
  , jwwLastAction     :: Maybe JsonWorkflowAction
  , jwwPayload        :: Map WorkflowPayloadLabel JsonWorkflowPayload
  } deriving (Generic)

data JsonWorkflowAction = JsonWorkflowAction
  { jwaIx   :: CryptoUUIDWorkflowStateIndex
  , jwaTo   :: Maybe WorkflowGraphNodeLabel
  , jwaUser :: Maybe JsonWorkflowUser
  , jwaTime :: UTCTime
  } deriving (Generic)

data JsonWorkflowInstance = JsonWorkflowInstance
  { jwiScope :: RouteWorkflowScope
  , jwiName  :: WorkflowInstanceName
  } deriving (Generic)

data JsonWorkflowPayload = JsonWorkflowPayload
  { jwpPayload :: [WorkflowFieldPayloadW Void JsonWorkflowUser]
  , jwpHasFiles :: Bool
  } deriving (Generic)

data JsonWorkflowUser
  = JsonWorkflowUserUser
    { jwuDisplayName   :: UserDisplayName
    , jwuMatriculation :: Maybe UserMatriculation
    , jwuDisplayEmail  :: UserEmail
    }
  | JsonWorkflowUserAnonymous
  | JsonWorkflowUserHidden
  | JsonWorkflowUserGone
  deriving (Generic)

resultWorkflowWorkflowId :: Lens' WorkflowWorkflowData CryptoFileNameWorkflowWorkflow
resultWorkflowWorkflowId = _dbrOutput . _1

resultRouteScope :: Lens' WorkflowWorkflowData (Maybe RouteWorkflowScope)
resultRouteScope = _dbrOutput . _2

resultWorkflowWorkflow :: Lens' WorkflowWorkflowData (Entity WorkflowWorkflow)
resultWorkflowWorkflow = _dbrOutput . _3

resultWorkflowInstance :: Lens' WorkflowWorkflowData (Maybe (Entity WorkflowInstance))
resultWorkflowInstance = _dbrOutput . _4

resultWorkflowInstanceDescription :: Lens' WorkflowWorkflowData (Maybe (Entity WorkflowInstanceDescription))
resultWorkflowInstanceDescription = _dbrOutput . _5

resultWorkflowInstanceTitle :: Getter WorkflowWorkflowData Text
resultWorkflowInstanceTitle = to $ \x -> case x ^? resultWorkflowInstanceDescription . _Just . _entityVal . _workflowInstanceDescriptionTitle of
  Just dTitle -> dTitle
  Nothing -> x ^. resultWorkflowInstance . _Just . _entityVal . _workflowInstanceName . to CI.original

resultLastAction :: Lens' WorkflowWorkflowData (Maybe WorkflowWorkflowActionData)
resultLastAction = _dbrOutput . _6

resultPersons :: Traversal' WorkflowWorkflowData (Entity User)
resultPersons = _dbrOutput . _7 . traverse

actionTo :: Lens' WorkflowWorkflowActionData (Maybe Text)
actionTo = _1

actionTime :: Lens' WorkflowWorkflowActionData UTCTime
actionTime = _2

actionActor :: Lens' WorkflowWorkflowActionData WorkflowHistoryItemUser
actionActor = _3

actionFinal :: Lens' WorkflowWorkflowActionData (Maybe Icon)
actionFinal = _4

data WorkflowWorkflowListColumns = WWListColumns
  { wwListColumnInstance :: Bool
  , wwListColumnScope    :: Bool
  }

instance Default WorkflowWorkflowListColumns where
  def = WWListColumns
    { wwListColumnInstance = True
    , wwListColumnScope    = True
    }

workflowWorkflowList :: ( RenderMessage UniWorX title, RenderMessage UniWorX heading)
                     => (title, heading)
                     -> WorkflowWorkflowListColumns
                     -> (WorkflowWorkflowTableExpr -> E.SqlExpr (E.Value Bool))
                     -> Handler Html
workflowWorkflowList (title, heading) WWListColumns{..} sqlPred = do
  mAuthId <- maybeAuthId

  workflowTable <- runDB $ do
    relevantGraphs <- fmap (map $(E.unValueN 6)) . E.select . E.from . runReaderT $ do
      workflowWorkflow <- view queryWorkflowWorkflow
      workflowInstance <- view queryWorkflowInstance
      let distinct =
            [ E.asc $ workflowInstance E.?. WorkflowInstanceId, E.asc $ workflowWorkflow E.^. WorkflowWorkflowGraph
            , E.asc . E.maybe (E.just $ workflowWorkflow E.^. WorkflowWorkflowId) (const E.nothing) $ workflowInstance E.?. WorkflowInstanceId
            ]
      hoist (E.distinctOnOrderBy distinct) $ do
        lift . E.on $ workflowWorkflow E.^. WorkflowWorkflowInstance E.==. workflowInstance E.?. WorkflowInstanceId
        lift <=< asks $ E.where_ . sqlPred
        return ( workflowInstance E.?. WorkflowInstanceId
               , workflowWorkflow E.^. WorkflowWorkflowId
               , workflowInstance E.?. WorkflowInstanceScope
               , workflowWorkflow E.^. WorkflowWorkflowScope
               , workflowInstance E.?. WorkflowInstanceName
               , workflowWorkflow E.^. WorkflowWorkflowGraph
               )
    graphAuths <- fmap Map.fromList . forM relevantGraphs $ \(wiId, wwId, wiScope, wwScope, win, wwGraph) -> case (wiId, wiScope, win) of
      (Just wiId', Just wiScope', Just win') -> fmap (Right (wiId', wwGraph), ) . maybeT (return $ const E.false) $ do
        rScope <- toRouteWorkflowScope $ _DBWorkflowScope # wiScope'
        let route = _WorkflowScopeRoute # (rScope, WorkflowInstanceR win' $ WIWorkflowsR WorkflowWorkflowListAll)
        lift $ workflowAuthWhere'' (Left wwGraph) (Set.singleton WorkflowGraphAuthViewWorkflowWorkflow) route False
      _other -> fmap (Left wwId, ) . maybeT (return $ const E.false) $ do
        rScope <- toRouteWorkflowScope $ _DBWorkflowScope # wwScope
        cID <- encrypt wwId
        let route = _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
        lift $ workflowAuthWhere'' (Left wwGraph) (Set.singleton WorkflowGraphAuthViewWorkflowWorkflow) route False

    let
      workflowWorkflowDBTable = DBTable{..}
        where
          dbtSQLQuery = runReaderT $ do
            workflowWorkflow <- view queryWorkflowWorkflow
            workflowInstance <- view queryWorkflowInstance
            lift . E.on $ workflowWorkflow E.^. WorkflowWorkflowInstance E.==. workflowInstance E.?. WorkflowInstanceId
            lift <=< asks $ E.where_ . sqlPred
            -- Assumption is that only very few `WorkflowWorkflow`s ever have no `WorkflowInstance`
            lift . E.where_ $ if
              | Map.null graphAuths -> E.false
              | otherwise -> E.case_
                [ case authPred of
                    Right (wiId, wwGraph) ->
                      (       E.maybe E.false (E.==. E.val wiId) (workflowInstance E.?. WorkflowInstanceId)
                        E.&&. workflowWorkflow E.^. WorkflowWorkflowGraph E.==. E.val wwGraph
                      , $$(E.annSqlExpr) . authWhere . WorkflowAuthWhereWorkflow $ workflowWorkflow E.^. WorkflowWorkflowId
                      )
                    Left wwId ->
                      ( workflowWorkflow E.^. WorkflowWorkflowId E.==. E.val wwId
                      , $$(E.annSqlExpr) . authWhere . WorkflowAuthWhereWorkflow $ workflowWorkflow E.^. WorkflowWorkflowId
                      )
                | (authPred, authWhere) <- Map.toList graphAuths
                ]
                E.false
            return (workflowWorkflow, workflowInstance)
          dbtRowKey = views queryWorkflowWorkflow (E.^. WorkflowWorkflowId)
          dbtProj = (views _dbtProjRow . set _dbrOutput) =<< do
            ww@(Entity wwId WorkflowWorkflow{..}) <- view $ _dbtProjRow . _dbrOutput . _1
            mwi <- view $ _dbtProjRow . _dbrOutput . _2

            cID <- encrypt wwId
            forMM_ (view $ _dbtProjFilter . _wwProjFilterWorkflowWorkflow) $ \criteria ->
              let haystack = map CI.mk . unpack $ toPathPiece cID
               in guard $ any (`isInfixOf` haystack) criteria

            rScope <- lift . lift . runMaybeT . toRouteWorkflowScope $ _DBWorkflowScope # workflowWorkflowScope

            wiDesc <- lift . lift . $cachedHereBinary (entityKey <$> mwi) . runMaybeT $ do
              Entity wiId _ <- hoistMaybe mwi
              MaybeT $ selectWorkflowInstanceDescription wiId

            WorkflowGraph{..} <- lift . lift . getSharedIdWorkflowGraph $ ww ^. _entityVal . _workflowWorkflowGraph

            let
              sourceWorkflowWorkflowActionInfos' :: ConduitT () (WorkflowStateIndex, IdWorkflowWorkflowActionInfoAuthorized) DB ()
              sourceWorkflowWorkflowActionInfos' = sourceWorkflowWorkflowActionInfosAuthorized onlyPersons (const id) ww
                where onlyPersons (_ `E.LeftOuterJoin` workflowWorkflowPayload) = (E.&&.) $
                        E.maybe E.true ((E.==. E.val (toPathPiece WFPUser')) . (E.->>. "tag")) (workflowWorkflowPayload E.?. WorkflowWorkflowPayloadPayload)
              toRes :: forall m. (MonadHandler m, HandlerSite m ~ UniWorX)
                    => ( Monoid.Last (ReaderT SqlBackend (HandlerFor UniWorX) WorkflowWorkflowActionData)
                       , (Set UserId, Map WorkflowPayloadLabel (Set UserId))
                       )
                    -> ReaderT SqlBackend m (Maybe WorkflowWorkflowActionData, [Entity User])
              toRes (Monoid.Last mActionData, (actors, personPayloads)) = hoist liftHandler $ (,)
                <$> sequenceA mActionData
                <*> mapMaybeM (MaybeT . getEntity) (toList $ actors <> fold personPayloads)
              accActionInfos :: (WorkflowStateIndex, IdWorkflowWorkflowActionInfoAuthorized)
                             -> WriterT ( Monoid.Last (ReaderT SqlBackend (HandlerFor UniWorX) WorkflowWorkflowActionData)
                                        , (Set UserId, Map WorkflowPayloadLabel (Set UserId))
                                        ) (ReaderT SqlBackend (HandlerFor UniWorX)) ()
              accActionInfos (_, WorkflowWorkflowActionInfoAuthorized{..}) = do
                let
                  mTo = Map.lookup (wwaiaTo ^. _2) wgNodes
                  actName = runMaybeT $ do
                    WorkflowNodeView{..} <- hoistMaybe $ mTo >>= wgnViewers
                    guard $ wwaiaTo ^. _1
                    selectLanguageI18n wnvDisplayLabel
                  actUser = for (wwaiaUser ^. _2) $ \uid -> if
                    | Just uid == mAuthId -> return WHIASelf
                    | otherwise -> maybeT (return WHIAHidden) $ do
                        guard $ wwaiaUser ^. _1
                        fmap (maybe WHIAGone WHIAOther) . lift $ getEntity uid
                  actFinal = wgnFinal =<< mTo
                scribe (simple . _1) . Monoid.Last . Just $ do
                  actName' <- actName
                  actUser' <- actUser
                  return ( actName'
                         , wwaiaTime
                         , actUser'
                         , actName' *> actFinal
                         )

                scribeM (simple . _2 . _1) . maybeT (pure mempty) $ do
                  guard $ wwaiaUser ^. _1
                  hoistMaybe . fmap Set.singleton $ wwaiaUser ^? _2 . _wauUser

                iforM_ wwaiaPayload $ \payloadLbl (_, payload) ->
                  scribe (_2 . _2 . at payloadLbl) . Just $ setOf (folded . _WorkflowFieldPayloadW . _WorkflowFieldPayload) payload
            (lastAct, persons) <- lift . lift $ toRes =<< runConduit (sourceWorkflowWorkflowActionInfos' .| execWriterC (C.mapM_ accActionInfos))

            whenIsJust (lastAct ^? _Just . actionTo . _Just) $ \lastActTo ->
              forMM_ (view $ _dbtProjFilter . _wwProjFilterCurrentState) $ \criteria ->
                let haystack = map CI.mk $ unpack lastActTo
                 in guard $ any (`isInfixOf` haystack) criteria
            whenIsJust (lastAct ^? _Just . actionFinal . to (is _Just)) $ \isFinal ->
              forMM_ (view $ _dbtProjFilter . _wwProjFilterFinal) $ \criterion ->
                guard $ criterion == isFinal

            return (cID, rScope, ww, mwi, wiDesc, lastAct, persons)
          dbtColonnade :: Colonnade Sortable _ _
          dbtColonnade = mconcat -- TODO: columns
            [ sortable (Just "workflow-workflow") (i18nCell MsgWorkflowWorkflowListNumber) . (addCellClass ("cryptoid" :: Text) .) . anchorWorkflowWorkflow . views resultWorkflowWorkflowId $ toWidget . (toPathPiece :: CryptoFileNameWorkflowWorkflow -> Text)
            , guardMonoid wwListColumnScope . sortable (Just "scope") (i18nCell MsgWorkflowWorkflowListScope) $ \x -> foldMap (\t -> anchorWorkflowScope (const $ i18n t :: _ -> Widget) x) $ view resultRouteScope x
            , guardMonoid wwListColumnInstance . sortable (Just "instance") (i18nCell MsgWorkflowWorkflowListInstance) $ \x -> foldMap (\t -> anchorWorkflowInstance (const t) x) $ preview resultWorkflowInstanceTitle x
            , sortable Nothing (i18nCell MsgWorkflowWorkflowListPersons) $ \x ->
                let lCell = flip listCell (uncurry userCell) . sortBy personCmp $ x ^.. resultPersons . _entityVal . to ((,) <$> userDisplayName <*> userSurname)
                 in lCell & cellAttrs <>~ [("class", "list--inline list--comma-separated")]
            , sortable (Just "current-state") (i18nCell MsgWorkflowWorkflowListCurrentState) $ fromMaybe mempty . previews (resultLastAction . _Just . $(multifocusL 2) actionTo actionFinal) stateCell
            , sortable (Just "last-action-time") (i18nCell MsgWorkflowWorkflowListLastActionTime) $ fromMaybe mempty . previews (resultLastAction . _Just . actionTime) dateTimeCell
            , sortable (Just "last-action-user") (i18nCell MsgWorkflowWorkflowListLastActionUser) $ fromMaybe mempty . previews (resultLastAction . _Just . actionActor) actorCell
            ]
            where
              personCmp = (RFC5051.compareUnicode `on` (pack . toListOf (_2 . to (unpack . CI.foldCase) . folded)))
                       <> (RFC5051.compareUnicode `on` (pack . toListOf (_1 . to (unpack . CI.foldCase) . folded)))

              stateCell = \case
                (Nothing, _) -> i18nCell MsgWorkflowWorkflowWorkflowHistoryStateHidden & addCellClass ("explanation" :: Text)
                (Just n, Nothing) -> textCell n
                (Just n, Just fin) -> cell [whamlet|#{icon fin}&nbsp;#{n}|]
              actorCell = \case
                WorkflowActionAutomatic -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserAutomatic & addCellClass ("explanation" :: Text)
                WorkflowActionUser WHIASelf -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserSelf & addCellClass ("explanation" :: Text)
                WorkflowActionUser WHIAGone -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserGone & addCellClass ("explanation" :: Text)
                WorkflowActionUser WHIAHidden -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserHidden & addCellClass ("explanation" :: Text)
                WorkflowActionUnauthenticated -> i18nCell MsgWorkflowWorkflowWorkflowHistoryUserNotLoggedIn & addCellClass ("explanation" :: Text)
                WorkflowActionUser (WHIAOther (Entity _ User{..})) -> userCell userDisplayName userSurname

              anchorWorkflowWorkflow :: (WorkflowWorkflowData -> Widget) -> _
              anchorWorkflowWorkflow f x = cell $ do
                mLink <- runMaybeT . flip runReaderT x $ do
                  cID <- view resultWorkflowWorkflowId
                  rScope <- hoistMaybe =<< view resultRouteScope
                  return $ _WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)
                case mLink of
                  Just route -> do
                    linkUrl <- toTextUrl route
                    -- No authorization check for performance; sufficient auth already happens in sql
                    let widget = f x
                    $(widgetFile "table/cell/link")
                  Nothing -> f x
              anchorWorkflowScope f = maybeAnchorCellCM <$> mkCacheIdent <*> mkLink <*> f
                where mkLink = runReaderT $ do
                        rScope <- hoistMaybe =<< view resultRouteScope
                        return $ _WorkflowScopeRoute # (rScope, WorkflowWorkflowListR WorkflowWorkflowListActive)
                      mkCacheIdent = ($cacheIdentHere, ) . view resultRouteScope
              anchorWorkflowInstance f = maybeAnchorCellCM <$> mkCacheIdent <*> mkLink <*> f
                where mkLink = runReaderT $ do
                        rScope <- hoistMaybe =<< view resultRouteScope
                        win <- hoistMaybe =<< preview (resultWorkflowInstance . _Just . _entityVal . _workflowInstanceName)
                        return $ _WorkflowScopeRoute # (rScope, WorkflowInstanceR win $ WIWorkflowsR WorkflowWorkflowListActive)
                      mkCacheIdent = runReader $ do
                        rScope <- view resultRouteScope
                        win <- preview (resultWorkflowInstance . _Just . _entityVal . _workflowInstanceName)
                        return ($cacheIdentHere, rScope, win)
          dbtSorting = mconcat
            [ singletonMap "workflow-workflow" . SortProjected . comparing $ view resultWorkflowWorkflowId
            , singletonMap "scope" . SortProjected . comparing $ view resultRouteScope
            , singletonMap "instance" . SortProjected . comparing $ preview resultWorkflowInstanceTitle
            , singletonMap "current-state" . SortProjected . comparing . preview $ resultLastAction . _Just . actionTo . _Just
            , singletonMap "last-action-time" . SortProjected . comparing . preview $ resultLastAction . _Just . actionTime
            , singletonMap "last-action-user" . SortProjected . comparing . preview $ resultLastAction . _Just . actionActor . to (over (mapped . mapped) $ \(Entity _ User{..}) -> (userSurname, userDisplayName))
            , singletonMap "final" . SortProjected . comparing $ \x -> guardOnM (has (resultLastAction . _Just . actionTo . _Just) x) (x ^? resultLastAction . _Just . actionFinal . _Just)
            ]
          dbtFilter = mconcat
            [ singletonMap "workflow-workflow" . FilterProjected $ \(criteria :: Set Text) ->
                let criteria' = map CI.mk . unpack <$> Set.toList criteria
                 in _wwProjFilterWorkflowWorkflow ?~ criteria'
            , singletonMap "current-state" . FilterProjected $ \(criteria :: Set Text) -> -- TODO
                let criteria' = map CI.mk . unpack <$> Set.toList criteria
                 in _wwProjFilterCurrentState ?~ criteria'
            , singletonMap "final" . FilterProjected $ \(criterion :: Monoid.Last Bool) -> case Monoid.getLast criterion of -- TODO
                Nothing -> id
                Just needle -> _wwProjFilterFinal ?~ needle
            ]
            -- [ singletonMap "workflow-workflow" . FilterProjected $ \x (criteria :: Set Text) ->
            --     let cid = map CI.mk . unpack . toPathPiece $ x ^. resultWorkflowWorkflowId
            --         criteria' = map CI.mk . unpack <$> Set.toList criteria
            --      in any (`isInfixOf` cid) criteria'
            -- ,

            -- , singletonMap "may-access" . FilterPreProjected $ \(x :: DBRow (Entity WorkflowWorkflow, Maybe (Entity WorkflowInstance))) (Any b) -> fmap (== b) . maybeT (return False) $ do
            --     let Entity wwId WorkflowWorkflow{..} = x ^. _dbrOutput . _1
            --     cID <- encrypt wwId
            --     rScope <- toRouteWorkflowScope $ _DBWorkflowScope # workflowWorkflowScope
            --     lift $ is _Authorized <$> evalAccess (_WorkflowScopeRoute # (rScope, WorkflowWorkflowR cID WWWorkflowR)) False :: MaybeT (YesodDB UniWorX) Bool
            -- , singletonMap "current-state" . FilterProjected $ \x (criteria :: Set Text) ->
            --     let criteria' = map CI.mk . unpack <$> Set.toList criteria
            --      in maybe False (\cSt -> any (`isInfixOf` cSt) criteria') $ x ^? resultLastAction . _Just . actionTo . _Just . to (map CI.mk . unpack)
            -- , singletonMap "final" . FilterProjected $ \x (criterion :: Monoid.Last Bool) -> case Monoid.getLast criterion of
            --     Nothing -> True
            --     Just needle -> let val = has (resultLastAction . _Just . actionTo . _Just) x
            --                           && has (resultLastAction . _Just . actionFinal . _Just) x
            --                     in needle == val
            -- ]
          dbtFilterUI = mconcat
            [ flip (prismAForm $ singletonFilter "workflow-workflow") $ aopt textField (fslI MsgWorkflowWorkflowListNumber)
            , flip (prismAForm $ singletonFilter "current-state") $ aopt textField (fslI MsgWorkflowWorkflowListCurrentState)

            , flip (prismAForm (singletonFilter "final" . maybePrism _PathPiece)) $ aopt (boolField . Just $ SomeMessage MsgBoolIrrelevant) (fslI MsgWorkflowWorkflowListIsFinal)
            ]
          dbtStyle = def { dbsFilterLayout = defaultDBSFilterLayout }
          dbtParams = def
          dbtIdent :: Text
          dbtIdent = "workflow-workflows"
          dbtCsvEncode = noCsvEncode
          dbtCsvDecode = Nothing
          dbtExtraReps = [ DBTExtraRep $ toPrettyJSON <$> repWorkflowWorkflowJson, DBTExtraRep $ toYAML <$> repWorkflowWorkflowJson ]

          repWorkflowWorkflowJson :: ConduitT (E.Value WorkflowWorkflowId, WorkflowWorkflowData) Void DB (Map CryptoFileNameWorkflowWorkflow JsonWorkflowWorkflow)
          repWorkflowWorkflowJson = C.foldMapM $ \(E.Value wwId, res) -> do
            cID <- encrypt wwId
            Map.singleton cID <$> do
              let jwwScope = guardOnM wwListColumnScope $ res ^. resultRouteScope
              jwwInstance <- fmap join . for (guardOnM wwListColumnInstance $ res ^. resultWorkflowInstance) $ \(Entity _ WorkflowInstance{..}) -> runMaybeT $ do
                jwiScope <- toRouteWorkflowScope $ _DBWorkflowScope # workflowInstanceScope
                let jwiName = workflowInstanceName
                return JsonWorkflowInstance{..}

              (fmap getLast -> wState) <-
                let go :: forall m.
                          ( MonadHandler m
                          , HandlerSite m ~ UniWorX
                          , MonadCatch m
                          , MonadUnliftIO m
                          )
                       => (WorkflowStateIndex, JSONIdWorkflowWorkflowActionInfoAuthorized)
                       -> RWST
                            ()
                            (Maybe (Last (CryptoUUIDWorkflowStateIndex, Maybe WorkflowGraphNodeLabel, Maybe JsonWorkflowUser, UTCTime, Map WorkflowPayloadLabel JsonWorkflowPayload)))
                            (Maybe JSONIdWorkflowRestrictionState)
                            (SqlPersistT m)
                            ()
                    go (stIx, wwaia@WorkflowWorkflowActionInfoAuthorized{..}) = maybeT_ $ do
                      currentPayload <- State.state $ \prevSt ->
                        let currentRestr = appendWorkflowRestrictionState prevSt $ wwaia ^. _wwaiaUnauthorized
                         in ( wwrCurrentPayload currentRestr
                            , Just currentRestr
                            )

                      stCID <- encryptWorkflowStateIndex wwId stIx

                      aUser <- case wwaiaUser ^. _2 of
                        WorkflowActionAutomatic -> return Nothing
                        WorkflowActionUnauthenticated -> return $ Just JsonWorkflowUserAnonymous
                        WorkflowActionUser{..} -> fmap Just . lift . maybeT (return JsonWorkflowUserHidden) $ do
                          guard $ wwaiaUser ^. _1
                          lift . lift $ getUser wauUser

                      payload <- forM currentPayload $ \(otoList -> payloads) -> fmap (uncurry JsonWorkflowPayload . over _2 getAny) . execWriterT @_ @(_, Any) . forM_ payloads $ \case
                          WorkflowFieldPayloadW (WFPText   t  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPText t)
                          WorkflowFieldPayloadW (WFPNumber n  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPNumber n)
                          WorkflowFieldPayloadW (WFPBool   b  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPBool b)
                          WorkflowFieldPayloadW (WFPDay    d  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPDay d)
                          WorkflowFieldPayloadW (WFPTime   t  ) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPTime t)
                          WorkflowFieldPayloadW (WFPDateTime t) -> tell . (, mempty) . pure $ WorkflowFieldPayloadW (WFPDateTime t)
                          WorkflowFieldPayloadW (WFPFile   f  ) -> tell (mempty, f)
                          WorkflowFieldPayloadW (WFPUser   uid) -> tell . (, mempty) . pure . review (_WorkflowFieldPayloadW . _WorkflowFieldPayload) =<< lift (lift . lift $ getUser uid)

                      nTo <- runMaybeT $ do
                        guard $ wwaiaTo ^. _1
                        return $ wwaiaTo ^. _2

                      tell . Just $ Last (stCID, nTo, aUser, wwaiaTime, payload)

                    getUser uid = $cachedHereBinary uid $ getEntity uid <&> \case
                      Just (Entity _ User{..}) -> JsonWorkflowUserUser
                        { jwuDisplayName   = userDisplayName
                        , jwuMatriculation = userMatrikelnummer
                        , jwuDisplayEmail  = userDisplayEmail
                        }
                      Nothing -> JsonWorkflowUserGone

                    sourceWorkflowWorkflowActionInfos' = sourceWorkflowWorkflowActionInfosAuthorizedForJSON (const id) (const id) $ res ^. resultWorkflowWorkflow
                 in fmap (view _2) . runConduit $ sourceWorkflowWorkflowActionInfos' .| execRWSC () Nothing (C.mapM_ go)

              let jwwLastAction = wState <&> \(jwaIx, jwaTo, jwaUser, jwaTime, _) -> JsonWorkflowAction{..}
                  jwwPayload = wState ^. _Just . _5

              return JsonWorkflowWorkflow{..}
      workflowWorkflowDBTableValidator = def
        & defaultSorting defSort
      defSort | wwListColumnInstance = SortAscBy "instance" : defSort'
              | otherwise = defSort'
        where defSort' = [SortAscBy "final", SortAscBy "current-state", SortDescBy "last-action-time"]
     in dbTableDB' workflowWorkflowDBTableValidator workflowWorkflowDBTable

  siteLayoutMsg heading $ do
    setTitleI title
    $(widgetFile "workflows/workflow-list")

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''JsonWorkflowWorkflow

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''JsonWorkflowAction

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''JsonWorkflowInstance

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''JsonWorkflowPayload

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 3
  , fieldLabelModifier = camelToPathPiece' 1
  } ''JsonWorkflowUser
