-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Workflow.Delete
  ( getGWWDeleteR, postGWWDeleteR
  , getSWWDeleteR, postSWWDeleteR
  , getTWWDeleteR, postTWWDeleteR
  , getTSWWDeleteR, postTSWWDeleteR
  , workflowDeleteR
  ) where

import Import

import Utils.Workflow


getGWWDeleteR, postGWWDeleteR :: CryptoFileNameWorkflowWorkflow -> Handler Html
getGWWDeleteR = postGWWDeleteR
postGWWDeleteR = workflowDeleteR WSGlobal

getSWWDeleteR, postSWWDeleteR :: SchoolId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getSWWDeleteR = postSWWDeleteR
postSWWDeleteR ssh = workflowDeleteR $ WSSchool ssh

getTWWDeleteR, postTWWDeleteR :: TermId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getTWWDeleteR = postTWWDeleteR
postTWWDeleteR tid = workflowDeleteR $ WSTerm tid

getTSWWDeleteR, postTSWWDeleteR :: TermId -> SchoolId -> CryptoFileNameWorkflowWorkflow -> Handler Html
getTSWWDeleteR = postTSWWDeleteR
postTSWWDeleteR tid ssh = workflowDeleteR $ WSTermSchool tid ssh

workflowDeleteR :: RouteWorkflowScope -> CryptoFileNameWorkflowWorkflow -> Handler Html
workflowDeleteR = error "not implemented"
