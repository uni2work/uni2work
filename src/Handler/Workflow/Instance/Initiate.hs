-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Instance.Initiate
  ( getGWIInitiateR, postGWIInitiateR
  , getSWIInitiateR, postSWIInitiateR
  , getTWIInitiateR, postTWIInitiateR
  , getTSWIInitiateR, postTSWIInitiateR
  , workflowInstanceInitiateR
  ) where

import Import

import Utils.Form
import Utils.Workflow

import Handler.Utils
import Handler.Utils.Workflow

import qualified Data.CaseInsensitive as CI
import qualified Data.List.NonEmpty as NonEmpty


getGWIInitiateR, postGWIInitiateR :: WorkflowInstanceName -> Handler Html
getGWIInitiateR = postGWIInitiateR
postGWIInitiateR = workflowInstanceInitiateR WSGlobal

getSWIInitiateR, postSWIInitiateR :: SchoolId -> WorkflowInstanceName -> Handler Html
getSWIInitiateR = postSWIInitiateR
postSWIInitiateR ssh = workflowInstanceInitiateR $ WSSchool ssh

getTWIInitiateR, postTWIInitiateR :: TermId -> WorkflowInstanceName -> Handler Html
getTWIInitiateR = postTWIInitiateR
postTWIInitiateR tid = workflowInstanceInitiateR $ WSTerm tid

getTSWIInitiateR, postTSWIInitiateR :: TermId -> SchoolId -> WorkflowInstanceName -> Handler Html
getTSWIInitiateR = postTSWIInitiateR
postTSWIInitiateR tid ssh = workflowInstanceInitiateR $ WSTermSchool tid ssh

workflowInstanceInitiateR :: RouteWorkflowScope -> WorkflowInstanceName -> Handler Html
workflowInstanceInitiateR rScope win = workflowsDisabledWarning MsgWorkflowInstanceInitiateTitleDisabled MsgWorkflowInstanceInitiateHeadingDisabled $ do
  (WorkflowInstance{..}, ((edgeAct, edgeView'), edgeEnc), mDesc) <- runDB $ do
    scope <- maybeT notFound $ fromRouteWorkflowScope rScope
    Entity wiId wi@WorkflowInstance{..} <- getBy404 . UniqueWorkflowInstance win $ scope ^. _DBWorkflowScope
    edgeForm <- maybeT notFound . MaybeT $ workflowEdgeForm (Left wiId) Nothing

    descs <- selectList [ WorkflowInstanceDescriptionInstance ==. wiId ] []
    mDesc <- runMaybeT $ do
      langs <- hoistMaybe . nonEmpty $ map (workflowInstanceDescriptionLanguage . entityVal) descs
      lang <- selectLanguage langs
      hoistMaybe . preview _head $ do
        Entity _ desc@WorkflowInstanceDescription{..} <- descs
        guard $ workflowInstanceDescriptionLanguage == lang
        return desc

    ((edgeRes, edgeView), edgeEnc) <- runFormPost $ renderAForm FormStandard edgeForm
    edgeRes' <- runMaybeT $ formResultMaybeT' edgeRes
                        <|> assertStdMethod POST *> MaybeT (acceptJsonWorkflowWorkflowEdgeForm $ Left wiId)

    edgeAct <- for edgeRes' $ \edgeRes'' -> do
      wwId <- insert WorkflowWorkflow
        { workflowWorkflowInstance = Just wiId
        , workflowWorkflowScope = workflowInstanceScope
        , workflowWorkflowGraph = workflowInstanceGraph
        , workflowWorkflowArchived = Nothing
        }

      wGraph <- getSharedIdWorkflowGraph workflowInstanceGraph
      runConduit $ followEdge wGraph edgeRes'' Nothing .| sinkWorkflowWorkflowActionInfos wwId

      return $ do
        memcachedByInvalidate (AuthCacheWorkflowInstanceRelevantGraphs win rScope) $ Proxy @(Set SharedWorkflowGraphId)
        memcachedByInvalidate (AuthCacheWorkflowScopeRelevantGraphs rScope) $ Proxy @(Set SharedWorkflowGraphId)
        when (isTopWorkflowScope rScope) $
          memcachedByInvalidate AuthCacheTopWorkflowScopesRelevantGraphs $ Proxy @(Set (SharedWorkflowGraphId, (DBWorkflowScope, RouteWorkflowScope)))

        addMessageI Success MsgWorkflowInstanceInitiateSuccess

        cID <- encrypt wwId
        redirectAlternatives $ NonEmpty.fromList
          [ _WorkflowScopeRoute # ( rScope, WorkflowWorkflowR cID WWWorkflowR )
          , _WorkflowScopeRoute # ( rScope, WorkflowInstanceR workflowInstanceName $ WIWorkflowsR WorkflowWorkflowListActive )
          , _WorkflowScopeRoute # ( rScope, WorkflowInstanceListR )
          ]

    return (wi, ((edgeAct, edgeView), edgeEnc), mDesc)

  sequence_ edgeAct

  (heading, title) <- case rScope of
    WSGlobal -> return (MsgGlobalWorkflowInstanceInitiateHeading $ maybe (CI.original workflowInstanceName) workflowInstanceDescriptionTitle mDesc, MsgGlobalWorkflowInstanceInitiateTitle)
    WSSchool ssh -> return (MsgSchoolWorkflowInstanceInitiateHeading ssh $ maybe (CI.original workflowInstanceName) workflowInstanceDescriptionTitle mDesc, MsgSchoolWorkflowInstanceInitiateTitle ssh)
    WSTerm tid -> return (MsgTermWorkflowInstanceInitiateHeading tid $ maybe (CI.original workflowInstanceName) workflowInstanceDescriptionTitle mDesc, MsgTermWorkflowInstanceInitiateTitle tid)
    WSTermSchool tid ssh -> return (MsgTermSchoolWorkflowInstanceInitiateHeading tid ssh $ maybe (CI.original workflowInstanceName) workflowInstanceDescriptionTitle mDesc, MsgTermSchoolWorkflowInstanceInitiateTitle tid ssh)
    _other -> error "not implemented"

  siteLayoutMsg heading $ do
    setTitleI title
    let edgeView = wrapForm edgeView' FormSettings
          { formMethod = POST
          , formAction = Just . SomeRoute $ _WorkflowScopeRoute # (rScope, WorkflowInstanceR workflowInstanceName WIInitiateR)
          , formEncoding = edgeEnc
          , formAttrs = []
          , formSubmit = FormSubmit
          , formAnchor = Nothing :: Maybe Text
          }
    $(widgetFile "workflows/instance-initiate")
