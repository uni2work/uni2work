-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Instance.New
  ( getAdminWorkflowInstanceNewR, postAdminWorkflowInstanceNewR
  , adminWorkflowInstanceNewR
  , getGlobalWorkflowInstanceNewR, postGlobalWorkflowInstanceNewR
  , getSchoolWorkflowInstanceNewR, postSchoolWorkflowInstanceNewR
  , getTermWorkflowInstanceNewR, postTermWorkflowInstanceNewR
  , getTermSchoolWorkflowInstanceNewR, postTermSchoolWorkflowInstanceNewR
  , workflowInstanceNewR
  ) where

import Import
import Handler.Utils
import Handler.Utils.Workflow.Form
import Utils.Workflow

import Handler.Workflow.Instance.Form

getAdminWorkflowInstanceNewR, postAdminWorkflowInstanceNewR :: Handler Html
getAdminWorkflowInstanceNewR = postAdminWorkflowInstanceNewR
postAdminWorkflowInstanceNewR = adminWorkflowInstanceNewR Nothing

adminWorkflowInstanceNewR :: Maybe WorkflowDefinitionId -> Handler Html
adminWorkflowInstanceNewR wdId = do
  cRoute <- getCurrentRoute
  (((_, instForm), instEncoding), act) <- runDB $ do
    form@((instRes, _), _) <- runFormPost $ workflowInstanceForm wdId Nothing

    act <- formResultMaybe instRes $ \WorkflowInstanceForm{..} -> do
      wifGraph' <- fromWorkflowGraphForm wifGraph
      workflowInstanceGraph <- insertSharedWorkflowGraph wifGraph'
      let wifScope' = wifScope
            & over _wisTerm   unTermKey
            & over _wisSchool unSchoolKey
            & over _wisCourse (view _SqlKey)
      instId <- insertUnique WorkflowInstance
        { workflowInstanceDefinition = wdId
        , workflowInstanceGraph
        , workflowInstanceScope = wifScope'
        , workflowInstanceName = wifName
        , workflowInstanceCategory = wifCategory
        }

      for_ instId $ \instId' -> do
        iforM_ wifDescriptions $ \widLang (widTitle, widDesc) ->
          insert WorkflowInstanceDescription
            { workflowInstanceDescriptionInstance = instId'
            , workflowInstanceDescriptionLanguage = widLang
            , workflowInstanceDescriptionTitle = widTitle
            , workflowInstanceDescriptionDescription = widDesc
            }

        forM_ wifOverviews $ \(wofSpec, workflowInstanceOverviewName, workflowInstanceOverviewTitle, workflowInstanceOverviewPrimary) -> do
          workflowInstanceOverviewOverview <- insertSharedWorkflowOverviewSpec =<< fromWorkflowOverviewSpecForm wofSpec
          insert_ WorkflowInstanceOverview
            { workflowInstanceOverviewInstance = instId'
            , ..
            }

      return . Just $ case instId of
        Nothing -> addMessageI Error MsgWorkflowInstanceCollision
        Just _
          | is _Just wdId -> do
              addMessageI Success MsgWorkflowDefinitionInstantiated
              redirect AdminWorkflowInstanceListR
          | otherwise -> do
              addMessageI Success MsgWorkflowInstanceCreated
              redirect AdminWorkflowInstanceListR

    return (form, act)

  forM_ act id

  let instWidget = wrapForm instForm def
        { formAction = SomeRoute <$> cRoute
        , formEncoding = instEncoding
        }

  siteLayoutMsg MsgWorkflowDefinitionInstantiateTitle $ do
    setTitleI MsgWorkflowDefinitionInstantiateTitle

    instWidget


getGlobalWorkflowInstanceNewR, postGlobalWorkflowInstanceNewR :: Handler Html
getGlobalWorkflowInstanceNewR = postGlobalWorkflowInstanceNewR
postGlobalWorkflowInstanceNewR = workflowInstanceNewR WSGlobal

getSchoolWorkflowInstanceNewR, postSchoolWorkflowInstanceNewR :: SchoolId -> Handler Html
getSchoolWorkflowInstanceNewR = postSchoolWorkflowInstanceNewR
postSchoolWorkflowInstanceNewR = workflowInstanceNewR . WSSchool

getTermWorkflowInstanceNewR, postTermWorkflowInstanceNewR :: TermId -> Handler Html
getTermWorkflowInstanceNewR = postTermWorkflowInstanceNewR
postTermWorkflowInstanceNewR = workflowInstanceNewR . WSTerm

getTermSchoolWorkflowInstanceNewR, postTermSchoolWorkflowInstanceNewR :: TermId -> SchoolId -> Handler Html
getTermSchoolWorkflowInstanceNewR = postTermSchoolWorkflowInstanceNewR
postTermSchoolWorkflowInstanceNewR tid ssh = workflowInstanceNewR $ WSTermSchool tid ssh

workflowInstanceNewR :: RouteWorkflowScope -> Handler Html
workflowInstanceNewR = error "not implemented"
