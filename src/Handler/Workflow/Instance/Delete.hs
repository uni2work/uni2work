-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Instance.Delete
  ( getGWIDeleteR, postGWIDeleteR 
  , getSWIDeleteR, postSWIDeleteR
  , getTWIDeleteR, postTWIDeleteR
  , getTSWIDeleteR, postTSWIDeleteR
  , workflowInstanceDeleteR
  ) where

import Import

import Utils.Workflow


getGWIDeleteR, postGWIDeleteR :: WorkflowInstanceName -> Handler Html
getGWIDeleteR = postGWIDeleteR
postGWIDeleteR = workflowInstanceDeleteR WSGlobal
  
getSWIDeleteR, postSWIDeleteR :: SchoolId -> WorkflowInstanceName -> Handler Html
getSWIDeleteR = postSWIDeleteR
postSWIDeleteR ssh = workflowInstanceDeleteR $ WSSchool ssh
  
getTWIDeleteR, postTWIDeleteR :: TermId -> WorkflowInstanceName -> Handler Html
getTWIDeleteR = postTWIDeleteR
postTWIDeleteR tid = workflowInstanceDeleteR $ WSTerm tid
  
getTSWIDeleteR, postTSWIDeleteR :: TermId -> SchoolId -> WorkflowInstanceName -> Handler Html
getTSWIDeleteR = postTSWIDeleteR
postTSWIDeleteR tid ssh = workflowInstanceDeleteR $ WSTermSchool tid ssh

workflowInstanceDeleteR :: RouteWorkflowScope -> WorkflowInstanceName -> Handler Html
workflowInstanceDeleteR = error "not implemented"
