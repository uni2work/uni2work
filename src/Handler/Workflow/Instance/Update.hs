-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Instance.Update
  ( WorkflowInstanceUpdateButton(..)
  , workflowInstanceCanUpdate
  , postGWIUpdateR, postSWIUpdateR, postTWIUpdateR, postTSWIUpdateR
  ) where

import Import
import Utils.Form
import Utils.Workflow

import Handler.Utils.Workflow.CanonicalRoute

import qualified Data.CaseInsensitive as CI

import qualified Data.Set as Set
import qualified Data.Map.Strict as Map

-- import Handler.Utils.Memcached


data WorkflowInstanceUpdateButton
  = BtnWorkflowInstanceUpdate
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite)

nullaryPathPiece ''WorkflowInstanceUpdateButton $ camelToPathPiece' 3
embedRenderMessage ''UniWorX ''WorkflowInstanceUpdateButton id

instance Button UniWorX WorkflowInstanceUpdateButton where
  btnClasses _ = [BCIsButton]


data WorkflowInstanceUpdateAction
  = WIUpdateGraph SharedWorkflowGraphId
  | WIUpdateCategory (Maybe WorkflowInstanceCategory)
  | WIUpdateInstanceDescription Lang (Maybe (Text, Maybe StoredMarkup))
  | WIUpdateInstanceOverview WorkflowOverviewName (Maybe (SharedWorkflowOverviewSpecId, I18nText, Maybe Rational))
  deriving (Eq, Ord, Read, Show, Generic, Typeable)


workflowInstanceUpdates :: WorkflowInstanceId
                        -> DB (Set WorkflowInstanceUpdateAction)
workflowInstanceUpdates wiId = execWriterT . maybeT_ $ do
  WorkflowInstance{..} <- MaybeT . lift $ get wiId
  wdId <- hoistMaybe workflowInstanceDefinition
  WorkflowDefinition{..} <- MaybeT . lift $ get wdId

  when (workflowDefinitionGraph /= workflowInstanceGraph) $
    tellPoint $ WIUpdateGraph workflowDefinitionGraph

  when (workflowDefinitionInstanceCategory /= workflowInstanceCategory) $
    tellPoint $ WIUpdateCategory workflowDefinitionInstanceCategory

  iDescs <- lift . lift $ selectList [WorkflowInstanceDescriptionInstance ==. wiId] []
  dDescs <- lift . lift $ selectList [WorkflowDefinitionInstanceDescriptionDefinition ==. wdId] []

  let iDescs' = Map.fromList $ map (\(Entity _ WorkflowInstanceDescription{..}) -> (CI.mk workflowInstanceDescriptionLanguage, (workflowInstanceDescriptionTitle, workflowInstanceDescriptionDescription))) iDescs
      dDescs' = Map.fromList $ map (\(Entity _ WorkflowDefinitionInstanceDescription{..}) -> (CI.mk workflowDefinitionInstanceDescriptionLanguage, (workflowDefinitionInstanceDescriptionTitle, workflowDefinitionInstanceDescriptionDescription))) dDescs

  forM_ (Map.keysSet iDescs' `Set.union` Map.keysSet dDescs') $ \lang -> if
    | Just iDesc <- Map.lookup lang iDescs'
    , Just dDesc <- Map.lookup lang dDescs'
    , iDesc /= dDesc
      -> tellPoint . WIUpdateInstanceDescription (CI.original lang) $ Just dDesc
    | Just dDesc <- Map.lookup lang dDescs'
    , not $ Map.member lang iDescs'
      -> tellPoint . WIUpdateInstanceDescription (CI.original lang) $ Just dDesc
    | Map.member lang iDescs'
    , not $ Map.member lang dDescs'
      -> tellPoint $ WIUpdateInstanceDescription (CI.original lang) Nothing
    | otherwise
      -> return ()

  iOverviews <- lift . lift $ selectList [WorkflowInstanceOverviewInstance ==. wiId] []
  dOverviews <- lift . lift $ selectList [WorkflowDefinitionOverviewDefinition ==. wdId] []

  let iOverviews' = Map.fromList $ map (\(Entity _ WorkflowInstanceOverview{..}) -> (workflowInstanceOverviewName, (workflowInstanceOverviewOverview, workflowInstanceOverviewTitle, workflowInstanceOverviewPrimary))) iOverviews
      dOverviews' = Map.fromList $ map (\(Entity _ WorkflowDefinitionOverview{..}) -> (workflowDefinitionOverviewName, (workflowDefinitionOverviewOverview, workflowDefinitionOverviewDefaultTitle, workflowDefinitionOverviewDefaultPrimary))) dOverviews

  forM_ (Map.keysSet iOverviews' `Set.union` Map.keysSet dOverviews') $ \name -> if
    | Just iOverview <- Map.lookup name iOverviews'
    , Just dOverview <- Map.lookup name dOverviews'
    , iOverview /= dOverview
      -> tellPoint . WIUpdateInstanceOverview name $ Just dOverview
    | Just dOverview <- Map.lookup name dOverviews'
    , not $ Map.member name iOverviews'
      -> tellPoint . WIUpdateInstanceOverview name $ Just dOverview
    | Map.member name iOverviews'
    , not $ Map.member name dOverviews'
      -> tellPoint $ WIUpdateInstanceOverview name Nothing
    | otherwise
      -> return ()

workflowInstanceCanUpdate :: WorkflowInstanceId
                          -> DB Bool
workflowInstanceCanUpdate wiId = not . null <$> workflowInstanceUpdates wiId


postGWIUpdateR :: WorkflowInstanceName -> Handler Void
postGWIUpdateR = updateR WSGlobal

postSWIUpdateR :: SchoolId -> WorkflowInstanceName -> Handler Void
postSWIUpdateR ssh = updateR $ WSSchool ssh

postTWIUpdateR :: TermId -> WorkflowInstanceName -> Handler Void
postTWIUpdateR tid = updateR $ WSTerm tid

postTSWIUpdateR :: TermId -> SchoolId -> WorkflowInstanceName -> Handler Void
postTSWIUpdateR tid ssh = updateR $ WSTermSchool tid ssh


updateR :: RouteWorkflowScope -> WorkflowInstanceName -> Handler a
updateR rScope win = do
  runDB $ do
    scope <- maybeT notFound $ fromRouteWorkflowScope rScope
    wiId <- getKeyBy404 . UniqueWorkflowInstance win $ scope ^. _DBWorkflowScope
    updates <- workflowInstanceUpdates wiId

    when (null updates) $
      addMessageI Warning MsgWorkflowInstanceUpdateNoActions

    forM_ updates $ \case
      WIUpdateGraph graphId -> do
        update wiId [ WorkflowInstanceGraph =. graphId ]
        addMessageI Success MsgWorkflowInstanceUpdateUpdatedGraph
      WIUpdateCategory iCat -> do
        update wiId [ WorkflowInstanceCategory =. iCat ]
        addMessageI Success MsgWorkflowInstanceUpdateUpdatedCategory
      WIUpdateInstanceDescription lang Nothing -> do
        deleteBy $ UniqueWorkflowInstanceDescription wiId lang
        addMessageI Success $ MsgWorkflowInstanceUpdateDeletedDescriptionLanguage lang
      WIUpdateInstanceDescription lang (Just (title, mDesc)) -> do
        void $ upsertBy
          (UniqueWorkflowInstanceDescription wiId lang)
          WorkflowInstanceDescription
            { workflowInstanceDescriptionInstance = wiId
            , workflowInstanceDescriptionLanguage = lang
            , workflowInstanceDescriptionTitle = title
            , workflowInstanceDescriptionDescription = mDesc
            }
          [ WorkflowInstanceDescriptionTitle =. title
          , WorkflowInstanceDescriptionDescription =. mDesc
          ]
        addMessageI Success $ MsgWorkflowInstanceUpdateUpdatedDescriptionLanguage lang
      WIUpdateInstanceOverview name Nothing -> do
        deleteBy $ UniqueWorkflowInstanceOverview wiId name
        addMessageI Success $ MsgWorkflowInstanceUpdateDeletedOverview name
      WIUpdateInstanceOverview name (Just (wioOverview, wioTitle, wioPrimary)) -> do
        void $ upsertBy
          (UniqueWorkflowInstanceOverview wiId name)
          WorkflowInstanceOverview
            { workflowInstanceOverviewInstance = wiId
            , workflowInstanceOverviewOverview = wioOverview
            , workflowInstanceOverviewName = name
            , workflowInstanceOverviewTitle = wioTitle
            , workflowInstanceOverviewPrimary = wioPrimary
            }
          [ WorkflowInstanceOverviewOverview =. wioOverview
          , WorkflowInstanceOverviewTitle =. wioTitle
          , WorkflowInstanceOverviewPrimary =. wioPrimary
          ]
        addMessageI Success $ MsgWorkflowInstanceUpdateUpdatedOverview name

  redirect $ _WorkflowScopeRoute # ( rScope, WorkflowInstanceListR )
