-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Instance.Edit
  ( getGWIEditR, postGWIEditR 
  , getSWIEditR, postSWIEditR 
  , getTWIEditR, postTWIEditR 
  , getTSWIEditR, postTSWIEditR 
  , workflowInstanceEditR
  , getAWIEditR, postAWIEditR
  ) where

import Import

import Utils.Workflow


getGWIEditR, postGWIEditR :: WorkflowInstanceName -> Handler Html
getGWIEditR = postGWIEditR
postGWIEditR = workflowInstanceEditR WSGlobal
  
getSWIEditR, postSWIEditR :: SchoolId -> WorkflowInstanceName -> Handler Html
getSWIEditR = postSWIEditR
postSWIEditR ssh = workflowInstanceEditR $ WSSchool ssh
  
getTWIEditR, postTWIEditR :: TermId -> WorkflowInstanceName -> Handler Html
getTWIEditR = postTWIEditR
postTWIEditR tid = workflowInstanceEditR $ WSTerm tid
  
getTSWIEditR, postTSWIEditR :: TermId -> SchoolId -> WorkflowInstanceName -> Handler Html
getTSWIEditR = postTSWIEditR
postTSWIEditR tid ssh = workflowInstanceEditR $ WSTermSchool tid ssh

workflowInstanceEditR :: RouteWorkflowScope -> WorkflowInstanceName -> Handler Html
workflowInstanceEditR = error "not implemented"


getAWIEditR, postAWIEditR :: CryptoUUIDWorkflowInstance -> Handler Html
getAWIEditR = postAWIEditR
postAWIEditR = error "not implemented"
