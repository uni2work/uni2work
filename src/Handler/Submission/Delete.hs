-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Submission.Delete
  ( getSubDelR, postSubDelR
  ) where

import Import

import Handler.Utils.Delete
import Handler.Utils.Submission

import qualified Data.Set as Set


getSubDelR, postSubDelR :: TermId -> SchoolId -> CourseShorthand -> SheetName -> CryptoFileNameSubmission -> Handler Html
getSubDelR = postSubDelR
postSubDelR tid ssh csh shn cID = do
  subId <- runDB $ submissionMatchesSheet tid ssh csh shn cID
  deleteR $ (submissionDeleteRoute $ Set.singleton subId)
    { drAbort = SomeRoute $ CSubmissionR tid ssh csh shn cID SubShowR
    , drSuccess = SomeRoute $ CSheetR tid ssh csh shn SShowR
    }
