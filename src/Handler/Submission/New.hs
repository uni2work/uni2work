-- SPDX-FileCopyrightText: 2022-2023 Sarah Vaupel <sarah.vaupel@uniworx.de>, Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Submission.New
  ( getSubmissionNewR, postSubmissionNewR
  , getSSubsNewR, postSSubsNewR
  ) where

import Import

import Handler.Submission.Helper
import Handler.Utils


getSubmissionNewR, postSubmissionNewR :: TermId -> SchoolId -> CourseShorthand -> SheetName -> Handler Html
getSubmissionNewR = postSubmissionNewR
postSubmissionNewR tid ssh csh shn = submissionHelper tid ssh csh shn Nothing

getSSubsNewR, postSSubsNewR :: TermId -> SchoolId -> CourseShorthand -> SheetName -> Handler Html
getSSubsNewR = postSSubsNewR
postSSubsNewR tid ssh csh shn = do
  let uploadMode = NoUpload -- TODO
  ((_formRes, formWidget), formEnctype) <- runFormPost . renderAForm FormStandard $ fileUploadMultiUserForm True (fslI MsgSubmissionArchive) uploadMode Nothing

  siteLayoutMsg (MsgSubmissionHeadingNewMultiple tid ssh csh shn) $ do
    setTitleI $ MsgSubmissionTitleNewMultiple tid ssh csh shn
    wrapForm formWidget def
      { formAction   = Just . SomeRoute $ CSheetR tid ssh csh shn SSubsNewR
      , formEncoding = formEnctype
      }
