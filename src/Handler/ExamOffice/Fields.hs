-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.ExamOffice.Fields
  ( getEOFieldsR
  , postEOFieldsR
  ) where

import Import
import Utils.Form

import qualified Database.Esqueleto.Legacy as E

import qualified Data.Set as Set
import qualified Data.Map as Map


data ExamOfficeFieldMode
  = EOFNotSubscribed
  | EOFSubscribed
  | EOFForced
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Typeable)
embedRenderMessage ''UniWorX ''ExamOfficeFieldMode $ concat . set (ix 0) "ExamOfficeField" . splitCamel
instance Universe ExamOfficeFieldMode
instance Finite ExamOfficeFieldMode
nullaryPathPiece ''ExamOfficeFieldMode $ camelToPathPiece' 1
instance Default ExamOfficeFieldMode where
  def = EOFNotSubscribed

eofModeField :: (MonadHandler m, HandlerSite m ~ UniWorX) => Field m ExamOfficeFieldMode
-- ^ Always required
eofModeField = Field{..}
  where
    fieldEnctype = UrlEncoded
    fieldView = \theId name attrs val _isReq -> $(widgetFile "widgets/fields/examOfficeFieldMode")
    fieldParse = \e _ -> return $ parser e

    parser [] = Right Nothing
    parser (x:_)
      | Just mode <- fromPathPiece x
      = Right $ Just mode
    parser (x:_)
      = Left . SomeMessage $ MsgInvalidExamOfficeFieldMode x

    isChecked :: Eq a => a -> Either Text a -> Bool
    isChecked opt = either (const False) (== opt)


makeExamOfficeFieldsForm :: UserId -> Maybe (Map StudyTermsId (Map StudyFieldType Bool)) -> Form (Map StudyTermsId (Map StudyFieldType Bool))
makeExamOfficeFieldsForm uid template = renderWForm FormStandard $ do
  availableFields <- liftHandler . runDB . E.select . E.from $ \(terms `E.InnerJoin` schoolTerms) -> do
    E.on $ terms E.^. StudyTermsId E.==. schoolTerms E.^. SchoolTermsTerms
    E.where_ . E.exists . E.from $ \userFunction ->
      E.where_ $ userFunction E.^. UserFunctionUser E.==. E.val uid
           E.&&. userFunction E.^. UserFunctionFunction E.==. E.val SchoolExamOffice
           E.&&. userFunction E.^. UserFunctionSchool E.==. schoolTerms E.^. SchoolTermsSchool
    return terms

  let
    available :: Map StudyTermsId (StudyTerms, Map StudyFieldType ExamOfficeFieldMode)
    available = imap (\k terms -> (terms, Map.fromList $ universeF <&> \fieldType -> (fieldType, view forced $ template >>= Map.lookup k >>= Map.lookup fieldType))) $ toMapOf (folded .> _entityVal) availableFields

    forced :: Iso' (Maybe Bool) ExamOfficeFieldMode
    forced = iso fromForced toForced
      where
        fromForced = maybe EOFNotSubscribed $ bool EOFSubscribed EOFForced
        toForced = \case
          EOFNotSubscribed -> Nothing
          EOFSubscribed    -> Just False
          EOFForced        -> Just True

  fmap (fmap (fmap $ Map.mapMaybe (review forced)) . sequence) . forM available $ \(StudyTerms{..}, template') ->
    let label = fromMaybe (tshow studyTermsKey) $ studyTermsName <|> studyTermsShorthand
     in wformSection label *>
        ( fmap (fmap Map.fromList . sequence) . forM universeF $ \fieldType ->
          fmap (fieldType,) <$> wpopt eofModeField (fslI fieldType) (Map.lookup fieldType template')
        )


-- | Manage the list of `StudyTerms` this user (in her function as exam-office)
--   has an interest in, i.e. that authorize her to view an users grades, iff
--   they study one of the selected fields
getEOFieldsR, postEOFieldsR :: Handler Html
getEOFieldsR = postEOFieldsR
postEOFieldsR = do
  uid <- requireAuthId

  oldFields <- runDB $ do
    fields <- E.select . E.from $ \examOfficeField -> do
      E.where_ $ examOfficeField E.^. ExamOfficeFieldOffice E.==. E.val uid
      return (examOfficeField E.^. ExamOfficeFieldField, (examOfficeField E.^. ExamOfficeFieldType, examOfficeField E.^. ExamOfficeFieldForced))
    return . Map.fromListWith Map.union $ fields <&> \(E.Value termsId, (E.Value fieldType, E.Value forced)) -> (termsId, Map.singleton fieldType forced)

  ((fieldsRes, fieldsView), fieldsEnc) <- runFormPost . makeExamOfficeFieldsForm uid $ Just oldFields

  formResult fieldsRes $ \newFields -> do
    forM_ universeF $ \fieldType -> do
      runDB . forM_ (Map.keysSet newFields `Set.union` Map.keysSet oldFields) $ \fieldId -> if
        | Just forced <- Map.lookup fieldId newFields >>= Map.lookup fieldType
        , fieldId `Map.member` oldFields -> do
            updateBy (UniqueExamOfficeField uid fieldId fieldType) [ ExamOfficeFieldForced =. forced ]
            audit $ TransactionExamOfficeFieldEdit uid fieldId fieldType
        | Just forced <- Map.lookup fieldId newFields >>= Map.lookup fieldType -> do
            insert_ $ ExamOfficeField uid fieldId fieldType forced
            audit $ TransactionExamOfficeFieldEdit uid fieldId fieldType
        | otherwise -> do
            deleteBy $ UniqueExamOfficeField uid fieldId fieldType
            audit $ TransactionExamOfficeFieldDelete uid fieldId fieldType
    addMessageI Success $ MsgTransactionExamOfficeFieldsUpdated (Set.size . Set.map (view _1) $ (setSymmDiff `on` assocsSet) newFields oldFields)
    redirect $ ExamOfficeR EOExamsR

  let
    fieldsView' = wrapForm fieldsView def
      { formAction = Just . SomeRoute $ ExamOfficeR EOFieldsR
      , formEncoding = fieldsEnc
      }

  siteLayoutMsg MsgHeadingExamOfficeFields $ do
    setTitleI MsgHeadingExamOfficeFields

    [whamlet|
      $newline never
      <p>
        _{MsgExamOfficeSubscribedFieldsExplanation}
      ^{fieldsView'}
    |]
