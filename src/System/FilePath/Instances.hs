-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module System.FilePath.Instances
  (
  ) where

import ClassyPrelude

import qualified Data.Text as Text

import Web.PathPieces


instance {-# OVERLAPS #-} PathMultiPiece FilePath where
  fromPathMultiPiece = Just . unpack . intercalate "/"
  toPathMultiPiece = Text.splitOn "/" . pack
