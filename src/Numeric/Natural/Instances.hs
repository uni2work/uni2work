-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Numeric.Natural.Instances
  (
  ) where

import ClassyPrelude
import Numeric.Natural
import Web.PathPieces

instance PathPiece Natural where
  toPathPiece = tshow
  fromPathPiece = readMay
