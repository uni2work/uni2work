-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Foundation.Types
  ( UpsertCampusUserMode(..)
  , _UpsertCampusUserLoginLdap, _UpsertCampusUserLoginDummy, _UpsertCampusUserLoginOther, _UpsertCampusUserLdapSync, _UpsertCampusUserGuessUser
  , _upsertCampusUserIdent
  ) where

import Import.NoFoundation


data UpsertCampusUserMode
  = UpsertCampusUserLoginLdap
  | UpsertCampusUserLoginDummy { upsertCampusUserIdent :: UserIdent }
  | UpsertCampusUserLoginOther { upsertCampusUserIdent :: UserIdent }
  | UpsertCampusUserLdapSync   { upsertCampusUserIdent :: UserIdent }
  | UpsertCampusUserGuessUser
  deriving (Eq, Ord, Read, Show, Generic, Typeable)

makeLenses_ ''UpsertCampusUserMode
makePrisms ''UpsertCampusUserMode
