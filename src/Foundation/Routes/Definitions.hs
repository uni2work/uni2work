-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Foundation.Routes.Definitions
  ( uniworxRoutes
  ) where

import ClassyPrelude.Yesod
import Yesod.Routes.TH.Types (ResourceTree)


uniworxRoutes :: [ResourceTree String]
uniworxRoutes = $(parseRoutesFile "routes")
