-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Foundation.Authorization.Types
  ( WorkflowAuthWhereContext(..)
  , _WorkflowAuthWhereWorkflow, _WorkflowAuthWhereAction, _WorkflowAuthWherePayload
  , _wawWorkflowId, _wawAction, _wawPayload
  , WorkflowAuth
  ) where

import Import.NoFoundation

import qualified Database.Esqueleto.Legacy as E


data WorkflowAuthWhereContext
  = WorkflowAuthWhereWorkflow { wawWorkflowId :: E.SqlExpr (E.Value WorkflowWorkflowId)    }
  | WorkflowAuthWhereAction   { wawAction     :: E.SqlExpr (Entity WorkflowWorkflowAction) }
  | WorkflowAuthWherePayload  { wawAction     :: E.SqlExpr (Entity WorkflowWorkflowAction)
                              , wawPayload    :: E.SqlExpr (Entity WorkflowWorkflowPayload)
                              }
  deriving (Generic, Typeable)

type WorkflowAuth = WorkflowAuthWhereContext -> E.SqlExpr (E.Value Bool)
  

makeLenses_ ''WorkflowAuthWhereContext
makePrisms ''WorkflowAuthWhereContext
