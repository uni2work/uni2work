-- SPDX-FileCopyrightText: 2022-2023 Sarah Vaupel <sarah.vaupel@uniworx.de>, Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Foundation.Routes
  ( module Foundation.Routes.Definitions
  , module Foundation.Routes
  ) where

import Import.NoFoundation
import Foundation.Type

import Foundation.Routes.Definitions


import ServantApi.ExternalApis.Type


-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the following documentation
-- for an explanation for this split:
-- http://www.yesodweb.com/book/scaffolding-and-the-site-template#scaffolding-and-the-site-template_foundation_and_application_modules
--
-- This function also generates the following type synonyms:
-- type Handler x = HandlerFor UniWorX x
-- type Widget    = WidgetFor  UniWorX ()
mkYesodData "UniWorX" uniworxRoutes

deriving instance Generic CourseR
deriving instance Generic SheetR
deriving instance Generic SubmissionR
deriving instance Generic MaterialR
deriving instance Generic TutorialR
deriving instance Generic ExamR
deriving instance Generic EExamR
deriving instance Generic CourseApplicationR
deriving instance Generic AllocationR
deriving instance Generic TermR
deriving instance Generic TermSchoolR
deriving instance Generic SchoolR
deriving instance Generic ExamOfficeR
deriving instance Generic CourseNewsR
deriving instance Generic CourseEventR
deriving instance Generic AdminWorkflowDefinitionR
deriving instance Generic AdminWorkflowInstanceR
deriving instance Generic GlobalWorkflowInstanceR
deriving instance Generic GlobalWorkflowWorkflowR
deriving instance Generic SchoolWorkflowInstanceR
deriving instance Generic SchoolWorkflowWorkflowR
deriving instance Generic TermWorkflowInstanceR
deriving instance Generic TermWorkflowWorkflowR
deriving instance Generic TermSchoolWorkflowInstanceR
deriving instance Generic TermSchoolWorkflowWorkflowR
deriving instance Generic AMatchingR
deriving instance Generic (Route UniWorX)


instance Hashable CourseR
instance Hashable SheetR
instance Hashable SubmissionR
instance Hashable MaterialR
instance Hashable TutorialR
instance Hashable ExamR
instance Hashable EExamR
instance Hashable CourseApplicationR
instance Hashable AllocationR
instance Hashable TermR
instance Hashable TermSchoolR
instance Hashable SchoolR
instance Hashable ExamOfficeR
instance Hashable CourseNewsR
instance Hashable CourseEventR
instance Hashable AdminWorkflowDefinitionR
instance Hashable AdminWorkflowInstanceR
instance Hashable GlobalWorkflowInstanceR
instance Hashable GlobalWorkflowWorkflowR
instance Hashable SchoolWorkflowInstanceR
instance Hashable SchoolWorkflowWorkflowR
instance Hashable TermWorkflowInstanceR
instance Hashable TermWorkflowWorkflowR
instance Hashable TermSchoolWorkflowInstanceR
instance Hashable TermSchoolWorkflowWorkflowR
instance Hashable AMatchingR
instance Hashable (Route UniWorX)
instance Hashable (Route EmbeddedStatic) where
  hashWithSalt s = hashWithSalt s . renderRoute
instance Hashable (Route Auth) where
  hashWithSalt s = hashWithSalt s . renderRoute
  
instance Ord (Route Auth) where
  compare = compare `on` renderRoute
instance Ord (Route EmbeddedStatic) where
  compare = compare `on` renderRoute

deriving instance Ord CourseR
deriving instance Ord SheetR
deriving instance Ord SubmissionR
deriving instance Ord MaterialR
deriving instance Ord TutorialR
deriving instance Ord ExamR
deriving instance Ord EExamR
deriving instance Ord CourseApplicationR
deriving instance Ord AllocationR
deriving instance Ord TermR
deriving instance Ord TermSchoolR
deriving instance Ord SchoolR
deriving instance Ord ExamOfficeR
deriving instance Ord CourseNewsR
deriving instance Ord CourseEventR
deriving instance Ord AdminWorkflowDefinitionR
deriving instance Ord AdminWorkflowInstanceR
deriving instance Ord GlobalWorkflowInstanceR
deriving instance Ord GlobalWorkflowWorkflowR
deriving instance Ord SchoolWorkflowInstanceR
deriving instance Ord SchoolWorkflowWorkflowR
deriving instance Ord TermWorkflowInstanceR
deriving instance Ord TermWorkflowWorkflowR
deriving instance Ord TermSchoolWorkflowInstanceR
deriving instance Ord TermSchoolWorkflowWorkflowR
deriving instance Ord AMatchingR
deriving instance Ord (Route UniWorX)

data RouteChildren
type instance Children RouteChildren a = ChildrenRouteChildren a
type family ChildrenRouteChildren a where
  ChildrenRouteChildren (Route (ServantApi _)) = '[]
  ChildrenRouteChildren (Route EmbeddedStatic) = '[]
  ChildrenRouteChildren (Route Auth) = '[]
  ChildrenRouteChildren UUID = '[]
  ChildrenRouteChildren (Key a) = '[]
  ChildrenRouteChildren (CI a) = '[]

  ChildrenRouteChildren a = Children ChGeneric a

-- Pattern Synonyms for convenience
pattern CSheetR :: TermId -> SchoolId -> CourseShorthand -> SheetName -> SheetR -> Route UniWorX
pattern CSheetR tid ssh csh shn ptn
  = CourseR tid ssh csh (SheetR shn ptn)

pattern CMaterialR :: TermId -> SchoolId -> CourseShorthand -> MaterialName -> MaterialR -> Route UniWorX
pattern CMaterialR tid ssh csh mnm ptn
  = CourseR tid ssh csh (MaterialR mnm ptn)

pattern CTutorialR :: TermId -> SchoolId -> CourseShorthand -> TutorialName -> TutorialR -> Route UniWorX
pattern CTutorialR tid ssh csh tnm ptn
  = CourseR tid ssh csh (TutorialR tnm ptn)

pattern CExamR :: TermId -> SchoolId -> CourseShorthand -> ExamName -> ExamR -> Route UniWorX
pattern CExamR tid ssh csh tnm ptn
  = CourseR tid ssh csh (ExamR tnm ptn)

pattern CSubmissionR :: TermId -> SchoolId -> CourseShorthand -> SheetName -> CryptoFileNameSubmission -> SubmissionR -> Route UniWorX
pattern CSubmissionR tid ssh csh shn cid ptn
  = CSheetR tid ssh csh shn (SubmissionR cid ptn)

pattern CApplicationR :: TermId -> SchoolId -> CourseShorthand -> CryptoFileNameCourseApplication -> CourseApplicationR -> Route UniWorX
pattern CApplicationR tid ssh csh appId ptn
  = CourseR tid ssh csh (CourseApplicationR appId ptn)

pattern CNewsR :: TermId -> SchoolId -> CourseShorthand -> CryptoUUIDCourseNews -> CourseNewsR -> Route UniWorX
pattern CNewsR tid ssh csh nId ptn
  = CourseR tid ssh csh (CourseNewsR nId ptn)

pattern CEventR :: TermId -> SchoolId -> CourseShorthand -> CryptoUUIDCourseEvent -> CourseEventR -> Route UniWorX
pattern CEventR tid ssh csh nId ptn
  = CourseR tid ssh csh (CourseEventR nId ptn)
