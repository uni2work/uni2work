-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Control.Arrow.Instances
  (
  ) where

import ClassyPrelude
import Control.Arrow


instance (a ~ b, Monad m) => Monoid (Kleisli m a b) where
  mempty = Kleisli return

instance (a ~ b, Monad m) => Semigroup (Kleisli m a b) where
  Kleisli f <> Kleisli g = Kleisli $ f <=< g
