-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Control.Monad.Catch.Instances
  () where

import ClassyPrelude
import Control.Monad.Catch


deriving instance Functor ExitCase
