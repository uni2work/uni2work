-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Control.Monad.Trans.Except.Instances
  () where

import ClassyPrelude

import Control.Monad.Trans.Except (ExceptT(..), runExceptT)

import Control.Arrow (left)


newtype UnliftIOExceptTError e = UnliftIOExceptTError { getUnliftIOExceptTError :: e }
  deriving (Read, Show, Generic, Typeable)
  deriving newtype (Exception)
  

instance (Exception e, MonadUnliftIO m) => MonadUnliftIO (ExceptT e m) where
  withRunInIO cont = ExceptT (withRunInIO $ \runInner -> fmap (left getUnliftIOExceptTError) . try $ cont (either (throwIO . UnliftIOExceptTError) return <=< runInner . runExceptT))
