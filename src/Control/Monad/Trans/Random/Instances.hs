-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Control.Monad.Trans.Random.Instances
  () where

-- import ClassyPrelude
import qualified Control.Monad.Trans.Random.Strict as Strict
import qualified Control.Monad.Trans.Random.Lazy as Lazy

import Control.Monad.Morph (MFunctor(..))


instance MFunctor (Strict.RandT g) where
  hoist = Strict.mapRandT

instance MFunctor (Lazy.RandT g) where
  hoist = Lazy.mapRandT
