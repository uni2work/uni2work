-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Web.PathPieces.Instances
  (
  ) where

import Prelude

import Utils.PathPiece


$(mapM tuplePathPiece [2..4])
