-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Web.Cookie.Instances
  () where

import ClassyPrelude
import Web.Cookie

import qualified Data.ByteString.Builder as BS


instance Hashable SameSiteOption where
  hashWithSalt s opt = hashWithSalt s . BS.toLazyByteString $ renderSetCookie def{ setCookieSameSite = Just opt }
