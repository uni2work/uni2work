-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Colonnade.Instances
  (
  ) where

import ClassyPrelude

import Control.Lens.Indexed (FunctorWithIndex(imap))

import Colonnade.Encode (Colonnade(..), OneColonnade(..))

instance Functor h => FunctorWithIndex (Maybe a) (Colonnade h a) where
 imap f (Colonnade ones) = Colonnade $ dimapColonnade' <$> ones
   where
     dimapColonnade' OneColonnade{..} = OneColonnade
       { oneColonnadeEncode = \x -> f (Just x) $ oneColonnadeEncode x
       , oneColonnadeHead   = f Nothing <$> oneColonnadeHead
       }
