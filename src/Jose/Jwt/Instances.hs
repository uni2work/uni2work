-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Jose.Jwt.Instances
  (
  ) where

import ClassyPrelude.Yesod
import Model.Types.TH.PathPiece

import Jose.Jwt


deriving instance Ord Jwt
deriving instance Read Jwt
deriving instance Generic Jwt
deriving instance Typeable Jwt
deriving anyclass instance NFData Jwt

instance PathPiece Jwt where
  toPathPiece (Jwt bytes) = decodeUtf8 bytes
  fromPathPiece = Just . Jwt . encodeUtf8

instance Hashable Jwt

derivePersistFieldPathPiece ''Jwt


deriving instance Generic JwtError
deriving instance Typeable JwtError

instance Exception JwtError
