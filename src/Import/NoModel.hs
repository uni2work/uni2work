-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <vaupel.sarah@campus.lmu.de>, Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Import.NoModel
  ( module Import
  , MForm
  , WeekDay
  ) where

import ClassyPrelude.Yesod    as Import
  hiding ( formatTime
         , derivePersistFieldJSON, toPersistValueJSON, fromPersistValueJSON
         , getMessages, addMessage, addMessageI
         , (.=)
         , MForm
         , Proxy(..)
         , foldlM
         , static
         , boolField, identifyForm, addClass
         , HasHttpManager(..)
         , embed
         , try, embed, catches, handle, catch, bracket, bracketOnError, bracket_, catchJust, finally, handleJust, mask, mask_, onException, tryJust, uninterruptibleMask, uninterruptibleMask_
         , htmlField, fileField, urlField
         , selectField
         , mreq, areq, wreq -- Use `mreqMsg`, `areqMsg`, `wreqMsg`
         , sinkFile, sourceFile
         , defaultYesodMiddleware
         , authorizationCheck
         , mkMessage, mkMessageFor, mkMessageVariant
         , YesodBreadcrumbs(..)
         , MonoZip(..), ozipWith
         )

import UnliftIO.Async.Utils as Import

import Model.Types.TH.JSON as Import
import Model.Types.TH.Wordlist as Import
import Model.Types.TH.Binary as Import

import Mail                   as Import

import Yesod.Auth             as Import hiding (requireAuth, requireAuthId, requireAuthPair)
import Yesod.Core.Types       as Import (loggerSet)
import Yesod.Default.Config2  as Import
import Yesod.Core.Types.Instances as Import
import Yesod.Servant          as Import
  hiding ( MonadHandler(..), HasRoute(..), MonadRequest(..)
         , runDB, defaultRunDB
         )
import Servant.Docs           as Import
  ( ToSample(..), samples, noSamples, singleSample
  )

import Utils                  as Import
import Utils.Frontend.I18n    as Import
import Utils.DB               as Import
import Utils.Sql              as Import
import Utils.Widgets          as Import
import Utils.Auth             as Import

import Settings.Cluster.Volatile as Import

import Data.Fixed             as Import

import Data.UUID              as Import (UUID)

import Data.CaseInsensitive   as Import (CI, FoldCase(..), foldedCase)

import Text.Lucius            as Import
import Text.Julius            as Import
import Text.Shakespeare.Text  as Import hiding (text, stext)
import Text.Hamlet            as Import (ihamlet)

import Data.Universe          as Import
import Data.Universe.TH       as Import
import UnliftIO.Pool          as Import (Pool)
import Network.HaskellNet.SMTP as Import (SMTPConnection)

import Data.Data              as Import (Data)
import GHC.Exts               as Import (IsList)
import Data.Ix                as Import (Ix)
import GHC.Stack              as Import (CallStack, HasCallStack, callStack)

import Data.Hashable          as Import
import Data.List              as Import (elemIndex)
import Data.List.NonEmpty     as Import (NonEmpty(..), nonEmpty)
import Data.Text.Encoding.Error as Import(UnicodeException(..))
import Data.Semigroup         as Import (Min(..), Max(..))
import Data.Monoid            as Import (Last(..), First(..), Any(..), All(..), Sum(..), Endo(..), Alt(..), Dual(..), Ap(..))
import Data.Binary            as Import (Binary)

import Data.Binary.Orphans as Import ()
import Data.Binary.Instances.Aeson as Import ()
import Data.Binary.Instances.Hashable as Import ()
import Data.Binary.Instances.Scientific as Import ()
import Data.Binary.Instances.Tagged as Import ()
import Data.Binary.Instances.Text as Import ()
import Data.Binary.Instances.Time as Import ()
import Data.Binary.Instances.UnorderedContainers as Import ()
import Data.Binary.Instances.Vector as Import ()

import Data.Dynamic           as Import (Dynamic)
import Data.Dynamic.Lens      as Import

import System.FilePath        as Import hiding (joinPath, normalise, isValid, makeValid)

import Numeric.Natural        as Import (Natural)
import Data.Ratio             as Import ((%))

import Network.IP.Addr        as Import (IP)

import Database.Persist.Sql as Import (SqlReadBackend, SqlReadT, SqlWriteT, IsSqlBackend, fromSqlKey, toSqlKey)

import Ldap.Client.Pool as Import

import Control.Monad as Import (zipWithM)

import System.Random as Import (Random(..))
import Control.Monad.Random.Class as Import (MonadRandom(..))

import Control.Monad.Morph as Import
import Control.Monad.Trans.Resource as Import (ReleaseKey)
import Control.Monad.Trans.Reader as Import
  ( runReader, mapReader, withReader
  , mapReaderT, withReaderT
  )
import Control.Monad.Reader.Class as Import (MonadReader(..))
import Control.Monad.Trans.State as Import
  ( State, runState, mapState, withState
  , StateT(..), mapStateT, withStateT
  )
import Control.Monad.Trans.Accum as Import
  ( Accum, runAccum, mapAccum, execAccum
  , AccumT, runAccumT, execAccumT, evalAccumT, mapAccumT
  , look, looks, add
  )
import Control.Monad.State.Class as Import (MonadState(state))
import Control.Monad.Trans.Writer.Lazy as Import
  ( Writer, runWriter, mapWriter, execWriter
  , WriterT(..), mapWriterT, execWriterT
  )
import Control.Monad.Writer.Class as Import (MonadWriter(..))
import Control.Monad.Trans.Except as Import
  ( except, Except, runExcept, mapExcept
  , ExceptT(..), runExceptT, mapExceptT, throwE
  )
import Control.Monad.Base as Import
import Control.Monad.Catch as Import hiding (Handler(..))
import Control.Monad.Trans.Control as Import hiding (embed)
import Control.Monad.Fail as Import

import Jose.Jwk as Import (JwkSet, Jwk(..))
import Jose.Jwt as Import (Jwt(..))

import Data.Time.Calendar as Import
import Data.Time.Clock as Import
import Data.Time.LocalTime as Import hiding (utcToLocalTime, utcToZonedTime, localTimeToUTC)

import Network.Mime as Import

import Data.Aeson.TH as Import
import Data.Aeson.Types as Import (FromJSONKey(..), ToJSONKey(..), toJSONKeyText, FromJSONKeyFunction(..), ToJSONKeyFunction(..))

import Data.SafeJSON as Import (SafeJSON, safeToJSON, safeFromJSON)

import Data.Constraint as Import (Dict(..))

import Algebra.Lattice as Import

import Data.Proxy as Import (Proxy(..))

import Data.List.PointedList as Import (PointedList)

import Language.Haskell.TH.Syntax as Import (Lift(liftTyped))

import Network.URI as Import (URI, parseURI, uriToString)

import Data.SemVer as Import (Version)

import Language.Haskell.TH.Instances as Import ()
import Data.NonNull.Instances as Import ()
import Data.Monoid.Instances  as Import ()
import Data.Maybe.Instances  as Import ()
import Data.CryptoID.Instances  as Import ()
import Data.Sum.Instances     as Import ()
import Data.Fixed.Instances   as Import ()
import Data.Scientific.Instances   as Import ()
import Data.Set.Instances     as Import ()
import Data.Time.Clock.Instances as Import ()
import Data.Time.LocalTime.Instances as Import ()
import Data.Time.Calendar.Instances as Import ()
import Data.Time.Format.Instances as Import ()
import Network.Mail.Mime.Instances as Import
import Yesod.Core.Instances as Import ()
import Data.Aeson.Types.Instances as Import ()
import Database.Esqueleto.Instances as Import ()
import Numeric.Natural.Instances as Import ()
import Text.Blaze.Instances as Import ()
import Jose.Jwt.Instances as Import ()
import Jose.Jwk.Instances as Import ()
import Web.PathPieces.Instances as Import ()
import Data.Universe.Instances.Reverse.MonoTraversable ()
import Data.Universe.Instances.Reverse.WithIndex ()
import Database.Persist.Class.Instances as Import ()
import Database.Persist.Types.Instances as Import ()
import Data.UUID.Instances as Import ()
import System.FilePath.Instances as Import ()
import Network.IP.Addr.Instances as Import ()
import Data.Void.Instances as Import ()
import Crypto.Hash.Instances as Import ()
import Colonnade.Instances as Import ()
import Data.Bool.Instances as Import ()
import Data.Encoding.Instances as Import ()
import Prometheus.Instances as Import ()
import Yesod.Form.Fields.Instances as Import ()
import Yesod.Form.Types.Instances as Import ()
import Data.MonoTraversable.Instances as Import ()
import Servant.Client.Core.BaseUrl.Instances as Import ()
import Control.Monad.Trans.Except.Instances as Import ()
import Servant.Server.Instances as Import ()
import Servant.Docs.Internal.Pretty.Instances as Import ()
import Network.URI.Instances as Import ()
import Data.HashSet.Instances as Import ()
import Web.Cookie.Instances as Import ()
import Network.HTTP.Types.Method.Instances as Import ()
import Crypto.Random.Instances as Import ()
import Network.Minio.Instances as Import ()
import System.Clock.Instances as Import ()
import Data.Word.Word24.Instances as Import ()
import Control.Monad.Trans.Memo.StateCache.Instances as Import (hoistStateCache)
import Database.Persist.Sql.Types.Instances as Import ()
import Control.Monad.Catch.Instances as Import ()
import Text.Shakespeare.Text.Instances as Import ()
import Ldap.Client.Instances as Import ()
import Data.MultiSet.Instances as Import ()
import Control.Arrow.Instances as Import ()
import Data.SemVer.Instances as Import ()
import Control.Monad.Trans.Random.Instances as Import ()

import Crypto.Hash as Import (Digest, SHA3_256, SHA3_512)
import Crypto.Random as Import (ChaChaDRG, Seed)

import Control.Lens as Import
  hiding ( (<.>)
         , universe
         , cons, uncons, snoc, unsnoc, (<|)
         , Index, index, (<.), (.>)
         )
import Control.Lens.Extras as Import (is)
import Data.Set.Lens as Import

import Control.Arrow as Import (left, right, Kleisli(..))

import Data.Encoding as Import (DynEncoding, decodeLazyByteString, encodeLazyByteString)
import Data.Encoding.UTF8 as Import (UTF8(UTF8))

import GHC.TypeLits as Import (KnownSymbol)

import Data.Word.Word24 as Import

import Data.Kind as Import (Type, Constraint)

import Data.Scientific as Import (Scientific, formatScientific)

import Data.MultiSet as Import (MultiSet)

import Data.MonoTraversable.Keys as Import

import Data.MonoTraversable.Lens as Import


import Control.Monad.Trans.RWS (RWST)


type MForm m = RWST (Maybe (Env, FileEnv), HandlerSite m, [Lang]) Enctype Ints m

type WeekDay = DayOfWeek
