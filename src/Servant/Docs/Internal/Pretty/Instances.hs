-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Servant.Docs.Internal.Pretty.Instances () where

import ClassyPrelude

import Servant.Docs.Internal.Pretty
import Servant.API.ContentTypes

import Data.Proxy


instance MimeUnrender JSON a => MimeUnrender PrettyJSON a where
  mimeUnrender _ = mimeUnrender $ Proxy @JSON
