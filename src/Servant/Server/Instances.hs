-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Servant.Server.Instances
  () where

import ClassyPrelude hiding (Handler(..))
import Servant.Server

import Control.Monad.Trans.Except.Instances ()


instance MonadUnliftIO Handler where
  withRunInIO cont = Handler (withRunInIO $ \runInner -> cont (runInner . runHandler'))
