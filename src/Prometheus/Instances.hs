-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Prometheus.Instances
  () where

import ClassyPrelude
import Prometheus
import Data.Aeson

import qualified Data.Map.Strict as Map


instance ToJSON SampleType where
  toJSON = String . tshow

instance ToJSON SampleGroup where
  toJSON (SampleGroup Info{..} sgType samples) = object
    [ "name" .= metricName
    , "help" .= metricHelp
    , "type" .= sgType
    , "metrics" .= samples
    ]

instance ToJSON Sample where
  toJSON (Sample sName sLabels sValue) = object
    [ "name" .= sName
    , "labels" .= Map.fromList sLabels
    , "value" .= decodeUtf8 sValue
    ]
