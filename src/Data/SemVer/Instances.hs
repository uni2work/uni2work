-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.SemVer.Instances
  () where

import ClassyPrelude
import qualified Data.SemVer as SemVer
import qualified Data.SemVer.Constraint as SemVer (Constraint(..))
import qualified Data.SemVer.Constraint as SemVer.Constraint

import Control.Monad.Fail

import Data.Aeson
import Web.HttpApiData
import Data.Binary


instance ToHttpApiData SemVer.Version where
  toUrlPiece = SemVer.toText

instance ToHttpApiData SemVer.Constraint where
  toUrlPiece SemVer.CAny = "*"
  toUrlPiece (SemVer.CLt v) = "<" <> toUrlPiece v
  toUrlPiece (SemVer.CLtEq v) = "<=" <> toUrlPiece v
  toUrlPiece (SemVer.CGt v) = ">" <> toUrlPiece v
  toUrlPiece (SemVer.CGtEq v) = ">=" <> toUrlPiece v
  toUrlPiece (SemVer.CEq v) = toUrlPiece v
  toUrlPiece (SemVer.CAnd a b) = toUrlPiece a <> " " <> toUrlPiece b
  toUrlPiece (SemVer.COr a b) = toUrlPiece a <> " || " <> toUrlPiece b

instance FromHttpApiData SemVer.Version where
  parseUrlPiece = first pack . SemVer.fromText

instance FromHttpApiData SemVer.Constraint where
  parseUrlPiece = first pack . SemVer.Constraint.fromText

instance FromJSON SemVer.Version where
  parseJSON = withText "Version" $ either fail return . SemVer.fromText

instance Binary SemVer.Version where
  get = either fail return . SemVer.fromText =<< get
  put = put . SemVer.toText
