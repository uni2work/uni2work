-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.MultiSet.Instances
  (
  ) where

import ClassyPrelude
import Data.MultiSet


type instance Element (MultiSet a) = a
  
instance MonoFoldable (MultiSet a)
instance GrowingAppend (MultiSet a)
