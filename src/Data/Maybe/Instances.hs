-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Maybe.Instances
  (
  ) where

import ClassyPrelude

import Text.Blaze (ToMarkup(..), string)

instance ToMarkup a => ToMarkup (Maybe a) where
  toMarkup Nothing  = string ""
  toMarkup (Just x) = toMarkup x
