-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS -fno-warn-orphans #-}

module Data.Time.Format.Instances
  (
  ) where

import qualified Language.Haskell.TH.Syntax as TH

import Data.Time.Format

import Data.Time.LocalTime.Instances ()


deriving instance TH.Lift TimeLocale
