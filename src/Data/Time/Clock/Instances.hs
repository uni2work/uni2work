-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Time.Clock.Instances
  () where

import ClassyPrelude

import Database.Persist.Sql

import Data.Proxy

import Data.Time.Clock
import Data.Time.Clock.Instances.TH ()
import Data.Time.Calendar.Instances ()
import Web.PathPieces

import qualified Data.Csv as Csv

import Data.Time.Format.ISO8601

import Servant.Docs (ToSample(..), samples)

import qualified Language.Haskell.TH as TH
import qualified Language.Haskell.TH.Syntax as TH


instance Hashable DiffTime where
  hashWithSalt s = hashWithSalt s . toRational

instance Hashable NominalDiffTime where
  hashWithSalt s = hashWithSalt s . toRational

instance PersistField NominalDiffTime where
  toPersistValue   = toPersistValue . toRational
  fromPersistValue = fmap fromRational . fromPersistValue

instance PersistFieldSql NominalDiffTime where
  sqlType _ = sqlType (Proxy @Rational)


deriving instance Generic UTCTime
instance Hashable UTCTime

instance PathPiece UTCTime where
  toPathPiece = pack . iso8601Show
  fromPathPiece = iso8601ParseM . unpack

instance Csv.ToField UTCTime where
  toField = Csv.toField . iso8601Show

instance Csv.FromField UTCTime where
  parseField = iso8601ParseM <=< Csv.parseField


instance ToSample UTCTime where
  toSamples _ = samples $ do
    diff <- [0,172801..]
    sign <- [1, -1]
    return $ (sign * diff) `addUTCTime` now
    where now = $(TH.lift =<< TH.runIO getCurrentTime)
