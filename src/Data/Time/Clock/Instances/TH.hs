-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Time.Clock.Instances.TH
  () where

import ClassyPrelude

import Data.Time.Calendar.Instances ()

import qualified Language.Haskell.TH.Syntax as TH


instance TH.Lift UTCTime where
  liftTyped UTCTime{..} = [e||UTCTime $$(TH.liftTyped utctDay) $ fromRational $$(TH.liftTyped $ toRational utctDayTime)||]
