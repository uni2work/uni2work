-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Time.Calendar.Instances
  (
  ) where

import ClassyPrelude

import Data.Time.Calendar

import Data.Universe

import Language.Haskell.TH.Syntax (Lift)
import Type.Reflection


deriving instance Lift Day
instance Hashable Day where
  hashWithSalt s (ModifiedJulianDay jDay) = s `hashWithSalt` hash (typeRep @Day) `hashWithSalt` jDay

-- -- Available since time-1.11
-- deriving instance Ord DayOfWeek

instance Universe DayOfWeek where
  universe = [Monday .. Sunday]
instance Finite DayOfWeek
