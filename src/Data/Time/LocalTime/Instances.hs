-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Time.LocalTime.Instances
  (
  ) where

import ClassyPrelude

import Data.Time.LocalTime

import qualified Language.Haskell.TH.Syntax as TH


deriving instance Generic TimeOfDay
deriving instance Typeable TimeOfDay

instance Hashable TimeOfDay


deriving instance TH.Lift TimeZone
