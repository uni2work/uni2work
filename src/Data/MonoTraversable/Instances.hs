-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.MonoTraversable.Instances
  () where

import ClassyPrelude

import Data.Monoid (Any(..), All(..))


type instance Element Any = Bool
type instance Element All = Bool

instance MonoFunctor Any where
  omap f = Any . f . getAny

instance MonoFunctor All where
  omap f = All . f . getAll

instance MonoPointed Any where
  opoint = Any

instance MonoPointed All where
  opoint = All

instance MonoFoldable Any where
  ofoldMap f = f . getAny
  ofoldr f x (Any b) = f b x
  ofoldl' f x (Any b) = f x b
  ofoldr1Ex _ = getAny
  ofoldl1Ex' _ = getAny

instance MonoFoldable All where
  ofoldMap f = f . getAll
  ofoldr f x (All b) = f b x
  ofoldl' f x (All b) = f x b
  ofoldr1Ex _ = getAll
  ofoldl1Ex' _ = getAll
