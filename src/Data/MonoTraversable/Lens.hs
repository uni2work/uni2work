-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Data.MonoTraversable.Lens
  ( ofolded
  ) where

import ClassyPrelude
import Control.Lens

import Data.Functor.Contravariant (phantom)


ofolded :: MonoFoldable mono => Fold mono (Element mono)
ofolded f = phantom . ofoldr (\a fa -> f a *> fa) noEffect
  where noEffect = phantom $ pure ()
