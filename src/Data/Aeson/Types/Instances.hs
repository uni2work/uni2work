-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Aeson.Types.Instances
  (
  ) where

import ClassyPrelude

import Data.Aeson.Types (Parser, Value)
import Control.Monad.Catch

import Model.Types.TH.JSON (derivePersistFieldJSON)

import Control.Monad.Fail


instance MonadThrow Parser where
  throwM = fail . show


derivePersistFieldJSON ''Value
