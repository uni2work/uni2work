-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.HashSet.Instances
  () where

import ClassyPrelude

import Servant.Docs

import qualified Data.HashSet as HashSet

import Control.Lens
import Data.Proxy


instance (ToSample a, Hashable a, Eq a) => ToSample (HashSet a) where
  toSamples _ = over _2 HashSet.fromList <$> toSamples (Proxy @[a])
