-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Void.Instances
  (
  ) where

import ClassyPrelude.Yesod
import Data.Void

instance ToContent Void where
  toContent = absurd
instance ToTypedContent Void where
  toTypedContent = absurd

instance RenderMessage site Void where
  renderMessage _ _ = absurd
