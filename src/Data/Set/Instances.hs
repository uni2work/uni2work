-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Set.Instances
  (
  ) where

import ClassyPrelude

import qualified Data.Set as Set


instance (Ord a, Hashable a) => Hashable (Set a) where
  hashWithSalt s xs = hashWithSalt s $ Set.toAscList xs
