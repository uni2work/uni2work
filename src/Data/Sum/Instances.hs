-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Sum.Instances
  (
  ) where

import ClassyPrelude
import Data.Monoid (Sum(..))

import Text.Blaze (ToMarkup(..))

instance ToMarkup a => ToMarkup (Sum a) where
  toMarkup = toMarkup . getSum
