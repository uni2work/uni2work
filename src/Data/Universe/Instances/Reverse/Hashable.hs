-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Universe.Instances.Reverse.Hashable
  (
  ) where

import ClassyPrelude

import Data.Universe


instance (Hashable a, Hashable b, Finite a) => Hashable (a -> b) where
  hashWithSalt s f = s `hashWithSalt` [ (k, f k) | k <- universeF ]
