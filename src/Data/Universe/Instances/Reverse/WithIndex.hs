-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Universe.Instances.Reverse.WithIndex
  (
  ) where

import ClassyPrelude

import Data.Universe
import Control.Lens.Indexed

import Data.Universe.Instances.Reverse ()

import qualified Data.Map.Strict as Map


instance Finite a => FoldableWithIndex a ((->) a) where
  ifoldMap f g = fold [ f x (g x) | x <- universeF ]
instance (Ord a, Finite a) => TraversableWithIndex a ((->) a) where
  itraverse f g = (Map.!) . Map.fromList <$> sequenceA [ (x, ) <$> f x (g x) | x <- universeF ]
