-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Encoding.Instances
  (
  ) where

import ClassyPrelude
import Utils.PathPiece
import Text.Read

import Web.PathPieces

import Data.Encoding

import Control.Monad.Fail


instance PathPiece DynEncoding where
  toPathPiece = showToPathPiece
  fromPathPiece = encodingFromStringExplicit . unpack

pathPieceJSON ''DynEncoding


instance IsString DynEncoding where
  fromString = encodingFromString
instance Read DynEncoding where
  readPrec = parens $ lexP >>= \case
    Ident str -> maybe (fail "Could not parse encoding") return $ encodingFromStringExplicit str
    _ -> fail "Ident lexeme expected"


instance Ord DynEncoding where
  compare = comparing show

instance Hashable DynEncoding where
  hashWithSalt s = hashWithSalt s . show


instance NFData DynEncoding where
  rnf enc = rnf $ show enc
