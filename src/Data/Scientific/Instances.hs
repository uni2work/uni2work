-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Scientific.Instances
  (
  ) where

import ClassyPrelude
import Data.Scientific

import Web.PathPieces

import Text.ParserCombinators.ReadP (readP_to_S)

import Control.Monad.Fail


instance PathPiece Scientific where
  toPathPiece = pack . formatScientific Fixed Nothing

  fromPathPiece = disambiguate . readP_to_S scientificP . unpack
    where
      disambiguate strs = case filter (\(_, rStr) -> null rStr) strs of
        [(x, _)] -> pure x
        _other   -> fail "fromPathPiece Scientific: Ambiguous parse"
        
