-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Text.Blaze.Instances
  (
  ) where

import ClassyPrelude
import Text.Blaze
import qualified Text.Blaze.Renderer.Text as Text

import Text.Read (Read(..))

import Data.Aeson (ToJSON(..), FromJSON(..))
import qualified Data.Aeson as Aeson

import qualified Data.Csv as Csv

import Data.Binary (Binary(..))


instance Eq Markup where
  (==) = (==) `on` Text.renderMarkup

instance Ord Markup where
  compare = comparing Text.renderMarkup

instance Read Markup where
  readPrec = preEscapedLazyText <$> readPrec

instance Show Markup where
  showsPrec prec = showsPrec prec . Text.renderMarkup

instance Hashable Markup where
  hashWithSalt s = hashWithSalt s . Text.renderMarkup

instance ToJSON Markup where
  toJSON = Aeson.String . toStrict . Text.renderMarkup

instance FromJSON Markup where
  parseJSON = Aeson.withText "Html" $ return . preEscapedText

instance Csv.ToField Markup where
  toField = Csv.toField . Text.renderMarkup

instance Csv.FromField Markup where
  parseField = fmap preEscapedText . Csv.parseField

instance NFData Markup where
  rnf = rnf . Text.renderMarkup

instance Binary Markup where
  put = put . Text.renderMarkup
  get = preEscapedText <$> get
