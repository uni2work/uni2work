-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Text.Shakespeare.Text.Instances
  (
  ) where

import ClassyPrelude
import Text.Shakespeare.Text

import qualified Data.Text.Lazy.Builder as Builder

import Numeric.Natural (Natural)


instance ToText Natural where
  toText = Builder.fromText . tshow
