-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Database.Esqueleto.Instances
  (
  ) where

import ClassyPrelude.Yesod

import qualified Database.Esqueleto.Legacy as E

import Data.Binary (Binary)
import qualified Data.Binary as B


instance ToJSON a => ToJSON (E.Value a) where
  toJSON = toJSON . E.unValue

instance FromJSON a => FromJSON (E.Value a) where
  parseJSON = fmap E.Value . parseJSON


instance Binary a => Binary (E.Value a) where
  put = B.put . E.unValue
  get = E.Value <$> B.get
  putList = B.putList . map E.unValue
