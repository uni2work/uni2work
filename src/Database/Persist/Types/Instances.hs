-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Database.Persist.Types.Instances
  (
  ) where

import ClassyPrelude

import Database.Persist.Types

import Data.Time.Calendar.Instances ()
import Data.Time.LocalTime.Instances ()
import Data.Time.Clock.Instances ()
import Data.Binary.Instances.Time as Import ()

import Data.Binary (Binary)


deriving instance Generic LiteralType
deriving instance Typeable LiteralType

instance Hashable LiteralType
instance Binary LiteralType
instance NFData LiteralType

  
deriving instance Generic PersistValue
deriving instance Typeable PersistValue

instance Hashable PersistValue
instance Binary PersistValue
instance NFData PersistValue

instance (NFData record, NFData (Key record)) => NFData (Entity record) where
  rnf Entity{..} = rnf entityKey `seq` rnf entityVal

deriving instance Generic Checkmark
deriving anyclass instance NFData Checkmark
