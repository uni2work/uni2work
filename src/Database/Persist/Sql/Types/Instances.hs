-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Database.Persist.Sql.Types.Instances
  (
  ) where

import ClassyPrelude

import Database.Persist.Sql

import Data.Binary (Binary)


instance BackendCompatible SqlWriteBackend SqlWriteBackend where
  projectBackend = id

instance BackendCompatible SqlReadBackend SqlReadBackend where
  projectBackend = id

instance BackendCompatible SqlReadBackend SqlBackend where
  projectBackend = SqlReadBackend

instance BackendCompatible SqlWriteBackend SqlBackend where
  projectBackend = SqlWriteBackend

deriving newtype instance Binary (BackendKey SqlBackend)
deriving anyclass instance NFData (BackendKey SqlBackend)
