-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE UndecidableInstances, GeneralizedNewtypeDeriving #-}

module Database.Persist.Class.Instances
  (
  ) where

import ClassyPrelude

import Database.Persist.Class
import Database.Persist.Types.Instances ()
import Database.Persist.Sql

import Data.Binary (Binary)
import qualified Data.Binary as Binary
import Data.Binary.Instances.Time as Import ()

import qualified Data.Map as Map

import Data.Aeson (ToJSONKey, FromJSONKey)

import Control.Monad.Fail

import Servant.Docs (ToSample(..), samples)


instance PersistEntity record => Hashable (Key record) where
  hashWithSalt s = hashWithSalt s . toPersistValue

instance PersistEntity record => Binary (Key record) where
  put = Binary.put . toPersistValue
  putList = Binary.putList . map toPersistValue
  get = either (fail . unpack) return . fromPersistValue =<< Binary.get


uniqueToMap :: PersistEntity record => Unique record -> Map (FieldNameHS, FieldNameDB) PersistValue
uniqueToMap = fmap Map.fromList $ zip <$> fmap toList persistUniqueToFieldNames <*> persistUniqueToValues

instance PersistEntity record => Eq (Unique record) where
  (==) = (==) `on` uniqueToMap


deriving newtype instance ToJSONKey (BackendKey SqlBackend)
deriving newtype instance FromJSONKey (BackendKey SqlBackend)

instance ToSample (BackendKey SqlBackend) where
  toSamples _ = samples [0..]
