-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.PersonalisedSheetFiles
  ( dispatchJobPruneFallbackPersonalisedSheetFilesKeys
  ) where

import Import

import Database.Persist.Sql (deleteWhereCount)


dispatchJobPruneFallbackPersonalisedSheetFilesKeys :: JobHandler UniWorX
dispatchJobPruneFallbackPersonalisedSheetFilesKeys = JobHandlerAtomicWithFinalizer act fin
  where
    act = hoist lift $ do
      now <- liftIO getCurrentTime
      expires <- getsYesod $ view _appFallbackPersonalisedSheetFilesKeysExpire
      deleteWhereCount [ FallbackPersonalisedSheetFilesKeyGenerated <. addUTCTime (- expires) now ]
    fin n = $logInfoS "PruneFallbackPersonalisedSheetFilesKeys" [st|Deleted #{n} expired fallback personalised sheet files keys|]
