-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.SendNotification.CorrectionsNotDistributed
  ( dispatchNotificationCorrectionsNotDistributed
  ) where

import Import

import Handler.Utils.Mail

import Text.Hamlet
import qualified Data.CaseInsensitive as CI

dispatchNotificationCorrectionsNotDistributed :: SheetId -> UserId -> Handler ()
dispatchNotificationCorrectionsNotDistributed nSheet jRecipient = do
  (Course{..}, Sheet{..}, nbrSubs) <- runDB $ do
    sheet <- getJust nSheet
    course <- belongsToJust sheetCourse sheet
    nbrSubs <- count [ SubmissionSheet      ==. nSheet
                     , SubmissionRatingBy   ==. Nothing
                     ]
    return (course, sheet, nbrSubs)
  when (nbrSubs > 0) . userMailT jRecipient $ do
    replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
    setSubjectI $ MsgMailSubjectSubmissionsUnassigned courseShorthand sheetName
    MsgRenderer mr <- getMailMsgRenderer
    let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
        tid = courseTerm
        ssh = courseSchool
        csh = courseShorthand
        shn = sheetName

    addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/correctionsUndistributed.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
