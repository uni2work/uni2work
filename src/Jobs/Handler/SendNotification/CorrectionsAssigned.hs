-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-do-bind #-} -- ihamletFile discards do results

module Jobs.Handler.SendNotification.CorrectionsAssigned
  ( dispatchNotificationCorrectionsAssigned
  ) where

import Import

import Jobs.Handler.SendNotification.Utils
import Handler.Utils.Mail

import Text.Hamlet
import qualified Data.CaseInsensitive as CI

dispatchNotificationCorrectionsAssigned :: UserId -> SheetId -> UserId -> Handler ()
dispatchNotificationCorrectionsAssigned  nUser nSheet jRecipient = do
  (Course{..}, Sheet{..}, nbrSubs) <- runDB $ do
    sheet <- getJust nSheet
    course <- belongsToJust sheetCourse sheet
    nbrSubs <- count [ SubmissionSheet      ==. nSheet
                     , SubmissionRatingBy   ==. Just nUser
                     , SubmissionRatingTime ==. Nothing
                     ]
    return (course, sheet, nbrSubs)
  when (nbrSubs > 0) . userMailT jRecipient $ do
    replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
    setSubjectI $ MsgMailSubjectCorrectionsAssigned courseShorthand sheetName

    MsgRenderer mr <- getMailMsgRenderer
    let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm

    editNotifications <- mkEditNotifications jRecipient

    addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/correctionsAssigned.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
