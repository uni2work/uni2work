-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-do-bind #-} -- ihamletFile discards do results

module Jobs.Handler.SendNotification.UserAuthModeUpdate
  ( dispatchNotificationUserAuthModeUpdate
  ) where

import Import

import Auth.PWHash (PWHashMessage(..))

import Handler.Utils.Mail
import Jobs.Handler.SendNotification.Utils

import Text.Hamlet
-- import qualified Data.CaseInsensitive as CI

dispatchNotificationUserAuthModeUpdate :: UserId -> AuthenticationMode -> UserId -> Handler ()
dispatchNotificationUserAuthModeUpdate  nUser _nOriginalAuthMode jRecipient = userMailT jRecipient $ do
    User{..} <- liftHandler . runDB $ getJust nUser
    replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
    setSubjectI MsgMailSubjectUserAuthModeUpdate

    editNotifications <- ihamletSomeMessage <$> mkEditNotifications jRecipient

    addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/userAuthModeUpdate.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))

