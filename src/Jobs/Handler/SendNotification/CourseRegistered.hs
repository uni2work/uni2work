-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-do-bind #-} -- ihamletFile discards do results

module Jobs.Handler.SendNotification.CourseRegistered
  ( dispatchNotificationCourseRegistered
  ) where

import Import

import Handler.Utils.Mail
import Jobs.Handler.SendNotification.Utils

import Text.Hamlet
import qualified Data.CaseInsensitive as CI

dispatchNotificationCourseRegistered :: UserId -> CourseId -> UserId -> Handler ()
dispatchNotificationCourseRegistered nUser nCourse jRecipient = userMailT jRecipient $ do
  (User{..}, Course{..}) <- liftHandler . runDB $ (,) <$> getJust nUser <*> getJust nCourse

  let isSelf = nUser == jRecipient

  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ if
    | isSelf    -> MsgMailSubjectCourseRegistered courseShorthand
    | otherwise -> MsgMailSubjectCourseRegisteredOther userDisplayName courseShorthand

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
      tid = courseTerm
      ssh = courseSchool
      csh = courseShorthand

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/courseRegistered.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
