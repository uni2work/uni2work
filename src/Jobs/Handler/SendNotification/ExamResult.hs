-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-do-bind #-} -- ihamletFile discards do results

module Jobs.Handler.SendNotification.ExamResult
  ( dispatchNotificationExamResult
  ) where

import Import

import Handler.Utils.Mail
import Jobs.Handler.SendNotification.Utils

import Text.Hamlet
import qualified Data.CaseInsensitive as CI

dispatchNotificationExamResult :: ExamId -> UserId -> Handler ()
dispatchNotificationExamResult nExam jRecipient = userMailT jRecipient $ do
  (Course{..}, Exam{..}) <- liftHandler . runDB $ do
    exam <- getJust nExam
    course <- belongsToJust examCourse exam
    return (course, exam)
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ MsgMailSubjectExamResult courseShorthand examName

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
      tid = courseTerm
      ssh = courseSchool
      csh = courseShorthand
      examn = examName

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/examResult.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
