-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.SendNotification.Utils
  ( mkEditNotifications
  , ihamletSomeMessage
  ) where

import Import

import Text.Hamlet

import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HashMap


ihamletSomeMessage :: HtmlUrlI18n (SomeMessage UniWorX) route -> HtmlUrlI18n (SomeMessage UniWorX) route
ihamletSomeMessage f trans = f $ trans . SomeMessage

mkEditNotifications :: (MonadHandler m, HandlerSite m ~ UniWorX) => UserId -> m (HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
mkEditNotifications uid = liftHandler $ do
  cID <- encrypt uid
  jwt <- encodeBearer =<< bearerToken (HashSet.singleton $ Right uid) Nothing (HashMap.singleton BearerTokenRouteEval . HashSet.singleton $ UserNotificationR cID) Nothing Nothing Nothing
  let
    editNotificationsUrl :: SomeRoute UniWorX
    editNotificationsUrl = SomeRoute (UserNotificationR cID, [(toPathPiece GetBearer, toPathPiece jwt)])
  editNotificationsUrl' <- toTextUrl editNotificationsUrl
  return ($(ihamletFile "templates/mail/editNotifications.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
 