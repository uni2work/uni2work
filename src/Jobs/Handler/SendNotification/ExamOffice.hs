-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-do-bind #-} -- ihamletFile discards do results

module Jobs.Handler.SendNotification.ExamOffice
  ( dispatchNotificationExamOfficeExamResults
  , dispatchNotificationExamOfficeExamResultsChanged
  , dispatchNotificationExamOfficeExternalExamResults
  ) where

import Import

import Handler.Utils.Mail
import Jobs.Handler.SendNotification.Utils

import Text.Hamlet
import qualified Data.CaseInsensitive as CI

import qualified Data.Set as Set


dispatchNotificationExamOfficeExamResults :: ExamId -> UserId -> Handler ()
dispatchNotificationExamOfficeExamResults nExam jRecipient = userMailT jRecipient $ do
  (Course{..}, Exam{..}) <- liftHandler . runDB $ do
    exam <- getJust nExam
    course <- belongsToJust examCourse exam
    return (course, exam)
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ MsgMailSubjectExamOfficeExamResults courseName examName

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
      tid = courseTerm
      ssh = courseSchool
      csh = courseShorthand
      examn = examName

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/examOffice/examResults.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))

dispatchNotificationExamOfficeExamResultsChanged :: Set ExamResultId -> UserId -> Handler ()
dispatchNotificationExamOfficeExamResultsChanged nExamResults jRecipient = do
  entitiesExamResults <- runDB $ selectList [ ExamResultId <-. Set.toList nExamResults ] []
  let exams = Set.fromList $ map (examResultExam . entityVal) entitiesExamResults

  forM_ exams $ \nExam -> userMailT jRecipient $ do
    (Course{..}, Exam{..}) <- liftHandler . runDB $ do
      exam <- getJust nExam
      course <- belongsToJust examCourse exam
      return (course, exam)
    replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
    setSubjectI $ MsgMailSubjectExamOfficeExamResultsChanged courseName examName

    MsgRenderer mr <- getMailMsgRenderer
    let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
        tid = courseTerm
        ssh = courseSchool
        csh = courseShorthand
        examn = examName

    editNotifications <- mkEditNotifications jRecipient

    addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/examOffice/examResultsChanged.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))


dispatchNotificationExamOfficeExternalExamResults :: ExternalExamId -> UserId -> Handler ()
dispatchNotificationExamOfficeExternalExamResults nExternalExam jRecipient = userMailT jRecipient $ do
  ExternalExam{..} <- liftHandler . runDB $ getJust nExternalExam
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ MsgMailSubjectExamOfficeExternalExamResults externalExamCourseName externalExamExamName

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey externalExamTerm
      tid = externalExamTerm
      ssh = externalExamSchool
      coursen = externalExamCourseName
      examn = externalExamExamName

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/examOffice/externalExamResults.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
