-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-do-bind #-} -- ihamletFile discards do results

module Jobs.Handler.SendNotification.SheetActive
  ( dispatchNotificationSheetActive
  , dispatchNotificationSheetHint
  , dispatchNotificationSheetSolution
  ) where

import Import

import Handler.Utils.Mail
import Jobs.Handler.SendNotification.Utils

import Text.Hamlet
import qualified Data.CaseInsensitive as CI

dispatchNotificationSheetActive, dispatchNotificationSheetHint, dispatchNotificationSheetSolution :: SheetId -> UserId -> Handler ()
dispatchNotificationSheetActive nSheet jRecipient = userMailT jRecipient $ do
  (Course{..}, Sheet{..}) <- liftHandler . runDB $ do
    sheet <- getJust nSheet
    course <- belongsToJust sheetCourse sheet
    return (course, sheet)
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ MsgMailSubjectSheetActive courseShorthand sheetName

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
      tid = courseTerm
      ssh = courseSchool
      csh = courseShorthand
      shn = sheetName

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/sheetActive.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
dispatchNotificationSheetHint nSheet jRecipient = userMailT jRecipient $ do
  (Course{..}, Sheet{..}) <- liftHandler . runDB $ do
    sheet <- getJust nSheet
    course <- belongsToJust sheetCourse sheet
    return (course, sheet)
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ MsgMailSubjectSheetHint courseShorthand sheetName

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
      tid = courseTerm
      ssh = courseSchool
      csh = courseShorthand
      shn = sheetName

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/sheetHint.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
dispatchNotificationSheetSolution nSheet jRecipient = userMailT jRecipient $ do
  (Course{..}, Sheet{..}) <- liftHandler . runDB $ do
    sheet <- getJust nSheet
    course <- belongsToJust sheetCourse sheet
    return (course, sheet)
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI $ MsgMailSubjectSheetSolution courseShorthand sheetName

  MsgRenderer mr <- getMailMsgRenderer
  let termDesc = mr . ShortTermIdentifier $ unTermKey courseTerm
      tid = courseTerm
      ssh = courseSchool
      csh = courseShorthand
      shn = sheetName

  editNotifications <- mkEditNotifications jRecipient

  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/sheetSolution.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
