-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.SendPasswordReset
  ( dispatchJobSendPasswordReset
  ) where

import Import

import Handler.Utils
import Handler.Utils.Users

import qualified Data.ByteString.Base64 as Base64
import qualified Data.ByteArray as BA
import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HashMap

import Text.Hamlet

dispatchJobSendPasswordReset :: UserId
                             -> JobHandler UniWorX
dispatchJobSendPasswordReset jRecipient = JobHandlerException . userMailT jRecipient $ do
  cID <- encrypt jRecipient
  User{..} <- liftHandler . runDB $ getJust jRecipient

  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI MsgMailSubjectPasswordReset

  now <- liftIO getCurrentTime
  let
    localNow = utcToLocalTime now
    tomorrowEndOfDay = case localTimeToUTC (LocalTime (addDays 2 $ localDay localNow) midnight) of
      LTUUnique utc' _ -> utc'
      _other           -> UTCTime (addDays 2 $ utctDay now) 0

  resetBearer' <- bearerToken (HashSet.singleton $ Right jRecipient) Nothing (HashMap.singleton BearerTokenRouteEval . HashSet.singleton $ UserPasswordR cID) Nothing (Just $ Just tomorrowEndOfDay) Nothing
  let resetBearer = resetBearer'
        & bearerRestrict (UserPasswordR cID) (decodeUtf8 . Base64.encode . BA.convert $ computeUserAuthenticationDigest userAuthentication)
  encodedBearer <- encodeBearer resetBearer
   
  resetUrl <- toTextUrl (UserPasswordR cID, [(toPathPiece GetBearer, toPathPiece encodedBearer)])
  activeTime <- formatTimeMail SelFormatDateTime tomorrowEndOfDay
  addHtmlMarkdownAlternatives ($(ihamletFile "templates/mail/passwordReset.hamlet") :: HtmlUrlI18n (SomeMessage UniWorX) (Route UniWorX))
