-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.ExternalApis
  ( dispatchJobExternalApiExpire
  ) where

import Import


dispatchJobExternalApiExpire :: ExternalApiId -> JobHandler UniWorX
dispatchJobExternalApiExpire apiId = JobHandlerAtomic $ do
  now <- liftIO getCurrentTime
  expiry <- getsYesod $ view _appExternalApisExpiry
  void . runMaybeT $ do
    ExternalApi{..} <- MaybeT $ get apiId
    guard $ externalApiLastAlive <= addUTCTime (- expiry) now
    lift $ delete apiId
