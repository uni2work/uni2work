-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.PruneInvitations
  ( dispatchJobPruneInvitations
  ) where

import Import

import Database.Persist.Sql (deleteWhereCount)

dispatchJobPruneInvitations :: JobHandler UniWorX
dispatchJobPruneInvitations = JobHandlerAtomicWithFinalizer act fin
  where
    act = hoist lift $ do
      now <- liftIO getCurrentTime
      deleteWhereCount [ InvitationExpiresAt <. Just now ]
    fin n = $logInfoS "PruneInvitations" [st|Deleted #{n} expired invitations|]
