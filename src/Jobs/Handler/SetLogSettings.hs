-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.SetLogSettings
  ( dispatchJobSetLogSettings
  ) where

import Import

dispatchJobSetLogSettings :: InstanceId -> LogSettings -> JobHandler UniWorX
dispatchJobSetLogSettings jInstance jLogSettings = JobHandlerException $ do
  instanceId <- getsYesod appInstanceID
  unless (instanceId == jInstance) . liftIO $ fail "Incorrect instance"
  lSettings <- getsYesod appLogSettings
  atomically $ writeTVar lSettings jLogSettings
