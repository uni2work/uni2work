-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.DistributeCorrections
  ( dispatchJobDistributeCorrections
  ) where

import Import

import Jobs.Queue

import Handler.Utils.Submission

import qualified Data.Set as Set


dispatchJobDistributeCorrections :: SheetId
                                 -> JobHandler UniWorX
dispatchJobDistributeCorrections jSheet = JobHandlerAtomic $ do
  unassigned <- runMaybeT . catchMaybeT (Proxy @AssignSubmissionException) . fmap (view _2) . hoist lift $ assignSubmissions jSheet Nothing
  unless (maybe False Set.null unassigned) $
    queueDBJob . JobQueueNotification $ NotificationCorrectionsNotDistributed jSheet
