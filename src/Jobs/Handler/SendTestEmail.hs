-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jobs.Handler.SendTestEmail
  ( dispatchJobSendTestEmail
  ) where

import Import

import Handler.Utils.Mail
import Handler.Utils.DateTime

dispatchJobSendTestEmail :: Email -> MailContext -> JobHandler UniWorX
dispatchJobSendTestEmail jEmail jMailContext = JobHandlerException . mailT jMailContext $ do
  _mailTo .= [Address Nothing jEmail]
  replaceMailHeader "Auto-Submitted" $ Just "auto-generated"
  setSubjectI MsgMailTestSubject
  now <- liftIO getCurrentTime
  nDT <- formatTimeMail SelFormatDateTime now
  nD <- formatTimeMail SelFormatDate now
  nT <- formatTimeMail SelFormatTime now
  addHtmlMarkdownAlternatives $ \(MsgRenderer mr) -> [shamlet|
    <p>
      #{mr MsgMailTestContent}

    <p>
      #{mr MsgMailTestDateTime}
      <ul>
        <li>#{nDT}
        <li>#{nD}
        <li>#{nT}
    |]
