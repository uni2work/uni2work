-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Language.Haskell.TH.Instances
  (
  ) where

import ClassyPrelude

import Language.Haskell.TH
-- import Language.Haskell.TH.Syntax
import Language.Haskell.TH.Lift (deriveLift)
-- import Data.Binary (Binary)

import GHCi.TH.Binary ()

-- instance Binary Loc

deriveLift ''Loc


-- instance Binary OccName
-- instance Binary ModName
-- instance Binary NameSpace
-- instance Binary PkgName
-- instance Binary NameFlavour
-- instance Binary Name


instance Semigroup (Q [Dec]) where
  (<>) = liftA2 (<>)

instance Monoid (Q [Dec]) where
  mempty  = pure mempty
  mappend = (<>)
