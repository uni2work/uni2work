-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Yesod.Form.Types.Instances
  () where

import Yesod.Form.Types

import Data.Default


instance Default (FieldSettings site) where
  def = ""
