-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Settings.Mime
  ( mimeMap
  , mimeLookup
  , mimeExtensions
  , archiveTypes, videoTypes
  , module Network.Mime
  ) where

import ClassyPrelude

import qualified Data.Map as Map
import qualified Data.Set as Set

import Network.Mime
  ( FileName, MimeType, MimeMap, Extension
  , mimeByExt, defaultMimeType
  )
import Network.Mime.TH


mimeMap :: MimeMap
mimeMap = $(mimeMapFile "config/mimetypes")

mimeLookup :: FileName -> MimeType
mimeLookup = mimeByExt mimeMap defaultMimeType

mimeExtensions :: MimeType -> Set Extension
mimeExtensions needle = Set.fromList [ ext | (ext, typ) <- Map.toList mimeMap, typ == needle ]

archiveTypes, videoTypes :: Set MimeType
archiveTypes = $(mimeSetFile "config/archive-types")
videoTypes = $(mimeSetFile "config/video-types")
