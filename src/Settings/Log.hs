-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Settings.Log
  ( LogSettings(..)
  , LogDestination(..)
  , LogLevel(..)
  , ReadLogSettings(..)
  ) where

import ClassyPrelude.Yesod
import Numeric.Natural

import Data.Aeson.TH
import Utils.PathPiece


data LogSettings = LogSettings
  { logAll, logDetailed :: Bool
  , logMinimumLevel :: LogLevel
  , logDestination :: LogDestination
  , logSerializableTransactionRetryLimit :: Maybe Natural
  } deriving (Show, Read, Generic, Eq, Ord)

data LogDestination = LogDestStderr | LogDestStdout | LogDestFile { logDestFile :: !FilePath }
  deriving (Show, Read, Generic, Eq, Ord)

deriving instance Generic LogLevel
instance Hashable LogLevel
instance NFData LogLevel
instance Hashable LogSettings
instance NFData LogSettings
instance Hashable LogDestination
instance NFData LogDestination

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  , sumEncoding = UntaggedValue
  } ''LogLevel

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 2
  , fieldLabelModifier = camelToPathPiece' 2
  , sumEncoding = UntaggedValue
  , unwrapUnaryRecords = True
  } ''LogDestination

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''LogSettings

class ReadLogSettings m where
  readLogSettings :: m LogSettings
