-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Settings.WellKnownFiles
  ( WellKnownFileName(..)
  , getWellKnownR
  , wellKnownHtmlLinks
  ) where

import Settings.WellKnownFiles.TH

import Settings (appWellKnownDir, appWellKnownLinkFile, compileTimeAppSettings)

mkWellKnown "de-de-formal" (appWellKnownDir compileTimeAppSettings) (appWellKnownLinkFile compileTimeAppSettings)
