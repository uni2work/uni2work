-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Settings.Locale
  ( getTimeLocale'
  , appTZ
  , appLanguages
  ) where

import Utils.DateTime

import Data.List.NonEmpty

import Text.Shakespeare.I18N (Lang)
  

getTimeLocale' :: [Lang] -> TimeLocale
getTimeLocale' = $(timeLocaleMap [("de-de", "de_DE.utf8"), ("en-GB", "en_GB.utf8")])

appTZ :: TZ
appTZ = $(includeSystemTZ "Europe/Berlin")

appLanguages :: NonEmpty Lang
appLanguages = "de-de-formal" :| ["en-eu"]
