-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
-- Listing only files directly used by consumers of this module
-- prevents rebuilds if files change, that are not directly used (like
-- webpack bundles)
module Settings.StaticFiles
  ( img_lmu_sigillum_svg
  , webpackLinks_main
  , embeddedStatic
  , module Yesod.EmbeddedStatic
  ) where

import ClassyPrelude.Yesod

import Settings     (appStaticDir, appWebpackEntrypoints, compileTimeAppSettings)
import Settings.StaticFiles.Generator
import Settings.StaticFiles.Webpack
import Yesod.EmbeddedStatic

-- This generates easy references to files in the static directory at compile time,
-- giving you compile-time verification that referenced files exist.
-- Warning: any files added to your static directory during run-time can't be
-- accessed this way.
--
-- For example, to refer to @static/js/script.js@ via an identifier, you'd use:
--
--     js_script_js

#ifdef DEVELOPMENT
#define DEV_BOOL True
#else
#define DEV_BOOL False
#endif

mkEmbeddedStatic DEV_BOOL "embeddedStatic" . pure . staticGenerator $ appStaticDir compileTimeAppSettings
mkWebpackEntrypoints (appWebpackEntrypoints compileTimeAppSettings) (pure staticGenerator) $ appStaticDir compileTimeAppSettings
