-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Settings.TH
  ( embedSemVerFile
  ) where

import Import.NoModel hiding (lift)

import qualified Data.SemVer as SemVer

import qualified Data.Text.Encoding as T
import qualified Data.ByteString as BS

import Language.Haskell.TH.Syntax
import Language.Haskell.TH.Lib


embedSemVerFile :: FilePath -> Q Exp
embedSemVerFile fp = do
    qAddDependentFile fp
    t <- runIO $ T.decodeUtf8 <$> BS.readFile fp
    either fail (const $ return ()) $ SemVer.fromText t
    [e|either error id|] `appE` ([e|SemVer.fromText|] `appE` lift t)
