-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.Upload
  ( UploadNonce, newUploadNonce
  ) where

import Import.NoModel
import Model.Types.TH.PathPiece

import qualified Crypto.Nonce as Nonce
import System.IO.Unsafe


newtype UploadNonce = UploadNonce Text
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving newtype (ToJSON, FromJSON, ToJSONKey, FromJSONKey, PathPiece)
makeWrapped ''UploadNonce

derivePersistFieldPathPiece ''UploadNonce

uploadNonceGen :: Nonce.Generator
uploadNonceGen = unsafePerformIO Nonce.new
{-# NOINLINE uploadNonceGen #-}

newUploadNonce :: MonadIO m => m UploadNonce
newUploadNonce = review _Wrapped <$> Nonce.nonce128urlT uploadNonceGen
