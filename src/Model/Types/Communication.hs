-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.Communication
  ( CommunicationContent(..), _ccSubject, _ccBody, _ccAttachments
  ) where

import Import.NoModel
import Model.Types.File

import Utils.Lens.TH


data CommunicationContent = CommunicationContent
  { ccSubject :: Maybe Text
  , ccBody :: Html
  , ccAttachments :: Set FileReference
  } deriving stock (Eq, Ord, Show, Read, Generic, Typeable)
    deriving anyclass (Hashable, NFData)

deriveJSON  defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  } ''CommunicationContent
makeLenses_ ''CommunicationContent
