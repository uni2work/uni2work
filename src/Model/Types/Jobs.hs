-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.Jobs where

import Import.NoModel

import Database.Persist.Sql (PersistFieldSql(..))

import Data.ByteArray (ByteArrayAccess)

import Web.HttpApiData (ToHttpApiData, FromHttpApiData)


newtype JobContentReference = JobContentReference (Digest SHA3_512)
  deriving (Eq, Ord, Read, Show, Lift, Generic, Typeable)
  deriving newtype ( PersistField
                   , PathPiece, ToHttpApiData, FromHttpApiData, ToJSON, FromJSON
                   , Hashable, NFData
                   , ByteArrayAccess
                   , Binary
                   )

instance PersistFieldSql JobContentReference where
  sqlType _ = sqlType $ Proxy @(Digest SHA3_512)

makeWrapped ''JobContentReference
