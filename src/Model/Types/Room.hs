-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.Room
  ( RoomReference(..)
  , RoomReference'(..), classifyRoomReference
  ) where

import Import.NoModel
import Model.Types.Markup

import Data.Text.Lens (unpacked)


data RoomReference
  = RoomReferenceSimple { roomRefText :: Text }
  | RoomReferenceLink
    { roomRefLink :: URI
    , roomRefInstructions :: Maybe StoredMarkup
    }
  deriving (Eq, Ord, Show, Generic, Typeable)
  deriving anyclass (NFData)

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 2
  , fieldLabelModifier = camelToPathPiece' 2
  , omitNothingFields = True
  } ''RoomReference
derivePersistFieldJSON ''RoomReference

instance IsString RoomReference where
  fromString = RoomReferenceSimple . pack


data RoomReference' = RoomReferenceSimple' | RoomReferenceLink'
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite)

nullaryPathPiece ''RoomReference' $ camelToPathPiece' 2 . over unpacked (dropSuffix "'")

classifyRoomReference :: RoomReference -> RoomReference'
classifyRoomReference = \case
  RoomReferenceSimple{} -> RoomReferenceSimple'
  RoomReferenceLink{}   -> RoomReferenceLink'
