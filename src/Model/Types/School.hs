-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.School where

import Import.NoModel
import Model.Types.TH.PathPiece

import Database.Persist.Sql (PersistFieldSql(..))
import Web.HttpApiData (ToHttpApiData, FromHttpApiData)
import Data.ByteArray (ByteArrayAccess)

import qualified Crypto.Hash as Crypto
import qualified Data.Binary as Binary

import Model.Types.Markup


data SchoolFunction
  = SchoolAdmin
  | SchoolLecturer
  | SchoolEvaluation
  | SchoolExamOffice
  | SchoolAllocation
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite, Hashable, NFData)

nullaryPathPiece ''SchoolFunction $ camelToPathPiece' 1
pathPieceJSON ''SchoolFunction
pathPieceJSONKey ''SchoolFunction
derivePersistFieldPathPiece ''SchoolFunction
pathPieceBinary ''SchoolFunction

data SchoolAuthorshipStatementMode
  = SchoolAuthorshipStatementModeNone
  | SchoolAuthorshipStatementModeOptional
  | SchoolAuthorshipStatementModeRequired
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)

finitePathPiece ''SchoolAuthorshipStatementMode [ "no-statement", "optional", "required" ] -- avoid @none@ since it does not play nice with yesod-form (`selectField` etc.)
pathPieceJSON ''SchoolAuthorshipStatementMode
pathPieceJSONKey ''SchoolAuthorshipStatementMode
derivePersistFieldPathPiece ''SchoolAuthorshipStatementMode
pathPieceBinary ''SchoolAuthorshipStatementMode
pathPieceHttpApiData ''SchoolAuthorshipStatementMode

newtype AuthorshipStatementReference = AuthorshipStatementReference (Digest SHA3_512)
  deriving (Eq, Ord, Read, Show, Lift, Generic, Typeable)
  deriving newtype ( PersistField
                   , PathPiece, ToHttpApiData, FromHttpApiData, ToJSON, FromJSON
                   , Hashable, NFData
                   , ByteArrayAccess
                   , Binary
                   )

instance PersistFieldSql AuthorshipStatementReference where
  sqlType _ = sqlType $ Proxy @(Digest SHA3_512)

makeWrapped ''AuthorshipStatementReference

toAuthorshipStatementReference :: I18nStoredMarkup -> AuthorshipStatementReference
toAuthorshipStatementReference = review _Wrapped . Crypto.hashlazy . Binary.encode
