-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE UndecidableInstances #-}

module Model.Types.Workflow.Overviews
  ( WorkflowOverviewSpec(..)
  , WorkflowOverviewColumnSpec(..), _WOCSWorkflowWorkflowId, _WOCSWorkflowWorkflowState, _WOCSWorkflowWorkflowLastActionTime, _WOCSWorkflowWorkflowLastActionUser, _WOCSWorkflowWorkflowCurrentPayload, _wocsPayloadLabel
  , WorkflowOverviewReference(..)
  ) where

import Import.NoModel

import Data.Aeson (genericToJSON, genericParseJSON)
import qualified Data.SafeJSON as SafeJSON

import Database.Persist.Sql (PersistFieldSql(..))
import Web.HttpApiData (ToHttpApiData, FromHttpApiData)
import Data.ByteArray (ByteArrayAccess)

import Model.Types.Workflow.Workflow

import Utils.Lens.TH


data WorkflowOverviewSpec userid = WorkflowOverviewSpec
  { wosViewers :: NonNull (Set (WorkflowRole userid))
  , wosColumns :: NonEmpty WorkflowOverviewColumnSpec
  } deriving (Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowOverviewColumnSpec
  = WOCSWorkflowWorkflowId
  | WOCSWorkflowWorkflowState
  | WOCSWorkflowWorkflowLastActionTime
  | WOCSWorkflowWorkflowLastActionUser
  | WOCSWorkflowWorkflowCurrentPayload { wocsPayloadLabel :: WorkflowPayloadLabel, wocsHeader :: I18nText }
  deriving (Generic, Typeable)
  deriving anyclass (NFData)


deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 1
  } ''WorkflowOverviewColumnSpec

instance (ToJSON userid, Ord userid) => ToJSON (WorkflowOverviewSpec userid) where
  toJSON = genericToJSON workflowOverviewSpecAesonOptions

instance (FromJSON userid, Ord userid) => FromJSON (WorkflowOverviewSpec userid) where
  parseJSON = genericParseJSON workflowOverviewSpecAesonOptions

instance (ToJSON userid, FromJSON userid, Ord userid) => SafeJSON (WorkflowOverviewSpec userid) where
  typeName = SafeJSON.typeName1

  kind = SafeJSON.base
  version = 0


newtype WorkflowOverviewReference = WorkflowOverviewReference (Digest SHA3_256)
  deriving (Eq, Ord, Read, Show, Lift, Generic, Typeable)
  deriving newtype ( PersistField
                   , PathPiece, ToHttpApiData, FromHttpApiData, ToJSON, FromJSON
                   , ByteArrayAccess
                   , Binary
                   )
  deriving anyclass (Hashable, NFData)

instance PersistFieldSql WorkflowOverviewReference where
  sqlType _ = sqlType $ Proxy @(Digest SHA3_256)


derivePersistFieldSafeJSON ''WorkflowOverviewSpec

makeWrapped ''WorkflowOverviewReference
makePrisms ''WorkflowOverviewColumnSpec
makeLenses_ ''WorkflowOverviewColumnSpec
