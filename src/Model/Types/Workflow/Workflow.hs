-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE UndecidableInstances #-}

module Model.Types.Workflow.Workflow
  ( WorkflowGraph(..), WorkflowGraphReference(..)
  , WorkflowGraphStageLabel
  , WorkflowGraphStage(..)
  , WorkflowGraphSubstageMode(..), WorkflowGraphSubstageShowWhen(..)
  , WorkflowGraphSubstage(..)
  , WorkflowGraphNodeLabel
  , WorkflowGraphNode(..)
  , WorkflowNodeView(..)
  , WorkflowNodeMessage(..)
  , WorkflowGraphEdgeLabel
  , WorkflowGraphEdge(..)
  , WorkflowEdgeMessage(..)
  , WorkflowGraphRestriction(..)
  , WorkflowGraphEdgeFormOrder
  , WorkflowGraphEdgeForm(..)
  , WorkflowRole(..)
  , WorkflowPayloadView(..)
  , WorkflowPayloadSpec(..), _WorkflowPayloadSpec
  , WorkflowPayloadFieldReference
  , WorkflowPayloadTimeCapture, WorkflowPayloadTimeCapturePrecision(..)
  , WorkflowPayloadTextPreset(..)
  , WorkflowPayloadTextType(..)
  , WorkflowPayloadField(..)
  , WorkflowScope(..)
  , WorkflowScope'(..), classifyWorkflowScope
  , WorkflowPayloadLabel(..)
  , WorkflowActionUser(..)
  , WorkflowStateIndex(..) -- , workflowStateIndex, workflowStateSection
  -- , WorkflowState
  -- , WorkflowActionInfo(..), workflowActionInfos
  -- , WorkflowAction(..), _wpTo, _wpVia, _wpPayload, _wpUser, _wpTime
  , WorkflowFieldPayloadW(..), _WorkflowFieldPayloadW, IsWorkflowFieldPayload', IsWorkflowFieldPayload, WorkflowFieldPayloads
  , workflowPayloadSort
  , WorkflowFieldPayload(..), _WorkflowFieldPayload
  , WorkflowFieldPayload'(..)
  -- , workflowStatePayload, workflowStateCurrentPayloads
  , WorkflowChildren
  , WorkflowWorkflowListType(..), workflowWorkflowListTypeNext
  , WorkflowGraphAuth(..), WorkflowGraphAuthRole(..), WorkflowGraphAuthExpression(..), WorkflowGraphAuthAction(..), WorkflowGraphAuthActionPredicate(..), WorkflowGraphAuthActionReference(..), WorkflowGraphAuthActionPredicate'(..), WorkflowGraphAuthPayloadPredicate(..), WorkflowGraphAuthPayloadReference(..), WorkflowGraphAuthPayloadPredicate'(..)
  , _WorkflowGraphAuthTrue, _WorkflowGraphAuthHasAction, _WorkflowGraphAuthHasPayload, _WorkflowGraphAuthOr, _WorkflowGraphAuthRoleAny, _WorkflowGraphAuthRoleUser, _WorkflowGraphAuthRolePayloadReference, _WorkflowGraphAuthRoleAuthorized, _WorkflowGraphAuthRoleInitiator
  , _WorkflowGraphAuthActionReferencePayload
  , _WorkflowGraphAuthViewWorkflowWorkflow, _WorkflowGraphAuthViewWorkflowWorkflowAction, _WorkflowGraphAuthViewWorkflowWorkflowActor, _WorkflowGraphAuthViewWorkflowWorkflowPayload, _WorkflowGraphAuthHasAllowedEdge, _WorkflowGraphAuthHasAllowedInitialEdge
  , mkWorkflowGraphAuth
  , mkWorkflowGraphAuthUnoptimized, optimizeWorkflowGraphAuth
  ) where

import Import.NoModel

import Model.Types.Security
import Model.Types.File (FileContentReference, FileFieldUserOption, FileField, _fieldAdditionalFiles, FileReferenceTitleMapConvertible(..), FileReferenceTitleMap)

import Database.Persist.Sql (PersistFieldSql(..))
import Web.HttpApiData (ToHttpApiData, FromHttpApiData)
import Data.ByteArray (ByteArrayAccess)

import Data.Maybe (fromJust)

import Data.Aeson (genericToJSON, genericParseJSON)
import qualified Data.Aeson as JSON
import qualified Data.Aeson.Types as JSON
import qualified Data.Aeson.Encoding as JSON
import Data.Aeson.Lens (_Null)
import Data.Aeson.Types (Parser)

import qualified Data.Set as Set
import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.CaseInsensitive as CI

import Type.Reflection (eqTypeRep, typeRep, typeOf, (:~~:)(..))
import Data.Typeable (cast)

import Data.Generics.Product.Types

import Unsafe.Coerce

import Utils.Lens.TH

import Data.RFC5051 (compareUnicode)

import qualified Data.Binary as Binary


-----  WORKFLOW GRAPH  -----

data WorkflowGraph fileid userid = WorkflowGraph
  { wgStages :: [WorkflowGraphStage]
  , wgNodes  :: Map WorkflowGraphNodeLabel (WorkflowGraphNode fileid userid)
  }
  deriving (Generic, Typeable)

deriving instance (Eq fileid, Eq userid, Typeable fileid, Typeable userid, Eq (FileField fileid)) => Eq (WorkflowGraph fileid userid)
deriving instance (Ord fileid, Ord userid, Typeable fileid, Typeable userid, Ord (FileField fileid)) => Ord (WorkflowGraph fileid userid)
deriving instance (Show fileid, Show userid, Show (FileField fileid)) => Show (WorkflowGraph fileid userid)
deriving anyclass instance (NFData fileid, NFData userid, NFData (FileField fileid)) => NFData (WorkflowGraph fileid userid)

newtype WorkflowGraphReference = WorkflowGraphReference (Digest SHA3_256)
  deriving (Eq, Ord, Read, Show, Lift, Generic, Typeable)
  deriving newtype ( PersistField
                   , PathPiece, ToHttpApiData, FromHttpApiData, ToJSON, FromJSON
                   , ByteArrayAccess
                   , Binary
                   )
  deriving anyclass (Hashable, NFData)

instance PersistFieldSql WorkflowGraphReference where
  sqlType _ = sqlType $ Proxy @(Digest SHA3_256)


-----  WORKFLOW GRAPH: STAGES -----

data WorkflowGraphSubstageMode = WorkflowSubstageOptional | WorkflowSubstageRequired
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)

data WorkflowGraphSubstageShowWhen = WorkflowSubstageShowNever | WorkflowSubstageShowUnfulfilled | WorkflowSubstageShowFulfilled | WorkflowSubstageShowAlways
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)

data WorkflowGraphSubstage = WorkflowGraphSubstage
  { wgssMode         :: WorkflowGraphSubstageMode
  , wgssShowWhen     :: WorkflowGraphSubstageShowWhen
  , wgssDisplayLabel :: I18nText
  , wgssPredicate    :: PredDNF WorkflowGraphRestriction
  }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (NFData)

newtype WorkflowGraphStageLabel = WorkflowGraphStageLabel { unWorkflowGraphStageLabel :: CI Text }
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving newtype (IsString, ToJSON, ToJSONKey, FromJSON, FromJSONKey, PathPiece, PersistField, Binary)
  deriving anyclass (NFData)

data WorkflowGraphStage = WorkflowGraphStage
  { wgsLabel        :: WorkflowGraphStageLabel
  , wgsDisplayLabel :: I18nText
  , wgsSubstages    :: [WorkflowGraphSubstage]
  }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (NFData)


-----  WORKFLOW GRAPH: NODES  -----

newtype WorkflowGraphNodeLabel = WorkflowGraphNodeLabel { unWorkflowGraphNodeLabel :: CI Text }
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving newtype (IsString, ToJSON, ToJSONKey, FromJSON, FromJSONKey, PathPiece, PersistField, Binary)
  deriving anyclass (NFData)

instance PersistFieldSql WorkflowGraphNodeLabel where
  sqlType _ = sqlType $ Proxy @(CI Text)

data WorkflowGraphNode fileid userid = WGN
  { wgnFinal        :: Maybe Icon
  , wgnViewers      :: Maybe (WorkflowNodeView userid)
  , wgnMessages     :: Set (WorkflowNodeMessage userid)
  , wgnEdges        :: Map WorkflowGraphEdgeLabel (WorkflowGraphEdge fileid userid)
  , wgnPayloadView  :: Map WorkflowPayloadLabel   (WorkflowPayloadView userid)
  }
  deriving (Generic, Typeable)

deriving instance (Eq fileid, Eq userid, Typeable fileid, Typeable userid, Eq (FileField fileid)) => Eq (WorkflowGraphNode fileid userid)
deriving instance (Ord fileid, Ord userid, Typeable fileid, Typeable userid, Ord (FileField fileid)) => Ord (WorkflowGraphNode fileid userid)
deriving instance (Show fileid, Show userid, Show (FileField fileid)) => Show (WorkflowGraphNode fileid userid)
deriving anyclass instance (NFData fileid, NFData userid, NFData (FileField fileid)) => NFData (WorkflowGraphNode fileid userid)

data WorkflowNodeView userid = WorkflowNodeView
  { wnvViewers :: NonNull (Set (WorkflowRole userid))
  , wnvDisplayLabel :: I18nText
  }
  deriving (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowNodeMessage userid = WorkflowNodeMessage
  { wnmViewers     :: NonNull (Set (WorkflowRole userid))
  , wnmRestriction :: Maybe (PredDNF WorkflowGraphRestriction)
  , wnmStatus      :: MessageStatus
  , wnmContent     :: I18nHtml
  }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (NFData)

-----  WORKFLOW GRAPH: EDGES  -----

newtype WorkflowGraphEdgeLabel = WorkflowGraphEdgeLabel { unWorkflowGraphEdgeLabel :: CI Text }
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving newtype (IsString, ToJSON, ToJSONKey, FromJSON, FromJSONKey, PathPiece, PersistField, Binary)
  deriving anyclass (NFData)

instance PersistFieldSql WorkflowGraphEdgeLabel where
  sqlType _ = sqlType $ Proxy @(CI Text)

data WorkflowGraphRestriction
  = WorkflowGraphRestrictionPayloadFilled { wgrPayloadFilled :: WorkflowPayloadLabel }
  | WorkflowGraphRestrictionPreviousNode { wgrPreviousNode :: WorkflowGraphNodeLabel }
  | WorkflowGraphRestrictionPreviousEdge { wgrPreviousEdge :: WorkflowGraphEdgeLabel }
  | WorkflowGraphRestrictionNodeInHistory { wgrNodeInHistory :: WorkflowGraphNodeLabel }
  | WorkflowGraphRestrictionEdgeInHistory { wgrEdgeInHistory :: WorkflowGraphEdgeLabel }
  | WorkflowGraphRestrictionInitial
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowGraphEdge fileid userid
  = WorkflowGraphEdgeManual
    { wgeSource       :: WorkflowGraphNodeLabel
    , wgeActors       :: Set (WorkflowRole userid)
    , wgeForm         :: WorkflowGraphEdgeForm fileid userid
    , wgeDisplayLabel :: I18nText
    , wgeViewActor    :: Set (WorkflowRole userid)
    , wgeMessages     :: Set (WorkflowEdgeMessage userid)
    }
  | WorkflowGraphEdgeAutomatic
    { wgeSource      :: WorkflowGraphNodeLabel
    , wgeRestriction :: Maybe (PredDNF WorkflowGraphRestriction)
    }
  | WorkflowGraphEdgeInitial
    { wgeActors       :: Set (WorkflowRole userid)
    , wgeForm         :: WorkflowGraphEdgeForm fileid userid
    , wgeDisplayLabel :: I18nText
    , wgeViewActor    :: Set (WorkflowRole userid)
    , wgeMessages     :: Set (WorkflowEdgeMessage userid)
    }
  deriving (Generic, Typeable)

deriving instance (Eq fileid, Eq userid, Typeable fileid, Typeable userid, Eq (FileField fileid)) => Eq (WorkflowGraphEdge fileid userid)
deriving instance (Ord fileid, Ord userid, Typeable fileid, Typeable userid, Ord (FileField fileid)) => Ord (WorkflowGraphEdge fileid userid)
deriving instance (Show fileid, Show userid, Show (FileField fileid)) => Show (WorkflowGraphEdge fileid userid)
deriving anyclass instance (NFData fileid, NFData userid, NFData (FileField fileid)) => NFData (WorkflowGraphEdge fileid userid)

data WorkflowEdgeMessage userid = WorkflowEdgeMessage
  { wemViewers     :: NonNull (Set (WorkflowRole userid))
  , wemRestriction :: Maybe (PredDNF WorkflowGraphRestriction)
  , wemStatus      :: MessageStatus
  , wemContent     :: I18nHtml
  }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (NFData)

-- | A wrapped `Scientific`
--
-- Due to arbitrary precision this allows inserting new fields anywhere
newtype WorkflowGraphEdgeFormOrder = WorkflowGraphEdgeFormOrder { unWorkflowGraphEdgeFormOrder :: Maybe Scientific }
  deriving (Read, Show, Generic, Typeable)
  deriving (Eq, Ord) via (NTop (Maybe Scientific))
  deriving (Semigroup, Monoid) via (Maybe (Min Scientific))
  deriving anyclass (NFData)

newtype WorkflowGraphEdgeForm fileid userid
  = WorkflowGraphEdgeForm
    { wgefFields :: Map WorkflowPayloadLabel (NonNull (Set (NonNull (Map WorkflowGraphEdgeFormOrder (WorkflowPayloadSpec fileid userid)))))
      -- ^ field requirement forms a cnf:
      --
      -- - all labels must be filled
      -- - for each label any field must be filled
      -- - optional fields are always considered to be filled
      --
      -- since fields can reference other labels this allows arbitrary requirements to be encoded.
    }
  deriving (Generic, Typeable)

deriving instance (Eq fileid, Eq userid, Typeable fileid, Typeable userid, Eq (FileField fileid)) => Eq (WorkflowGraphEdgeForm fileid userid)
deriving instance (Ord fileid, Ord userid, Typeable fileid, Typeable userid, Ord (FileField fileid)) => Ord (WorkflowGraphEdgeForm fileid userid)
deriving instance (Show fileid, Show userid, Show (FileField fileid)) => Show (WorkflowGraphEdgeForm fileid userid)
deriving anyclass instance (NFData fileid, NFData userid, NFData (FileField fileid)) => NFData (WorkflowGraphEdgeForm fileid userid)

-----  WORKFLOW GRAPH: ROLES / ACTORS  -----

data WorkflowRole userid
  = WorkflowRoleUser             { workflowRoleUser         :: userid               }
  | WorkflowRolePayloadReference { workflowRolePayloadLabel :: WorkflowPayloadLabel }
  | WorkflowRoleAuthorized       { workflowRoleAuthorized   :: AuthDNF              }
  | WorkflowRoleInitiator
  deriving (Eq, Ord, Show, Read, Data, Generic, Typeable)
  deriving anyclass (NFData)


-----  WORKFLOW GRAPH: PAYLOAD SPECIFICATION  -----

data WorkflowPayloadView userid = WorkflowPayloadView
  { wpvViewers :: NonNull (Set (WorkflowRole userid))
  , wpvDisplayLabel :: I18nText
  }
  deriving (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowPayloadSpec fileid userid = forall payload. Typeable payload => WorkflowPayloadSpec (WorkflowPayloadField fileid userid payload)
  deriving (Typeable)

deriving instance (Show fileid, Show userid, Show (FileField fileid)) => Show (WorkflowPayloadSpec fileid userid)
instance (NFData fileid, NFData userid, NFData (FileField fileid)) => NFData (WorkflowPayloadSpec fileid userid) where
  rnf (WorkflowPayloadSpec pField) = rnf pField

data WorkflowPayloadFieldReference
  deriving (Typeable)

data WorkflowPayloadTimeCapture
  deriving (Typeable)

data WorkflowPayloadTimeCapturePrecision
  = WFCaptureDate | WFCaptureTime | WFCaptureDateTime
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)
instance Default WorkflowPayloadTimeCapturePrecision where
  def = WFCaptureDateTime

data WorkflowPayloadTextPreset = WorkflowPayloadTextPreset
  { wptpText :: Text
  , wptpLabel :: I18nText
  , wptpTooltip :: Maybe I18nHtml
  } deriving (Eq, Ord, Read, Show, Generic, Typeable)
    deriving anyclass (NFData)

data WorkflowPayloadTextType
  = WorkflowPayloadTextTypeText
  | WorkflowPayloadTextTypeLarge
  | WorkflowPayloadTextTypeEmail
  | WorkflowPayloadTextTypeUrl
  | WorkflowPayloadTextTypePassword
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)

-- Don't forget to update the NFData instance for every change!
data WorkflowPayloadField fileid userid (payload :: Type) where
  WorkflowPayloadFieldText      :: { wpftLabel       :: I18nText
                                   , wpftPlaceholder :: Maybe I18nText
                                   , wpftTooltip     :: Maybe I18nHtml
                                   , wpftDefault     :: Maybe Text
                                   , wpftOptional    :: Bool
                                   , wpftPresets     :: Maybe (NonEmpty WorkflowPayloadTextPreset)
                                   , wpftType        :: WorkflowPayloadTextType
                                   } -> WorkflowPayloadField fileid userid Text
  WorkflowPayloadFieldNumber    :: { wpfnLabel       :: I18nText
                                   , wpfnPlaceholder :: Maybe I18nText
                                   , wpfnTooltip     :: Maybe I18nHtml
                                   , wpfnDefault
                                   , wpfnMin
                                   , wpfnMax
                                   , wpfnStep        :: Maybe Scientific
                                   , wpfnOptional    :: Bool
                                   } -> WorkflowPayloadField fileid userid Scientific
  WorkflowPayloadFieldBool      :: { wpfbLabel       :: I18nText
                                   , wpfbTooltip     :: Maybe I18nHtml
                                   , wpfbDefault     :: Maybe Bool
                                   , wpfbOptional    :: Maybe I18nText -- ^ Optional if `Just`; encodes label of `Nothing`-Option
                                   } -> WorkflowPayloadField fileid userid Bool
  WorkflowPayloadFieldDay       :: { wpfdLabel       :: I18nText
                                   , wpfdTooltip     :: Maybe I18nHtml
                                   , wpfdDefault     :: Maybe Day
                                   , wpfdOptional    :: Bool
                                   , wpfdMaxPast, wpfdMaxFuture :: Maybe Integer
                                   } -> WorkflowPayloadField fileid userid Day
  WorkflowPayloadFieldTime      :: { wpfttLabel       :: I18nText
                                   , wpfttTooltip     :: Maybe I18nHtml
                                   , wpfttDefault     :: Maybe TimeOfDay
                                   , wpfttOptional    :: Bool
                                   } -> WorkflowPayloadField fileid userid TimeOfDay
  WorkflowPayloadFieldDateTime  :: { wpfdtLabel       :: I18nText
                                   , wpfdtTooltip     :: Maybe I18nHtml
                                   , wpfdtDefault     :: Maybe UTCTime
                                   , wpfdtOptional    :: Bool
                                   , wpfdtMaxPast, wpfdtMaxFuture :: Maybe NominalDiffTime
                                   } -> WorkflowPayloadField fileid userid UTCTime
  WorkflowPayloadFieldFile      :: { wpffLabel       :: I18nText
                                   , wpffTooltip     :: Maybe I18nHtml
                                   , wpffConfig      :: FileField fileid
                                   , wpffOptional    :: Bool
                                   } -> WorkflowPayloadField fileid userid fileid
  WorkflowPayloadFieldUser      :: { wpfuLabel       :: I18nText
                                   , wpfuTooltip     :: Maybe I18nHtml
                                   , wpfuDefault     :: Maybe userid
                                   , wpfuOptional    :: Bool
                                   } -> WorkflowPayloadField fileid userid userid
  WorkflowPayloadFieldCaptureUser :: WorkflowPayloadField fileid userid userid
  WorkflowPayloadFieldCaptureDateTime :: { wpfcdtPrecision :: WorkflowPayloadTimeCapturePrecision
                                         , wpfcdtLabel     :: I18nText
                                         , wpfcdtTooltip   :: Maybe I18nHtml
                                         } -> WorkflowPayloadField fileid userid WorkflowPayloadTimeCapture
  WorkflowPayloadFieldReference :: { wpfrTarget      :: WorkflowPayloadLabel
                                   } -> WorkflowPayloadField fileid userid WorkflowPayloadFieldReference
  WorkflowPayloadFieldMultiple  :: { wpfmLabel       :: I18nText
                                   , wpfmTooltip     :: Maybe I18nHtml
                                   , wpfmDefault     :: Maybe (NonEmpty (WorkflowFieldPayloadW fileid userid))
                                   , wpfmSub         :: WorkflowPayloadSpec fileid userid
                                   , wpfmMin         :: Natural
                                   , wpfmRange       :: Maybe Natural -- ^ `wpfmMax = (+ wpfmMin) <$> wpfmRange
                                   } -> WorkflowPayloadField fileid userid (NonEmpty (WorkflowFieldPayloadW fileid userid))
  deriving (Typeable)

deriving instance (Show fileid, Show userid, Show (FileField fileid)) => Show (WorkflowPayloadField fileid userid payload)
deriving instance (Typeable fileid, Typeable userid, Eq fileid, Eq userid, Eq (FileField fileid)) => Eq (WorkflowPayloadField fileid userid payload)
deriving instance (Typeable fileid, Typeable userid, Ord fileid, Ord userid, Ord (FileField fileid)) => Ord (WorkflowPayloadField fileid userid payload)

instance (Eq fileid, Eq userid, Typeable fileid, Typeable userid, Eq (FileField fileid)) => Eq (WorkflowPayloadSpec fileid userid) where
  (WorkflowPayloadSpec a) == (WorkflowPayloadSpec b)
    = case typeOf a `eqTypeRep` typeOf b of
        Just HRefl -> a == b
        Nothing    -> False

instance (Ord fileid, Ord userid, Typeable fileid, Typeable userid, Ord (FileField fileid)) => Ord (WorkflowPayloadSpec fileid userid) where
  (WorkflowPayloadSpec a) `compare` (WorkflowPayloadSpec b)
    = case typeOf a `eqTypeRep` typeOf b of
        Just HRefl -> a `compare` b
        Nothing   -> case (a, b) of
          (WorkflowPayloadFieldText{}, _) -> LT
          (WorkflowPayloadFieldNumber{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldNumber{}, _) -> LT
          (WorkflowPayloadFieldBool{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldBool{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldBool{}, _) -> LT
          (WorkflowPayloadFieldDay{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldDay{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldDay{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldDay{}, _) -> LT
          (WorkflowPayloadFieldTime{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldTime{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldTime{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldTime{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldTime{}, _) -> LT
          (WorkflowPayloadFieldDateTime{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldDateTime{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldDateTime{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldDateTime{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldDateTime{}, WorkflowPayloadFieldTime{}) -> GT
          (WorkflowPayloadFieldDateTime{}, _) -> LT
          (WorkflowPayloadFieldFile{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldFile{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldFile{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldFile{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldFile{}, WorkflowPayloadFieldTime{}) -> GT
          (WorkflowPayloadFieldFile{}, WorkflowPayloadFieldDateTime{}) -> GT
          (WorkflowPayloadFieldFile{}, _) -> LT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldTime{}) -> GT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldDateTime{}) -> GT
          (WorkflowPayloadFieldUser{}, WorkflowPayloadFieldFile{}) -> GT
          (WorkflowPayloadFieldUser{}, _) -> LT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldTime{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldDateTime{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldFile{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, WorkflowPayloadFieldUser{}) -> GT
          (WorkflowPayloadFieldCaptureUser{}, _) -> LT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldTime{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldDateTime{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldFile{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldUser{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, WorkflowPayloadFieldCaptureUser{}) -> GT
          (WorkflowPayloadFieldCaptureDateTime{}, _) -> LT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldText{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldNumber{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldBool{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldDay{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldTime{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldDateTime{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldFile{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldUser{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldCaptureUser{}) -> GT
          (WorkflowPayloadFieldReference{}, WorkflowPayloadFieldCaptureDateTime{}) -> GT
          (WorkflowPayloadFieldReference{}, _) -> LT
          (WorkflowPayloadFieldMultiple{}, _) -> GT

instance (NFData fileid, NFData userid, NFData (FileField fileid)) => NFData (WorkflowPayloadField fileid userid payload) where
  rnf = \case
    WorkflowPayloadFieldText{..} -> wpftLabel `deepseq` wpftPlaceholder `deepseq` wpftTooltip `deepseq` wpftDefault `deepseq` wpftOptional `deepseq` wpftPresets `deepseq` wpftType `deepseq` ()
    WorkflowPayloadFieldNumber{..} -> wpfnLabel `deepseq` wpfnPlaceholder `deepseq` wpfnTooltip `deepseq` wpfnDefault `deepseq` wpfnMin `deepseq` wpfnMax `deepseq` wpfnStep `deepseq` wpfnOptional `deepseq` ()
    WorkflowPayloadFieldBool{..} -> wpfbLabel `deepseq` wpfbTooltip `deepseq` wpfbDefault `deepseq` wpfbOptional `deepseq` ()
    WorkflowPayloadFieldDay{..} -> wpfdLabel `deepseq` wpfdTooltip `deepseq` wpfdDefault `deepseq` wpfdOptional `deepseq` wpfdMaxPast `deepseq` wpfdMaxFuture `deepseq` ()
    WorkflowPayloadFieldTime{..} -> wpfttLabel `deepseq` wpfttTooltip `deepseq` wpfttDefault `deepseq` wpfttOptional `deepseq` ()
    WorkflowPayloadFieldDateTime{..} -> wpfdtLabel `deepseq` wpfdtTooltip `deepseq` wpfdtDefault `deepseq` wpfdtOptional `deepseq` wpfdtMaxPast `deepseq` wpfdtMaxFuture `deepseq` ()
    WorkflowPayloadFieldFile{..} -> wpffLabel `deepseq` wpffTooltip `deepseq` wpffConfig `deepseq` wpffOptional `deepseq` ()
    WorkflowPayloadFieldUser{..} -> wpfuLabel `deepseq` wpfuTooltip `deepseq` wpfuDefault `deepseq` wpfuOptional `deepseq` ()
    WorkflowPayloadFieldCaptureUser -> ()
    WorkflowPayloadFieldCaptureDateTime{..} -> wpfcdtPrecision `deepseq` wpfcdtLabel `deepseq` wpfcdtTooltip `deepseq` ()
    WorkflowPayloadFieldReference{..} -> wpfrTarget `deepseq` ()
    WorkflowPayloadFieldMultiple{..} -> wpfmLabel `deepseq` wpfmTooltip `deepseq` wpfmDefault `deepseq` wpfmSub `deepseq` wpfmMin `deepseq` wpfmRange `deepseq` ()

_WorkflowPayloadSpec :: forall payload fileid userid.
                        ( Typeable payload, Typeable fileid, Typeable userid )
                     => Prism' (WorkflowPayloadSpec fileid userid) (WorkflowPayloadField fileid userid payload)
_WorkflowPayloadSpec = prism' WorkflowPayloadSpec $ \(WorkflowPayloadSpec pF) -> cast pF

data WorkflowPayloadField' = WPFText' | WPFNumber' | WPFBool' | WPFDay' | WPFTime' | WPFDateTime' | WPFFile' | WPFUser' | WPFCaptureUser' | WPFCaptureDateTime' | WPFReference' | WPFMultiple'
  deriving (Eq, Ord, Enum, Bounded, Show, Read, Data, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)


-----  WORKFLOW INSTANCE  -----

data WorkflowScope termid schoolid courseid
  = WSGlobal
  | WSTerm   { wisTerm   :: termid   }
  | WSSchool { wisSchool :: schoolid }
  | WSTermSchool { wisTerm :: termid, wisSchool :: schoolid }
  | WSCourse { wisCourse :: courseid }
  deriving (Eq, Ord, Show, Read, Data, Generic, Typeable)
  deriving anyclass (Hashable, NFData)

data WorkflowScope'
  = WSGlobal' | WSTerm' | WSSchool' | WSTermSchool' | WSCourse'
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Data, Generic, Typeable)
  deriving anyclass (Hashable, Universe, Finite, NFData)

classifyWorkflowScope :: WorkflowScope termid schoolid courseid -> WorkflowScope'
classifyWorkflowScope = \case
  WSGlobal       -> WSGlobal'
  WSTerm{}       -> WSTerm'
  WSSchool{}     -> WSSchool'
  WSTermSchool{} -> WSTermSchool'
  WSCourse{}     -> WSCourse'


-----  WORKFLOW PAYLOAD  -----

newtype WorkflowPayloadLabel = WorkflowPayloadLabel { unWorkflowPayloadLabel :: CI Text }
  deriving stock (Eq, Ord, Show, Read, Data, Generic, Typeable)
  deriving newtype (IsString, ToJSON, ToJSONKey, FromJSON, FromJSONKey, PathPiece, PersistField, Binary)
  deriving anyclass (Hashable, NFData)

instance PersistFieldSql WorkflowPayloadLabel where
  sqlType _ = sqlType $ Proxy @(CI Text)

data WorkflowActionUser userid
  = WorkflowActionAutomatic
  | WorkflowActionUnauthenticated
  | WorkflowActionUser { wauUser :: userid }
  deriving stock (Eq, Ord, Show, Read, Data, Generic, Typeable)
  deriving stock (Functor, Foldable, Traversable)
  deriving anyclass (NFData)

newtype WorkflowStateIndex = WorkflowStateIndex { unWorkflowStateIndex :: Word64 }
  deriving stock (Eq, Ord, Show, Read, Data, Generic, Typeable)
  deriving newtype (Num, Real, Integral, Enum, Bounded, ToJSON, FromJSON, PathPiece, PersistField, Binary)
  deriving anyclass (NFData)

instance PersistFieldSql WorkflowStateIndex where
  sqlType _ = sqlType $ Proxy @Word64

-- type WorkflowState fileid userid = NonNull (Seq (WorkflowAction fileid userid))

-- workflowStateIndex :: Alternative m
--                    => WorkflowStateIndex
--                    -> WorkflowState fileid userid
--                    -> m (WorkflowAction fileid userid)
-- workflowStateIndex (fromIntegral -> i) = maybe empty pure . flip index i . toNullable

-- workflowStateSection :: MonadPlus m
--                      => WorkflowStateIndex
--                      -> WorkflowState fileid userid
--                      -> m (WorkflowState fileid userid)
-- workflowStateSection i wSt = maybe mzero return . fromNullable . Seq.fromList =<< traverse (`workflowStateIndex` wSt) [0..i]

-- data WorkflowAction fileid userid = WorkflowAction
--   { wpTo      :: WorkflowGraphNodeLabel
--   , wpVia     :: WorkflowGraphEdgeLabel
--   , wpPayload :: Map WorkflowPayloadLabel (Set (WorkflowFieldPayloadW fileid userid))
--   , wpUser    :: Maybe (Maybe userid) -- ^ Outer `Maybe` encodes automatic/manual, inner `Maybe` encodes whether user was authenticated
--   , wpTime    :: UTCTime
--   }
--   deriving (Eq, Ord, Show, Generic, Typeable)
--   deriving anyclass (NFData)

-- data WorkflowActionInfo fileid userid = WorkflowActionInfo
--   { waiIx      :: WorkflowStateIndex
--   , waiFrom    :: Maybe WorkflowGraphNodeLabel
--   , waiHistory :: [WorkflowAction fileid userid]
--   , waiAction  :: WorkflowAction fileid userid
--   }
--   deriving (Eq, Ord, Show, Generic, Typeable)
--   deriving anyclass (NFData)

-- workflowActionInfos :: WorkflowState fileid userid -> [WorkflowActionInfo fileid userid]
-- workflowActionInfos wState
--   = [ WorkflowActionInfo{..}
--     | waiFrom <- Nothing : map (Just . wpTo) wState'
--     | waiAction <- wState'
--     | waiIx <- [minBound..]
--     | waiHistory <- tailEx $ inits wState'
--     ]
--   where wState' = otoList wState

data WorkflowFieldPayloadW fileid userid = forall payload. IsWorkflowFieldPayload' fileid userid payload => WorkflowFieldPayloadW (WorkflowFieldPayload fileid userid payload)
  deriving (Typeable)

instance (NFData fileid, NFData userid) => NFData (WorkflowFieldPayloadW fileid userid) where
  rnf (WorkflowFieldPayloadW fPayload) = rnf fPayload

instance (Eq fileid, Eq userid, Typeable fileid, Typeable userid) => Eq (WorkflowFieldPayloadW fileid userid) where
  (WorkflowFieldPayloadW a) == (WorkflowFieldPayloadW b)
    = case typeOf a `eqTypeRep` typeOf b of
        Just HRefl -> a == b
        Nothing    -> False

instance (Ord fileid, Ord userid, Typeable fileid, Typeable userid) => Ord (WorkflowFieldPayloadW fileid userid) where
  (WorkflowFieldPayloadW a) `compare` (WorkflowFieldPayloadW b)
    = case typeOf a `eqTypeRep` typeOf b of
        Just HRefl -> a `compare` b
        Nothing   -> case (a, b) of
          (WFPText{}, _) -> LT
          (WFPNumber{}, WFPText{}) -> GT
          (WFPNumber{}, _) -> LT
          (WFPBool{}, WFPText{}) -> GT
          (WFPBool{}, WFPNumber{}) -> GT
          (WFPBool{}, _) -> LT
          (WFPDay{}, WFPText{}) -> GT
          (WFPDay{}, WFPNumber{}) -> GT
          (WFPDay{}, WFPBool{}) -> GT
          (WFPDay{}, _) -> LT
          (WFPTime{}, WFPText{}) -> GT
          (WFPTime{}, WFPNumber{}) -> GT
          (WFPTime{}, WFPBool{}) -> GT
          (WFPTime{}, WFPDay{}) -> GT
          (WFPTime{}, _) -> LT
          (WFPDateTime{}, WFPText{}) -> GT
          (WFPDateTime{}, WFPNumber{}) -> GT
          (WFPDateTime{}, WFPBool{}) -> GT
          (WFPDateTime{}, WFPDay{}) -> GT
          (WFPDateTime{}, WFPTime{}) -> GT
          (WFPDateTime{}, _) -> LT
          (WFPFile{}, WFPText{}) -> GT
          (WFPFile{}, WFPNumber{}) -> GT
          (WFPFile{}, WFPBool{}) -> GT
          (WFPFile{}, WFPDay{}) -> GT
          (WFPFile{}, WFPTime{}) -> GT
          (WFPFile{}, WFPDateTime{}) -> GT
          (WFPFile{}, _) -> LT
          (WFPUser{}, _) -> GT

workflowPayloadSort
  :: forall fileid userid.
     (fileid -> fileid -> Ordering)
  -> (userid -> userid -> Ordering)
  -> (WorkflowFieldPayloadW fileid userid -> WorkflowFieldPayloadW fileid userid -> Ordering)
-- ^ @workflowPayloadSort compare compare /= compare@
workflowPayloadSort ordFiles ordUsers (WorkflowFieldPayloadW a) (WorkflowFieldPayloadW b) = case (a, b) of
  (WFPText a',   WFPText b'  ) -> compareUnicode a' b'
  (WFPText{},    _           ) -> LT
  (WFPNumber a', WFPNumber b') -> compare a' b'
  (WFPNumber{},  WFPText{}   ) -> GT
  (WFPNumber{},  _           ) -> LT
  (WFPBool a',   WFPBool b'  ) -> compare a' b'
  (WFPBool{},    WFPText{}   ) -> GT
  (WFPBool{},    WFPNumber{} ) -> GT
  (WFPBool{},    _           ) -> LT
  (WFPDay a',    WFPDay b'   ) -> compare a' b'
  (WFPDay{},     WFPText{}   ) -> GT
  (WFPDay{},     WFPNumber{} ) -> GT
  (WFPDay{},     WFPBool{}   ) -> GT
  (WFPDay{},     _           ) -> LT
  (WFPTime a',   WFPTime b'  ) -> compare a' b'
  (WFPTime{},    WFPText{}   ) -> GT
  (WFPTime{},    WFPNumber{} ) -> GT
  (WFPTime{},    WFPBool{}   ) -> GT
  (WFPTime{},    WFPDay{}    ) -> GT
  (WFPTime{},    _           ) -> LT
  (WFPDateTime a', WFPDateTime b') -> compare a' b'
  (WFPDateTime{},  WFPText{}     ) -> GT
  (WFPDateTime{},  WFPNumber{}   ) -> GT
  (WFPDateTime{},  WFPBool{}     ) -> GT
  (WFPDateTime{},  WFPDay{}      ) -> GT
  (WFPDateTime{},  WFPTime{}     ) -> GT
  (WFPDateTime{},  _             ) -> LT
  (WFPFile a',   WFPFile b'  ) -> ordFiles a' b'
  (WFPFile{},    WFPText{}   ) -> GT
  (WFPFile{},    WFPNumber{} ) -> GT
  (WFPFile{},    WFPBool{}   ) -> GT
  (WFPFile{},    WFPDay{}    ) -> GT
  (WFPFile{},    WFPTime{}   ) -> GT
  (WFPFile{},    WFPDateTime{}) -> GT
  (WFPFile{},    _           ) -> LT
  (WFPUser a',   WFPUser b'  ) -> ordUsers a' b'
  (WFPUser{},    _           ) -> GT

instance (Show fileid, Show userid) => Show (WorkflowFieldPayloadW fileid userid) where
  show (WorkflowFieldPayloadW payload) = show payload

-- Don't forget to update the NFData instance for every change!
data WorkflowFieldPayload fileid userid (payload :: Type) where
  WFPText     :: Text       -> WorkflowFieldPayload fileid userid Text
  WFPNumber   :: Scientific -> WorkflowFieldPayload fileid userid Scientific
  WFPBool     :: Bool       -> WorkflowFieldPayload fileid userid Bool
  WFPDay      :: Day        -> WorkflowFieldPayload fileid userid Day
  WFPTime     :: TimeOfDay  -> WorkflowFieldPayload fileid userid TimeOfDay
  WFPDateTime :: UTCTime    -> WorkflowFieldPayload fileid userid UTCTime
  WFPFile     :: fileid     -> WorkflowFieldPayload fileid userid fileid
  WFPUser     :: userid     -> WorkflowFieldPayload fileid userid userid
  deriving (Typeable)

deriving instance (Show fileid, Show userid) => Show (WorkflowFieldPayload fileid userid payload)
deriving instance (Typeable fileid, Typeable userid, Eq fileid, Eq userid) => Eq (WorkflowFieldPayload fileid userid payload)
deriving instance (Typeable fileid, Typeable userid, Ord fileid, Ord userid) => Ord (WorkflowFieldPayload fileid userid payload)

instance (NFData fileid, NFData userid) => NFData (WorkflowFieldPayload fileid userid payload) where
  rnf = \case
    WFPText t     -> rnf t
    WFPNumber n   -> rnf n
    WFPBool b     -> rnf b
    WFPDay d      -> rnf d
    WFPTime t     -> rnf t
    WFPDateTime t -> rnf t
    WFPFile f     -> rnf f
    WFPUser u     -> rnf u

_WorkflowFieldPayloadW :: forall payload fileid userid.
                          ( IsWorkflowFieldPayload' fileid userid payload, Typeable fileid, Typeable userid )
                       => Prism' (WorkflowFieldPayloadW fileid userid) (WorkflowFieldPayload fileid userid payload)
_WorkflowFieldPayloadW = prism' WorkflowFieldPayloadW $ \(WorkflowFieldPayloadW fp) -> cast fp

data WorkflowFieldPayload' = WFPText' | WFPNumber' | WFPBool' | WFPDay' | WFPTime' | WFPDateTime' | WFPFile' | WFPUser'
  deriving (Eq, Ord, Enum, Bounded, Show, Read, Data, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData, Binary)

type IsWorkflowFieldPayload' fileid userid payload = IsWorkflowFieldPayload fileid fileid userid userid payload payload

class Typeable payload => IsWorkflowFieldPayload fileid fileid' userid userid' payload payload' where
  _WorkflowFieldPayload :: Prism (WorkflowFieldPayload fileid userid payload) (WorkflowFieldPayload fileid' userid' payload') payload payload'

instance IsWorkflowFieldPayload fileid fileid userid userid Text Text where
  _WorkflowFieldPayload = prism' WFPText $ \case { WFPText x -> Just x; _other -> Nothing }
instance IsWorkflowFieldPayload fileid fileid userid userid Scientific Scientific where
  _WorkflowFieldPayload = prism' WFPNumber $ \case { WFPNumber x -> Just x; _other -> Nothing }
instance IsWorkflowFieldPayload fileid fileid userid userid Bool Bool where
  _WorkflowFieldPayload = prism' WFPBool $ \case { WFPBool x -> Just x; _other -> Nothing }
instance IsWorkflowFieldPayload fileid fileid userid userid Day Day where
  _WorkflowFieldPayload = prism' WFPDay $ \case { WFPDay x -> Just x; _other -> Nothing }
instance IsWorkflowFieldPayload fileid fileid userid userid TimeOfDay TimeOfDay where
  _WorkflowFieldPayload = prism' WFPTime $ \case { WFPTime x -> Just x; _other -> Nothing }
instance IsWorkflowFieldPayload fileid fileid userid userid UTCTime UTCTime where
  _WorkflowFieldPayload = prism' WFPDateTime $ \case { WFPDateTime x -> Just x; _other -> Nothing }
instance Typeable fileid => IsWorkflowFieldPayload fileid fileid' userid userid fileid fileid' where
  _WorkflowFieldPayload = prism WFPFile $ \case { WFPFile x -> Right x; other -> Left $ unsafeCoerce other }
instance Typeable userid => IsWorkflowFieldPayload fileid fileid userid userid' userid userid' where
  _WorkflowFieldPayload = prism WFPUser $ \case { WFPUser x -> Right x; other -> Left $ unsafeCoerce other }

type WorkflowFieldPayloads fileid fileid' userid userid'
  = ( IsWorkflowFieldPayload' fileid userid Text
    , IsWorkflowFieldPayload' fileid userid Scientific
    , IsWorkflowFieldPayload' fileid userid Bool
    , IsWorkflowFieldPayload' fileid userid Day
    , IsWorkflowFieldPayload' fileid userid TimeOfDay
    , IsWorkflowFieldPayload' fileid userid UTCTime
    , IsWorkflowFieldPayload fileid fileid' userid userid fileid fileid'
    , IsWorkflowFieldPayload fileid fileid userid userid' userid userid'
    )

-- workflowStatePayload :: forall fileid userid payload.
--                         ( IsWorkflowFieldPayload' fileid userid payload
--                         , Ord fileid, Ord userid, Ord payload
--                         , Typeable fileid, Typeable userid
--                         , Show userid, Show fileid
--                         )
--                      => WorkflowPayloadLabel -> WorkflowState fileid userid -> Seq (Maybe (Set payload))
-- workflowStatePayload label acts = flip ofoldMap acts $ \WorkflowAction{..} -> Seq.singleton . Map.lookup label $ fmap (Set.fromList . concatMap extractPayload . otoList) wpPayload
--   where
--     extractPayload :: WorkflowFieldPayloadW fileid userid -> [payload]
--     extractPayload = \case
--       WorkflowFieldPayloadW fieldPayload@(WFPMultiple ps) -> traceShow ("multiple", fieldPayload) . concatMap extractPayload $ otoList ps
--       WorkflowFieldPayloadW fieldPayload
--         | Just HRefl <- traceShow ("single", fieldPayload) $ typeOf fieldPayload `eqTypeRep` typeRep @(WorkflowFieldPayload fileid userid payload)
--           -> fieldPayload ^.. _WorkflowFieldPayload
--         | otherwise
--           -> traceShow ("none", fieldPayload) mempty

-- workflowStateCurrentPayloads :: forall fileid userid mono.
--                                 ( Element mono ~ WorkflowAction fileid userid
--                                 , MonoFoldable mono
--                                 )
--                              => mono
--                              -> Map WorkflowPayloadLabel (Set (WorkflowFieldPayloadW fileid userid))
-- workflowStateCurrentPayloads = Map.unionsWith (\_ v -> v) . map wpPayload . otoList


----- Workflow routing types -----

data WorkflowWorkflowListType = WorkflowWorkflowListActive | WorkflowWorkflowListArchive | WorkflowWorkflowListAll
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Typeable)
  deriving anyclass (Universe, Finite)

instance Hashable WorkflowWorkflowListType

workflowWorkflowListTypeNext :: WorkflowWorkflowListType
                             -> WorkflowWorkflowListType
workflowWorkflowListTypeNext = \case
  WorkflowWorkflowListActive -> WorkflowWorkflowListArchive
  _other                     -> WorkflowWorkflowListActive


----- Lenses needed here -----

makeLenses_ ''WorkflowGraphEdge
makeLenses_ ''WorkflowEdgeMessage
makeLenses_ ''WorkflowNodeMessage


----- Generic traversal -----

type family Concat as bs where
  Concat '[] bs = bs
  Concat as '[] = as
  Concat (a ': as) bs = a ': Concat as bs

data WorkflowChildren
type instance Children WorkflowChildren a = ChildrenWorkflowChildren a
type family ChildrenWorkflowChildren a where
  ChildrenWorkflowChildren (Map k v) = '[k, v]
  ChildrenWorkflowChildren (Set a) = '[a]
  ChildrenWorkflowChildren (Seq a) = '[a]
  ChildrenWorkflowChildren [a] = '[a]
  ChildrenWorkflowChildren (a, b) = '[a, b]
  ChildrenWorkflowChildren (Maybe a) = '[a]
  ChildrenWorkflowChildren (NonEmpty a) = '[a]
  ChildrenWorkflowChildren (NonNull mono) = '[Element mono]
  ChildrenWorkflowChildren (CI a) = '[a]
  ChildrenWorkflowChildren Bool = '[]
  ChildrenWorkflowChildren UUID = '[]
  ChildrenWorkflowChildren Html = '[]
  ChildrenWorkflowChildren Scientific = '[]
  ChildrenWorkflowChildren (BackendKey SqlBackend) = '[]
  ChildrenWorkflowChildren (Key record) = '[]
  ChildrenWorkflowChildren FileContentReference = '[]
  ChildrenWorkflowChildren Day = '[]
  ChildrenWorkflowChildren TimeOfDay = '[]
  ChildrenWorkflowChildren UTCTime = '[]
  ChildrenWorkflowChildren (ConduitT () ByteString m ()) = '[]
  ChildrenWorkflowChildren (WorkflowPayloadSpec fileid userid)
           = ChildrenWorkflowChildren I18nText
    `Concat` ChildrenWorkflowChildren (Maybe I18nText)
    `Concat` ChildrenWorkflowChildren (Maybe I18nHtml)
    `Concat` ChildrenWorkflowChildren (Maybe Text)
    `Concat` ChildrenWorkflowChildren (Maybe Scientific)
    `Concat` ChildrenWorkflowChildren (Maybe Bool)
    `Concat` ChildrenWorkflowChildren (Maybe Day)
    `Concat` ChildrenWorkflowChildren (Maybe TimeOfDay)
    `Concat` ChildrenWorkflowChildren (Maybe UTCTime)
    `Concat` ChildrenWorkflowChildren (Maybe fileid)
    `Concat` ChildrenWorkflowChildren (Maybe userid)
    `Concat` ChildrenWorkflowChildren Bool
    `Concat` ChildrenWorkflowChildren WorkflowPayloadLabel
  ChildrenWorkflowChildren (WorkflowFieldPayloadW fileid userid)
           = ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid Text)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid Scientific)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid Bool)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid Day)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid TimeOfDay)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid UTCTime)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid fileid)
    `Concat` ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid userid)
  ChildrenWorkflowChildren (WorkflowFieldPayload fileid userid payload)
           = ChildrenWorkflowChildren payload
  ChildrenWorkflowChildren a = Children ChGeneric a

instance HasTypesCustom WorkflowChildren a a a a where
  typesCustom = id

instance (HasTypesCustom WorkflowChildren a' b' a b, HasTypesCustom WorkflowChildren c' d' a b) => HasTypesCustom WorkflowChildren (a', c') (b', d') a b where
  typesCustom = beside (typesCustom @WorkflowChildren) (typesCustom @WorkflowChildren)

instance HasTypesCustom WorkflowChildren a' b' a b => HasTypesCustom WorkflowChildren (NonEmpty a') (NonEmpty b') a b where
  typesCustom = traverse . typesCustom @WorkflowChildren

instance {-# OVERLAPPING #-} HasTypesCustom WorkflowChildren v v a a => HasTypesCustom WorkflowChildren (Map k v) (Map k v) a a where
  typesCustom = traverse . typesCustom @WorkflowChildren

instance HasTypesCustom WorkflowChildren v v' a a' => HasTypesCustom WorkflowChildren (Map k v) (Map k v') a a' where
  typesCustom = traverse . typesCustom @WorkflowChildren

instance (Ord k', HasTypesCustom WorkflowChildren k k' a a') => HasTypesCustom WorkflowChildren (Map k v) (Map k' v) a a' where
  typesCustom = iso Map.toList Map.fromList . traverse . _1 . typesCustom @WorkflowChildren

instance (Ord b', HasTypesCustom WorkflowChildren a' b' a b) => HasTypesCustom WorkflowChildren (Set a') (Set b') a b where
  typesCustom = iso Set.toList Set.fromList . traverse . typesCustom @WorkflowChildren

instance (HasTypesCustom WorkflowChildren a' b' a b) => HasTypesCustom WorkflowChildren (Seq a') (Seq b') a b where
  typesCustom = traverse . typesCustom @WorkflowChildren

instance (HasTypesCustom WorkflowChildren mono mono' a a', MonoFoldable mono') => HasTypesCustom WorkflowChildren (NonNull mono) (NonNull mono') a a' where
  typesCustom = iso toNullable impureNonNull . typesCustom @WorkflowChildren

instance (HasTypesCustom WorkflowChildren a' b' a b, FoldCase b') => HasTypesCustom WorkflowChildren (CI a') (CI b') a b where
  typesCustom = iso CI.original CI.mk . typesCustom @WorkflowChildren

data WorkflowPayloadSpecChildren
type instance Children WorkflowPayloadSpecChildren a = ChildrenWorkflowPayloadSpecChildren a
type family ChildrenWorkflowPayloadSpecChildren a where
  ChildrenWorkflowPayloadSpecChildren (WorkflowPayloadSpec fileid userid) = '[fileid, userid]

instance {-# OVERLAPPABLE #-} HasTypesCustom WorkflowPayloadSpecChildren (WorkflowPayloadSpec fileid userid) (WorkflowPayloadSpec fileid' userid') a a' => HasTypesCustom WorkflowChildren (WorkflowPayloadSpec fileid userid) (WorkflowPayloadSpec fileid' userid') a a' where
  typesCustom = typesCustom @WorkflowPayloadSpecChildren

instance (Typeable userid, Typeable fileid, Typeable fileid', Ord fileid', userid ~ userid', FileReferenceTitleMapConvertible (FileFieldUserOption Bool) fileid fileid') => HasTypesCustom WorkflowPayloadSpecChildren (WorkflowPayloadSpec fileid userid) (WorkflowPayloadSpec fileid' userid') fileid fileid' where
  typesCustom f (WorkflowPayloadSpec WorkflowPayloadFieldFile{..}) = traverseOf (_fieldAdditionalFiles . _FileReferenceTitleMap . _1) f wpffConfig <&> \wpffConfig' -> WorkflowPayloadSpec WorkflowPayloadFieldFile{ wpffConfig = wpffConfig', .. }
  typesCustom f (WorkflowPayloadSpec WorkflowPayloadFieldMultiple{ wpfmSub = sub, wpfmDefault = def', ..}) = (WorkflowPayloadSpec .) . toField <$> traverseOf (typesCustom @WorkflowPayloadSpecChildren) f sub <*> traverseOf (traverse . traverse . typesCustom @WorkflowChildren) f def'
    where toField wpfmSub wpfmDefault = WorkflowPayloadFieldMultiple{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldText{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldText{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldNumber{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldNumber{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldBool{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldBool{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldDay{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldDay{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldTime{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldTime{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldDateTime{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldDateTime{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldUser{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldUser{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldCaptureUser) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldCaptureUser
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldCaptureDateTime{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldCaptureDateTime{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldReference{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldReference{..}

instance (Typeable userid, Typeable userid', Typeable fileid, fileid ~ fileid') => HasTypesCustom WorkflowPayloadSpecChildren (WorkflowPayloadSpec fileid userid) (WorkflowPayloadSpec fileid' userid') userid userid' where
  typesCustom f (WorkflowPayloadSpec WorkflowPayloadFieldUser{ wpfuDefault = Just fid, .. }) = f fid <&> \fid' -> WorkflowPayloadSpec WorkflowPayloadFieldUser{ wpfuDefault = Just fid', .. }
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldUser{ wpfuDefault = Nothing, ..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldUser{ wpfuDefault = Nothing, ..}
  typesCustom f (WorkflowPayloadSpec WorkflowPayloadFieldMultiple{ wpfmSub = sub, wpfmDefault = def', ..}) = (WorkflowPayloadSpec .) . toField <$> typesCustom @WorkflowPayloadSpecChildren f sub <*> (traverse . typesCustom @WorkflowChildren) f def'
    where toField wpfmSub wpfmDefault = WorkflowPayloadFieldMultiple{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldText{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldText{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldNumber{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldNumber{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldBool{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldBool{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldDay{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldDay{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldTime{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldTime{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldDateTime{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldDateTime{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldFile{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldFile{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldCaptureUser) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldCaptureUser
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldCaptureDateTime{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldCaptureDateTime{..}
  typesCustom _ (WorkflowPayloadSpec WorkflowPayloadFieldReference{..}) = pure $ WorkflowPayloadSpec WorkflowPayloadFieldReference{..}

instance (Typeable userid, Typeable fileid) => HasTypesCustom WorkflowChildren (WorkflowPayloadSpec fileid userid) (WorkflowPayloadSpec fileid userid) WorkflowPayloadLabel WorkflowPayloadLabel where
  typesCustom f (WorkflowPayloadSpec WorkflowPayloadFieldReference{ wpfrTarget }) = f wpfrTarget <&> \wpfrTarget' -> WorkflowPayloadSpec WorkflowPayloadFieldReference{ wpfrTarget = wpfrTarget' }
  typesCustom f (WorkflowPayloadSpec WorkflowPayloadFieldMultiple{..}) = typesCustom @WorkflowChildren f wpfmSub <&> \wpfmSub' -> WorkflowPayloadSpec WorkflowPayloadFieldMultiple{ wpfmSub = wpfmSub', .. }
  typesCustom _ x = pure x

instance (Typeable payload, Typeable fileid, Typeable userid, IsWorkflowFieldPayload' fileid userid payload, IsWorkflowFieldPayload' fileid' userid' payload', fileid ~ fileid', userid ~ userid') => HasTypesCustom WorkflowChildren (WorkflowFieldPayloadW fileid userid) (WorkflowFieldPayloadW fileid' userid') payload payload' where
  typesCustom f pw@(WorkflowFieldPayloadW p) = case typeOf p `eqTypeRep` typeRep @(WorkflowFieldPayload fileid userid payload) of
    Just HRefl -> WorkflowFieldPayloadW <$> typesCustom @WorkflowChildren @(WorkflowFieldPayload fileid userid payload) @(WorkflowFieldPayload fileid' userid' payload') @payload @payload' f p
    Nothing    -> pure pw

instance {-# OVERLAPPING #-} (Typeable fileid, Typeable userid, IsWorkflowFieldPayload' fileid userid userid, IsWorkflowFieldPayload' fileid' userid' userid', fileid ~ fileid') => HasTypesCustom WorkflowChildren (WorkflowFieldPayloadW fileid userid) (WorkflowFieldPayloadW fileid' userid') userid userid' where
  typesCustom f pw@(WorkflowFieldPayloadW p) = case typeOf p `eqTypeRep` typeRep @(WorkflowFieldPayload fileid userid userid) of
    Just HRefl -> WorkflowFieldPayloadW <$> typesCustom @WorkflowChildren @(WorkflowFieldPayload fileid userid userid) @(WorkflowFieldPayload fileid' userid' userid') @userid @userid' f p
    Nothing    -> pure $ unsafeCoerce @(WorkflowFieldPayloadW fileid userid) @(WorkflowFieldPayloadW fileid userid') pw -- We have proof that @p@ does not contain a value of type @userid@, therefor coercion is safe

instance {-# OVERLAPPING #-} (Typeable userid, Typeable fileid, IsWorkflowFieldPayload' fileid userid fileid, IsWorkflowFieldPayload' fileid' userid' fileid', userid ~ userid') => HasTypesCustom WorkflowChildren (WorkflowFieldPayloadW fileid userid) (WorkflowFieldPayloadW fileid' userid') fileid fileid' where
  typesCustom f pw@(WorkflowFieldPayloadW p) = case typeOf p `eqTypeRep` typeRep @(WorkflowFieldPayload fileid userid fileid) of
    Just HRefl -> WorkflowFieldPayloadW <$> typesCustom @WorkflowChildren @(WorkflowFieldPayload fileid userid fileid) @(WorkflowFieldPayload fileid' userid' fileid') @fileid @fileid' f p
    Nothing    -> pure $ unsafeCoerce @(WorkflowFieldPayloadW fileid userid) @(WorkflowFieldPayloadW fileid' userid) pw -- We have proof that @p@ does not contain a value of type @fileid@, therefor coercion is safe

instance (IsWorkflowFieldPayload' fileid userid payload, IsWorkflowFieldPayload' fileid' userid' payload') => HasTypesCustom WorkflowChildren (WorkflowFieldPayload fileid userid payload) (WorkflowFieldPayload fileid' userid' payload') payload payload' where
  typesCustom f x = case x ^? _WorkflowFieldPayload of
    Just x' -> review _WorkflowFieldPayload <$> f x'
    Nothing -> error "@WorkflowFieldPayload fileid userid payload@ does not contain value of type @payload@; this means `IsWorkflowFieldPayload` is invalid"

-- instance (Ord userid, Ord fileid, Typeable payload, Typeable fileid, Typeable userid, IsWorkflowFieldPayload' fileid userid payload, IsWorkflowFieldPayload' fileid' userid' payload', fileid ~ fileid', userid ~ userid') => HasTypesCustom WorkflowChildren (WorkflowAction fileid userid) (WorkflowAction fileid' userid') payload payload' where
--   typesCustom = _wpPayload . typesCustom @WorkflowChildren

-- instance {-# OVERLAPPING #-} (Ord userid', Ord fileid, Typeable fileid, IsWorkflowFieldPayload' fileid userid userid, IsWorkflowFieldPayload' fileid' userid' userid', fileid ~ fileid') => HasTypesCustom WorkflowChildren (WorkflowAction fileid userid) (WorkflowAction fileid' userid') userid userid' where
--   typesCustom f WorkflowAction{..} = WorkflowAction wpTo wpVia
--     <$> traverseOf (typesCustom @WorkflowChildren @_ @_ @userid @userid') f wpPayload
--     <*> traverseOf (_Just . _Just) f wpUser
--     <*> pure wpTime


-- workflowStatePayload :: forall fileid userid payload.
--                         ( HasTypesCustom WorkflowChildren (WorkflowFieldPayloadW fileid userid) (WorkflowFieldPayloadW fileid userid) payload payload
--                         , Ord payload
--                         )
--                      => WorkflowPayloadLabel -> WorkflowState fileid userid -> Seq (Maybe (Set payload))
-- workflowStatePayload label acts = flip ofoldMap acts $ \WorkflowAction{..} -> Seq.singleton . Map.lookup label $ fmap (setOf $ folded . typesCustom @WorkflowChildren @(WorkflowFieldPayloadW fileid userid) @(WorkflowFieldPayloadW fileid userid) @payload @payload) wpPayload


----- WORKFLOW: AUTHORIZATION -----

data WorkflowGraphAuthAction
  = WorkflowGraphAuthViewWorkflowWorkflow
  | WorkflowGraphAuthViewWorkflowWorkflowAction -- ^ Either the action that is the context auth is being evaluated in or the latest action if context is larger
  | WorkflowGraphAuthViewWorkflowWorkflowActor -- ^ Actor of either the action that is the context auth is being evaluated in or the latest action if context is larger
  | WorkflowGraphAuthViewWorkflowWorkflowNodeLabel
  | WorkflowGraphAuthViewWorkflowWorkflowPayload -- ^ Either the payload that is the context auth is being evaluated in or any payload of the focused/latest action if context is larger
  | WorkflowGraphAuthViewWorkflowWorkflowPayloadLabel
  | WorkflowGraphAuthHasAllowedEdge
  | WorkflowGraphAuthHasAllowedInitialEdge
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowGraphAuthActionPredicate'
  = WorkflowGraphAuthActionToNode
    { wgaapNode :: NonNull (Set WorkflowGraphNodeLabel)
    , wgaapViaEdge :: Maybe (NonNull (Set WorkflowGraphEdgeLabel))
    }
  | WorkflowGraphAuthActionWithPayload
    { wgaapPayload :: NonNull (Set WorkflowPayloadLabel)
    , wgaapToNode :: Maybe (NonNull (Set WorkflowGraphNodeLabel))
    }
  | WorkflowGraphAuthActionCurrentUserIsActor
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowGraphAuthActionPredicate = WorkflowGraphAuthActionPredicate
  { wgaapReference :: WorkflowGraphAuthActionReference
  , wgaapPredicate :: WorkflowGraphAuthActionPredicate'
  } deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
    deriving anyclass (NFData)

data WorkflowGraphAuthActionReference
  = WorkflowGraphAuthActionReferenceAny
  | WorkflowGraphAuthActionReferenceFocus -- ^ Latest action if evaluated in context larger than single action
  | WorkflowGraphAuthActionReferenceFirst
  deriving stock (Eq, Ord, Read, Show, Enum, Bounded, Data, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)

data WorkflowGraphAuthPayloadPredicate'
  = WorkflowGraphAuthPayloadHasLabel
    { wgappLabels :: NonNull (Set WorkflowPayloadLabel)
    }
  | WorkflowGraphAuthPayloadWithinAction
    { wgappAction :: WorkflowGraphAuthActionPredicate'
    }
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowGraphAuthPayloadPredicate = WorkflowGraphAuthPayloadPredicate
  { wgappReference :: WorkflowGraphAuthPayloadReference
  , wgappPredicate :: WorkflowGraphAuthPayloadPredicate'
  } deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
    deriving anyclass (NFData)

data WorkflowGraphAuthPayloadReference
  = WorkflowGraphAuthPayloadReferenceAny
  | WorkflowGraphAuthPayloadReferenceFocus -- ^ Any payload of (latest) action if evaluated in context larger than single payload
  deriving stock (Eq, Ord, Read, Show, Enum, Bounded, Data, Generic, Typeable)
  deriving anyclass (Universe, Finite, NFData)

_WorkflowGraphAuthActionReferencePayload :: Prism' WorkflowGraphAuthActionReference WorkflowGraphAuthPayloadReference
_WorkflowGraphAuthActionReferencePayload = prism' toAction fromAction
  where toAction = \case
          WorkflowGraphAuthPayloadReferenceAny   -> WorkflowGraphAuthActionReferenceAny
          WorkflowGraphAuthPayloadReferenceFocus -> WorkflowGraphAuthActionReferenceFocus
        fromAction = \case
          WorkflowGraphAuthActionReferenceAny   -> Just WorkflowGraphAuthPayloadReferenceAny
          WorkflowGraphAuthActionReferenceFocus -> Just WorkflowGraphAuthPayloadReferenceFocus
          _                                     -> Nothing

data WorkflowGraphAuthExpression
  = WorkflowGraphAuthTrue
  | WorkflowGraphAuthHasAction { wgaActionPredicate :: PredDNF WorkflowGraphAuthActionPredicate }
  | WorkflowGraphAuthHasPayload { wgaPayloadPredicate :: PredDNF WorkflowGraphAuthPayloadPredicate }
  | WorkflowGraphAuthOr (NonNull (Set WorkflowGraphAuthExpression))
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

data WorkflowGraphAuthRole userid
  = WorkflowGraphAuthRoleAny
  | WorkflowGraphAuthRoleUser { wgarUser :: userid }
  | WorkflowGraphAuthRolePayloadReference { wgarPayloadLabel :: WorkflowPayloadLabel }
  | WorkflowGraphAuthRoleAuthorized { wgarAuthorized :: AuthDNF }
  | WorkflowGraphAuthRoleInitiator
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

makePrisms ''WorkflowGraphAuthExpression

-- | Disjunctive
instance Semigroup WorkflowGraphAuthExpression where
  a <> b = flat . WorkflowGraphAuthOr $ opoint a <> opoint b
    where
      flat = \case
        WorkflowGraphAuthOr xs
          | (ys, nonEmpty -> Just xs') <- partitioning _WorkflowGraphAuthOr . map flat $ otoList xs
            -> WorkflowGraphAuthOr . maybe id (<>) (fromNullable ys) $ sconcat xs'
        other -> other

makePrisms ''WorkflowGraphAuthAction
makePrisms ''WorkflowGraphAuthActionPredicate'
makeLenses_ ''WorkflowGraphAuthActionPredicate'
makeLenses_ ''WorkflowGraphAuthActionPredicate
makePrisms ''WorkflowGraphAuthActionReference
makePrisms ''WorkflowGraphAuthRole

-- | For given context (`WorkflowGraphAuthAction`) and every
-- `WorkflowRole` the current user satifies, evaluate: does any
-- @WorkflowWorkflow@ in the scope being considered fulfill the
-- `WorkflowGraphAuthActionPredicate`?
newtype WorkflowGraphAuth userid
  = WorkflowGraphAuth { getWorkflowGraphAuth :: Map WorkflowGraphAuthAction (Map (WorkflowGraphAuthRole userid) WorkflowGraphAuthExpression) }
  deriving stock (Eq, Ord, Read, Show, Data, Generic, Typeable)
  deriving anyclass (NFData)

deriving via (MergeMap WorkflowGraphAuthAction (MergeMap (WorkflowGraphAuthRole userid) WorkflowGraphAuthExpression)) instance Ord userid => Semigroup (WorkflowGraphAuth userid)
deriving via (MergeMap WorkflowGraphAuthAction (MergeMap (WorkflowGraphAuthRole userid) WorkflowGraphAuthExpression)) instance Ord userid => Monoid (WorkflowGraphAuth userid)


mkWorkflowGraphAuthUnoptimized :: forall fileid userid. Ord userid => WorkflowGraph fileid userid -> WorkflowGraphAuth userid
mkWorkflowGraphAuthUnoptimized WorkflowGraph{..} = execWriter @(WorkflowGraphAuth userid) $ do
  tell . WorkflowGraphAuth . Map.singleton WorkflowGraphAuthViewWorkflowWorkflow . Map.singleton WorkflowGraphAuthRoleAny . hasAnyAction . predDNFSingleton $ PLVariable WorkflowGraphAuthActionCurrentUserIsActor
  tell . WorkflowGraphAuth . Map.singleton WorkflowGraphAuthViewWorkflowWorkflowAction . Map.singleton WorkflowGraphAuthRoleAny . hasFocusAction . predDNFSingleton $ PLVariable WorkflowGraphAuthActionCurrentUserIsActor
  tell . WorkflowGraphAuth . Map.singleton WorkflowGraphAuthViewWorkflowWorkflowPayload . Map.singleton WorkflowGraphAuthRoleAny . hasFocusAction . predDNFSingleton $ PLVariable WorkflowGraphAuthActionCurrentUserIsActor

  iforM_ wgNodes $ \toNode WGN{..} -> do
    forM_ wgnViewers $ \WorkflowNodeView{..} -> do
      tell . fromRoleSet WorkflowGraphAuthViewWorkflowWorkflow (toNullable wnvViewers) . hasAnyAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionToNode (opoint toNode) Nothing
      tell . fromRoleSet WorkflowGraphAuthViewWorkflowWorkflowAction (toNullable wnvViewers) . hasFocusAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionToNode (opoint toNode) Nothing
      tell . fromRoleSet WorkflowGraphAuthViewWorkflowWorkflowNodeLabel (toNullable wnvViewers) . hasFocusAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionToNode (opoint toNode) Nothing
    iforM_ wgnEdges $ \edgeLbl edge -> do
      tell . fromRoleSet WorkflowGraphAuthViewWorkflowWorkflowActor (edge ^. _wgeViewActor) . hasFocusAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionToNode (opoint toNode) (Just $ opoint edgeLbl)

      case edge of
        WorkflowGraphEdgeAutomatic{} -> return ()
        WorkflowGraphEdgeInitial{..}
          -> tell $ fromRoleSet WorkflowGraphAuthHasAllowedInitialEdge wgeActors WorkflowGraphAuthTrue
        WorkflowGraphEdgeManual{..}
          -> tell . fromRoleSet WorkflowGraphAuthHasAllowedEdge wgeActors . hasFocusAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionToNode (opoint wgeSource) Nothing
    iforM_ wgnPayloadView $ \payloadLbl WorkflowPayloadView{..} -> do
      tell . fromRoleSet WorkflowGraphAuthViewWorkflowWorkflowAction (toNullable wpvViewers) . hasFocusAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionWithPayload (opoint payloadLbl) (Just $ opoint toNode)
      tell . fromRoleSet WorkflowGraphAuthViewWorkflowWorkflow (toNullable wpvViewers) . hasAnyAction . predDNFSingleton . PLVariable $ WorkflowGraphAuthActionWithPayload (opoint payloadLbl) (Just $ opoint toNode)
      tell $ fromRoleSet WorkflowGraphAuthViewWorkflowWorkflowPayload (toNullable wpvViewers) . hasFocusPayload $ predDNFSingleton (PLVariable . WorkflowGraphAuthPayloadHasLabel $ opoint payloadLbl) `predDNFAnd` predDNFSingleton (PLVariable . WorkflowGraphAuthPayloadWithinAction $ WorkflowGraphAuthActionToNode (opoint toNode) Nothing)
      tell $ fromRoleSet WorkflowGraphAuthViewWorkflowWorkflowPayloadLabel (toNullable wpvViewers) . hasFocusPayload $ predDNFSingleton (PLVariable . WorkflowGraphAuthPayloadHasLabel $ opoint payloadLbl) `predDNFAnd` predDNFSingleton (PLVariable . WorkflowGraphAuthPayloadWithinAction $ WorkflowGraphAuthActionToNode (opoint toNode) Nothing)
  where
    fromRoleSet :: WorkflowGraphAuthAction -> Set (WorkflowRole userid) -> WorkflowGraphAuthExpression -> WorkflowGraphAuth userid
    fromRoleSet act roles expr = WorkflowGraphAuth . Map.singleton act $ Map.fromSet (const expr) roles'
      where roles' = flip ofoldMap roles $ \case
              WorkflowRoleUser{..} -> Set.singleton $ WorkflowGraphAuthRoleUser workflowRoleUser
              WorkflowRolePayloadReference{..} -> Set.singleton $ WorkflowGraphAuthRolePayloadReference workflowRolePayloadLabel
              WorkflowRoleAuthorized{..} -> Set.singleton $ WorkflowGraphAuthRoleAuthorized workflowRoleAuthorized
              WorkflowRoleInitiator -> Set.singleton WorkflowGraphAuthRoleInitiator

    hasAnyAction, hasFocusAction :: PredDNF WorkflowGraphAuthActionPredicate' -> WorkflowGraphAuthExpression
    hasAnyAction = WorkflowGraphAuthHasAction . over plVars (WorkflowGraphAuthActionPredicate WorkflowGraphAuthActionReferenceAny)
    hasFocusAction = WorkflowGraphAuthHasAction . over plVars (WorkflowGraphAuthActionPredicate WorkflowGraphAuthActionReferenceFocus)

    hasFocusPayload :: PredDNF WorkflowGraphAuthPayloadPredicate' -> WorkflowGraphAuthExpression
    hasFocusPayload = WorkflowGraphAuthHasPayload . over plVars (WorkflowGraphAuthPayloadPredicate WorkflowGraphAuthPayloadReferenceFocus)

optimizeWorkflowGraphAuth :: forall fileid userid. (Ord fileid, Ord userid, Ord (FileReferenceTitleMap fileid (FileFieldUserOption Bool)), Typeable fileid, Typeable userid, HasTypesUsing WorkflowChildren userid userid WorkflowPayloadLabel WorkflowPayloadLabel) => WorkflowGraph fileid userid -> WorkflowGraphAuth userid -> WorkflowGraphAuth userid
optimizeWorkflowGraphAuth WorkflowGraph{..} = optimize
  where
    optimize x | x == x' = x'
               | otherwise = optimize x'
      where
        x' = optimizationStep x

        optimizationStep :: WorkflowGraphAuth userid -> WorkflowGraphAuth userid
        optimizationStep (WorkflowGraphAuth gaMap) = WorkflowGraphAuth gaMap'
          where
            gaMap' = gaMap
              <&> mapped %~ optimizeStepExpr
              <&> Map.filter (not . exprIsKnownFalse)
               &  Map.filter (not . Map.null)

            exprIsKnownFalse = \case
              WorkflowGraphAuthTrue -> False
              WorkflowGraphAuthHasAction aPred -> aPred == predDNFFalse
              WorkflowGraphAuthHasPayload pPred -> pPred == predDNFFalse
              WorkflowGraphAuthOr as -> all exprIsKnownFalse as

            optimizeStepExpr, optimizeExprStructure :: WorkflowGraphAuthExpression -> WorkflowGraphAuthExpression
            optimizeExprStructure = \case
              WorkflowGraphAuthOr xs
                | any (is _WorkflowGraphAuthTrue) xs
                  -> WorkflowGraphAuthTrue
                | (fromNullable -> ys, nonEmpty -> Just xs') <- partitioning _WorkflowGraphAuthOr xs
                  -> optimizeExprStructure . WorkflowGraphAuthOr . maybe id (<>) ys $ sconcat xs'
                | (fromNullable -> ys, fromNullable @(Set _) -> Just xs') <- partitioning _WorkflowGraphAuthHasAction xs
                , length xs' >= 2 || is _Nothing ys
                  -> optimizeExprStructure $
                       let xs'' = WorkflowGraphAuthHasAction $ ofoldr1 predDNFOr xs'
                        in case ys of
                             Nothing -> xs''
                             Just ys' -> WorkflowGraphAuthOr $ opoint xs'' <> ys'
                | (fromNullable -> ys, fromNullable @(Set _) -> Just xs') <- partitioning _WorkflowGraphAuthHasPayload xs
                , length xs' >= 2 || is _Nothing ys
                  -> optimizeExprStructure $
                       let xs'' = WorkflowGraphAuthHasPayload $ ofoldr1 predDNFOr xs'
                        in case ys of
                             Nothing -> xs''
                             Just ys' -> WorkflowGraphAuthOr $ opoint xs'' <> ys'
              other -> other

            optimizeStepExpr = fromMaybe WorkflowGraphAuthTrue . traverseOf (_WorkflowGraphAuthHasAction . _dnfTerms) (fmap Set.fromList . mapM filterAIsKnownTrue . Set.toList)
                           >>> over (_WorkflowGraphAuthHasAction . _dnfTerms) filterAIsKnownFalse
                           >>> over (_WorkflowGraphAuthHasAction . _dnfTerms) unionToNode
                           >>> over (_WorkflowGraphAuthHasAction . _dnfTerms) unionWithPayload

                           >>> fromMaybe WorkflowGraphAuthTrue . traverseOf (_WorkflowGraphAuthHasPayload . _dnfTerms) (fmap Set.fromList . mapM filterPIsKnownTrue . Set.toList)
                           >>> over (_WorkflowGraphAuthHasPayload . _dnfTerms) filterPIsKnownFalse
                           >>> over (_WorkflowGraphAuthHasPayload . _dnfTerms) unionLabels
                           >>> (\x'' -> maybe x'' unionWithinAction $ x'' ^? _WorkflowGraphAuthHasPayload . _dnfTerms)

                           >>> over _WorkflowGraphAuthOr (impureNonNull . Set.fromList . map optimizeStepExpr . otoList)
                           >>> optimizeExprStructure
              where
                allNodeLabels :: Set WorkflowGraphNodeLabel
                -- allNodeLabels = setOf (typesUsing @WorkflowChildren) graph
                allNodeLabels = Map.keysSet wgNodes

                allEdgeLabels :: Set WorkflowGraphEdgeLabel
                allEdgeLabels = foldMap (Map.keysSet . wgnEdges) wgNodes

                -- unionToNode = ala Endo foldMap $ (.) <$> uncollect <*> collect
                --   where
                --     collect :: WorkflowGraphAuthActionReference
                --             -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                --             -> MergeMap (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (Set WorkflowGraphNodeLabel))))
                --     collect ref = foldMap go
                --       where
                --         go (toNullable -> conj) = case toNodes of
                --           Nothing -> MergeMap $ Map.singleton other Nothing
                --           Just toNodes' -> foldMap (MergeMap . Map.singleton other . Just) toNodes'
                --           where
                --             (Set.fromList -> other, fromNullable -> toNodes) = partitionWith positiveNodes $ Set.toList conj
                --             positiveNodes lit = maybe (Left lit) Right $ do
                --               WorkflowGraphAuthActionPredicate{..} <- return $ lit ^. _plVar
                --               guard $ wgaapReference == ref
                --               WorkflowGraphAuthActionToNode{..} <- return wgaapPredicate
                --               return . fromNullable . bool id (allNodeLabels `Set.difference`) (is _PLNegated lit) $ toNullable wgaapNode

                --     uncollect :: WorkflowGraphAuthActionReference
                --               -> MergeMap (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (Set WorkflowGraphNodeLabel))))
                --               -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                --     uncollect wgaapReference = ifoldMap $ \rest -> \case
                --       Nothing -> maybe Set.empty Set.singleton $ fromNullable rest
                --       Just Nothing -> Set.empty -- empty set of nodes is not satisfiable
                --       Just (Just wgaapNode) -> Set.singleton . maybe id (<>) (fromNullable rest) . opoint $ PLVariable WorkflowGraphAuthActionPredicate{ wgaapPredicate = WorkflowGraphAuthActionToNode{..}, .. }

                unionToNode = ala Endo foldMap ((.) <$> uncollectNodes <*> collectNodes)
                          >>> ala Endo foldMap ((.) <$> uncollectViaEdges <*> collectViaEdges)
                  where
                    collectNodes :: WorkflowGraphAuthActionReference
                                 -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                                 -> MergeMap (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (MergeMap (Maybe (NonNull (Set WorkflowGraphEdgeLabel))) (NonNull (Set WorkflowGraphNodeLabel))))))
                    collectNodes ref = foldMap go
                      where
                        go (toNullable -> conj) = case toNodes of
                          Nothing -> MergeMap $ Map.singleton other Nothing
                          Just toNodes' -> foldMap (MergeMap . Map.singleton other) toNodes'
                          where
                            (Set.fromList -> other, fromNullable -> toNodes) = partitionWith positiveNodes $ Set.toList conj
                            positiveNodes lit = maybe (Left lit) Right $ do
                              WorkflowGraphAuthActionPredicate{..} <- return $ lit ^. _plVar
                              guard $ wgaapReference == ref
                              WorkflowGraphAuthActionToNode{..} <- return wgaapPredicate
                              return $ case wgaapViaEdge of
                                Nothing -> Just . fmap (impureNonNull . MergeMap . Map.singleton Nothing) . fromNullable . bool id (allNodeLabels `Set.difference`) (is _PLNegated lit) $ toNullable wgaapNode
                                Just viaEdges
                                  | is _PLNegated lit -> Just (impureNonNull . MergeMap . Map.singleton Nothing <$> fromNullable (allNodeLabels `Set.difference` toNullable wgaapNode))
                                               `mappend` fmap (\viaEdges' -> impureNonNull . MergeMap . Map.singleton (Just viaEdges') <$> fromNullable allNodeLabels) (fromNullable $ allEdgeLabels `Set.difference` toNullable viaEdges)
                                  | otherwise -> Just . fmap (impureNonNull . MergeMap . Map.singleton wgaapViaEdge) . fromNullable $ toNullable wgaapNode

                    uncollectNodes :: WorkflowGraphAuthActionReference
                                   -> MergeMap (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (MergeMap (Maybe (NonNull (Set WorkflowGraphEdgeLabel))) (NonNull (Set WorkflowGraphNodeLabel))))))
                                   -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                    uncollectNodes wgaapReference = ifoldMap $ \rest -> \case
                      Nothing -> maybe Set.empty Set.singleton $ fromNullable rest
                      Just Nothing -> Set.empty -- empty set of nodes is not satisfiable
                      Just (Just toNodes) -> flip ifoldMap1 toNodes $ \wgaapViaEdge wgaapNode -> Set.singleton . maybe id (<>) (fromNullable rest) . opoint $ PLVariable WorkflowGraphAuthActionPredicate{ wgaapPredicate = WorkflowGraphAuthActionToNode{..}, .. }

                    collectViaEdges :: WorkflowGraphAuthActionReference
                                    -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                                    -> Map (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel)))))))
                    collectViaEdges ref = foldr go Map.empty
                      where
                        go (toNullable -> conj) = case withViaEdges of
                          Nothing -> Map.insertWith merge other Nothing
                          Just withViaEdges' -> Map.unionsWith merge . (: map (Map.singleton other) (toNullable withViaEdges'))
                          where
                            (Set.fromList -> other, fromNullable -> withViaEdges) = partitionWith positiveViaEdges $ Set.toList conj
                            positiveViaEdges :: PredLiteral WorkflowGraphAuthActionPredicate
                                            -> Either (PredLiteral WorkflowGraphAuthActionPredicate) (Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel)))))))
                            positiveViaEdges lit = maybe (Left lit) Right $ do
                              WorkflowGraphAuthActionPredicate{..} <- return $ lit ^. _plVar
                              guard $ wgaapReference == ref
                              WorkflowGraphAuthActionToNode{..} <- return wgaapPredicate
                              return $ case wgaapViaEdge of
                                Nothing
                                  | is _PLNegated lit -> Just $ impureNonNull . flip Map.singleton Nothing <$> fromNullable (allNodeLabels `Set.difference` toNullable wgaapNode)
                                  | otherwise -> Just . Just . impureNonNull $ Map.singleton wgaapNode Nothing
                                Just viaEdges
                                  | is _PLNegated lit -> Just (impureNonNull . Map.singleton wgaapNode . Just <$> fromNullable (allEdgeLabels `Set.difference` toNullable viaEdges))
                                                 `merge` Just (impureNonNull . flip Map.singleton Nothing <$> fromNullable (allNodeLabels `Set.difference` toNullable wgaapNode))
                                  | otherwise -> Just . Just . impureNonNull $ Map.singleton wgaapNode wgaapViaEdge
                            merge :: Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel))))))
                                  -> Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel))))))
                                  -> Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel))))))
                            merge = liftM2 . liftM2 $ ((impureNonNull . mergeMapKeys) .) . (Map.unionWithKey merge' `on` toNullable)
                              where
                                merge' _ Nothing _ = Nothing
                                merge' _ _ Nothing = Nothing
                                merge' nodeLbls ns1 ns2 = assertM (not . (nodeEdgeLabels `Set.isSubsetOf`) . toNullable) $ ns1 <> ns2
                                  where nodeEdgeLabels = foldMap (foldMap (Map.keysSet . wgnEdges) . flip Map.lookup wgNodes) nodeLbls

                                mergeMapKeys :: Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel)))
                                             -> Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel)))
                                mergeMapKeys lits = Map.fromList . go' $ Map.toList lits
                                  where
                                    go' [] = []
                                    go' (l1@(ns1, es1) : remLits) = fromMaybe (l1 : go' remLits) $ asum
                                      [ do
                                          let toQLbls (ns, es) = do
                                                n <- otoList ns
                                                let nodeLabels = foldMap (Map.keysSet . wgnEdges) (Map.lookup n wgNodes)
                                                e <- otoList $ maybe nodeLabels toNullable es
                                                guard $ e `Set.member` nodeLabels
                                                return (n, e)
                                              l' = (ns1 <> ns2, (<>) <$> es1 <*> es2)
                                          guard $ Set.fromList (toQLbls l1 ++ toQLbls l2) == Set.fromList (toQLbls l')
                                          return $ l' : go' remLits'
                                      | (l2@(ns2, es2), remLits') <- foci remLits
                                      ]
                                      where
                                        foci xs = [ (f, i ++ t)
                                                  | i <- List.inits xs
                                                  | f : t <- List.tails xs
                                                  ]

                    uncollectViaEdges :: WorkflowGraphAuthActionReference
                                      -> Map (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowGraphNodeLabel)) (Maybe (NonNull (Set WorkflowGraphEdgeLabel)))))))
                                      -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                    uncollectViaEdges wgaapReference = ifoldMap $ \rest -> \case
                      Nothing -> maybe Set.empty Set.singleton $ fromNullable rest
                      Just Nothing -> Set.empty -- empty set of nodes/edges is not satisfiable
                      Just (Just withViaEdges) -> flip ifoldMap1 withViaEdges $ \wgaapNode wgaapViaEdge -> Set.singleton . maybe id (<>) (fromNullable rest) . opoint $ PLVariable WorkflowGraphAuthActionPredicate{ wgaapPredicate = WorkflowGraphAuthActionToNode{..}, .. }

                allPayloadLabels :: Set WorkflowPayloadLabel
                -- allPayloadLabels = setOf (typesCustom @WorkflowChildren) graph
                allPayloadLabels = foldMap (\WGN{..} -> Map.keysSet wgnPayloadView <> foldMap (\WorkflowPayloadView{..} -> setOf (typesCustom @WorkflowChildren) wpvViewers) wgnPayloadView <> foldMap (\wge -> setOf (_wgeForm . typesCustom @WorkflowChildren) wge <> setOf (_wgeRestriction . typesCustom @WorkflowChildren) wge <> setOf (_wgeMessages . folded . ((_wemViewers . typesCustom @WorkflowChildren) <> (_wemRestriction . typesCustom @WorkflowChildren))) wge) wgnEdges <> setOf (folded . (_wnmViewers . typesCustom @WorkflowChildren <> _wnmRestriction . typesCustom @WorkflowChildren)) wgnMessages) wgNodes

                unionWithPayload = ala Endo foldMap ((.) <$> uncollectPayloads <*> collectPayloads)
                               >>> ala Endo foldMap ((.) <$> uncollectToNodes <*> collectToNodes)
                  where
                    collectPayloads :: WorkflowGraphAuthActionReference
                                    -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                                    -> MergeMap (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (MergeMap (Maybe (NonNull (Set WorkflowGraphNodeLabel))) (NonNull (Set WorkflowPayloadLabel))))))
                    collectPayloads ref = foldMap go
                      where
                        go (toNullable -> conj) = case withPayloads of
                          Nothing -> MergeMap $ Map.singleton other Nothing
                          Just withPayloads' -> foldMap (MergeMap . Map.singleton other) withPayloads'
                          where
                            (Set.fromList -> other, fromNullable -> withPayloads) = partitionWith positivePayloads $ Set.toList conj
                            positivePayloads lit = maybe (Left lit) Right $ do
                              WorkflowGraphAuthActionPredicate{..} <- return $ lit ^. _plVar
                              guard $ wgaapReference == ref
                              WorkflowGraphAuthActionWithPayload{..} <- return wgaapPredicate
                              return $ case wgaapToNode of
                                Nothing -> Just . fmap (impureNonNull . MergeMap . Map.singleton Nothing) . fromNullable . bool id (allPayloadLabels `Set.difference`) (is _PLNegated lit) $ toNullable wgaapPayload
                                Just toNodes
                                  | is _PLNegated lit -> Just (impureNonNull . MergeMap . Map.singleton Nothing <$> fromNullable (allPayloadLabels `Set.difference` toNullable wgaapPayload))
                                               `mappend` fmap (\toNodes' -> impureNonNull . MergeMap . Map.singleton (Just toNodes') <$> fromNullable allPayloadLabels) (fromNullable $ allNodeLabels `Set.difference` toNullable toNodes)
                                  | otherwise -> Just . fmap (impureNonNull . MergeMap . Map.singleton wgaapToNode) . fromNullable $ toNullable wgaapPayload

                    uncollectPayloads :: WorkflowGraphAuthActionReference
                                      -> MergeMap (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (MergeMap (Maybe (NonNull (Set WorkflowGraphNodeLabel))) (NonNull (Set WorkflowPayloadLabel))))))
                                      -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                    uncollectPayloads wgaapReference = ifoldMap $ \rest -> \case
                      Nothing -> maybe Set.empty Set.singleton $ fromNullable rest
                      Just Nothing -> Set.empty -- empty set of payloads is not satisfiable
                      Just (Just withPayloads) -> flip ifoldMap1 withPayloads $ \wgaapToNode wgaapPayload -> Set.singleton . maybe id (<>) (fromNullable rest) . opoint $ PLVariable WorkflowGraphAuthActionPredicate{ wgaapPredicate = WorkflowGraphAuthActionWithPayload{..}, .. }

                    collectToNodes :: WorkflowGraphAuthActionReference
                                   -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                                   -> Map (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowPayloadLabel)) (Maybe (NonNull (Set WorkflowGraphNodeLabel)))))))
                    collectToNodes ref = foldr go Map.empty
                      where
                        go (toNullable -> conj) = case withToNodes of
                          Nothing -> Map.insertWith merge other Nothing
                          Just withToNodes' -> Map.unionsWith merge . (: map (Map.singleton other) (toNullable withToNodes'))
                          where
                            (Set.fromList -> other, fromNullable -> withToNodes) = partitionWith positiveToNodes $ Set.toList conj
                            positiveToNodes :: PredLiteral WorkflowGraphAuthActionPredicate
                                            -> Either (PredLiteral WorkflowGraphAuthActionPredicate) (Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowPayloadLabel)) (Maybe (NonNull (Set WorkflowGraphNodeLabel)))))))
                            positiveToNodes lit = maybe (Left lit) Right $ do
                              WorkflowGraphAuthActionPredicate{..} <- return $ lit ^. _plVar
                              guard $ wgaapReference == ref
                              WorkflowGraphAuthActionWithPayload{..} <- return wgaapPredicate
                              return $ case wgaapToNode of
                                Nothing
                                  | is _PLNegated lit -> Just $ impureNonNull . flip Map.singleton Nothing <$> fromNullable (allPayloadLabels `Set.difference` toNullable wgaapPayload)
                                  | otherwise -> Just . Just . impureNonNull $ Map.singleton wgaapPayload Nothing
                                Just toNodes
                                  | is _PLNegated lit -> Just (impureNonNull . Map.singleton wgaapPayload . Just <$> fromNullable (allNodeLabels `Set.difference` toNullable toNodes))
                                                 `merge` Just (impureNonNull . flip Map.singleton Nothing <$> fromNullable (allPayloadLabels `Set.difference` toNullable wgaapPayload))
                                  | otherwise -> Just . Just . impureNonNull $ Map.singleton wgaapPayload wgaapToNode
                            merge :: Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowPayloadLabel)) (Maybe (NonNull (Set WorkflowGraphNodeLabel))))))
                                  -> Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowPayloadLabel)) (Maybe (NonNull (Set WorkflowGraphNodeLabel))))))
                                  -> Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowPayloadLabel)) (Maybe (NonNull (Set WorkflowGraphNodeLabel))))))
                            merge = liftM2 . liftM2 $ (impureNonNull .) . (Map.unionWith merge' `on` toNullable)
                              where
                                merge' Nothing _ = Nothing
                                merge' _ Nothing = Nothing
                                merge' ns1 ns2 = assertM (not . (allNodeLabels `Set.isSubsetOf`) . toNullable) $ ns1 <> ns2

                    uncollectToNodes :: WorkflowGraphAuthActionReference
                                      -> Map (Set (PredLiteral WorkflowGraphAuthActionPredicate)) (Maybe (Maybe (NonNull (Map (NonNull (Set WorkflowPayloadLabel)) (Maybe (NonNull (Set WorkflowGraphNodeLabel)))))))
                                      -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                    uncollectToNodes wgaapReference = ifoldMap $ \rest -> \case
                      Nothing -> maybe Set.empty Set.singleton $ fromNullable rest
                      Just Nothing -> Set.empty -- empty set of payloads/nodes is not satisfiable
                      Just (Just withToNodes) -> flip ifoldMap1 withToNodes $ \wgaapPayload wgaapToNode -> Set.singleton . maybe id (<>) (fromNullable rest) . opoint $ PLVariable WorkflowGraphAuthActionPredicate{ wgaapPredicate = WorkflowGraphAuthActionWithPayload{..}, .. }

                unionLabels = ala Endo foldMap ((.) <$> uncollectLabels <*> collectLabels)
                  where
                    collectLabels :: WorkflowGraphAuthPayloadReference
                                  -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)))
                                  -> MergeMap (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)) (Maybe (Maybe (NonNull (Set WorkflowPayloadLabel))))
                    collectLabels ref = foldMap go
                      where
                        go (toNullable -> conj) = case labels of
                          Nothing -> MergeMap $ Map.singleton other Nothing
                          Just labels' -> foldMap (MergeMap . Map.singleton other . Just) labels'
                          where
                            (Set.fromList -> other, fromNullable -> labels) = partitionWith positiveLabels $ Set.toList conj
                            positiveLabels lit = maybe (Left lit) Right $ do
                              WorkflowGraphAuthPayloadPredicate{..} <- return $ lit ^. _plVar
                              guard $ wgappReference == ref
                              WorkflowGraphAuthPayloadHasLabel{..} <- return wgappPredicate
                              return . fromNullable . bool id (allPayloadLabels `Set.difference`) (is _PLNegated lit) $ toNullable wgappLabels
                    uncollectLabels :: WorkflowGraphAuthPayloadReference
                                    -> MergeMap (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)) (Maybe (Maybe (NonNull (Set WorkflowPayloadLabel))))
                                    -> Set (NonNull (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)))
                    uncollectLabels wgappReference = ifoldMap $ \rest -> \case
                      Nothing -> maybe Set.empty Set.singleton $ fromNullable rest
                      Just Nothing -> Set.empty -- empty set of payloads is not satisfiable
                      Just (Just wgappLabels) -> Set.singleton . maybe id (<>) (fromNullable rest) . opoint $ PLVariable WorkflowGraphAuthPayloadPredicate{ wgappPredicate = WorkflowGraphAuthPayloadHasLabel{..}, .. }

                unionWithinAction = uncollectWithinActions . collectWithinActions
                  where
                    collectWithinActions :: Set (NonNull (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)))
                                         -> MergeMap (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)) (Any, Maybe (NonNull (Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate))))))
                    collectWithinActions = foldMap go
                      where
                        go (toNullable -> conj) = MergeMap $ Map.singleton other (Any $ is _Nothing actions, opoint <$> actions)
                          where
                            actions :: Maybe (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                            (Set.fromList -> other, fromNullable . Set.fromList -> actions) = partitionWith filterActions $ Set.toList conj
                            filterActions :: PredLiteral WorkflowGraphAuthPayloadPredicate
                                          -> Either (PredLiteral WorkflowGraphAuthPayloadPredicate) (PredLiteral WorkflowGraphAuthActionPredicate)
                            filterActions lit = maybe (Left lit) Right $ do
                              WorkflowGraphAuthPayloadPredicate{..} <- return $ lit ^. _plVar
                              WorkflowGraphAuthPayloadWithinAction{..} <- return wgappPredicate
                              return $ lit & _plVar .~ WorkflowGraphAuthActionPredicate (_WorkflowGraphAuthActionReferencePayload # wgappReference) wgappAction
                    uncollectWithinActions :: MergeMap (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)) (Any, Maybe (NonNull (Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate))))))
                                           -> WorkflowGraphAuthExpression
                    uncollectWithinActions = (fromMaybe WorkflowGraphAuthTrue .) . ifoldMap $ \rest (Any hasBare, actionsDNF) ->
                        let bare = maybe mempty (Just . WorkflowGraphAuthHasPayload . PredDNF . Set.singleton) $ fromNullable rest
                         in    guardMonoid hasBare bare
                            <> maybeMonoid (actionsDNF <&> maybe bare (Just . WorkflowGraphAuthHasPayload . PredDNF . maybe id (Set.map . (<>)) (fromNullable rest) . toNullable) . toPayloadPredicates . toNullable)
                      where
                        toPayloadPredicates :: Set (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                                            -> Maybe (NonNull (Set (NonNull (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)))))
                        toPayloadPredicates = PredDNF
                                          >>> (\actionDNF -> case optimizeStepExpr $ WorkflowGraphAuthHasAction actionDNF of
                                                  WorkflowGraphAuthTrue -> Nothing
                                                  WorkflowGraphAuthHasAction actionDNF'@(fromNullable . dnfTerms -> Just _) -> Just actionDNF'
                                                  otherAuthExpr -> error $ "Expected optimizeStepExpr on WorkflowGraphAuthHasAction to Return 'HasAction or 'True but got: " <> show otherAuthExpr
                                              )
                                          >>> over (mapped . plVars) (\WorkflowGraphAuthActionPredicate{..} -> WorkflowGraphAuthPayloadPredicate{ wgappReference = fromMaybe (error "Optimization of 'ActionPredicato for 'WithinAction produced 'Reference not applicable to 'PayloadPredicate") $ wgaapReference ^? _WorkflowGraphAuthActionReferencePayload, wgappPredicate = WorkflowGraphAuthPayloadWithinAction wgaapPredicate })
                                          >>> fmap (impureNonNull . dnfTerms)

                aPredIsKnown :: PredLiteral WorkflowGraphAuthActionPredicate -> Maybe Bool
                aPredIsKnown lit@(view _plVar -> var)
                  | is _PLNegated lit = guardOn (aPredIsKnownTrue  var) False
                                    <|> guardOn (aPredIsKnownFalse var) True
                  | otherwise         = guardOn (aPredIsKnownFalse var) False
                                    <|> guardOn (aPredIsKnownTrue  var) True
                  where
                    aPredIsKnownTrue = \case
                      WorkflowGraphAuthActionPredicate{ wgaapPredicate = WorkflowGraphAuthActionToNode{..} }
                        -> allNodeLabels `Set.isSubsetOf` toNullable wgaapNode
                      _other -> False
                    aPredIsKnownFalse _ = False -- TODO

                filterAIsKnownTrue :: NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate))
                                  -> Maybe (NonNull (Set (PredLiteral WorkflowGraphAuthActionPredicate)))
                filterAIsKnownTrue = fromNullable . Set.filter (maybe True not . aPredIsKnown) . toNullable

                filterAIsKnownFalse = Set.filter . oall $ fromMaybe True . aPredIsKnown


                pPredIsKnown :: PredLiteral WorkflowGraphAuthPayloadPredicate -> Maybe Bool
                pPredIsKnown lit@(view _plVar -> var)
                  | is _PLNegated lit = guardOn (pPredIsKnownTrue  var) False
                                    <|> guardOn (pPredIsKnownFalse var) True
                  | otherwise         = guardOn (pPredIsKnownFalse var) False
                                    <|> guardOn (pPredIsKnownTrue  var) True
                  where
                    pPredIsKnownTrue = \case
                      WorkflowGraphAuthPayloadPredicate{ wgappPredicate = WorkflowGraphAuthPayloadHasLabel{..} }
                        -> allPayloadLabels `Set.isSubsetOf` toNullable wgappLabels
                      WorkflowGraphAuthPayloadPredicate{ wgappPredicate = WorkflowGraphAuthPayloadWithinAction{..} }
                        -> aPredIsKnown (PLVariable $ WorkflowGraphAuthActionPredicate WorkflowGraphAuthActionReferenceFocus wgappAction) == Just True
                    pPredIsKnownFalse = \case
                      WorkflowGraphAuthPayloadPredicate{ wgappPredicate = WorkflowGraphAuthPayloadWithinAction{..} }
                        -> aPredIsKnown (PLVariable $ WorkflowGraphAuthActionPredicate WorkflowGraphAuthActionReferenceFocus wgappAction) == Just False
                      _other -> False -- TODO

                filterPIsKnownTrue :: NonNull (Set (PredLiteral WorkflowGraphAuthPayloadPredicate))
                                   -> Maybe (NonNull (Set (PredLiteral WorkflowGraphAuthPayloadPredicate)))
                filterPIsKnownTrue = fromNullable . Set.filter (maybe True not . pPredIsKnown) . toNullable

                filterPIsKnownFalse = Set.filter . oall $ fromMaybe True . pPredIsKnown


mkWorkflowGraphAuth :: forall fileid userid. (Ord fileid, Ord userid, Ord (FileReferenceTitleMap fileid (FileFieldUserOption Bool)), Typeable fileid, Typeable userid, HasTypesUsing WorkflowChildren userid userid WorkflowPayloadLabel WorkflowPayloadLabel) => WorkflowGraph fileid userid -> WorkflowGraphAuth userid
mkWorkflowGraphAuth g = optimizeWorkflowGraphAuth g $ mkWorkflowGraphAuthUnoptimized g

----- PathPiece instances -----

nullaryPathPiece ''WorkflowScope' $ camelToPathPiece' 1 . fromJust . stripSuffix "'"
nullaryPathPiece ''WorkflowFieldPayload' $ camelToPathPiece' 1 . fromJust . stripSuffix "'"

nullaryPathPiece ''WorkflowPayloadField' $ camelToPathPiece' 1 . fromJust . stripSuffix "'"

derivePathPiece ''WorkflowScope (camelToPathPiece' 1) "--"

nullaryPathPiece ''WorkflowPayloadTimeCapturePrecision $ camelToPathPiece' 2

nullaryPathPiece ''WorkflowWorkflowListType $ camelToPathPiece' 3

-----  ToJSON / FromJSON instances  -----

omitNothing :: [JSON.Pair] -> [JSON.Pair]
omitNothing = filter . hasn't $ _2 . _Null


deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 2
  , constructorTagModifier = camelToPathPiece' 2
  } ''WorkflowRole

deriveToJSON workflowNodeViewAesonOptions ''WorkflowNodeView
deriveToJSON workflowNodeMessageAesonOptions ''WorkflowNodeMessage
deriveToJSON workflowEdgeMessageAesonOptions ''WorkflowEdgeMessage
deriveToJSON workflowPayloadViewAesonOptions ''WorkflowPayloadView
pathPieceJSON ''WorkflowFieldPayload'
pathPieceJSON ''WorkflowPayloadField'

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 3
  } ''WorkflowGraphRestriction

pathPieceJSON ''WorkflowPayloadTimeCapturePrecision

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''WorkflowPayloadTextPreset

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 4
  , allNullaryToStringTag = True
  } ''WorkflowPayloadTextType

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 2
  } ''WorkflowActionUser

instance (FromJSON userid, Ord userid) => FromJSON (WorkflowNodeMessage userid) where
  parseJSON = genericParseJSON workflowNodeMessageAesonOptions
instance (FromJSON userid, Ord userid) => FromJSON (WorkflowEdgeMessage userid) where
  parseJSON = genericParseJSON workflowEdgeMessageAesonOptions
instance (FromJSON userid, Ord userid) => FromJSON (WorkflowNodeView userid) where
  parseJSON = genericParseJSON workflowNodeViewAesonOptions
instance (FromJSON userid, Ord userid) => FromJSON (WorkflowPayloadView userid) where
  parseJSON = genericParseJSON workflowPayloadViewAesonOptions

nullaryPathPiece ''WorkflowGraphSubstageMode $ camelToPathPiece' 2
pathPieceJSON ''WorkflowGraphSubstageMode

nullaryPathPiece ''WorkflowGraphSubstageShowWhen $ camelToPathPiece' 3
pathPieceJSON ''WorkflowGraphSubstageShowWhen

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 3
  , fieldLabelModifier = camelToPathPiece' 1
  } ''WorkflowGraphSubstage

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 3
  , fieldLabelModifier = camelToPathPiece' 1
  } ''WorkflowGraphStage

instance (ToJSON fileid, ToJSON userid, ToJSON (FileField fileid)) => ToJSON (WorkflowGraph fileid userid) where
  toJSON = genericToJSON workflowGraphAesonOptions
instance ( FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , FromJSON (FileField fileid)
         , Ord (FileField fileid)
         ) => FromJSON (WorkflowGraph fileid userid) where
  parseJSON = JSON.withObject "WorkflowGraph" $ \o -> do
    wgStages <- o JSON..:? "stages" JSON..!= mempty
    wgNodes <- o JSON..: "nodes"
    return WorkflowGraph{..}

instance (ToJSON fileid, ToJSON userid, ToJSON (FileField fileid)) => ToJSON (WorkflowGraphEdge fileid userid) where
  toJSON = genericToJSON workflowGraphEdgeAesonOptions
instance ( FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , FromJSON (FileField fileid)
         , Ord (FileField fileid)
         ) => FromJSON (WorkflowGraphEdge fileid userid) where
  parseJSON = JSON.withObject "WorkflowGraphEdge" $ \o -> do
    mode <- o JSON..: "mode" :: JSON.Parser Text
    if | mode == "manual" -> do
           wgeSource <- o JSON..: "source"
           wgeActors <- o JSON..:? "actors" JSON..!= Set.empty
           wgeForm <- o JSON..:? "form" JSON..!= WorkflowGraphEdgeForm Map.empty
           wgeDisplayLabel <- o JSON..: "display-label"
           wgeViewActor <- o JSON..:? "view-actor" JSON..!= Set.empty
           wgeMessages <- o JSON..:? "messages" JSON..!= Set.empty
           return WorkflowGraphEdgeManual{..}
       | mode == "automatic" -> do
           wgeSource <- o JSON..: "source"
           wgeRestriction <- o JSON..:? "restriction"
           return WorkflowGraphEdgeAutomatic{..}
       | mode == "initial" -> do
           wgeActors <- o JSON..:? "actors" JSON..!= Set.empty
           wgeForm <- o JSON..:? "form" JSON..!= WorkflowGraphEdgeForm Map.empty
           wgeDisplayLabel <- o JSON..: "display-label"
           wgeViewActor <- o JSON..:? "view-actor" JSON..!= Set.empty
           wgeMessages <- o JSON..:? "messages" JSON..!= Set.empty
           return WorkflowGraphEdgeInitial{..}
       | otherwise -> fail "Could not parse WorkflowGraphEdge, expected mode to be one of: manual, automatic, initial"

instance ToJSON WorkflowGraphEdgeFormOrder where
  toJSON WorkflowGraphEdgeFormOrder{..} = case unWorkflowGraphEdgeFormOrder of
    Nothing  -> JSON.String "_"
    Just sci -> JSON.Number sci
instance FromJSON WorkflowGraphEdgeFormOrder where
  parseJSON v = fmap WorkflowGraphEdgeFormOrder $
        Just <$> parseJSON v
    <|> JSON.withText "WorkflowGraphEdgeFormOrder" (maybe (fail "WorkflowGraphEdgeFormOrder: could not parse String as Number") (return . Just) . readMay) v
    <|> JSON.withText "WorkflowGraphEdgeFormOrder" (bool (fail "WorkflowGraphEdgeFormOrder: unexpected String, expecting either number or \"_\"") (pure Nothing) . (== "_")) v

instance ToJSONKey WorkflowGraphEdgeFormOrder where
  toJSONKey = JSON.ToJSONKeyText (maybe "_" toText' . unWorkflowGraphEdgeFormOrder) (maybe (JSON.text "_") toEncoding' . unWorkflowGraphEdgeFormOrder)
    where toText' = decodeUtf8 . toStrict . JSON.encodingToLazyByteString . JSON.scientific
          toEncoding' = JSON.scientificText
instance FromJSONKey WorkflowGraphEdgeFormOrder where
  fromJSONKey = JSON.FromJSONKeyTextParser $ parseJSON . JSON.String

instance (ToJSON fileid, ToJSON userid, ToJSON (FileField fileid)) => ToJSON (WorkflowGraphEdgeForm fileid userid) where
  toJSON WorkflowGraphEdgeForm{..} = toJSON . flip map wgefFields $ \(toNullable -> disj) -> flip Set.map disj $ \(toNullable -> orderedFields) -> if
    | [(WorkflowGraphEdgeFormOrder Nothing, field)] <- Map.toList orderedFields
      -> toJSON field
    | otherwise
      -> toJSON orderedFields

instance ( FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , FromJSON (FileField fileid), Ord (FileField fileid)
         ) => FromJSON (WorkflowGraphEdgeForm fileid userid) where
  parseJSON = JSON.withObject "WorkflowGraphEdgeForm" $ \o -> do
    o' <- parseJSON $ JSON.Object o :: JSON.Parser (Map WorkflowPayloadLabel (NonNull (Set JSON.Value)))
    fmap WorkflowGraphEdgeForm . for o' $ \(Set.toList . toNullable -> o'') -> fmap (impureNonNull . Set.fromList) . for o'' $ \o''' ->
          parseJSON o'''
      <|> impureNonNull . Map.singleton (WorkflowGraphEdgeFormOrder Nothing) <$> parseJSON o'''

instance (ToJSON fileid, ToJSON userid, ToJSON (FileField fileid)) => ToJSON (WorkflowPayloadSpec fileid userid) where
  toJSON (WorkflowPayloadSpec f) = toJSON f

instance (ToJSON fileid, ToJSON userid, ToJSON (FileField fileid)) => ToJSON (WorkflowPayloadField fileid userid payload) where
  toJSON WorkflowPayloadFieldText{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFText'
    , "label"       JSON..=  wpftLabel
    , "placeholder" JSON..=  wpftPlaceholder
    , "tooltip"     JSON..=  wpftTooltip
    , "default"     JSON..=  wpftDefault
    , "optional"    JSON..=  wpftOptional
    , "presets"     JSON..=  wpftPresets
    , "type"        JSON..=  wpftType
    ]
  toJSON WorkflowPayloadFieldNumber{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFNumber'
    , "label"       JSON..=  wpfnLabel
    , "placeholder" JSON..=  wpfnPlaceholder
    , "tooltip"     JSON..=  wpfnTooltip
    , "default"     JSON..=  wpfnDefault
    , "min"         JSON..=  wpfnMin
    , "max"         JSON..=  wpfnMax
    , "step"        JSON..=  wpfnStep
    , "optional"    JSON..=  wpfnOptional
    ]
  toJSON WorkflowPayloadFieldBool{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFBool'
    , "label"       JSON..=  wpfbLabel
    , "tooltip"     JSON..=  wpfbTooltip
    , "default"     JSON..=  wpfbDefault
    , "optional"    JSON..=  wpfbOptional
    ]
  toJSON WorkflowPayloadFieldDay{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFDay'
    , "label"       JSON..=  wpfdLabel
    , "tooltip"     JSON..=  wpfdTooltip
    , "default"     JSON..=  wpfdDefault
    , "optional"    JSON..=  wpfdOptional
    , "max-past"    JSON..=  wpfdMaxPast
    , "max-future"  JSON..=  wpfdMaxFuture
    ]
  toJSON WorkflowPayloadFieldTime{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFTime'
    , "label"       JSON..=  wpfttLabel
    , "tooltip"     JSON..=  wpfttTooltip
    , "default"     JSON..=  wpfttDefault
    , "optional"    JSON..=  wpfttOptional
    ]
  toJSON WorkflowPayloadFieldDateTime{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFDateTime'
    , "label"       JSON..=  wpfdtLabel
    , "tooltip"     JSON..=  wpfdtTooltip
    , "default"     JSON..=  wpfdtDefault
    , "optional"    JSON..=  wpfdtOptional
    , "max-past"    JSON..=  wpfdtMaxPast
    , "max-future"  JSON..=  wpfdtMaxFuture
    ]
  toJSON WorkflowPayloadFieldFile{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFFile'
    , "label"       JSON..=  wpffLabel
    , "tooltip"     JSON..=  wpffTooltip
    , "config"      JSON..=  wpffConfig
    , "optional"    JSON..=  wpffOptional
    ]
  toJSON WorkflowPayloadFieldUser{..} = JSON.object $ omitNothing
    [ "tag"         JSON..=  WPFUser'
    , "label"       JSON..=  wpfuLabel
    , "tooltip"     JSON..=  wpfuTooltip
    , "default"     JSON..=  wpfuDefault
    , "optional"    JSON..=  wpfuOptional
    ]
  toJSON WorkflowPayloadFieldCaptureUser{} = JSON.object
    [ "tag"         JSON..=  WPFCaptureUser'
    ]
  toJSON WorkflowPayloadFieldCaptureDateTime{..} = JSON.object
    [ "tag"         JSON..=  WPFCaptureDateTime'
    , "label"       JSON..=  wpfcdtLabel
    , "tooltip"     JSON..=  wpfcdtTooltip
    , "precision"   JSON..=  wpfcdtPrecision
    ]
  toJSON WorkflowPayloadFieldReference{..} = JSON.object
    [ "tag"         JSON..=  WPFReference'
    , "target"      JSON..=  wpfrTarget
    ]
  toJSON WorkflowPayloadFieldMultiple{..} = JSON.object
    [ "tag"         JSON..=  WPFMultiple'
    , "label"       JSON..=  wpfmLabel
    , "tooltip"     JSON..=  wpfmTooltip
    , "default"     JSON..=  wpfmDefault
    , "sub"         JSON..=  wpfmSub
    , "min"         JSON..=  wpfmMin
    , "range"       JSON..=  wpfmRange
    ]

instance ( FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , FromJSON (FileField fileid)
         ) => FromJSON (WorkflowPayloadSpec fileid userid) where
  parseJSON = JSON.withObject "WorkflowPayloadSpec" $ \o -> do
    fieldTag <- o JSON..: "tag"
    case fieldTag of
      WPFText' -> do
        wpftLabel       <-  o JSON..:  "label"
        wpftPlaceholder <-  o JSON..:? "placeholder"
        wpftTooltip     <-  o JSON..:? "tooltip"
        wpftDefault     <-  o JSON..:? "default"
        wpftLarge       <-  o JSON..:? "large" JSON..!= False
        wpftOptional    <-  o JSON..:  "optional"
        wpftPresets     <-  o JSON..:? "presets"
        wpftType        <-  o JSON..:? "type" JSON..!= bool WorkflowPayloadTextTypeText WorkflowPayloadTextTypeLarge wpftLarge
        return $ WorkflowPayloadSpec WorkflowPayloadFieldText{..}
      WPFNumber' -> do
        wpfnLabel       <-  o JSON..:  "label"
        wpfnPlaceholder <-  o JSON..:? "placeholder"
        wpfnTooltip     <-  o JSON..:? "tooltip"
        wpfnDefault     <- (o JSON..:? "default" :: Parser (Maybe Scientific))
        wpfnMin         <-  o JSON..:? "min"
        wpfnMax         <-  o JSON..:? "max"
        wpfnStep        <-  o JSON..:? "step"
        wpfnOptional    <-  o JSON..:  "optional"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldNumber{..}
      WPFBool' -> do
        wpfbLabel       <-  o JSON..:  "label"
        wpfbTooltip     <-  o JSON..:? "tooltip"
        wpfbOptional    <-  o JSON..:? "optional"
        wpfbDefault     <- (o JSON..:? "default" :: Parser (Maybe Bool))
        return $ WorkflowPayloadSpec WorkflowPayloadFieldBool{..}
      WPFDay' -> do
        wpfdLabel       <-  o JSON..:  "label"
        wpfdTooltip     <-  o JSON..:? "tooltip"
        wpfdOptional    <-  o JSON..:  "optional"
        wpfdDefault     <- (o JSON..:? "default" :: Parser (Maybe Day))
        wpfdMaxPast     <-  o JSON..:? "max-past"
        wpfdMaxFuture   <-  o JSON..:? "max-future"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldDay{..}
      WPFTime' -> do
        wpfttLabel      <-  o JSON..:  "label"
        wpfttTooltip    <-  o JSON..:? "tooltip"
        wpfttOptional   <-  o JSON..:  "optional"
        wpfttDefault    <- (o JSON..:? "default" :: Parser (Maybe TimeOfDay))
        return $ WorkflowPayloadSpec WorkflowPayloadFieldTime{..}
      WPFDateTime' -> do
        wpfdtLabel      <-  o JSON..:  "label"
        wpfdtTooltip    <-  o JSON..:? "tooltip"
        wpfdtOptional   <-  o JSON..:  "optional"
        wpfdtDefault    <- (o JSON..:? "default" :: Parser (Maybe UTCTime))
        wpfdtMaxPast    <-  o JSON..:? "max-past"
        wpfdtMaxFuture  <-  o JSON..:? "max-future"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldDateTime{..}
      WPFFile' -> do
        wpffLabel       <-  o JSON..:  "label"
        wpffTooltip     <-  o JSON..:? "tooltip"
        wpffConfig      <-  o JSON..:  "config"
        wpffOptional    <-  o JSON..:  "optional"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldFile{..}
      WPFUser' -> do
        wpfuLabel       <-  o JSON..:  "label"
        wpfuTooltip     <-  o JSON..:? "tooltip"
        wpfuDefault     <- (o JSON..:? "default" :: Parser (Maybe userid))
        wpfuOptional    <-  o JSON..:  "optional"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldUser{..}
      WPFCaptureUser' -> pure $ WorkflowPayloadSpec WorkflowPayloadFieldCaptureUser
      WPFCaptureDateTime' -> do
        wpfcdtPrecision <-  o JSON..:? "precision" JSON..!= def
        wpfcdtLabel     <-  o JSON..:  "label"
        wpfcdtTooltip   <-  o JSON..:? "tooltip"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldCaptureDateTime{..}
      WPFReference' -> do
        wpfrTarget      <-  o JSON..:  "target"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldReference{..}
      WPFMultiple' -> do
        wpfmLabel       <-  o JSON..:  "label"
        wpfmTooltip     <-  o JSON..:? "tooltip"
        wpfmDefault     <-  o JSON..:? "default"
        wpfmSub         <-  o JSON..:  "sub"
        wpfmMin         <-  o JSON..:  "min"
        wpfmRange       <-  o JSON..:? "range"
        return $ WorkflowPayloadSpec WorkflowPayloadFieldMultiple{..}

defaultFinalIcon :: Icon
defaultFinalIcon = IconOK

instance (ToJSON fileid, ToJSON userid, ToJSON (FileField fileid)) => ToJSON (WorkflowGraphNode fileid userid) where
  toJSON WGN{..} = JSON.object
    [ ("final" JSON..=) $ case wgnFinal of
        Nothing -> toJSON False
        Just icn | icn == defaultFinalIcon -> toJSON True
        other -> toJSON other
    , "viewers" JSON..= wgnViewers
    , "messages" JSON..= wgnMessages
    , "edges" JSON..= wgnEdges
    , "payload-view" JSON..= wgnPayloadView
    ]
instance ( FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , FromJSON (FileField fileid)
         , Ord (FileField fileid)
         ) => FromJSON (WorkflowGraphNode fileid userid) where
  parseJSON = JSON.withObject "WorkflowGraphNode" $ \o -> do
    wgnFinal <- o JSON..:? "final"
            <|> fmap (bool Nothing $ Just defaultFinalIcon) (o JSON..: "final")
    wgnViewers <- o JSON..:? "viewers"
    wgnMessages <- o JSON..:? "messages" JSON..!= Set.empty
    wgnEdges <- o JSON..:? "edges" JSON..!= Map.empty
    wgnPayloadView <- o JSON..:? "payload-view" JSON..!= Map.empty
    return WGN{..}

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  , fieldLabelModifier = camelToPathPiece' 1
  } ''WorkflowScope
instance (FromJSON termid, FromJSON schoolid, FromJSON courseid) => FromJSONKey (WorkflowScope termid schoolid courseid)

pathPieceJSON ''WorkflowScope'

-- deriveToJSON workflowActionAesonOptions ''WorkflowAction

-- instance ( FromJSON fileid, FromJSON userid
--          ,      Ord fileid,      Ord userid
--          , Typeable fileid, Typeable userid
--          ) => FromJSON (WorkflowAction fileid userid) where
--   parseJSON = genericParseJSON workflowActionAesonOptions

instance (ToJSON fileid, ToJSON userid) => ToJSON (WorkflowFieldPayloadW fileid userid) where
  toJSON (WorkflowFieldPayloadW (WFPText   t)) = JSON.object
    [ "tag"    JSON..= WFPText'
    , toPathPiece WFPText' JSON..= t
    ]
  toJSON (WorkflowFieldPayloadW (WFPNumber n)) = JSON.object
    [ "tag"    JSON..= WFPNumber'
    , toPathPiece WFPNumber' JSON..= n
    ]
  toJSON (WorkflowFieldPayloadW (WFPBool   b)) = JSON.object
    [ "tag"    JSON..= WFPBool'
    , toPathPiece WFPBool' JSON..= b
    ]
  toJSON (WorkflowFieldPayloadW (WFPDay    d)) = JSON.object
    [ "tag"    JSON..= WFPDay'
    , toPathPiece WFPDay' JSON..= d
    ]
  toJSON (WorkflowFieldPayloadW (WFPTime   d)) = JSON.object
    [ "tag"    JSON..= WFPTime'
    , toPathPiece WFPTime' JSON..= d
    ]
  toJSON (WorkflowFieldPayloadW (WFPDateTime t)) = JSON.object
    [ "tag"    JSON..= WFPDateTime'
    , toPathPiece WFPDateTime' JSON..= t
    ]
  toJSON (WorkflowFieldPayloadW (WFPFile fid)) = JSON.object
    [ "tag"    JSON..= WFPFile'
    , toPathPiece WFPFile' JSON..= fid
    ]
  toJSON (WorkflowFieldPayloadW (WFPUser uid)) = JSON.object
    [ "tag"    JSON..= WFPUser'
    , toPathPiece WFPUser' JSON..= uid
    ]
instance (Ord fileid, FromJSON fileid, FromJSON userid, Typeable fileid, Typeable userid) => FromJSON (WorkflowFieldPayloadW fileid userid) where
  parseJSON = JSON.withObject "WorkflowFieldPayloadW" $ \o -> do
    fieldTag <- o JSON..: "tag"
    case fieldTag of
      WFPText'   -> do
        t <- o JSON..: toPathPiece WFPText'
        return $ WorkflowFieldPayloadW $ WFPText t
      WFPNumber' -> do
        n <- o JSON..: toPathPiece WFPNumber'
        return $ WorkflowFieldPayloadW $ WFPNumber n
      WFPBool'   -> do
        b <- o JSON..: toPathPiece WFPBool'
        return $ WorkflowFieldPayloadW $ WFPBool b
      WFPDay'   -> do
        d <- o JSON..: toPathPiece WFPDay'
        return $ WorkflowFieldPayloadW $ WFPDay d
      WFPTime'  -> do
        t <- o JSON..: toPathPiece WFPTime'
        return $ WorkflowFieldPayloadW $ WFPTime t
      WFPDateTime' -> do
        t <- o JSON..: toPathPiece WFPDateTime'
        return $ WorkflowFieldPayloadW $ WFPDateTime t
      WFPFile'  -> do
        fid <- o JSON..: toPathPiece WFPFile'
        return $ WorkflowFieldPayloadW $ WFPFile fid
      WFPUser'   -> do
        uid <- o JSON..: toPathPiece WFPUser'
        return $ WorkflowFieldPayloadW $ WFPUser uid

pathPieceJSON ''WorkflowWorkflowListType


-----  PersistField / PersistFieldSql instances  -----

instance (   ToJSON fileid,   ToJSON userid
         , FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , ToJSON (FileField fileid), FromJSON (FileField fileid)
         , Ord (FileField fileid)
         ) => PersistField (WorkflowGraph fileid userid) where
  toPersistValue   = toPersistValueJSON
  fromPersistValue = fromPersistValueJSON
instance (   ToJSON fileid,   ToJSON userid
         , FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         , ToJSON (FileField fileid), FromJSON (FileField fileid)
         , Ord (FileField fileid)
         ) => PersistFieldSql (WorkflowGraph fileid userid) where
  sqlType _ = sqlTypeJSON


instance (   ToJSON termid,   ToJSON schoolid,   ToJSON courseid
         , FromJSON termid, FromJSON schoolid, FromJSON courseid
         , Typeable termid, Typeable schoolid, Typeable courseid
         ) => PersistField (WorkflowScope termid schoolid courseid) where
  toPersistValue   = toPersistValueJSON
  fromPersistValue = fromPersistValueJSON
instance (   ToJSON termid,   ToJSON schoolid,   ToJSON courseid
         , FromJSON termid, FromJSON schoolid, FromJSON courseid
         , Typeable termid, Typeable schoolid, Typeable courseid
         ) => PersistFieldSql (WorkflowScope termid schoolid courseid) where
  sqlType _ = sqlTypeJSON

derivePersistFieldJSON ''WorkflowScope'
derivePersistFieldJSON ''WorkflowFieldPayload'

instance (   ToJSON fileid,   ToJSON userid
         , FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         ) => PersistField (WorkflowFieldPayloadW fileid userid) where
  toPersistValue   = toPersistValueJSON
  fromPersistValue = fromPersistValueJSON
instance (   ToJSON fileid,   ToJSON userid
         , FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         ) => PersistFieldSql (WorkflowFieldPayloadW fileid userid) where
  sqlType _ = sqlTypeJSON

derivePersistFieldJSON ''WorkflowActionUser

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 3
  , allNullaryToStringTag = False
  } ''WorkflowGraphAuthAction

instance ToJSONKey WorkflowGraphAuthAction
instance FromJSONKey WorkflowGraphAuthAction

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 4
  } ''WorkflowGraphAuthActionPredicate'

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''WorkflowGraphAuthActionPredicate

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 5
  } ''WorkflowGraphAuthActionReference

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 4
  } ''WorkflowGraphAuthPayloadPredicate'

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  } ''WorkflowGraphAuthPayloadPredicate

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 5
  } ''WorkflowGraphAuthPayloadReference

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 3
  } ''WorkflowGraphAuthExpression

deriveJSON defaultOptions
  { fieldLabelModifier = camelToPathPiece' 1
  , constructorTagModifier = camelToPathPiece' 4
  } ''WorkflowGraphAuthRole

instance ToJSON userid => ToJSONKey (WorkflowGraphAuthRole userid)
instance FromJSON userid => FromJSONKey (WorkflowGraphAuthRole userid)

deriving newtype instance ToJSON userid => ToJSON (WorkflowGraphAuth userid)
deriving newtype instance (Ord userid, FromJSON userid) => FromJSON (WorkflowGraphAuth userid)

-----  Binary instances  -----

instance Binary WorkflowScope'
instance (Binary termid, Binary schoolid, Binary courseid) => Binary (WorkflowScope termid schoolid courseid)

instance Binary userid => Binary (WorkflowRole userid)

-- instance (Binary fileid, Binary userid, Typeable fileid, Typeable userid) => Binary (WorkflowAction fileid userid)
instance (Binary fileid, Binary userid, Typeable fileid, Typeable userid) => Binary (WorkflowFieldPayloadW fileid userid) where
  get = Binary.get >>= \case
    WFPText'   -> WorkflowFieldPayloadW . WFPText   <$> Binary.get
    WFPNumber' -> WorkflowFieldPayloadW . WFPNumber <$> Binary.get
    WFPBool'   -> WorkflowFieldPayloadW . WFPBool   <$> Binary.get
    WFPDay'    -> WorkflowFieldPayloadW . WFPDay    <$> Binary.get
    WFPTime'   -> WorkflowFieldPayloadW . WFPTime   <$> Binary.get
    WFPDateTime' -> WorkflowFieldPayloadW . WFPDateTime <$> Binary.get
    WFPFile'   -> WorkflowFieldPayloadW . WFPFile   <$> Binary.get
    WFPUser'   -> WorkflowFieldPayloadW . WFPUser   <$> Binary.get
  put = \case
    WorkflowFieldPayloadW (WFPText   t  ) -> Binary.put WFPText'   >> Binary.put t
    WorkflowFieldPayloadW (WFPNumber n  ) -> Binary.put WFPNumber' >> Binary.put n
    WorkflowFieldPayloadW (WFPBool   b  ) -> Binary.put WFPBool'   >> Binary.put b
    WorkflowFieldPayloadW (WFPDay    d  ) -> Binary.put WFPDay'    >> Binary.put d
    WorkflowFieldPayloadW (WFPTime   t  ) -> Binary.put WFPTime'   >> Binary.put t
    WorkflowFieldPayloadW (WFPDateTime t) -> Binary.put WFPDateTime' >> Binary.put t
    WorkflowFieldPayloadW (WFPFile   fid) -> Binary.put WFPFile'   >> Binary.put fid
    WorkflowFieldPayloadW (WFPUser   uid) -> Binary.put WFPUser'   >> Binary.put uid

instance Binary WorkflowGraphAuthAction
instance Binary WorkflowGraphAuthActionPredicate'
instance Binary WorkflowGraphAuthActionPredicate
instance Binary WorkflowGraphAuthActionReference
instance Binary WorkflowGraphAuthPayloadPredicate'
instance Binary WorkflowGraphAuthPayloadPredicate
instance Binary WorkflowGraphAuthPayloadReference
instance Binary WorkflowGraphAuthExpression
instance Binary userid => Binary (WorkflowGraphAuthRole userid)
instance Binary userid => Binary (WorkflowGraphAuth userid)

instance Binary WorkflowWorkflowListType

-----  TH Jail  -----

makeWrapped ''WorkflowGraphReference
