-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Winnie Ros <winnie.ros@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.Course
  ( module Model.Types.Course
  ) where

import Import.NoModel

import Model.Types.TH.PathPiece
import Utils.Lens.TH


data LecturerType = CourseLecturer | CourseAssistant | CourseAdministrator
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving (Universe, Finite, NFData) 

nullaryPathPiece ''LecturerType $ camelToPathPiece' 1
deriveJSON  defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  } ''LecturerType
derivePersistFieldJSON ''LecturerType

instance Hashable LecturerType


data CourseParticipantState
  = CourseParticipantActive
  | CourseParticipantInactive { courseParticipantNoShow :: Bool }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)
  deriving anyclass (NFData, Hashable)

makePrisms ''CourseParticipantState
makeLenses_ ''CourseParticipantState

deriveFinite ''CourseParticipantState
finitePathPiece ''CourseParticipantState
  ["active", "inactive", "no-show"]
pathPieceJSON ''CourseParticipantState
pathPieceJSONKey ''CourseParticipantState
derivePersistFieldPathPiece ''CourseParticipantState
