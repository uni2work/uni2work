-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Wolfgang Witt <Wolfgang.Witt@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.Changelog
  ( ChangelogItem(..)
  , changelogItemMap
  , ChangelogItemKind(..), _ChangelogItemFeature, _ChangelogItemBugfix
  , classifyChangelogItem
  , changelogItemDays
  ) where

import Import.NoModel

import Model.Types.TH.PathPiece

import qualified Data.Map as Map


mkI18nWidgetEnum "Changelog" "changelog"
derivePersistFieldPathPiece ''ChangelogItem
pathPieceJSONKey ''ChangelogItem
pathPieceJSON ''ChangelogItem
pathPieceHttpApiData ''ChangelogItem

data ChangelogItemKind 
  = ChangelogItemFeature
  | ChangelogItemBugfix
  deriving (Eq, Ord, Read, Show, Enum, Bounded, Generic, Typeable)
  deriving anyclass (Universe, Finite)

makePrisms ''ChangelogItemKind

classifyChangelogItem :: ChangelogItem -> ChangelogItemKind
classifyChangelogItem = \case
  ChangelogHaskellCampusLogin                       -> ChangelogItemBugfix
  ChangelogTooltipsWithoutJavascript                -> ChangelogItemBugfix
  ChangelogButtonsWorkWithoutJavascript             -> ChangelogItemBugfix
  ChangelogTableFormsWorkAfterAjax                  -> ChangelogItemBugfix
  ChangelogPassingByPointsWorks                     -> ChangelogItemBugfix
  ChangelogErrorMessagesForTableItemVanish          -> ChangelogItemBugfix
  ChangelogExamAchievementParticipantDuplication    -> ChangelogItemBugfix
  ChangelogFormsTimesReset                          -> ChangelogItemBugfix
  ChangelogAllocationCourseAcceptSubstitutesFixed   -> ChangelogItemBugfix
  ChangelogStoredMarkup                             -> ChangelogItemBugfix
  ChangelogFixPersonalisedSheetFilesKeep            -> ChangelogItemBugfix
  ChangelogHonorRoomHidden                          -> ChangelogItemBugfix
  ChangelogFixSheetBonusRounding                    -> ChangelogItemBugfix
  ChangelogFixExamBonusAllSheetsBonus               -> ChangelogItemBugfix
  ChangelogExamAutomaticRoomDistributionRespectSize -> ChangelogItemBugfix
  _other                                            -> ChangelogItemFeature

changelogItemDays :: Map ChangelogItem Day
changelogItemDays = Map.fromListWithKey (\k d1 d2 -> bool (error $ "Duplicate changelog days for " <> show k) d1 $ d1 /= d2)
  [ (ChangelogConfigurableDatetimeFormat,                      [day|2018-07-10|])
  , (ChangelogCourseListOverAllTerms,                          [day|2018-07-31|])
  , (ChangelogCorrectionsDisplayImprovements,                  [day|2018-07-31|])
  , (ChangelogHaskellCampusLogin,                              [day|2018-08-01|])
  , (ChangelogFileDownloadOption,                              [day|2018-08-06|])
  , (ChangelogSheetsNoSubmissionAndZipControl,                 [day|2018-09-18|])
  , (ChangelogSmartCorrectionDistribution,                     [day|2018-09-18|])
  , (ChangelogTableSummaries,                                  [day|2018-09-18|])
  , (ChangelogPersonalInformation,                             [day|2018-09-18|])
  , (ChangelogCourseShorthandsWithinSchools,                   [day|2018-09-18|])
  , (ChangelogTooltipsWithoutJavascript,                       [day|2018-09-18|])
  , (ChangelogEmailNotifications,                              [day|2018-10-19|])
  , (ChangelogSupportWidget,                                   [day|2018-10-19|])
  , (ChangelogAccountDeletionDuringTesting,                    [day|2018-10-19|])
  , (ChangelogImprovementsForCorrectors,                       [day|2018-11-09|])
  , (ChangelogButtonsWorkWithoutJavascript,                    [day|2018-11-09|])
  , (ChangelogTableFormsWorkAfterAjax,                         [day|2018-11-29|])
  , (ChangelogPassingByPointsWorks,                            [day|2018-11-30|])
  , (ChangelogErrorMessagesForTableItemVanish,                 [day|2019-01-16|])
  , (ChangelogAssignedCorrectionsFilters,                      [day|2019-01-16|])
  , (ChangelogCourseConvenienceLinks,                          [day|2019-01-16|])
  , (ChangelogAsidenav,                                        [day|2019-01-30|])
  , (ChangelogCourseAssociatedStudyField,                      [day|2019-03-20|])
  , (ChangelogStudyFeatures,                                   [day|2019-03-27|])
  , (ChangelogCourseAdministratorRoles,                        [day|2019-03-27|])
  , (ChangelogCourseAdministratorInvitations,                  [day|2019-04-20|])
  , (ChangelogCourseMessages,                                  [day|2019-04-20|])
  , (ChangelogCorrectorsOnCourseShow,                          [day|2019-04-29|])
  , (ChangelogTutorials,                                       [day|2019-04-29|])
  , (ChangelogCourseMaterials,                                 [day|2019-05-04|])
  , (ChangelogDownloadAllSheetFiles,                           [day|2019-05-10|])
  , (ChangelogImprovedSubmittorUi,                             [day|2019-05-10|])
  , (ChangelogCourseRegisterByAdmin,                           [day|2019-05-13|])
  , (ChangelogReworkedAutomaticCorrectionDistribution,         [day|2019-05-20|])
  , (ChangelogDownloadAllSheetFilesByType,                     [day|2019-06-07|])
  , (ChangelogSheetSpecificFiles,                              [day|2019-06-07|])
  , (ChangelogExams,                                           [day|2019-06-26|])
  , (ChangelogCsvExamParticipants,                             [day|2019-07-23|])
  , (ChangelogAllocationCourseRegistration,                    [day|2019-08-12|])
  , (ChangelogAllocationApplications,                          [day|2019-08-19|])
  , (ChangelogCsvCourseApplications,                           [day|2019-08-27|])
  , (ChangelogAllocationsNotifications,                        [day|2019-09-05|])
  , (ChangelogConfigurableDisplayEmails,                       [day|2019-09-12|])
  , (ChangelogConfigurableDisplayNames,                        [day|2019-09-12|])
  , (ChangelogEstimateAllocatedCourseCapacity,                 [day|2019-09-12|])
  , (ChangelogNotificationExamRegistration,                    [day|2019-09-13|])
  , (ChangelogExamClosure,                                     [day|2019-09-16|])
  , (ChangelogExamOfficeExamNotification,                      [day|2019-09-16|])
  , (ChangelogExamOffices,                                     [day|2019-09-16|])
  , (ChangelogExamAchievementParticipantDuplication,           [day|2019-09-25|])
  , (ChangelogFormsTimesReset,                                 [day|2019-09-25|])
  , (ChangelogExamAutomaticResults,                            [day|2019-09-25|])
  , (ChangelogExamAutomaticBoni,                               [day|2019-09-25|])
  , (ChangelogAutomaticallyAcceptCourseApplications,           [day|2019-09-27|])
  , (ChangelogCourseNews,                                      [day|2019-10-01|])
  , (ChangelogCsvExportCourseParticipants,                     [day|2019-10-08|])
  , (ChangelogNotificationCourseParticipantViaAdmin,           [day|2019-10-08|])
  , (ChangelogCsvExportCourseParticipantsFeatures,             [day|2019-10-09|])
  , (ChangelogCourseOccurences,                                [day|2019-10-09|])
  , (ChangelogTutorialRegistrationViaParticipantTable,         [day|2019-10-10|])
  , (ChangelogCsvExportCourseParticipantsRegisteredTutorials,  [day|2019-10-10|])
  , (ChangelogCourseParticipantsSex,                           [day|2019-10-14|])
  , (ChangelogTutorialTutorControl,                            [day|2019-10-14|])
  , (ChangelogCsvOptionCharacterSet,                           [day|2019-10-23|])
  , (ChangelogCsvOptionTimestamp,                              [day|2019-10-23|])
  , (ChangelogEnglish,                                         [day|2019-10-31|])
  , (ChangelogI18n,                                            [day|2019-10-31|])
  , (ChangelogLmuInternalFields,                               [day|2019-11-28|])
  , (ChangelogNotificationSubmissionChanged,                   [day|2019-12-05|])
  , (ChangelogExportCourseParticipants,                        [day|2020-01-17|])
  , (ChangelogExternalExams,                                   [day|2020-01-17|])
  , (ChangelogExamAutomaticRoomDistribution,                   [day|2020-01-29|])
  , (ChangelogWarningMultipleSemesters,                        [day|2020-01-30|])
  , (ChangelogExamAutomaticRoomDistributionBetterRulesDisplay, [day|2020-01-30|])
  , (ChangelogReworkedNavigation,                              [day|2020-02-07|])
  , (ChangelogExamCorrect,                                     [day|2020-02-08|])
  , (ChangelogExamGradingMode,                                 [day|2020-02-19|])
  , (ChangelogMarkdownEmails,                                  [day|2020-02-23|])
  , (ChangelogMarkdownHtmlInput,                               [day|2020-02-23|])
  , (ChangelogBetterCsvImport,                                 [day|2020-03-06|])
  , (ChangelogAdditionalDatetimeFormats,                       [day|2020-03-16|])
  , (ChangelogServerSideSessions,                              [day|2020-03-16|])
  , (ChangelogWebinterfaceAllocationAllocation,                [day|2020-03-16|])
  , (ChangelogBetterTableCellColourCoding,                     [day|2020-03-16|])
  , (ChangelogCourseOccurrenceNotes,                           [day|2020-03-31|])
  , (ChangelogHideSystemMessages,                              [day|2020-04-15|])
  , (ChangelogNonAnonymisedCorrection,                         [day|2020-04-17|])
  , (ChangelogBetterCourseParticipantDetailPage,               [day|2020-04-17|])
  , (ChangelogFaq,                                             [day|2020-04-24|])
  , (ChangelogRegisteredSubmissionGroups,                      [day|2020-04-28|])
  , (ChangelogFormerCourseParticipants,                        [day|2020-05-05|])
  , (ChangelogBetterFileUploads,                               [day|2020-05-05|])
  , (ChangelogSheetPassAlways,                                 [day|2020-05-23|])
  , (ChangelogBetterCourseCommunicationTutorials,              [day|2020-05-25|])
  , (ChangelogAdditionalSheetNotifications,                    [day|2020-05-25|])
  , (ChangelogCourseParticipantsListAddSheets,                 [day|2020-06-14|])
  , (ChangelogYamlRatings,                                     [day|2020-06-17|])
  , (ChangelogSubmissionOnlyExamRegistered,                    [day|2020-07-20|])
  , (ChangelogCourseVisibility,                                [day|2020-08-10|])
  , (ChangelogPersonalisedSheetFiles,                          [day|2020-08-10|])
  , (ChangelogAbolishCourseAssociatedStudyFeatures,            [day|2020-08-28|])
  , (ChangelogExamStaff,                                       [day|2020-10-12|])
  , (ChangelogExamAdditionalSchools,                           [day|2020-10-12|])
  , (ChangelogMaterialsVideoStreaming,                         [day|2020-11-10|])
  , (ChangelogFixPersonalisedSheetFilesKeep,                   [day|2020-11-10|])
  ]
