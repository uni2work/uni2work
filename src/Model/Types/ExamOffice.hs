-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.ExamOffice
  ( ExamOfficeLabelName
  ) where

import Import.NoModel


type ExamOfficeLabelName = Text
