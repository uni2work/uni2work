-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>, Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-|
Module: Model.Types.Common
Description: Common types used by most @Model.Types.*@-Modules

Types used by multiple other @Model.Types.*@-Modules
-}
module Model.Types.Common
  ( module Model.Types.Common
  ) where

import Import.NoModel

import qualified Yesod.Auth.Util.PasswordStore as PWStore


type Count  = Sum Integer
type Points = Centi

type Email = Text

type UserTitle         = Text
type UserFirstName     = Text
type UserSurname       = Text
type UserDisplayName   = Text
type UserIdent         = CI Text
type UserMatriculation = Text
type UserEmail         = CI Email

type StudyDegreeName      = Text
type StudyDegreeShorthand = Text
type StudyDegreeKey       = Int
type StudyTermsName       = Text
type StudyTermsShorthand  = Text
type StudyTermsKey        = Int
type StudySubTermsKey     = Int

type SchoolName      = CI Text
type SchoolShorthand = CI Text

type CourseName          = CI Text
type CourseShorthand     = CI Text
type MaterialName        = CI Text
type TutorialName        = CI Text
type SheetName           = CI Text
type SubmissionGroupName = CI Text

type ExamName           = CI Text
type ExamPartName       = CI Text
type ExamOccurrenceName = CI Text

type AllocationName      = CI Text
type AllocationShorthand = CI Text

type PWHashAlgorithm = ByteString -> PWStore.Salt -> Int -> ByteString

type InstanceId = UUID
type ClusterId  = UUID
type TokenId    = UUID

type TermCandidateIncidence = UUID

type SessionFileReference = Digest SHA3_256

type WorkflowDefinitionName   = CI Text
type WorkflowInstanceName     = CI Text
type WorkflowInstanceCategory = CI Text
type WorkflowOverviewName     = CI Text
