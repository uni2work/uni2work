-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE GeneralizedNewtypeDeriving, UndecidableInstances #-}

module Model.Types.Languages
  ( Languages(..)
  ) where

import ClassyPrelude.Yesod hiding (derivePersistFieldJSON)
import GHC.Exts (IsList)

import Model.Types.TH.JSON

import Control.Lens.TH (makeWrapped)


newtype Languages = Languages [Lang]
  deriving (Eq, Ord, Show, Read, Generic, Typeable)
  deriving newtype (FromJSON, ToJSON, IsList)

instance Default Languages where
  def = Languages []

instance Hashable Languages
instance NFData Languages
derivePersistFieldJSON ''Languages
makeWrapped ''Languages
