-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.SystemMessage where

import Import.NoModel


type SystemMessageVolatileClusterSettings = Set (VolatileClusterSettingsKey, Value)
