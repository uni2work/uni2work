-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE UndecidableInstances #-}

module Model.Migration.Types
  ( module Model.Migration.Types
  ) where

import ClassyPrelude.Yesod
import Data.Aeson
import Data.Aeson.TH (deriveJSON, mkParseJSON)

import Utils.PathPiece

import qualified Model as Current
import qualified Model.Types.TH.JSON as Current

import Data.Universe
import Data.Universe.TH

import qualified Data.Set as Set

import Model.Migration.Version as Model.Migration.Types

import Database.Persist.Sql (PersistFieldSql(..))

import qualified Data.Aeson.Types as JSON


data SheetType
  = Normal { maxPoints :: Current.Points } -- Erhöht das Maximum, wird gutgeschrieben
  | Bonus  { maxPoints :: Current.Points } -- Erhöht nicht das Maximum, wird gutgeschrieben
  | Pass   { maxPoints, passingPoints :: Current.Points }
  | NotGraded
  deriving (Show, Read, Eq)

sheetType :: SheetType -> Current.SheetType a
sheetType Bonus {..}  = Current.Bonus  Current.Points {..}
sheetType Normal {..} = Current.Normal Current.Points {..}
sheetType Pass {..}   = Current.Normal Current.PassPoints {..}
sheetType NotGraded   = Current.NotGraded


data UploadMode = NoUpload | Upload { unpackZips :: Bool }
  deriving (Show, Read, Eq, Ord, Generic)

deriveJSON defaultOptions ''UploadMode
Current.derivePersistFieldJSON ''UploadMode

deriveFinite ''UploadMode
finitePathPiece ''UploadMode ["no-upload", "no-unpack", "unpack"]

data SheetSubmissionMode = NoSubmissions
                         | CorrectorSubmissions
                         | UserSubmissions
  deriving (Show, Read, Eq, Ord, Enum, Bounded, Generic)

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece
  } ''SheetSubmissionMode
derivePersistField "SheetSubmissionMode"

instance Universe SheetSubmissionMode
instance Finite SheetSubmissionMode

nullaryPathPiece ''SheetSubmissionMode camelToPathPiece

deriveJSON defaultOptions ''SheetType
Current.derivePersistFieldJSON ''SheetType



data Transaction
  = TransactionTermEdit
    { transactionTerm   :: Current.TermIdentifier
    }
  | TransactionExamRegister
    { transactionTerm   :: Current.TermIdentifier
    , transactionSchool :: Current.SchoolShorthand
    , transactionCourse :: Current.CourseShorthand
    , transactionExam   :: Current.ExamName
    , transactionUser   :: Current.UserIdent
    }
  | TransactionExamDeregister
    { transactionTerm   :: Current.TermIdentifier
    , transactionSchool :: Current.SchoolShorthand
    , transactionCourse :: Current.CourseShorthand
    , transactionExam   :: Current.ExamName
    , transactionUser   :: Current.UserIdent
    }
  deriving (Eq, Ord, Read, Show, Generic, Typeable)

deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  , fieldLabelModifier = camelToPathPiece' 1
  , tagSingleConstructors = True
  , sumEncoding = TaggedObject "transaction" "data"
  } ''Transaction

Current.derivePersistFieldJSON ''Transaction



data PredLiteral a = PLVariable { plVar :: a } | PLNegated { plVar :: a }
  deriving (Eq, Ord)
deriveJSON defaultOptions
  { constructorTagModifier = camelToPathPiece' 1
  , sumEncoding = TaggedObject "val" "var"
  } ''PredLiteral
  
newtype PredDNF a = PredDNF { dnfTerms :: Set (NonNull (Set (PredLiteral a))) }

$(return [])

instance ToJSON (PredDNF a) where
  toJSON = error "toJSON @(Legacy.PredDNF _): not implemented"
instance (Ord a, FromJSON a) => FromJSON (PredDNF a) where
  parseJSON = $(mkParseJSON defaultOptions{ tagSingleConstructors = True, sumEncoding = ObjectWithSingleField } ''PredDNF)

newtype ExamModeDNF = ExamModeDNF (PredDNF Current.ExamModePredicate)
  deriving newtype (ToJSON, FromJSON)

Current.derivePersistFieldJSON ''ExamModeDNF

examModeDNF :: ExamModeDNF -> Current.ExamModeDNF
examModeDNF (ExamModeDNF PredDNF{..}) = Current.ExamModeDNF . Current.PredDNF $ Set.map (impureNonNull . Set.map toCurrentPredLiteral . toNullable) dnfTerms
  where toCurrentPredLiteral PLVariable{..} = Current.PLVariable plVar
        toCurrentPredLiteral PLNegated{..} = Current.PLNegated plVar


type WorkflowState fileid userid = NonNull (Seq (WorkflowAction fileid userid))
type DBWorkflowState = WorkflowState Current.FileReference Current.SqlBackendKey

data WorkflowAction fileid userid = WorkflowAction
  { wpTo      :: Current.WorkflowGraphNodeLabel
  , wpVia     :: Current.WorkflowGraphEdgeLabel
  , wpPayload :: Map Current.WorkflowPayloadLabel (Set (Current.WorkflowFieldPayloadW fileid userid))
  , wpUser    :: Maybe (Maybe userid) -- ^ Outer `Maybe` encodes automatic/manual, inner `Maybe` encodes whether user was authenticated
  , wpTime    :: UTCTime
  }
  deriving (Eq, Ord, Show, Generic, Typeable)
  deriving anyclass (NFData)

instance (   ToJSON fileid,   ToJSON userid
         , FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         ) => PersistField (WorkflowState fileid userid) where
  toPersistValue   = toPersistValueJSON
  fromPersistValue = fromPersistValueJSON
instance (   ToJSON fileid,   ToJSON userid
         , FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         ) => PersistFieldSql (WorkflowState fileid userid) where
  sqlType _ = Current.sqlTypeJSON

newtype JsonWorkflowActionUser userid = JsonWorkflowActionUser (Maybe (Maybe userid))

instance ToJSON userid => ToJSON (JsonWorkflowActionUser userid) where
  toJSON (JsonWorkflowActionUser x) = case x of
    Nothing -> JSON.Null
    Just Nothing -> JSON.object [ "tag" JSON..= ("unauthenticated" :: Text) ]
    Just (Just x') -> toJSON x'

instance FromJSON userid => FromJSON (JsonWorkflowActionUser userid) where
  parseJSON JSON.Null = pure $ JsonWorkflowActionUser Nothing
  parseJSON x@(JSON.Object _)
    | x == JSON.object [ "tag" JSON..= ("unauthenticated" :: Text) ]
    = pure . JsonWorkflowActionUser $ Just Nothing 
    | otherwise
    = JsonWorkflowActionUser . Just . Just <$> parseJSON x
  parseJSON x = JsonWorkflowActionUser . Just . Just <$> parseJSON x

instance (ToJSON fileid, ToJSON userid) => ToJSON (WorkflowAction fileid userid) where
  toJSON WorkflowAction{..} = JSON.object
    [ "to" JSON..= wpTo
    , "via" JSON..= wpVia
    , "payload" JSON..= wpPayload
    , "user" JSON..= JsonWorkflowActionUser wpUser
    , "time" JSON..= wpTime
    ]

instance ( FromJSON fileid, FromJSON userid
         ,      Ord fileid,      Ord userid
         , Typeable fileid, Typeable userid
         ) => FromJSON (WorkflowAction fileid userid) where
  parseJSON = JSON.withObject "WorkflowAction" $ \o -> do
    wpTo <- o JSON..: "to"
    wpVia <- o JSON..: "via"
    wpPayload <- o JSON..: "payload"
    JsonWorkflowActionUser wpUser <- o JSON..: "user"
    wpTime <- o JSON..: "time"
    return WorkflowAction{..}
