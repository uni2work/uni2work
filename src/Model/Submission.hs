-- SPDX-FileCopyrightText: 2022 Sarah Vaupel <vaupel.sarah@campus.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Submission where

import ClassyPrelude.Yesod
import CryptoID

data SubmissionSinkException = DuplicateFileTitle FilePath
                             | DuplicateRating
                             | RatingWithoutUpdate
                             | ForeignRating CryptoFileNameSubmission
                             | InvalidFileTitleExtension FilePath
  deriving (Typeable, Show)

instance Exception SubmissionSinkException

data SubmissionMultiSinkException
  = SubmissionSinkException
    { _submissionSinkId :: CryptoFileNameSubmission
    , _submissionSinkFedFile :: Maybe FilePath
    , _submissionSinkException :: SubmissionSinkException
    }
  deriving (Typeable, Show)

instance Exception SubmissionMultiSinkException
