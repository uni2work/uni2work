-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Tokens.Lens
  ( module Model.Tokens.Lens
  ) where

import Control.Lens


class HasTokenIdentifier s a | s -> a where
  _tokenIdentifier :: Lens' s a

class HasTokenIssuedBy s a | s -> a where
  _tokenIssuedBy :: Lens' s a

class HasTokenIssuedFor s a | s -> a where
  _tokenIssuedFor :: Lens' s a

class HasTokenIssuedAt s a | s -> a where
  _tokenIssuedAt :: Lens' s a

class HasTokenExpiresAt s a | s -> a where
  _tokenExpiresAt :: Lens' s a

class HasTokenStartsAt s a | s -> a where
  _tokenStartsAt :: Lens' s a
