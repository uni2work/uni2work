-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main
  ( main
  ) where

import Prelude
import Criterion.Main
import Network.Wai (Application)
import Network.Wai.Test
import Network.Wai.Test.Internal
import Control.DeepSeq
-- import Data.Int (Int64)
import Data.Function ((&))
-- import Debug.Trace
-- import Control.Monad.IO.Class
import GHC.Generics (Generic)
import qualified Control.Monad.Trans.Resource as ResourceT
import Network.HTTP.Types.Status (Status(..))
import Control.Monad
-- import Control.Concurrent (threadDelay)

import "uniworx" Application (getApplicationRepl, shutdownApp)
import "uniworx" Utils.PathPiece (camelToPathPiece')

import Data.Set (Set)
import qualified Data.Set as Set

import Data.Aeson.TH
import qualified Data.Yaml as Yaml

import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text

import Data.List (isPrefixOf)

import Control.Monad.Catch
import Data.Maybe
import System.Environment
import Data.Default
import Web.Cookie



deriving stock instance Generic Status
deriving anyclass instance NFData Status

deriving stock instance Generic SResponse
deriving anyclass instance NFData SResponse

deriving stock instance Generic ClientState
deriving anyclass instance NFData ClientState


data BenchmarkParameters = BenchmarkParameters
  { bpTopWorkflowInstanceListRUserIdents :: Set (Maybe Text)
  } deriving stock (Eq, Ord, Read, Show, Generic)
    deriving anyclass (NFData)
deriveJSON defaultOptions{ fieldLabelModifier = camelToPathPiece' 1 } ''BenchmarkParameters

-- | Parameters for result of `Database.Fill`
instance Default BenchmarkParameters where
  def = BenchmarkParameters
    { bpTopWorkflowInstanceListRUserIdents = Set.fromList
        [ Nothing, Just "G.Kleen@campus.lmu.de", Just "Stephan.Barth@campus.lmu.de" ]
    }


main :: IO ()
main = do
  BenchmarkParameters{..} <- getBenchmarkParameters

  defaultMain
    [ bgroup "TopWorkflowInstanceListR"
        [ bench (maybe "unauthenticated" Text.unpack ident) $ perRunEnvWithCleanup (doStart ident) cleanupStart benchTopWorkflowInstanceListR
        | ident <- Set.toList bpTopWorkflowInstanceListRUserIdents
        ]
    ]
  where
    getBenchmarkParameters = do
      parameterFile <- fromMaybe ".benchmark-params.yaml" <$> lookupEnv "BENCHMARK_PARAMETER_FILE"
      handleIf (\case { Yaml.InvalidYaml (Just (Yaml.YamlException str)) -> "Yaml file not found: " `isPrefixOf` str; _other -> False }) (const $ return def) $ Yaml.decodeFileThrow @_ @BenchmarkParameters parameterFile

doStart :: Maybe Text -> IO (Application, ResourceT.InternalState, ClientState)
doStart ident = do
  iSt <- ResourceT.createInternalState
  (_, _, app) <- ResourceT.runInternalState getApplicationRepl iSt
  (_, cSt) <- flip (runSessionWith initState) app $ do
    setClientCookie defaultSetCookie{ setCookieName = "ACTIVE-AUTH-TAGS", setCookieValue = "{\"admin\":true}" }
    forM_ ident $ \ident' ->
      setClientCookie defaultSetCookie{ setCookieName = "DUMMY-LOGIN", setCookieValue = Text.encodeUtf8 ident' }
  return (app, iSt, cSt)
cleanupStart :: (Application, ResourceT.InternalState, ClientState) -> IO ()
cleanupStart (_, iSt, _) = shutdownApp *> ResourceT.closeInternalState iSt

benchTopWorkflowInstanceListR :: (Application, ResourceT.InternalState, ClientState) -> IO SResponse
benchTopWorkflowInstanceListR (app, _, cSt) = fmap fst . flip (runSessionWith cSt) app $
  request $ defaultRequest
    & flip setPath "/workflow-instances"
