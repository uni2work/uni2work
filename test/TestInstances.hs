-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module TestInstances
  (
  ) where

import Text.Blaze.TestInstances as TestInstances ()
import Database.Persist.Sql.Types.TestInstances as TestInstances ()
import Data.NonNull.TestInstances as TestInstances ()
import Jose.Jwk.TestInstances as TestInstances ()
import Servant.Client.Core.BaseUrl.TestInstances as TestInstances ()
import Crypto.Hash.TestInstances as TestInstances ()
