-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module ServantApi.ExternalApis.TypeSpec where

import TestImport
import TestInstances ()
import Model.TypesSpec ()

import ServantApi.ExternalApis.Type


instance Arbitrary ExternalApiCreationRequest where
  arbitrary = ExternalApiCreationRequest
    <$> scale (`div` 2) arbitrary
    <*> scale (`div` 2) arbitrary
    <*> scale (`div` 2) arbitrary
  shrink = genericShrink


spec :: Spec
spec = return ()
