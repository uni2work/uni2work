-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Data.Scientific.InstancesSpec where

import TestImport
import Data.Scientific


spec :: Spec
spec = modifyMaxSuccess (* 10) $
  lawsCheckHspec (Proxy @Scientific)
    [ pathPieceLaws ]
