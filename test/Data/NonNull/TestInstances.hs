-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Data.NonNull.TestInstances
  (
  ) where

import TestImport


instance (Arbitrary a, MonoFoldable a) => Arbitrary (NonNull a) where
  arbitrary = arbitrary `suchThatMap` fromNullable

instance CoArbitrary a => CoArbitrary (NonNull a) where
  coarbitrary = coarbitrary . toNullable

instance (MonoFoldable a, Function a) => Function (NonNull a) where
  function = functionMap toNullable impureNonNull
