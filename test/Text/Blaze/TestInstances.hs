-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Text.Blaze.TestInstances
  (
  ) where

import TestImport

import Text.Blaze.Html
import Text.Blaze.Renderer.Text


instance Arbitrary Html where
  arbitrary = (preEscapedToHtml :: String -> Html) . getPrintableString <$> arbitrary
  shrink = map preEscapedToHtml . shrink . renderMarkup
