-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database
  ( main
  , truncateDb
  , module Database.Fill
  ) where

import "uniworx" Import hiding (Option(..), getArgs)
import "uniworx" Application (db', getAppSettings)
import "uniworx" Model.Migration (deleteAppliedMigration)
import "uniworx" Model.Migration.Definitions (ManualMigration, undoCustomMigrations)

import Database.Persist.Postgresql
import Database.Persist.SqlBackend.Internal ( connEscapeFieldName )
import Control.Monad.Logger

import System.Console.GetOpt
import System.Exit (exitWith, ExitCode(..))
import System.IO (hPutStrLn)
import System.Environment (getArgs, withArgs)

import Database.Persist.Sql.Raw.QQ

import Database.Fill

import qualified Utils.Pool as Custom

import qualified Data.Text as Text
import qualified Data.Map as Map

import qualified Data.CaseInsensitive as CI

import Utils.Workflow
import Handler.Utils.Users


data DBAction = DBClear
              | DBTruncate
              | DBMigrate
              | DBFill
              | DBUndoMigrate String
              | DBInsertWorkflowOverviewSpec FilePath


argsDescr :: [OptDescr DBAction]
argsDescr =
  [ Option ['c'] ["clear"]        (NoArg DBClear)                    "Delete everything accessable by the current database user"
  , Option ['t'] ["truncate"]     (NoArg DBTruncate)                 "Truncate all tables mentioned in the current schema (This cannot be run concurrently with any other activity accessing the database)"
  , Option ['m'] ["migrate"]      (NoArg DBMigrate)                  "Perform database migration"
  , Option ['u'] ["undo-migrate"] (ReqArg DBUndoMigrate "MIGRATION") "Undo a specific manual database migration"
  , Option ['f'] ["fill"]         (NoArg DBFill)                     "Fill database with example data"

  , Option [] ["insert-workflow-overview-spec"] (ReqArg DBInsertWorkflowOverviewSpec "FILEPATH") ""
  ]


main :: IO ()
main = do
  args <- map unpack <$> getArgs
  case getOpt' Permute argsDescr args of
    (acts@(_:_), nonOpts, unrecOpts, []) -> withArgs (unrecOpts ++ nonOpts) . forM_ acts $ \case
      DBClear   -> runStderrLoggingT $ do -- We don't use `db` here, since we do /not/ want any migrations to run, yet
        settings <- liftIO getAppSettings
        withPostgresqlConn (pgConnStr $ appDatabaseConf settings) . runSqlConn $ do
          [executeQQ|drop owned by current_user|] :: ReaderT SqlBackend _ ()
      DBTruncate -> db' $ do
        foundation <- getYesod
        Custom.purgePool $ appConnPool foundation
        truncateDb
      DBMigrate -> db' $ return ()
      DBFill    -> db' $ fillDb
      DBUndoMigrate str
        | Just migrationIdent <- fromPathPiece . Text.strip $ Text.pack str
        , Just migrationUndo <- Map.lookup migrationIdent undoCustomMigrations
          -> runResourceT . runStderrLoggingT $ do -- We don't use `db` here, since we do /not/ want any migrations to run
                settings <- liftIO getAppSettings
                withPostgresqlConn (pgConnStr $ appDatabaseConf settings) . runSqlConn $
                  migrationUndo >> deleteAppliedMigration migrationIdent
        | otherwise -> do
            hPutStrLn stderr $ "Migration ‘" <> str <> "’ " <> bool "does not exist" "has no undo implementation" (is _Just . fromPathPiece @ManualMigration . Text.strip $ Text.pack str)
            exitWith $ ExitFailure 2
      DBInsertWorkflowOverviewSpec fp ->
        db' . void $ insertSharedWorkflowOverviewSpec =<< parseWorkflowOverviewSpecFile fp
    (_, _, _, errs) -> do
      forM_ errs $ hPutStrLn stderr
      hPutStrLn stderr $ usageInfo "uniworxdb" argsDescr
      exitWith $ ExitFailure 2

parseWorkflowOverviewSpecFile :: (MonadHandler m, HandlerSite m ~ UniWorX) => FilePath -> SqlPersistT m DBWorkflowOverviewSpec
parseWorkflowOverviewSpecFile specPath = hoist liftHandler $ do
  oSpec <- decodeSafeYAMLFileThrow specPath
  fmap (view _DBWorkflowOverviewSpec) . forOf (typesCustom @WorkflowChildren @(WorkflowOverviewSpec UserIdent) @IdWorkflowOverviewSpec @UserIdent @UserId) oSpec $ \userIdent -> do
    userRes <- guessUser
      (             predDNFSingleton (PLVariable $ GuessUserIdent userIdent)
        `predDNFOr` predDNFSingleton (PLVariable $ GuessUserEmail userIdent)
      )
      (Just 2)
    case userRes of
      Just (Right (Entity uid _)) -> return uid
      _other -> terror $ "Could not uniquely identify user ‘" <> CI.original userIdent <> "’"


truncateDb :: MonadIO m => ReaderT SqlBackend m ()
truncateDb = do
  tables <- map unSingle <$> [sqlQQ|SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'|]
  sqlBackend <- ask

  let escapedTables = map (connEscapeFieldName sqlBackend . FieldNameDB) $ filter (not . (`elem` protected)) tables -- ugh. We assume `connEscapeFieldName` behaves identically to `connEscapeTableName`
      query = "TRUNCATE TABLE " ++ intercalate ", " escapedTables ++ " RESTART IDENTITY"
      protected = ["applied_migration"]
  rawExecute query []
