-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main
  ( main
  ) where

import Prelude (IO)

import Spec (spec)

import Test.Hspec.JUnit (hspecJUnit)

main :: IO ()
main = hspecJUnit spec
