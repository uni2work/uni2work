-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <sarah.vaupel@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module User
  ( fakeUser
  ) where

import ClassyPrelude

import Settings
import Model

import Data.Default
import System.IO.Unsafe


fakeUser :: (User -> User) -> User
fakeUser adjUser = adjUser User{..}
  where
    UserDefaultConf{..} = appUserDefaults compileTimeAppSettings
    
    userMatrikelnummer = Nothing
    userAuthentication = AuthLDAP
    userLastAuthentication = Nothing
    userTokensIssuedAfter = Nothing
    userIdent = "dummy@example.invalid"
    userEmail = "dummy@example.invalid"
    userDisplayEmail = "dummy@example.invalid"
    userDisplayName = "Dummy Example"
    userSurname = "Example"
    userFirstName = "Dummy"
    userTitle = Nothing
    userTheme = userDefaultTheme
    userMaxFavourites = userDefaultMaxFavourites
    userMaxFavouriteTerms = userDefaultMaxFavouriteTerms
    userDateTimeFormat = userDefaultDateTimeFormat
    userDateFormat = userDefaultDateFormat
    userTimeFormat = userDefaultTimeFormat
    userDownloadFiles = userDefaultDownloadFiles
    userLanguages = Nothing
    userWarningDays = userDefaultWarningDays
    userCsvOptions = def
    userExamOfficeGetSynced = True
    userExamOfficeGetLabels = True
    userSex = Nothing
    userShowSex = userDefaultShowSex
    userNotificationSettings = def
    userCreated = unsafePerformIO getCurrentTime
    userLastLdapSynchronisation = Nothing
    userLdapPrimaryKey = Nothing
