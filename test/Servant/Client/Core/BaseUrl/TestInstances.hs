-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Servant.Client.Core.BaseUrl.TestInstances
  () where

import TestImport

import Network.URI
import Network.URI.Arbitrary ()
import Servant.Client.Core.BaseUrl

import Control.Lens.Extras


instance Arbitrary BaseUrl where
  arbitrary = (`suchThatMap` toBaseUrl) $ do
      uri <- scale (min 10) arbitrary `suchThat` (is _Just . uriAuthority)
      uriScheme <- oneof $ map (return . (<> ":")) [ "http", "https" ]
      let uriAuthority'' = uriAuthority uri <&> \uriAuthority' -> uriAuthority'{ uriUserInfo = "" }
      return (uri, uriScheme, uriAuthority'')
    where
      toBaseUrl (uri, uriScheme, uriAuthority'') = either (const Nothing) Just . parseBaseUrl . ($ mempty) $ uriToString (const mempty) uri{ uriScheme, uriAuthority = uriAuthority'', uriQuery = "", uriFragment = "" }
