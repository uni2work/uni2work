-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Crypto.Hash.TestInstances
  () where

import Crypto.Hash (Digest, HashAlgorithm)
import qualified Crypto.Hash as Crypto
import TestImport
import qualified Data.ByteArray as BA


instance HashAlgorithm a => Arbitrary (Digest a) where
  arbitrary = Crypto.hash @ByteString @a <$> arbitrary
instance CoArbitrary (Digest a) where
  coarbitrary = coarbitrary . BA.convert @_ @ByteString
instance HashAlgorithm a => Function (Digest a) where
  function = functionShow
