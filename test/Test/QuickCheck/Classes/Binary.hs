-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Test.QuickCheck.Classes.Binary
  ( binaryLaws
  ) where

import ClassyPrelude
import Test.QuickCheck
import Test.QuickCheck.Classes

import Data.Proxy (Proxy(..))
import Data.Binary
import Data.Binary.Put

binaryLaws :: forall a. (Arbitrary a, Binary a, Eq a, Show a) => Proxy a -> Laws
binaryLaws _ = Laws "Binary"
  [ ("Partial Isomorphism", property $ \(a :: a) -> decode' (encode a) === Just a)
  , ("Valid list encoding", property $ \(as :: [a]) -> runPut (putList as) === runPut (put as))
  ]
  where decode' inp = case decodeOrFail inp of
          Right (unc, _, res)
            | null unc -> Just res
          _other
            -> Nothing
