-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Test.QuickCheck.Classes.Hashable
  ( hashableLaws
  ) where

import ClassyPrelude
import Test.QuickCheck
import Test.QuickCheck.Classes
import Data.Proxy

hashableLaws :: forall a. (Arbitrary a, Hashable a, Eq a, Show a) => Proxy a -> Laws
hashableLaws _ = Laws "Hashable"
  [ ("Injectivity", property $ \(a :: a) (a' :: a) (s :: Int) -> hashWithSalt s a /= hashWithSalt s a' ==> a /= a')
  ]
