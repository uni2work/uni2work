-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module MailSpec where

import TestImport
import Utils.DateTimeSpec ()
import Model.TypesSpec ()

import Mail

instance Arbitrary MailSmtpData where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance Arbitrary MailContext where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance Arbitrary Address where
  arbitrary = genericArbitrary
  shrink = genericShrink

spec :: Spec
spec = do
  parallel $ do
    lawsCheckHspec (Proxy @MailSmtpData)
      [ eqLaws, ordLaws, showReadLaws, monoidLaws ]
    lawsCheckHspec (Proxy @MailContext)
      [ eqLaws, ordLaws, showReadLaws, jsonLaws, hashableLaws ]
