-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Persist.Sql.Types.TestInstances
  (
  ) where

import TestImport

import Database.Persist.Sql
import Data.Binary (Binary)
  

deriving newtype instance Arbitrary (BackendKey SqlBackend)
deriving newtype instance Arbitrary (BackendKey SqlWriteBackend)
deriving newtype instance Arbitrary (BackendKey SqlReadBackend)
deriving newtype instance Binary (BackendKey SqlWriteBackend)
deriving newtype instance Binary (BackendKey SqlReadBackend)
