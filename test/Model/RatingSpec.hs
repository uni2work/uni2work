-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.RatingSpec where

import TestImport
import ModelSpec ()
import Model.Rating

instance Arbitrary RatingExamPartReference where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance Arbitrary Rating' where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance Arbitrary Rating where
  arbitrary = genericArbitrary
  shrink = genericShrink

spec :: Spec
spec = return ()
