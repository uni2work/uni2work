-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.MigrationSpec where

import TestImport

import Model.Migration


spec :: Spec
spec = withApp $ -- `withApp` does migration, if needed
  describe "Migration" $
    it "is idempotent" $
      (`shouldBe` False) <$> runDB requiresMigration -- Migration shouldn't be needed after `withApp` above
