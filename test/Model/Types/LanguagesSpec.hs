-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Model.Types.LanguagesSpec where

import TestImport


instance Arbitrary Languages where
  arbitrary = fmap Languages $ shuffle =<< sublistOf (toList appLanguages)
  shrink = genericShrink


spec :: Spec
spec = do
  lawsCheckHspec (Proxy @Languages)
    [ eqLaws, ordLaws, showReadLaws, isListLaws, jsonLaws, hashableLaws, persistFieldLaws ]
