-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.PathPieceSpec where

import TestImport

import Utils.PathPiece


spec :: Spec
spec = describe "pathPieceJoined" $ do
  it "is a prism" . property $ \(NonEmpty (pack -> joinPP)) -> isPrism $ pathPieceJoined joinPP
  it "behaves as expected on some examples" $ do
    let test xs t = do
          review (pathPieceJoined "--") xs `shouldBe` t
          preview (pathPieceJoined "--") t `shouldBe` Just xs
    test ["foo", "bar"] "foo--bar"
    test ["foo--bar", "baz"] "foo----bar--baz"
    test ["baz", "foo--bar"] "baz--foo----bar"
    test ["baz--quux", "foo--bar"] "baz----quux--foo----bar"
