-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.I18nSpec where

import TestImport

import Utils.I18n


instance Arbitrary a => Arbitrary (I18n a) where
  arbitrary = genericArbitrary
  shrink = genericShrink

spec :: Spec
spec = do
  parallel $ do
    lawsCheckHspec (Proxy @I18nText)
      [ eqLaws, ordLaws, showLaws, showReadLaws, jsonLaws, persistFieldLaws ]
    lawsCheckHspec (Proxy @I18n)
      [ foldableLaws, functorLaws, traversableLaws ]
