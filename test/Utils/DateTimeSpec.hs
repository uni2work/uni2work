-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Sarah Vaupel <vaupel.sarah@campus.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Utils.DateTimeSpec where

import TestImport

import Utils.DateTime


instance Arbitrary DateTimeFormat where
  arbitrary = DateTimeFormat <$> arbitrary
  shrink = genericShrink

instance Arbitrary SelDateTimeFormat where
  arbitrary = genericArbitrary
  shrink = genericShrink
instance CoArbitrary SelDateTimeFormat where
  coarbitrary = genericCoarbitrary

spec :: Spec
spec = do
  parallel $ do
    lawsCheckHspec (Proxy @DateTimeFormat)
      [ eqLaws, ordLaws, showReadLaws, jsonLaws, persistFieldLaws, hashableLaws ]
    lawsCheckHspec (Proxy @SelDateTimeFormat)
      [ eqLaws, ordLaws, showReadLaws, boundedEnumLaws, finiteLaws, hashableLaws, jsonLaws, jsonKeyLaws ]
