-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Auth.LDAP.ADSpec where

import TestImport

import Auth.LDAP.AD
import Ldap.Client


spec :: Spec
spec = do
  describe "parseADError" $ do
    it "parses some examples" . mapM_ exampleEntry $
      [ ( InvalidCredentials, ADAccountDisabled, "80090308: LdapErr: DSID-0C090446, comment: AcceptSecurityContext error, data 533, v2580")
      , ( InvalidCredentials, ADLogonFailure   , "80090308: LdapErr: DSID-0C090446, comment: AcceptSecurityContext error, data 52e, v2580")
      ]

exampleEntry :: ( ResultCode, ADError, Text ) -> Expectation
exampleEntry ( resCode, adError, errMsg ) = example $ parseADError resCode errMsg `shouldBe` Right adError
