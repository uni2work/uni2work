-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Jose.Jwk.TestInstances
  () where

import TestImport

import Crypto.Random
import Jose.Jwk
import Jose.Jwt


instance Arbitrary KeyUse where
  arbitrary = genericArbitrary
  
instance Arbitrary JwkSet where
  arbitrary = fmap (JwkSet . concat) . listOf $ do
    kId <- UTCKeyId <$> arbitrary
    kUse <- arbitrary
    oneof
      [ withDRG' $ do
          (kPub, kPriv) <- generateRsaKeyPair 2048 kId kUse Nothing
          return [kPub, kPriv]
      , withDRG' $
          pure <$> generateSymmetricKey 32 kId kUse Nothing
      ]
    where
      withDRG' c = do
        seed <- (,,,,)
          <$> arbitraryBoundedRandom
          <*> arbitraryBoundedRandom
          <*> arbitraryBoundedRandom
          <*> arbitraryBoundedRandom
          <*> arbitraryBoundedRandom
        let chacha = drgNewTest seed
        return . fst $ withDRG chacha c
