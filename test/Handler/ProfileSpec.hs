-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.ProfileSpec (spec) where

import TestImport

import qualified Data.CaseInsensitive as CI

import Yesod.Core.Handler (toTextUrl)

spec :: Spec
spec = withApp $ do
  describe "Profile page" $ do
    it "asserts no access to my-account for anonymous users" $ do
      get ProfileR

      loginText <- runHandler . toTextUrl $ AuthR LoginR

      assertHeader "Location" $ encodeUtf8 loginText
  
      either (throwM . userError . unpack) (\_ -> return ()) =<< followRedirect
      statusIs 200

    it "asserts access to my-account for authenticated users" $ do
      userEntity <- createUser id
      authenticateAs userEntity

      get ProfileR
      statusIs 200

    it "displays basic user data" $ do
      userEntity@(Entity _userId User{..}) <- createUser id
      authenticateAs userEntity

      get ProfileDataR
      statusIs 200

      forM_ (words userDisplayName) $ \nameWord -> do
        htmlAnyContain ".profile dd" $ unpack nameWord
      htmlAnyContain ".profile dd" $ unpack userSurname
      htmlAnyContain ".profile dd" . unpack $ CI.original userIdent
      htmlAnyContain ".profile dd" . unpack $ CI.original userEmail
          
