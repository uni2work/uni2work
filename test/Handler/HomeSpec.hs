-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.HomeSpec (spec) where

import TestImport

spec :: Spec
spec = withApp $ do
    describe "Homepage" $ do
        it "loads the index and checks it looks right" $ do
          request $ do
            setMethod "GET"
            setUrl NewsR
            addRequestHeader ("Accept-Language", "de")
          statusIs 200
          htmlAnyContain "h1" "Aktuelles"
