-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>,Steffen Jost <jost@tcs.ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.CommonSpec (spec) where

import TestImport

spec :: Spec
spec = withApp $ do
    describe "robots.txt" $ do
        it "gives a 200" $ do
            get $ WellKnownR RobotsTxt
            statusIs 200
        it "has correct User-agent" $ do
            get $ WellKnownR RobotsTxt
            bodyContains "User-agent: *"
    describe "favicon.ico" $ do
        it "gives a 200" $ do
            get $ WellKnownR FaviconIco
            statusIs 200
