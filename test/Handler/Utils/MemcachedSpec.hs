-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.MemcachedSpec where

import TestImport
import "uniworx" Import

import Handler.Utils.Memcached

import Control.Monad.Random.Strict (randomIO)


newtype TestValue = TestValue Word64
  deriving stock (Eq, Ord, Read, Show, Generic, Typeable)
  deriving newtype (Binary, Random)
  deriving anyclass (NFData)

spec :: Spec
spec = do
  withApp' (_appMemcachedLocalConf .~ Nothing) . describe "Memcached" $ do
    it "can store and retrieve values without expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcached) $
          liftIO $ pendingWith "Memcached not configured"
        memcachedBySet Nothing () tVal
        memcachedByGet ()
      liftIO $ tVal' `shouldBe` Just tVal
    it "can invalidate values stored without expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcached) $
          liftIO $ pendingWith "Memcached not configured"
        memcachedBySet Nothing () tVal
        memcachedByInvalidate () $ Proxy @TestValue
        memcachedByGet @TestValue ()
      liftIO $ tVal' `shouldBe` Nothing
    it "can store and retrieve values with expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcached) $
          liftIO $ pendingWith "Memcached not configured"
        memcachedBySet (Just $ Right 600) () tVal
        memcachedByGet ()
      liftIO $ tVal' `shouldBe` Just tVal
    it "can expire values" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcached) $
          liftIO $ pendingWith "Memcached not configured"
        now <- liftIO getCurrentTime
        memcachedBySet (Just . Left $ (-1) `addUTCTime` now) () tVal
        memcachedByGet @TestValue ()
      liftIO $ tVal' `shouldBe` Nothing
    it "can invalidate values stored with expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcached) $
          liftIO $ pendingWith "Memcached not configured"
        memcachedBySet (Just $ Right 600) () tVal
        memcachedByInvalidate () $ Proxy @TestValue
        memcachedByGet @TestValue ()
      liftIO $ tVal' `shouldBe` Nothing
  withApp' (_appMemcachedConf .~ Nothing) . describe "MemcachedLocal" $ do
    it "can store and retrieve values without expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcachedLocal) $
          liftIO $ pendingWith "MemcachedLocal not configured"
        memcachedBySet Nothing () tVal
        memcachedByGet ()
      liftIO $ tVal' `shouldBe` Just tVal
    it "can invalidate values stored without expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcachedLocal) $
          liftIO $ pendingWith "MemcachedLocal not configured"
        memcachedBySet Nothing () tVal
        memcachedByInvalidate () $ Proxy @TestValue
        memcachedByGet @TestValue ()
      liftIO $ tVal' `shouldBe` Nothing
    it "can store and retrieve values with expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcachedLocal) $
          liftIO $ pendingWith "MemcachedLocal not configured"
        memcachedBySet (Just $ Right 600) () tVal
        memcachedByGet ()
      liftIO $ tVal' `shouldBe` Just tVal
    it "can expire values" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcachedLocal) $
          liftIO $ pendingWith "MemcachedLocal not configured"
        now <- liftIO getCurrentTime
        memcachedBySet (Just . Left $ (-1) `addUTCTime` now) () tVal
        memcachedByGet @TestValue ()
      liftIO $ tVal' `shouldBe` Nothing
    it "can invalidate values stored with expiration" $ do
      tVal <- randomIO @TestValue
      tVal' <- runHandler $ do
        whenM (getsYesod $ isn't _Just . appMemcachedLocal) $
          liftIO $ pendingWith "MemcachedLocal not configured"
        memcachedBySet (Just $ Right 600) () tVal
        memcachedByInvalidate () $ Proxy @TestValue
        memcachedByGet @TestValue ()
      liftIO $ tVal' `shouldBe` Nothing
