-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Workflow.CanonicalRouteSpec where

import TestImport
import Handler.Utils.Workflow.CanonicalRoute
import ModelSpec ()
import FoundationSpec ()


instance Arbitrary WorkflowScopeRoute where
  arbitrary = genericArbitrary
  shrink = genericShrink
instance CoArbitrary WorkflowScopeRoute
instance Function WorkflowScopeRoute

instance Arbitrary WorkflowInstanceR where
  arbitrary = genericArbitrary
  shrink = genericShrink
instance CoArbitrary WorkflowInstanceR
instance Function WorkflowInstanceR

instance Arbitrary WorkflowWorkflowR where
  arbitrary = genericArbitrary
  shrink = genericShrink
instance CoArbitrary WorkflowWorkflowR
instance Function WorkflowWorkflowR
  

spec :: Spec
spec = describe "_WorkflowSpecRoute" $
  before_ (pendingWith "Missing routes") . it "is a prism" . property $ isPrism _WorkflowScopeRoute
