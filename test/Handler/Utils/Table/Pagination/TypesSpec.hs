-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Table.Pagination.TypesSpec where

import TestImport

import Handler.Utils.Table.Pagination.Types


instance Arbitrary FilterKey where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance Arbitrary SortingKey where
  arbitrary = genericArbitrary
  shrink = genericShrink

spec :: Spec
spec = do
  parallel $ do
    lawsCheckHspec (Proxy @FilterKey)
      [ eqLaws, ordLaws, showReadLaws, pathPieceLaws, jsonLaws, jsonKeyLaws ]
    lawsCheckHspec (Proxy @SortingKey)
      [ eqLaws, ordLaws, showReadLaws, pathPieceLaws, jsonLaws, jsonKeyLaws ]
