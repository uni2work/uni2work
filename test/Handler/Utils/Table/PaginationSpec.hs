-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Utils.Table.PaginationSpec where

import TestImport

import Handler.Utils.Table.Pagination
import Handler.Utils.Table.Pagination.TypesSpec ()

import Data.Aeson (encode)
  

instance Arbitrary SortDirection where
  arbitrary = genericArbitrary
  shrink = genericShrink
  
instance Arbitrary SortingSetting where
  arbitrary = genericArbitrary
  shrink = genericShrink
  
instance Arbitrary PaginationInput where
  arbitrary = scale (`div` 2) genericArbitrary
  shrink = genericShrink

instance Arbitrary PagesizeLimit where
  arbitrary = oneof
    [ pure PagesizeAll
    , PagesizeLimit . getNonNegative <$> arbitrary
    ]
  shrink = genericShrink

spec :: Spec
spec = do
  parallel $ do
    lawsCheckHspec (Proxy @SortDirection)
      [ eqLaws, ordLaws, showReadLaws, boundedEnumLaws, pathPieceLaws, finiteLaws, jsonLaws ]
    lawsCheckHspec (Proxy @SortingSetting)
      [ eqLaws, ordLaws, showReadLaws, jsonLaws, pathPieceLaws ]
    lawsCheckHspec (Proxy @PaginationInput)
      [ eqLaws, ordLaws, showReadLaws, jsonLaws ]
    lawsCheckHspec (Proxy @PagesizeLimit)
      [ eqLaws, ordLaws, showReadLaws, boundedEnumLaws, pathPieceLaws, jsonLaws ]
  
  describe "PaginationInput" $ do
    it "is unset iff it encodes to {}" . property $ \inp -> piIsUnset inp == (encode inp == "{}")
