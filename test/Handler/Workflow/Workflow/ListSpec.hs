-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE NumericUnderscores #-}

module Handler.Workflow.Workflow.ListSpec where

import TestImport hiding (count, ix)
import qualified "uniworx" Import as UniWorX


import Handler.Workflow.Utils (setupTestWorkflow)

import Utils.Metrics (histogramBuckets)

import qualified Data.Csv as Csv

import Control.Monad.Random (evalRandT)
import Control.Monad.Random.Class (weighted, getRandoms)


import qualified Data.ByteString.Lazy as LBS

import Control.Monad.Fail

import qualified Data.Foldable as Foldable

import qualified Data.Aeson.Types as Aeson
import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HashMap

import Data.Ratio ((%))

import qualified Test.HUnit as HUnit


data CardinalityHistogramBucket = CardinalityHistogramBucket
  { count, actions, payloads :: !Integer
  } deriving stock (Show, Eq, Generic, Typeable)
    deriving anyclass Csv.FromNamedRecord


spec :: Spec
spec = withApp $ do
  describe "Workflow lists" $ do
    describe "can be exported as JSON" $ do
      (_, Foldable.toList -> cardinalityHistogram) <- runIO $ either fail return . Csv.decodeByName =<< LBS.readFile (testdataDir </> "theses_scatter.csv")
      let cardinalityDistribution = do
            CardinalityHistogramBucket{..} <- cardinalityHistogram
            return ((actions, payloads), fromIntegral count)

      forM_ (map round $ 0 : histogramBuckets 1 10_000) $ \wfCount -> it ("with " <> show wfCount <> " running workflows") $ do
        when (wfCount > 10) . liftIO $
          pendingWith "too slow"

        userId <- runHandler . UniWorX.runDB $ do
          Entity userId _ <- insertEntity $ fakeUser id
          (_, wiId) <- setupTestWorkflow "export"
          WorkflowInstance{..} <- getJust wiId

          deleteWhere [ WorkflowWorkflowInstance ==. Just wiId ]

          drg <- $drgHere wfCount

          flip evalRandT drg . forM_ [1..wfCount] $ \(n :: Integer) -> do
            (wfActions, wfPayloads) <- weighted cardinalityDistribution
            lift . $(UniWorX.logInfo) $ tshow (n, wfActions, wfPayloads)

            now <- liftIO getCurrentTime
            wwId <- lift $ insert WorkflowWorkflow
              { workflowWorkflowInstance = Just wiId
              , workflowWorkflowScope = workflowInstanceScope
              , workflowWorkflowGraph = workflowInstanceGraph
              , workflowWorkflowArchived = Nothing
              }
            wwaId <- lift $ insert WorkflowWorkflowAction
              { workflowWorkflowActionWorkflow = wwId
              , workflowWorkflowActionIx = 1
              , workflowWorkflowActionTo = "initiated"
              , workflowWorkflowActionVia = "initiate"
              , workflowWorkflowActionUser = WorkflowActionUser $ userId ^. UniWorX._SqlKey
              , workflowWorkflowActionTime = now
              }
            lift $ insert_ WorkflowWorkflowPayload
              { workflowWorkflowPayloadAction = wwaId
              , workflowWorkflowPayloadLabel = "initiator"
              , workflowWorkflowPayloadPayload = Just $ _WorkflowFieldPayloadW # WFPUser (userId ^. UniWorX._SqlKey)
              , workflowWorkflowPayloadCurrent = True
              }

            let
              payloadsPerAct :: Integer
              payloadsPerAct = round $ max 0 (wfPayloads - 1) % max 1 (wfActions - 2)
            forM_ [1 .. wfActions - 2] $ \ix -> do
              wwaId' <- lift $ insert WorkflowWorkflowAction
                { workflowWorkflowActionWorkflow = wwId
                , workflowWorkflowActionIx = fromIntegral $ ix + 1
                , workflowWorkflowActionTo = "initiated"
                , workflowWorkflowActionVia = "add payload"
                , workflowWorkflowActionUser = WorkflowActionUser $ userId ^. UniWorX._SqlKey
                , workflowWorkflowActionTime = now
                }
              forM_ [1 .. payloadsPerAct * ix] $ \pIx -> do
                fContent <- getRandoms
                fileContentHash <- lift . UniWorX.sinkFileDB False . UniWorX.yield . pack $ take 64 fContent
                let fRef = FileReference
                      { fileReferenceTitle = show wfCount <> "_" <> show n <> "_" <> show ix <> "_" <> show pIx <> ".bin"
                      , fileReferenceModified = now
                      , fileReferenceContent = Just fileContentHash
                      }
                lift $ insert_ WorkflowWorkflowPayload
                  { workflowWorkflowPayloadAction = wwaId'
                  , workflowWorkflowPayloadLabel = "files"
                  , workflowWorkflowPayloadPayload = Just $ _WorkflowFieldPayloadW # WFPFile fRef
                  , workflowWorkflowPayloadCurrent = ix == wfActions - 2
                  }

            lift $ insert_ WorkflowWorkflowAction
              { workflowWorkflowActionWorkflow = wwId
              , workflowWorkflowActionIx = fromIntegral wfActions
              , workflowWorkflowActionTo = "finalized"
              , workflowWorkflowActionVia = "finalize"
              , workflowWorkflowActionUser = WorkflowActionUser $ userId ^. UniWorX._SqlKey
              , workflowWorkflowActionTime = now
              }

          return userId

        accessToken <- runHandler $ UniWorX.encodeBearer =<< UniWorX.bearerToken (HashSet.singleton $ Right userId) (Just userId) HashMap.empty Nothing Nothing Nothing

        timeout 150e6 $ do
          request $ do
            setMethod "GET"
            addRequestHeader ("Accept", "application/json")
            addRequestHeader ("Authorization", "Bearer " <> UniWorX.unJwt accessToken)
            setUrl . GlobalWorkflowInstanceR "export" $ GWIWorkflowsR WorkflowWorkflowListAll
          statusIs 200
          val <- requireJSONResponse
          liftIO $ case val of
            Aeson.Object obj -> HUnit.assertEqual "running workflow count" (fromIntegral wfCount) $ HashMap.size obj
            _other -> HUnit.assertFailure "Returned JSON is not an object"
