-- SPDX-FileCopyrightText: 2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Workflow.Utils
  ( setupTestWorkflow
  ) where

import TestImport

import Database.Fill (parseWorkflowGraphFile)
import Utils.Workflow

import System.FilePath ((<.>))


setupTestWorkflow :: FilePath -> DB (SharedWorkflowGraphId, WorkflowInstanceId)
setupTestWorkflow bName = do
  workflowDefinitionGraph <- insertSharedWorkflowGraph <=< parseWorkflowGraphFile $ testdataDir </> "test-workflows" </> bName <.> "yaml"
  let workflowDefinitionInstanceCategory = Just "test"
      workflowDefinitionName = fromString bName
      workflowDefinitionScope = WSGlobal'
  Entity wdId _ <- upsertBy (UniqueWorkflowDefinition workflowDefinitionName workflowDefinitionScope) WorkflowDefinition{..} []
  let workflowInstanceDefinition = Just wdId
      workflowInstanceGraph = workflowDefinitionGraph
      workflowInstanceScope = WSGlobal
      workflowInstanceName = fromString bName
      workflowInstanceCategory = workflowDefinitionInstanceCategory
  Entity wiId _ <- upsertBy (UniqueWorkflowInstance workflowInstanceName workflowInstanceScope) WorkflowInstance{..} []
  return (workflowDefinitionGraph, wiId)

