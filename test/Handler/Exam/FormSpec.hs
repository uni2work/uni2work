-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.Exam.FormSpec where

import TestImport
import ModelSpec ()
import CryptoID

import Handler.Exam.Form


instance Arbitrary ExamOccurrenceForm where
  arbitrary = ExamOccurrenceForm
    <$> (fmap (view _2) <$> (arbitrary :: Gen (Maybe (ExamOccurrenceId, CryptoUUIDExamOccurrence))))
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary

instance Arbitrary ExamPartForm where
  arbitrary = ExamPartForm
    <$> (fmap (view _2) <$> (arbitrary :: Gen (Maybe (ExamPartId, CryptoUUIDExamPart))))
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary


spec :: Spec
spec = do
  parallel $ do
    lawsCheckHspec (Proxy @ExamOccurrenceForm)
      [ eqLaws, ordLaws ]
    lawsCheckHspec (Proxy @ExamPartForm)
      [ eqLaws, ordLaws ]
