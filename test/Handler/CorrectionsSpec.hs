-- SPDX-FileCopyrightText: 2022 Gregor Kleen <gregor.kleen@ifi.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.CorrectionsSpec where

import TestImport

import ModelSpec ()


spec :: Spec
spec = withApp $
  xdescribe "CorrectionsR" $
    return ()
