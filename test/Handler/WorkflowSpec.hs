-- SPDX-FileCopyrightText: 2022-2023 Gregor Kleen <gregor.kleen@math.lmu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Handler.WorkflowSpec where

import TestImport
import qualified "uniworx" Import as UniWorX

import Handler.Workflow.Utils (setupTestWorkflow)

import qualified Data.Aeson as JSON

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HashMap
import qualified Crypto.Nonce as Nonce

import Utils.Tokens
import Utils.Lens (_SqlKey)
import Handler.Utils.Workflow.Types

import Jose.Jwt (Jwt(unJwt))

import qualified Test.HUnit as HUnit


spec :: Spec
spec = withApp $ do
  describe "Workflows" $ do
    it "can be initiated via json POST" $ do
      Entity userId _ <- runHandler . UniWorX.runDB $ do
        void $ setupTestWorkflow "json"
        workflowWorkflowCount <- count @_ @_ @WorkflowWorkflow []
        liftIO $ HUnit.assertEqual "Preexisting WorkflowWorkflows" 0 workflowWorkflowCount

        insertEntity $ fakeUser id

      accessToken <- runHandler $ encodeBearer =<< bearerToken (HashSet.singleton $ Right userId) (Just userId) HashMap.empty Nothing Nothing Nothing
      testText <- Nonce.nonce128urlT =<< Nonce.new

      request $ do
        setMethod "POST"
        addRequestHeader ("Content-Type", "application/json")
        addRequestHeader ("Authorization", "Bearer " <> unJwt accessToken)
        setRequestBody $ JSON.encode
          (WorkflowWorkflowEdgeForm
               { wwefTo = "initiated"
               , wwefVia = "initiate"
               , wwefPayload = Map.singleton "text" . Set.singleton . WorkflowFieldPayloadW $ WFPText testText
               }
             :: JsonWorkflowWorkflowEdgeForm
          )
        setUrl $ GlobalWorkflowInstanceR "json" GWIInitiateR
      nextLoc' <- getLocation

      nextLoc <- liftIO $ case nextLoc' of
        Right nextLoc@(GlobalWorkflowWorkflowR _cID GWWWorkflowR) -> return nextLoc
        Right otherRoute -> HUnit.assertFailure $ "Expected Location header to point to GWWWorkflowR, but got: " <> show otherRoute
        Left err -> HUnit.assertFailure $ unpack err

      request $ do
        setMethod "GET"
        addRequestHeader ("Content-Type", "text/html")
        addRequestHeader ("Authorization", "Bearer " <> unJwt accessToken)
        setUrl nextLoc
      statusIs 200
      htmlAnyContain ".workflow-state .deflist__dd" "Initiated"
      htmlAnyContain ".workflow-payload .workflow-payload--text" $ unpack testText
    it "can have manual edges performed via json POST" $ do
      nonceGen <- Nonce.new
      (Entity userId _, wwId) <- runHandler . UniWorX.runDB $ do
        (workflowWorkflowGraph, wiId) <- setupTestWorkflow "json"
        userEntity@(Entity userId _) <- insertEntity $ fakeUser id

        workflowWorkflowActionWorkflow <- insert WorkflowWorkflow
          { workflowWorkflowInstance = Just wiId
          , workflowWorkflowScope = WSGlobal
          , workflowWorkflowGraph
          , workflowWorkflowArchived = Nothing
          }

        workflowWorkflowActionTime <- liftIO getCurrentTime
        workflowWorkflowPayloadAction <- insert WorkflowWorkflowAction
          { workflowWorkflowActionWorkflow
          , workflowWorkflowActionIx = 1
          , workflowWorkflowActionTo = "initiated"
          , workflowWorkflowActionVia = "initiate"
          , workflowWorkflowActionUser = WorkflowActionUser $ userId ^. _SqlKey
          , workflowWorkflowActionTime
          }

        testText <- Nonce.nonce128urlT nonceGen
        insertMany_
          [ WorkflowWorkflowPayload
              { workflowWorkflowPayloadAction
              , workflowWorkflowPayloadLabel = "initiator"
              , workflowWorkflowPayloadPayload = Just . WorkflowFieldPayloadW . WFPUser $ userId ^. _SqlKey
              , workflowWorkflowPayloadCurrent = True
              }
          , WorkflowWorkflowPayload
              { workflowWorkflowPayloadAction
              , workflowWorkflowPayloadLabel = "text"
              , workflowWorkflowPayloadPayload = Just . WorkflowFieldPayloadW $ WFPText testText
              , workflowWorkflowPayloadCurrent = True
              }
          ]

        return (userEntity, workflowWorkflowActionWorkflow)

      cID <- runHandler $ UniWorX.encrypt wwId
      accessToken <- runHandler $ encodeBearer =<< bearerToken (HashSet.singleton $ Right userId) (Just userId) HashMap.empty Nothing Nothing Nothing
      testText <- Nonce.nonce128urlT nonceGen

      request $ do
        setMethod "POST"
        addRequestHeader ("Content-Type", "application/json")
        addRequestHeader ("Authorization", "Bearer " <> unJwt accessToken)
        setRequestBody $ JSON.encode
          (WorkflowWorkflowEdgeForm
               { wwefTo = "finalized"
               , wwefVia = "finalize"
               , wwefPayload = Map.singleton "text" . Set.singleton . WorkflowFieldPayloadW $ WFPText testText
               }
             :: JsonWorkflowWorkflowEdgeForm
          )
        setUrl $ GlobalWorkflowWorkflowR cID GWWWorkflowR
      nextLoc' <- getLocation

      nextLoc <- liftIO $ case nextLoc' of
        Right nextLoc@(GlobalWorkflowWorkflowR cID' GWWWorkflowR)
          | cID == cID' -> return nextLoc
          | otherwise -> HUnit.assertFailure $ "Expected CryptoID " <> show cID <> " but got " <> show cID'
        Right otherRoute -> HUnit.assertFailure $ "Expected Location header to point to GWWWorkflowR, but got: " <> show otherRoute
        Left err -> HUnit.assertFailure $ unpack err

      request $ do
        setMethod "GET"
        addRequestHeader ("Content-Type", "text/html")
        addRequestHeader ("Authorization", "Bearer " <> unJwt accessToken)
        setUrl nextLoc
      statusIs 200
      htmlAnyContain ".workflow-state .deflist__dd" "Finalized"
      htmlAnyContain ".workflow-payload .workflow-payload--text" $ unpack testText
