  - `SessionFile` should only have `touched` and `content`; just there to keep `FileContent` alive  
    Store `fileTitle` and `fileModified` within actual session

    Better symmetry with e.g. `SubmissionFile`
  - Restrict `genericFileField` to not allow duplicate `fileTitle`s

    Make `fieldAdditionalFiles` isomophoric to `[FileReference, FileFieldUserOption Bool]`

    `type FileUploads = Map FileTitle (Maybe FileContentReference, UTCTime)` (`~ [FileReference]`)
  - Route um Uploads nachzureichen

    Cronjob, der `FileContent` aus MinIO füttert sollte panische E-Mail mit Link auf jene Route verschicken, wenn Datei nicht zur Hand ist